# README #

This is a minimal README for the PSICO project : a GPU implementation of the simplex algorithm.
This project consists in a C++/CUDA implementation of the Simplex algorithm and the Cut-and-branch algorithm.

## Code organisation ##

### Solver ###
This folder contains the implementation of the simplex GPU implementation.
There are several implementations of the simplex. The most efficient are probably :

* **StandardMethodGPU** : standard simplex on GPU

* **RevisedASEOptiMethodGPU** : Optimised revised simplex

* **RevisedSparseAMethodGPU** : Optimised revised simplex with sparse matrix A (use cusparse)

See the file **src/Solver/cuda.cpp** for example of usage.
Most of the configuration of the various heuristics are defined in the file **src/Solver/Config.h**

### Worker ###
This folder contains the implementation of the Branch-and-Bound and variations that call the GPU simplex solver.
If your are not interested in Mixed-Integer linear programming, this code is of no use for you.
The files in the folder **src/Test** show example of usage of the B&B implementations.

## Compilations ##

Sadly, no makefile are made available. You must compile this code with the CUDA toolkit.
Library used are cublas and cusparse.

## How to cite ##

* Meyer, X.; Chopard, B.; Albuquerque, P., "A Branch-and-Bound algorithm using multiple GPU-based LP solvers," in High Performance Computing (HiPC), 2013 20th International Conference on , vol., no., pp.129-138, 18-21 Dec. 2013
doi: 10.1109/HiPC.2013.6799105

## Contact ##

* xav.meyer(at)gmail.com