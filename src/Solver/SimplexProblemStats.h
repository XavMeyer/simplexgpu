//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * SimplexProblemStats.h
 *
 *  Created on: December 2, 2012
 *      Author: meyerx
 *
 *
 */

#include <string.h>
#include <iostream>
#include <sstream>
using namespace std;

#ifndef SIMPLEXPROBLEMSTATS_H_
#define SIMPLEXPROBLEMSTATS_H_

class SimplexProblemStats {
public:

	double meanIt, meanSuccessIt;
	unsigned int nbR, nbSuccessR, nbReset;


	SimplexProblemStats(){
		init();
	}

	virtual	~SimplexProblemStats(){

	}

	void init(){
		meanIt = meanSuccessIt = 0.0;
		nbR = nbSuccessR = nbReset = 0;
	}

	void addRelaxation(unsigned int nbIteration, bool success){
		nbR++;
		meanIt += (nbIteration - meanIt)/nbR;

		if(success){
			nbSuccessR++;
			meanSuccessIt += (nbIteration - meanSuccessIt)/nbSuccessR;
		}
	}

	string toString(){

		ostringstream oss(ostringstream::out);
		oss << endl;
		oss << "*Nb reset : " << nbReset << endl;
		oss << "*Problem relaxation" << endl;
		oss << " Mean number of iteration : " << meanIt << endl;
		oss << " Number of relaxation : " << nbR << endl;
		oss << "*Problem SUCCESSFULL relaxation" << endl;
		oss << " Mean number of iteration : " << meanSuccessIt << endl;
		oss << " Number of relaxation : " << nbSuccessR << endl;

		return oss.str();
	}

};

#endif /* SIMPLEXPROBLEMSTATS_H_ */

