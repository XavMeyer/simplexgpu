//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedFormProblem.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "RevisedFormProblem.h"

RevisedFormProblem::RevisedFormProblem(SimplexProblem *inP) : Problem(inP->nRow, inP->nVar, inP->nSlack, false){
	this->createFromSimplexProblem(inP);
}


RevisedFormProblem::RevisedFormProblem(unsigned int inNRow, unsigned int inNVar, unsigned int inNSlack) :
																		Problem(inNRow, inNVar, inNSlack, false){
	// Copy the size
	//nRow = inNRow;
	//nVar = inNVar;
	//nSlack = inNSlack;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = nVar;
	this->initStructure();
}

RevisedFormProblem::RevisedFormProblem(unsigned int inNRow, unsigned int inNVar, unsigned int inNSlack, bool init) :
																				Problem(inNRow, inNVar, inNSlack, false){
	// Copy the size
	//nRow = inNRow;
	//nVar = inNVar;
	//nSlack = inNSlack;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = nVar;

	if(init){
		this->initStructure();
	} else {
		initPtr();
	}
}


RevisedFormProblem::~RevisedFormProblem() {

	if(Ab != NULL){

		delete [] Ab;
		delete [] An;
		delete [] B;

		delete [] lhs;

		delete [] bBounds;
		delete [] bBoundsP1;

		delete [] nBounds;
		delete [] nBoundsP1;

		delete [] bVal;
		delete [] nVal;

		delete [] bInd;
		delete [] nInd;

		delete [] Cb;
		delete [] CbP1;

		delete [] Cn;
		delete [] CnP1;


	}
}


void RevisedFormProblem::initPtr(){

	// Init matrix ptr to NULL
	Ab  		= NULL;
	An   		= NULL;
	B  			= NULL;

	// Init vectors ptr to NULL
	lhs  		= NULL;

	bBounds   	= NULL;
	bBoundsP1  	= NULL;

	nBounds 	= NULL;
	nBoundsP1	= NULL;

	bVal		= NULL;
	nVal		= NULL;

	bInd		= NULL;
	nInd		= NULL;

	Cb = CbP1 	= NULL;
	Cn = CnP1 	= NULL;

}

void RevisedFormProblem::createFromSimplexProblem(SimplexProblem *inP){

	// Copy the size
	//nRow = inP->nRow;
	//nVar = inP->nVar;
	//nSlack = inP->nSlack;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = inP->nVar;

	// Create the vector and array
	initStructure();

	// Init Ab and B matrix
	for(unsigned int i=0; i<nRow; i++){
		for(unsigned int j=0; j<nBasic; j++){
			if(i != j){
				Ab[IDX2C(i,j,nRow)] = 0;
				B[IDX2C(i,j,nRow)] = 0;
			} else {
				Ab[IDX2C(i,j,nRow)] = 1;
				B[IDX2C(i,j,nRow)] = 1;
			}
		}
	}

	// copy the initial problem
	memcpy(An, inP->eqs, nVar*nRow*sizeof(VAR_TYPE));

	// copy the LHS
	memcpy(lhs, &(inP->eqs[nVar*nRow]), nRow*sizeof(VAR_TYPE));

	// copy the bounds
	// basic
	memcpy(bBounds, inP->bBounds, nBasic*sizeof(float2));
	memcpy(bBoundsP1, inP->bBoundsP1, nBasic*sizeof(float2));
	// non-basic
	memcpy(nBounds, inP->xBounds, nNonBasic*sizeof(float2));
	memcpy(nBoundsP1, inP->xBoundsP1, nNonBasic*sizeof(float2));

	// Copy the values
	memcpy(bVal, inP->bVal, nBasic*sizeof(VAR_TYPE));
	memcpy(nVal, inP->xVal, nNonBasic*sizeof(VAR_TYPE));

	// Copy the index
	for(unsigned int i=0; i<nBasic; i++)
		bInd[i] = (unsigned int)inP->basis[i];
	for(unsigned int i=0; i<nNonBasic; i++)
		nInd[i] = (unsigned int)inP->xInd[i];

	// Objective function
	for(unsigned int i=0; i<nBasic; i++){
		Cb[i] = 0;
		CbP1[i] = 0;
	}
	memcpy(Cn, inP->objFunc, nNonBasic*sizeof(VAR_TYPE));
	memcpy(CnP1, inP->objFuncAux, nNonBasic*sizeof(VAR_TYPE));




}

void RevisedFormProblem::initStructure(){

	// Create matrix
	Ab  = new VAR_TYPE[nRow*nBasic];
	An  = new VAR_TYPE[nRow*nNonBasic];
	B	= new VAR_TYPE[nBasic*nBasic];

	// Create vectors
	lhs			= new VAR_TYPE[nRow];

	bBounds 	= new float2[nBasic];
	bBoundsP1 	= new float2[nBasic];

	nBounds 	= new float2[nNonBasic];
	nBoundsP1	= new float2[nNonBasic];

	bVal		= new VAR_TYPE[nBasic];
	nVal		= new VAR_TYPE[nNonBasic];

	bInd		= new int[nBasic];
	nInd		= new int[nNonBasic];

	Cb 			= new VAR_TYPE[nBasic];
	CbP1 		= new VAR_TYPE[nBasic];
	Cn 			= new VAR_TYPE[nNonBasic];
	CnP1 		= new VAR_TYPE[nNonBasic];

}

RevisedFormProblem * RevisedFormProblem::clone(){
	RevisedFormProblem* clone = new RevisedFormProblem(nRow, nVar, nSlack);

	memcpy(clone->Ab, Ab, nRow*nBasic*sizeof(VAR_TYPE));
	memcpy(clone->An, An, nRow*nNonBasic*sizeof(VAR_TYPE));
	memcpy(clone->B, B, nRow*nBasic*sizeof(VAR_TYPE));

	memcpy(clone->lhs, lhs, nRow*sizeof(VAR_TYPE));

	memcpy(clone->bBounds, bBounds, nBasic*sizeof(float2));
	memcpy(clone->bBoundsP1, bBoundsP1, nBasic*sizeof(float2));

	memcpy(clone->nBounds, nBounds, nNonBasic*sizeof(float2));
	memcpy(clone->nBoundsP1, nBoundsP1, nNonBasic*sizeof(float2));

	memcpy(clone->bVal, bVal, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->nVal, nVal, nNonBasic*sizeof(VAR_TYPE));

	memcpy(clone->bInd, bInd, nBasic*sizeof(int));
	memcpy(clone->nInd, nInd, nNonBasic*sizeof(int));

	memcpy(clone->Cb, Cb, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->CbP1, CbP1, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->Cn, Cn, nNonBasic*sizeof(VAR_TYPE));
	memcpy(clone->CnP1, CnP1, nNonBasic*sizeof(VAR_TYPE));

	return clone;
}


void RevisedFormProblem::print(){

	printf("********************************************\n");
	printf("REVISED FORM PROBLEM\n");
	printf(" An - Ab - RHS\n");
	printEquations();
	printf(" B \n");
	printBasis();

}

void RevisedFormProblem::printEquations(){

	for(unsigned int i=0; i<nNonBasic+nBasic; i++){
		if(i < nNonBasic){
			printf("[n%d]  \t\t", nInd[i]);
		} else {
			printf("[b%d]  \t\t", bInd[i-nNonBasic]);
		}
	}

	printf("\n");

	for(unsigned int i=0; i<nRow; i++) {
		for(unsigned int j=0; j<nNonBasic+nBasic; j++) {
			if(j < nNonBasic){
				printf("[%d,%d]%4.3f\t", i, j, An[IDX2C(i, j, nRow)]);
			} else {
				printf("[%d,%d]%4.3f\t", i, j, Ab[IDX2C(i, j-nNonBasic, nRow)]);
			}
		}

		printf("[RHS]%4.3f\n", lhs[i]);
	}

	printf("Obj func \n");

	for(unsigned int j=0; j<nNonBasic+nBasic; j++){
		if(j < nNonBasic){
			printf("[%d]%4.3f\t", j, Cn[j]);
		} else {
			printf("[%d]%4.3f\t", j, Cb[j-nNonBasic]);
		}
	}
	printf("\n");

	printf("Aux obj func \n");

	for(unsigned int j=0; j<nNonBasic+nBasic; j++){
		if(j < nNonBasic){
			printf("[%d]%4.3f\t", j, CnP1[j]);
		} else {
			printf("[%d]%4.3f\t", j, CbP1[j-nNonBasic]);
		}
	}
	printf("\n");

	printf("Vals \n");
	float2 bounds, boundsP1;
	VAR_TYPE val;

	for(unsigned int j=0; j<nNonBasic+nBasic; j++) {
		if(j < nNonBasic){
			bounds = nBounds[j];
			boundsP1 = nBoundsP1[j];
		} else {
			bounds = bBounds[j-nNonBasic];
			boundsP1 = bBoundsP1[j-nNonBasic];
		}

		if(bounds.x == -FLT_MAX)
			printf("[%d]  -inf   ", j);
		else
			printf("[%d]%4.3f", j, bounds.x);

		if(bounds.x != boundsP1.x){
			if(boundsP1.x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)",boundsP1.x);
		}
		printf("\t");
	}
	printf("\n");

	for(unsigned int j=0; j<nNonBasic+nBasic; j++) {
		if(j < nNonBasic){
			val = nVal[j];
		} else {
			val = bVal[j-nNonBasic];
		}
		printf("[%d]%4.3f\t", j, val);
	}
	printf("\n");

	for(unsigned int j=0; j<nNonBasic+nBasic; j++) {
		if(j < nNonBasic){
			bounds = nBounds[j];
			boundsP1 = nBoundsP1[j];
		} else {
			bounds = bBounds[j-nNonBasic];
			boundsP1 = bBoundsP1[j-nNonBasic];
		}
		if(bounds.y == FLT_MAX)
			printf("[%d]   inf  ", j);
		else
			printf("[%d]%4.3f", j, bounds.y);

		if(bounds.y != boundsP1.y){
			if(boundsP1.y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)",boundsP1.y);
		}
		printf("\t");
	}
	printf("\n");

}



void RevisedFormProblem::printBasis(){

	for(unsigned int i=0; i<nBasic; i++){
		for(unsigned int j=0; j<nBasic; j++){
			printf("[%d][%d]%4.3f", i, j, B[IDX2C(i,j,nRow)]);
		}
		printf("\n");
	}

}

