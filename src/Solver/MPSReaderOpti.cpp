//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 *
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * MPSReader2Phase.cpp
 *
 *  Created on: Sep 23, 2010
 *      Author: meyerx
 *
 *      edited by Kunzli Pierre
 *      jan 10 2011
 *
 *      (MPSReaderOpti::readBounds modified)
 */

#include "MPSReaderOpti.h"

MPSReaderOpti::MPSReaderOpti(string inFilePath) {
	filePath = inFilePath;
	nEquNoNull = 0;
	nObjNeg = 0;
	nVar = 0;
	nEqs = 0;
	nEquality = 0;
	markerInt = false;

	// tac
	line = new char[LINE_SIZE];
	lLine = 0;
	nWords = 0;
	lWords = new int[NB_WORDS];
	words = new char*[NB_WORDS];
	for(int i=0; i<NB_WORDS; i++){
		lWords[i] = 0;
		words[i] = new char[WORD_SIZE];
	}

}

MPSReaderOpti::~MPSReaderOpti() {

	delete [] line;
	delete [] lWords;
	for(int i=0; i<NB_WORDS; i++){
		delete [] words[i];
	}
	delete [] words;
}

bool MPSReaderOpti::splitLine(FILE *file) {

	int i = 0;
	nWords = 0;
	lWords[nWords] = 0;

	// If the line is a commentary drop line
	line[0] = '#';
	while(line[0] == '#' || line[0]=='*' || strlen(line) < 2){
		line = fgets(line, LINE_SIZE, file);
		if(line == NULL)
			return false;
	}

	// While line not ended
	while(line[i] != '\n'){
		// If char
		if(line[i] != ' ' && line[i] != '\t' && line[i] != '\r') {
			words[nWords][lWords[nWords]] = line[i];
			lWords[nWords]++;
		} else if(lWords[nWords] != 0) { // if space and not empty string
			words[nWords][lWords[nWords]] = '\0';
			nWords++;
			lWords[nWords] = 0;
		} // else we just drop
		i++;
	}

	if(lWords[nWords] != 0){
		words[nWords][lWords[nWords]] = '\0';
		nWords++;
	}

	//debugSL();
	//getchar();

	return true;
}

void MPSReaderOpti::checkError(error_t err){

	switch(err){
		case(NO_ERROR):
			return;
		case(NO_FILE):
			printf("MPS file not found!\n"); break;
		case(NAME_ERROR):
			printf("Error reading the PROBLEM part!\n"); break;
		case(ROW_ERROR):
			printf("Error reading the ROWS part!\n"); break;
		case(COLUMNS_ERROR):
			printf("Error reading the COLUMNS part!\n"); break;
		case(RHS_ERROR):
			printf("Error reading the RHS part!\n"); break;
		case(BOUNDS_ERROR):
			printf("Error reading the BOUNDS part!\n"); break;
		case(RANGES_ERROR):
			printf("Error reading the RANGE part!\n"); break;
		case(NROW_NCOL_ERROR):
			printf("Internal error!\n"); break;
		default: ;
	}

	throw 10;
}

SimplexProblem* MPSReaderOpti::readFile() {
	checkError(cntPhase());
	SimplexProblem *p = initStructures();
	checkError(readPhase(p));
	return p;
}


void MPSReaderOpti::debugSL(){
	for(int i=0; i<nWords; i++)
		printf("[%d] %s |\n", i, words[i]);
}


MPSReaderOpti::error_t MPSReaderOpti::cntPhase() {

	char temp[20];
	temp[0] = '\0';

	FILE *mpsFile = fopen(filePath.c_str(), "r");
	if (mpsFile == NULL) return NO_FILE;

	//Name
	if(!splitLine(mpsFile)) return NAME_ERROR;
	if(strcmp(words[0], MPS_NAME) != 0) return NAME_ERROR;

	//Rows
	if(!splitLine(mpsFile)) return ROW_ERROR;
	if(strcmp(words[0], MPS_ROWS) != 0) return ROW_ERROR;
	if(!splitLine(mpsFile)) return ROW_ERROR;
	while(strcmp(words[0], MPS_COLUMNS) != 0){
		if(nWords != 2) return ROW_ERROR;
		nEqs++;
		if(words[0][0] == MPS_ROW_EQUAL) nEquality++;
		if(!splitLine(mpsFile)) return ROW_ERROR;
		if(nWords == 0) return COLUMNS_ERROR;
	}
	nEqs--;//We don't count the obj func

	//Cols
	if(!splitLine(mpsFile)) return COLUMNS_ERROR;
	while(strcmp(words[0], MPS_RHS) != 0){
		if(nWords != 3 && nWords != 5) return COLUMNS_ERROR;
		if(strcmp(words[1], MPS_MARKER) !=0){
			if(strcmp(words[0], temp) != 0){
				strcpy(temp, words[0]);
				nVar++;
			}
		}
		if(!splitLine(mpsFile)) return COLUMNS_ERROR;
		if(nWords == 0) return RHS_ERROR;
	}

	fclose(mpsFile);

	return NO_ERROR;
}


MPSReaderOpti::error_t MPSReaderOpti::readPhase(SimplexProblem *p){

	error_t error;

	FILE *mpsFile = fopen(filePath.c_str(), "r");

	// If it's done, we can read the content
	if (mpsFile != NULL)	{

		error = readName(mpsFile, p);
		if(error != NO_ERROR) goto err;

		error = readRows(mpsFile, p);
		if(error != NO_ERROR) goto err;

		error = readColumns(mpsFile, p);
		if(error != NO_ERROR) goto err;

		error = readRHS(mpsFile, p);
		if(error != NO_ERROR && error != NO_RANGE && error!= NO_RB)
			goto err;

		if(error != NO_RANGE && error != NO_RB){
			error = readRanges(mpsFile, p);
			if(error != NO_ERROR && error != NO_BOUND)
				goto err;
		}

		if(error != NO_BOUND && error != NO_RB){
			error = readBounds(mpsFile, p);
			if(error != NO_ERROR)  goto err;
		}

		createMatrix(p);

		fclose(mpsFile);
		return NO_ERROR;

	} else {
		return NO_FILE;
	}

err:
	//mpsFile.close();
	return error;

}

/*
 * This function read the the NAME part of a MPS file
 * \param &mpsFile : the input file stream to read
 * \return the result of the operation
 */
MPSReaderOpti::error_t MPSReaderOpti::readName(FILE *mpsFile, SimplexProblem *p) {

	if(!splitLine(mpsFile)) return NAME_ERROR;
	if(nWords >= 2){ // We are waiting someting like : NAME myname
		p->name = string(words[1]);
		return NO_ERROR;
	}
	return NAME_ERROR;
}

/*
 * This function read the the ROWS part of a MPS file
 * \param &mpsFile : the input file stream to read
 * \return the result of the operation
 */
MPSReaderOpti::error_t MPSReaderOpti::readRows(FILE *mpsFile, SimplexProblem *p) {

	int i=0;
	char eqType;

	splitLine(mpsFile);
	if(!splitLine(mpsFile)) return ROW_ERROR;
	while(strcmp(words[0], MPS_COLUMNS) != 0){
		eqType = words[0][0];
		if(eqType != MPS_ROW_COST){
			p->eqsType[i] = eqType;
			p->rowsMap[string(words[1])] = i;
			#if ROW_SCALING
				maxRow[i] = 0.0;
			#endif
			i++;
		} else {
			p->rowsMap[string(words[1])] = -1;
		}
		if(!splitLine(mpsFile)) return ROW_ERROR;
	}

	return NO_ERROR;
}


// TODO check marker mod if not working properly
MPSReaderOpti::error_t MPSReaderOpti::readColumns(FILE *mpsFile, SimplexProblem *p) {

	int j=-1;
	int row;
	char *temp;
	temp = new char[20];
	temp[0] = '\0';
	int nbNotNull = 0;

	if(!splitLine(mpsFile)) return COLUMNS_ERROR;
	while(strcmp(words[0], MPS_RHS) != 0){ // We are done when we read RHS
		if(strcmp(words[1], MPS_MARKER) == 0){ // We have read a marker
			// Checking what kind of maker
			if(strcmp(words[2], MPS_MARKER_INTORG) == 0) {
				markerInt = true; // int variable follows
			} else if(strcmp(words[2], MPS_MARKER_INTEND) == 0) {
				markerInt = false; // floating variable follows
			}
		} else { // Normal column reading
			if(strcmp(words[0], temp) != 0){
				j++;
				temp = strcpy(temp, words[0]);
				p->colsMap[string(temp)] = j;
				p->columns.push_back(string(temp));
				if(markerInt) // Marker int read previously
					p->bounds[j] = SimplexProblem::INTEGER_BOUND; // Setting bound to int
				else
					p->bounds[j] = SimplexProblem::LOWER_BOUND; // Setting bound to float
			}

			for(int i=1; i<nWords; i=i+2){
				nbNotNull++;
				row = p->rowsMap.find(string(words[i]))->second;
				if(row != -1){ // if row == -1 then it's the objective function
					p->eqs[IDX2C(row, j, p->nRow)] = atof(words[i+1]);
					#if ROW_SCALING
						maxRow[row] = MAX(maxRow[row], fabs(p->eqs[IDX2C(row, j, p->nRow)]));
					#endif
				}else{
					p->objFunc[j] = atof(words[i+1]);
				}
			}
		}

		if(!splitLine(mpsFile)) return COLUMNS_ERROR;
	}

	//printf("Problem density : %e\n", (double)nbNotNull/(p->nRow*p->nCol));
	delete [] temp;

	return NO_ERROR;
}

/*
 * This function read the the RHS part of a MPS file
 * \param &mpsFile : the input file stream to read
 * \return the result of the operation
 */
MPSReaderOpti::error_t MPSReaderOpti::readRHS(FILE *mpsFile, SimplexProblem *p) {

	int row;
	// If we are there it means that we have read "RHS" already
	if(!splitLine(mpsFile)) return RHS_ERROR;
	// We are done when we read BOUNDS or ENDATA or RANGES
	while(strcmp(words[0], MPS_BOUNDS) != 0 && strcmp(words[0], MPS_END) != 0 && strcmp(words[0], MPS_RANGE) != 0){
		if(!(nWords == 3 || nWords == 5)){ // we are checking for : RHS1 (ROW_NAME VALUE)x 1 or 2
			return RHS_ERROR;
		}
		for(int i=1; i<nWords; i=i+2){ // for each RHS entry
			row = p->rowsMap.find(string(words[i]))->second;
			p->eqs[IDX2C(row, p->nCol-1, p->nRow)] = atof(words[i+1]);
		}
		if(!splitLine(mpsFile)) return RHS_ERROR;
	}

	if(strcmp(words[0], MPS_BOUNDS) == 0) return NO_RANGE;
	if(strcmp(words[0], MPS_END) == 0) return NO_RB;
	return NO_ERROR;
}

MPSReaderOpti::error_t MPSReaderOpti::readRanges(FILE *mpsFile, SimplexProblem *p) {

	int row;
	VAR_TYPE value = 0.0;

	// If we are there it means that we have read "BOUNDS" already
	if(!splitLine(mpsFile)) return RANGES_ERROR;
	while(strcmp(words[0], MPS_END) != 0 && strcmp(words[0], MPS_BOUNDS) != 0){ // We are done when we read ENDATA
		if(nWords != 3 && nWords != 5 ) // we are checking for : RANGE ROW VAL ROW VAL
			return RANGES_ERROR;

		for(int i=1; i<nWords; i=i+2){
			row = p->rowsMap.find(string(words[i]))->second;
			value = atof(words[i+1]);

			if(p->eqsType[row] == MPS_ROW_EQUAL) {
				if(atof(words[i+1]) < 0)
					p->bBounds[row].x = value;
				else
					p->bBounds[row].y = value;
			} else if(p->eqsType[row] == MPS_ROW_GREATER){
				p->bBounds[row].x = -fabs(value);
			}else if(p->eqsType[row] == MPS_ROW_LOWER){
				p->bBounds[row].y = fabs(value);
			}
		}

		if(!splitLine(mpsFile)) return RANGES_ERROR;
	}

	if(strcmp(words[0], MPS_END) == 0) return NO_BOUND;
	return NO_ERROR;
}

MPSReaderOpti::error_t MPSReaderOpti::readBounds(FILE *mpsFile, SimplexProblem *p) {

	int col;
	VAR_TYPE value = 0.0;

	// If we are there it means that we have read "BOUNDS" already
	if(!splitLine(mpsFile)) return BOUNDS_ERROR;
	while(strcmp(words[0], MPS_END) != 0){ // We are done when we read ENDATA
		if(nWords < 3 || nWords > 4 ) // we are checking for : UP/LO NAME VAR VAL
			return BOUNDS_ERROR;

		// get the row of the variable
		col = p->colsMap.find(string(words[2]))->second;
		// get the value of the bounds
		if(nWords == 4)
			value = atof(words[3]);

		if(strcmp(words[0], MPS_UPPER) == 0) {
			p->xBounds[col].y = value;
			if(p->isBoundInteger(col)){ // Integer bound
				if(p->bounds[col] == SimplexProblem::LOWER_INT_BOUND)
					p->bounds[col] = SimplexProblem::BOTH_INT_BOUND;
				else
					p->bounds[col] = SimplexProblem::UPPER_INT_BOUND;
			} else {
				if(p->bounds[col] == SimplexProblem::LOWER_BOUND)
					p->bounds[col] = SimplexProblem::BOTH_BOUND;
				else
					p->bounds[col] = SimplexProblem::UPPER_BOUND;
			}
		} else if(strcmp(words[0], MPS_LOWER) == 0) {
			p->xBounds[col].x = value;
			if(p->isBoundInteger(col)){ // Integer bound
				if(p->bounds[col] == SimplexProblem::UPPER_INT_BOUND)
					p->bounds[col] = SimplexProblem::BOTH_INT_BOUND;
				else
					p->bounds[col] = SimplexProblem::LOWER_INT_BOUND;
			} else {
				if(p->bounds[col] == SimplexProblem::UPPER_BOUND)
					p->bounds[col] = SimplexProblem::BOTH_BOUND;
				else
					p->bounds[col] = SimplexProblem::LOWER_BOUND;
			}
		} else if(strcmp(words[0], MPS_FREE) == 0) {
			p->xBounds[col].x = -FLT_MAX;
			p->xBounds[col].y = FLT_MAX;
			if(p->isBoundInteger(col))
				p->bounds[col] = SimplexProblem::FREE_INT_VAR;
			else
				p->bounds[col] = SimplexProblem::FREE_VAR;
		} else if(strcmp(words[0], MPS_FIXED) == 0) {
			p->xBounds[col].x = value;
			p->xBounds[col].y = value;
			p->bounds[col] = SimplexProblem::FIXED_VAR;
		} else if(strcmp(words[0], MPS_LOWER_INT) == 0) {
			p->xBounds[col].x = value;
			if(p->bounds[col] == SimplexProblem::UPPER_INT_BOUND)
				p->bounds[col] = SimplexProblem::BOTH_INT_BOUND;
			else
				p->bounds[col] = SimplexProblem::LOWER_INT_BOUND;
		} else if(strcmp(words[0], MPS_UPPER_INT) == 0) {
			p->xBounds[col].y = value;
			if(p->bounds[col] == SimplexProblem::LOWER_INT_BOUND)
				p->bounds[col] = SimplexProblem::BOTH_INT_BOUND;
			else
				p->bounds[col] = SimplexProblem::UPPER_INT_BOUND;
		} else if(strcmp(words[0], MPS_BIN) == 0) {
			if(nWords==4 && value!=1.0){
				return BOUNDS_ERROR;
			}
			p->xBounds[col].x = 0.0;
			p->xBounds[col].y = 1.0;
			p->bounds[col] = SimplexProblem::BIN_BOUND;
		} else {
			return BOUNDS_ERROR;
		}

		if(!splitLine(mpsFile)) return BOUNDS_ERROR;
	}

	return NO_ERROR;
}


SimplexProblem* MPSReaderOpti::initStructures(){
	// Create

	SimplexProblem *p = new SimplexProblem(nEqs, nVar, COL_SCALING == 1);
	p->nSlack = p->nRow;
	p->nEquality = nEquality;

#if ROW_SCALING
	maxRow = new VAR_TYPE[p->nRow];
#endif

/*#if COL_SCALING
	maxCol = new VAR_TYPE[p->nCol];
#endif*/

	return p;
}



void MPSReaderOpti::createMatrix(SimplexProblem *p){

	// Slack, Sign and Scaling
	// We are adding slack variable where needed : Slack
	// Changing the row sign if the equ type is greater than : Sign
	// Scaling the problem (kind of normalization) : Scaling
	SSS(p);

	// Process variable value
	initXVals(p);

	// set basis
	//setPrimalBasis3(p);

	p->xVal[p->nCol-1] = -1.0;

}


// Scale slack signus
void MPSReaderOpti::SSS(SimplexProblem *p){

	char rowType;
	VAR_TYPE *alpha = new VAR_TYPE[p->nRow];

	//defining row signus
	for(int iRow=0; iRow < p->nRow; iRow++){
		rowType = p->eqsType[iRow]; //pre-changing sign of equations
		/*alpha[iRow] = (((rowType == MPS_ROW_GREATER) ||
						(rowType == MPS_ROW_EQUAL &&
						 p->eqs[IDX2C(iRow, p->nCol-1, p->nRow)] < 0))
					  ? -1.0 : 1.0);*/
		alpha[iRow] = ((rowType == MPS_ROW_GREATER) ? -1.0 : 1.0);
		#if ROW_SCALING
			if(maxRow[iRow] > 0.0){
				alpha[iRow] /= maxRow[iRow];
				if(p->bBounds[iRow].x != -FLT_MAX)
					p->bBounds[iRow].x *= maxRow[iRow];
				if(p->bBounds[iRow].y != FLT_MAX)
					p->bBounds[iRow].y *= maxRow[iRow];
			}
		#endif
	}



	// for each var of each eqs we change the sign if needed and (opt) scale
	for(int j=0; j<p->nVar; j++){
		/*#if COL_SCALING
			maxCol[j] = 0.0;
		#endif*/
		for(int i=0; i<p->nRow; i++){
			if(p->eqs[IDX2C(i, j, p->nRow)] != 0.0){
				p->eqs[IDX2C(i, j, p->nRow)] *= alpha[i]; //changing sign of equation and row scaling (opt)
				#if COL_SCALING
					p->xScaling[j] = MAX(p->xScaling[j], fabs(p->eqs[IDX2C(i, j, p->nRow)]));//maxCol[j] = MAX(maxCol[j], fabs(p->eqs[IDX2C(i, j, p->nRow)]));
				#endif
			}
		}
	}

	// scaling the col so the max col value in -1..+1
#if COL_SCALING
	for(int j=0; j<p->nVar; j++){
		if(p->xScaling[j] > 0.0){
			p->objFunc[j] /= p->xScaling[j];
			if(p->xBounds[j].y != FLT_MAX)
				p->xBounds[j].y *= p->xScaling[j];
			if(p->xBounds[j].x != -FLT_MAX)
				p->xBounds[j].x *= p->xScaling[j];
			for(int i=0; i<p->nRow; i++){
				if(p->eqs[IDX2C(i, j, p->nRow)] != 0){
					p->eqs[IDX2C(i, j, p->nRow)] /= p->xScaling[j]; // scaling each columns
				}
			}
		}
	}
#endif

	// for the RHS we change the sign if needed
	// and add slack
	//int iSlack = p->nVar;
	for(int i=0; i<p->nRow; i++){
		// sign + scaling
		if(p->eqs[IDX2C(i, p->nCol-1, p->nRow)] != 0.0){
			p->eqs[IDX2C(i, p->nCol-1, p->nRow)] *= alpha[i]; //changing sign of rhs and opt row scaling
			//if(rowType != MPS_ROW_EQUAL && p->eqs[IDX2C(i, p->nCol-1, p->nRow)] < 0.0) p->nNegRHS++;
			//if(rowType == MPS_ROW_EQUAL && p->eqs[IDX2C(i, p->nCol-1, p->nRow)] != 0.0) nEquNoNull++;

		}
		if(p->eqsType[i] == MPS_ROW_EQUAL && p->bBounds[i].y == FLT_MAX)
			p->bBounds[i].y = 0.0;

	}

	delete[] alpha;
#if ROW_SCALING
	delete[] maxRow;
#endif
/*#if COL_SCALING
	delete[] maxCol;
#endif*/

}

/*
 * Init of the x values.
 * Initial problem variables takes one of their bounds as value if bounded, 0 else
 * Basis variables are processed in function of problem variables and RHS
 *
 */
void MPSReaderOpti::initXVals(SimplexProblem *p){

	// Init normal problem values
	for(int i=0; i<p->nVar; i++){

		/*if(p->objFunc[i] < 0.0){
			if(p->xBounds[i].y != FLT_MAX)
				p->xVal[i] = p->xBounds[i].y;
			else if(p->xBounds[i].x != -FLT_MAX)
				p->xVal[i] = p->xBounds[i].x;
			else
				p->xVal[i] = 0.0;
		} else {*/
			if(p->xBounds[i].x != -FLT_MAX)
				p->xVal[i] = p->xBounds[i].x;
			else if(p->xBounds[i].y != FLT_MAX)
				p->xVal[i] = p->xBounds[i].y;
			else
				p->xVal[i] = 0.0;
		//}
	}

	// Init slack values
	// Each slack value is equal to slack_i = RHS_i - sum(x_j*a_ij)
	// i => 0..nRow-1 and j => 0..nCol-1
	for(int i=0; i<p->nRow; i++){ // RHS value for nArt rows
		p->bVal[i] = p->eqs[IDX2C(i, p->nCol-1, p->nRow)];
	}

	// This processing is done in a column major way in order to improve performance
	for(int j=0; j<p->nVar; j++) // For each standard variable
		for(int i=0; i<p->nRow; i++) // For each basis / slack variables
			p->bVal[i] -= p->xVal[j]*p->eqs[IDX2C(i, j, p->nRow)]; // we subtract the "sum" of value

}
