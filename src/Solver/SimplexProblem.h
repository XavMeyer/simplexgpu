//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * SimplexProblem.h
 *
 *  Created on: April 6, 2010
 *      Author: meyerx
 *
 *   modified by Kunzli Pierre
 *
 *   jan 10 2011
 */

#ifndef SIMPLEXPROBLEM_H_
#define SIMPLEXPROBLEM_H_

#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <assert.h>
#include "Types.h"
#include "Config.h"
#include "Inequality.h"
#include "Problem.h"
using namespace std;

#define MAX_PRINT				500

/*
#define FREE_VAR_STR			"FR"
#define UPPER_BOUND_STR			"UP"
#define LOWER_BOUND_STR 		"LO"
#define BOTH_BOUND_STR 			"UL"
#define FIXED_VAR_STR 			"FX"
#define INTEGER_BOUND_STR 		"I"
#define LOWER_INT_BOUND_STR 	"LI"
#define UPPER_INT_BOUND_STR 	"UI"
#define BOTH_INT_BOUND_STR 		"BI"
#define BIN_BOUND_STR			"BV"
#define FREE_INT_VAR_STR 		"FI"
*/

class SimplexProblem : public Problem {
public:

	/*
	typedef enum {// Float variables : <0x80
				  FREE_VAR 			= 0x00,
				  UPPER_BOUND 		= 0x01,
				  LOWER_BOUND 		= 0x02,
		          BOTH_BOUND  		= 0x03,
		          FIXED_VAR   		= 0x04,
		          // Integral variables : >=0x80
		          INTEGER_BOUND	  	= 0x80,
		          LOWER_INT_BOUND 	= 0x81,
		          UPPER_INT_BOUND 	= 0x82,
		          BOTH_INT_BOUND  	= 0x83,
		          BIN_BOUND 		= 0x84,
		          FREE_INT_VAR		= 0x85
	} bound_t;
*/

					SimplexProblem();
					SimplexProblem(int inNEqs, int inNVar, bool inScaled);
	virtual 		~SimplexProblem();

	void 			print();
	void 			printEquations();
	void 			printEquations(int nbRows, int nbCols, VAR_TYPE* eqs);
	void 			printObjFunction();
	void 			printObjFunctionAux();
	void 			printX();

	/* DEPRECATED
	SimplexProblem	CreateDualFromInit();
	*/

	SimplexProblem*  clone();

	void 			changeToMinimize();
	void 			changeToMaximize();

	string			getBoundName(int index);

	bool			isMax();
	bool			isMin();

	bool			isBoundInteger(unsigned int index);

	void 			saveSolution(string fName, bool bounds);

	void 			set(SimplexProblem* p);

	void			addRows(vector<Inequality*> * rows);

	/*
	 * Name of the problem
	 */
	string name;

	/*
	 * Number of row and columns
	 */
	//int nRow;
	int nCol;

	/*
	 * Matrix containing the equations
	 */
	//VAR_TYPE* initEqs;
	VAR_TYPE* eqs;

	/*
	 * Vector containing the objective function
	 */
	VAR_TYPE* objFunc;
	VAR_TYPE* objFuncAux;

	/*
	 * Names of the equations and variables
	 */
	typedef map <string, int> MapType;
	vector<string> rows, columns;
	MapType rowsMap, colsMap;

	/*
	 * Type of each equation
	 */
	//char* eqsType;

	/*
	 * Bound for each variables U/L
	 * xBounds.x ==> lower bound
	 * xBounds.y ==> upper bound
	 */
	bound_t* bounds;
	float2* xBounds;
	float2* xBoundsP1;

	/*
	 * Basis for each equations (index of each basic variable)
	 */
	int* basis;

	/*
	 * Index for each non-basic variable
	 */
	int* xInd;

	/*
	 * Location of each decision variables
	 */
	//int* xLoc;

	/*
	 * Variable for ROW selection
	 * Most of this structures are redundant but necessary to optimize cuda
	 * processing
	 */
	VAR_TYPE* 	bVal; 	//basis variable value (1..nRow)
	VAR_TYPE* 	xVal; 	//variable value (1..nCol)
	float2*		bBounds;
	float2*		bBoundsP1;

	/*
	 * Two phase simplex important value
	 */
	//int nVar;
	int nEquality;
	int nNegRHS;
	//int nInf;
	//int nSlack;

	VAR_TYPE objAuxVal, dualBound;

	bool scaled;
	VAR_TYPE* xScaling;

	// Nb of relaxation and mean number of iterations done on this problem
	// TODO Find a better place to put these values
	unsigned int nbRelaxation;
	double meanIter;

	/*typedef enum { PRIMAL, AUX } mode_t ;
	mode_t mode;

	typedef enum { MAX, MIN}  optiType_t;
	optiType_t optiType;*/

private:

	VAR_TYPE* cloneVars();
	VAR_TYPE* cloneFunc();

	void resizeElements(unsigned int nRowOld);
	void initNewElements(unsigned int nRowOld);
	void copyRows(unsigned int nRowOld, vector<Inequality*> *rows);

	void initValues();

};

#endif /* SIMPLEXPROBLEM_H_ */
