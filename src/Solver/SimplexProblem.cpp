//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * SimplexProblem.cpp
 *
 *  Created on: Aug 3, 2010
 *      Author: meyerx
 */

#include "SimplexProblem.h"
#include "utils/resize_def.hpp"

SimplexProblem::SimplexProblem() : Problem(0, 0, false) {
	objFunc = NULL;
	objFuncAux = NULL;
	objVal = 0.0;
	nNegRHS = 0;
	nEquality = 0;
	//nRow = 0;
	nCol = 0;
	//nVar = 0;
	nSlack = 0;
	nInf = 0;
	scaled = false;
	optiType = MAX;
	nbRelaxation = 0;
    meanIter = 0.0;
}

SimplexProblem::SimplexProblem(int inNEqs, int inNVar, bool inScaled) : Problem(inNEqs, inNVar, false) {

	//nRow = inNEqs;
	//nVar = inNVar;
	nSlack = nRow;
	nCol = nVar + 1;
	objVal = 0;
	nNegRHS = 0;
	scaled = inScaled;

	nbRelaxation = 0;
	meanIter = 0.0;

	eqs 			= new VAR_TYPE[nRow*nCol];

	eqsType 		= new char[nRow];
	objFunc		 	= new VAR_TYPE[nCol];
	objFuncAux	 	= new VAR_TYPE[nCol];
	bounds 			= new bound_t[nCol];
	xBounds			= new float2[nCol];
	xBoundsP1		= new float2[nCol];
	xVal			= new VAR_TYPE[nCol];
	xInd			= new int[nCol];

	basis 			= new int[nRow];
	bVal			= new VAR_TYPE[nRow];
	bBounds			= new float2[nRow];
	bBoundsP1		= new float2[nRow];

	if(scaled)
		xScaling 	= new VAR_TYPE[nCol];

	optiType = MAX;

	this->initValues();
}

SimplexProblem::~SimplexProblem() {
	delete[] eqs;
	delete[] eqsType;
	delete[] objFunc;
	delete[] bounds;
	delete[] xBounds;
	delete[] xVal;
	delete[] xInd;

	delete[] basis;
	delete[] bVal;
	delete[] bBounds;
	delete[] bBoundsP1;

	delete[] xBoundsP1;
	delete[] objFuncAux;

	if(scaled)
		delete[] xScaling;

}



void SimplexProblem::print() {

	string boundType;

	printf("Name : %s \n", name.c_str());
	printf("\n");

	printf("Rows [%d] : \n", nRow);
	for(unsigned int i=0; i<rows.size(); i++)
		printf("%s \n", rows.at(i).c_str());
	printf("\n");

	printf("Columns [%d] - BoundsType - Lower - Upper : \n", nVar);
	for(unsigned int i=0; i<columns.size(); i++) {
		boundType = this->getBoundName(i);
		printf("%s - %s - %f - %f \n", columns.at(i).c_str(), boundType.c_str(), xBounds[i].x, xBounds[i].y);
	}
	printf("\n");

	if(nRow > MAX_PRINT || nCol > MAX_PRINT) return;

	printEquations();
	printObjFunction();
	if(objFuncAux != NULL)
		printObjFunctionAux();
	printX();
	printf("----------------------------------------------\n");
	printf("\n");

	//getchar();
}

string SimplexProblem::getBoundName(int index){
	switch(this->bounds[index]){
		case FREE_VAR : 		return FREE_VAR_STR;
		case UPPER_BOUND :  	return UPPER_BOUND_STR;
		case LOWER_BOUND :  	return LOWER_BOUND_STR;
		case BOTH_BOUND :		return BOTH_BOUND_STR;
		case FIXED_VAR :		return FIXED_VAR_STR;
		case INTEGER_BOUND :	return INTEGER_BOUND_STR;
		case LOWER_INT_BOUND :  return LOWER_INT_BOUND_STR;
		case UPPER_INT_BOUND :	return UPPER_INT_BOUND_STR;
		case BOTH_INT_BOUND :	return BOTH_INT_BOUND_STR;
		case BIN_BOUND :		return BIN_BOUND_STR;
		case FREE_INT_VAR :		return FREE_INT_VAR_STR;
	}
	return "ERR";
}

/*!
 * \brief Format and print the equations
 */
void SimplexProblem::printEquations() {

	printf("[xInd] - ");

	for(int i=0; i<nCol-1; i++){
		printf("[x%d]  \t\t", xInd[i]);
	}

	printf("\n");

	for(unsigned int i=0; i<nRow; i++) {

		printf("[x%d] - ", basis[i]);

		for(int j=0; j<nCol; j++) {
			printf("[%d,%d]%4.3f\t", i, j, eqs[IDX2C(i, j, nRow)]);
		}


		printf("[bVal] ");

		if(bBounds[i].x == -FLT_MAX)
			printf("-inf");
		else
			printf("%4.3f", bBounds[i].x);

		if(bBounds[i].x != bBoundsP1[i].x){
			if(bBoundsP1[i].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)", bBoundsP1[i].x);
		}

		printf(" > %4.3f > ", bVal[i]);

		if(bBounds[i].y == FLT_MAX)
			printf("inf");
		else
			printf("%4.3f", bBounds[i].y);

		if(bBounds[i].y != bBoundsP1[i].y){
			if(bBoundsP1[i].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)", bBoundsP1[i].y);
		}

		printf("\n");
	}

}

/*!
 * \brief print the objective function
 */
void SimplexProblem::printObjFunction() {

	printf("[-z] - ");

	for(int j=0; j<nCol; j++)
		printf("   [%d]%4.3f\t", j, objFunc[j]);

	printf("\n");
}


/*!
 * \brief print the objective function
 */
void SimplexProblem::printObjFunctionAux() {

	printf("[AUX]- ");

	for(int j=0; j<nCol; j++)
		printf("   [%d]%4.3f\t", j, objFuncAux[j]);

	printf("\n");
}

/*!
 * \brief Format and print the equations
 */
void SimplexProblem::printEquations(int nbRows, int nbCols, VAR_TYPE* eqs) {

	for(int i=0; i<nbRows; i++) {
		for(int j=0; j<nbCols; j++) {
			printf("[%d][%d]%4.3f\t", i, j, eqs[IDX2C(i, j, nbRows)]);
		}
		printf("\n");
	}
}

/*!
 * \brief Format and print the equations
 */
void SimplexProblem::printX() {

	printf("[xLo] - ");

	for(int j=0; j<nCol-1; j++) {

		if(xBounds[j].x == -FLT_MAX)
			printf("[%d]  -inf   ", j);
		else
			printf("[%d]%4.3f", j, xBounds[j].x);

		if(xBounds[j].x != xBoundsP1[j].x){
			if(xBoundsP1[j].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)",xBoundsP1[j].x);
		}
		printf("\t");
	}
	printf("\n");

	printf("[xVal] - ");

	for(int j=0; j<nCol; j++) {
		printf("[%d]%4.3f\t", j, xVal[j]);
	}
	printf("\n");

	printf("[xUp] - ");

	for(int j=0; j<nCol-1; j++) {
		if(xBounds[j].y == FLT_MAX)
			printf("[%d]   inf  ", j);
		else
			printf("[%d]%4.3f", j, xBounds[j].y);

		if(xBounds[j].y != xBoundsP1[j].y){
			if(xBoundsP1[j].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)",xBoundsP1[j].y);
		}
		printf("\t");
	}
	printf("\n");

}

/*!
 * \brief Create the dual problem
 * Only work after the first creation of a dual problem.
 */
/*
SimplexProblem SimplexProblem::CreateDualFromInit() {

	SimplexProblem dual;

	// nRow = (nCol-RHS) - nVarInBasis
	dual.nRow = (nCol-1) - nRow;
	dual.nCol = nCol;

	dual.basis = new int[dual.nRow];
	dual.eqs = new VAR_TYPE[dual.nRow*dual.nCol];
	dual.objFunc = new VAR_TYPE[dual.nCol];

	for(int i=0; i<dual.nRow; i++){
		dual.basis[i] = i;
	}

	// Aij
	for(int j=0; j<nRow; j++){
		for(int i=0; i<dual.nRow; i++){
			dual.eqs[IDX2C(i,j+dual.nRow,dual.nRow)] = -eqs[IDX2C(j,i,nRow)];
		}
	}

	// Slack
	for(int i=0; i<dual.nRow; i++){
		for(int j=0; j<dual.nRow; j++){
			if(i==j){
				dual.eqs[IDX2C(i,j,dual.nRow)] = 1;
			} else {
				dual.eqs[IDX2C(i,j,dual.nRow)] = 0;
			}
		}
	}

	// RHS
	for(int i=0; i<dual.nRow; i++){
		dual.eqs[IDX2C(i, dual.nCol-1, dual.nRow)] = -objFunc[i];//-eqs[IDX2C(nRow-1, i, nRow)];
	}

	// Objective function
	for(int i=0; i<dual.nCol; i++){
		dual.objFunc[i] = 0; // dual.eqs[IDX2C(dual.nRow-1, i, dual.nRow)] = 0;
	}
	for(int i=0; i<nRow; i++){
		dual.objFunc[i+dual.nRow] = -eqs[IDX2C(i, nCol-1, nRow)];//dual.eqs[IDX2C(dual.nRow-1, i+dual.nRow, dual.nRow)] = -eqs[IDX2C(i, nCol-1, nRow)];
	}

	return dual;

}*/


/*
 *
 * Cloner les variables de la fonction objectif
 *
 */

VAR_TYPE* SimplexProblem::cloneVars(){
	VAR_TYPE* cloneXVal = new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		cloneXVal[i]=xVal[i];
	}
	return cloneXVal;
}

/*
 *
 * Cloner les coefficients de la fonction objectif
 *
 */

VAR_TYPE* SimplexProblem::cloneFunc(){
	VAR_TYPE* cloneObjFunc	= new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		cloneObjFunc[i]=objFunc[i];
	}
	return cloneObjFunc;
}

void SimplexProblem::set(SimplexProblem* p){
	nRow = p->nRow;
	nVar = p->nVar;
	nSlack = p->nSlack;
	nCol = p->nCol;
	objVal = p->objVal;
	nNegRHS = p->nNegRHS;
	scaled = p->scaled;

	delete[] eqs;
	eqs = new VAR_TYPE[nRow*nCol];
	for(unsigned int i=0;i<nRow;i++){
		for(int j=0;j<nCol;j++){
			eqs[IDX2C(i,j,nRow)]=p->eqs[IDX2C(i,j,nRow)];
		}
	}

	delete[] eqsType;
	eqsType 		= new char[nRow];
	for(unsigned int i=0;i<nRow;i++){
		eqsType[i]=p->eqsType[i];
	}

	delete[] objFunc;
	objFunc		 	= new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		objFunc[i]=p->objFunc[i];
	}

	delete[] objFuncAux;
	objFuncAux	 	= new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		objFuncAux[i]=p->objFuncAux[i];
	}

	delete[] bounds;
	bounds 			= new bound_t[nCol];
	for(int i=0;i<nCol;i++){
		bounds[i]=p->bounds[i];
	}

	delete[] xBounds;
	xBounds			= new float2[nCol];
	for(int i=0;i<nCol;i++){
		xBounds[i]=p->xBounds[i];
	}

	delete[] xBoundsP1;
	xBoundsP1		= new float2[nCol];
	for(int i=0;i<nCol;i++){
		xBoundsP1[i]=p->xBoundsP1[i];
	}

	delete[] xVal;
	xVal			= new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		xVal[i]=p->xVal[i];
	}

	delete[] xInd;
	xInd			= new int[nCol];
	for(int i=0;i<nCol;i++){
		xInd[i]=p->xInd[i];
	}

	delete[] basis;
	basis 			= new int[nRow];
	for(unsigned int i=0;i<nRow;i++){
		basis[i]=p->basis[i];
	}

	delete[] bVal;
	bVal			= new VAR_TYPE[nRow];
	for(unsigned int i=0;i<nRow;i++){
		bVal[i]=p->bVal[i];
	}

	delete[] bBounds;
	bBounds			= new float2[nRow];
	for(unsigned int i=0;i<nRow;i++){
		bBounds[i]=p->bBounds[i];
	}

	delete[] bBoundsP1;
	bBoundsP1		= new float2[nRow];
	for(unsigned int i=0;i<nRow;i++){
		bBoundsP1[i]=p->bBoundsP1[i];
	}

	if(scaled)
		delete[] xScaling;
		xScaling 	= new VAR_TYPE[nCol];



}


/*
 *
 * Methode permettant de cloner un objet SimplexProble - utile pour le branch and bound
 * methode ajoutee par pierre kunzli
 *
 */

SimplexProblem* SimplexProblem::clone(){

	SimplexProblem* theClone = new SimplexProblem();

	theClone->nRow = nRow;
	theClone->nVar = nVar;
	theClone->nSlack = nSlack;
	theClone->nCol = nCol;
	theClone->objVal = objVal;
	theClone->nNegRHS = nNegRHS;
	theClone->scaled = scaled;
	theClone->optiType = optiType;
	theClone->nbRelaxation = nbRelaxation;
	theClone->meanIter = meanIter;

	theClone->eqs = new VAR_TYPE[nRow*nCol];
	for(unsigned int i=0;i<nRow;i++){
		for(int j=0;j<nCol;j++){
			theClone->eqs[IDX2C(i,j,nRow)]=eqs[IDX2C(i,j,nRow)];
		}
	}

	theClone->eqsType = new char[nRow];
	for(unsigned int i=0;i<nRow;i++){
		theClone->eqsType[i]=eqsType[i];
	}

	theClone->objFunc = new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		theClone->objFunc[i]=objFunc[i];
	}

	theClone->objFuncAux = new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		theClone->objFuncAux[i]=objFuncAux[i];
	}

	theClone->bounds = new bound_t[nCol];
	for(int i=0;i<nCol;i++){
		theClone->bounds[i]=bounds[i];
	}

	theClone->xBounds = new float2[nCol];
	for(int i=0;i<nCol;i++){
		theClone->xBounds[i]=xBounds[i];
	}

	theClone->xBoundsP1	= new float2[nCol];
	for(int i=0;i<nCol;i++){
		theClone->xBoundsP1[i]=xBoundsP1[i];
	}

	theClone->xVal = new VAR_TYPE[nCol];
	for(int i=0;i<nCol;i++){
		theClone->xVal[i]=xVal[i];
	}

	theClone->xInd = new int[nCol];
	for(int i=0;i<nCol;i++){
		theClone->xInd[i]=xInd[i];
	}


	theClone->basis = new int[nRow];
	for(unsigned int i=0;i<nRow;i++){
		theClone->basis[i]=basis[i];
	}


	theClone->bVal = new VAR_TYPE[nRow];
	for(unsigned int i=0;i<nRow;i++){
		theClone->bVal[i]=bVal[i];
	}


	theClone->bBounds = new float2[nRow];
	for(unsigned int i=0;i<nRow;i++){
		theClone->bBounds[i]=bBounds[i];
	}

	theClone->bBoundsP1 = new float2[nRow];
	for(unsigned int i=0;i<nRow;i++){
		theClone->bBoundsP1[i]=bBoundsP1[i];
	}

	if(scaled)
		theClone->xScaling = new VAR_TYPE[nCol];

	return theClone;

}


void SimplexProblem::changeToMinimize(){

	if(optiType == MIN) return;

	optiType = MIN;

	for(unsigned int i=0; i<nVar; i++)
		objFunc[i] *= -1.0;

}

void SimplexProblem::changeToMaximize(){

	if(optiType == MAX) return;

	optiType = MAX;

	for(unsigned int i=0; i<nVar; i++)
		objFunc[i] *= -1.0;

}

bool SimplexProblem::isMax(){
	return optiType == MAX;
}

bool SimplexProblem::isMin(){
	return optiType == MIN;
}

bool SimplexProblem::isBoundInteger(unsigned int index){
	if(index >= nVar) return false; // Then it is a slack
	else return (this->bounds[index] & INTEGER_BOUND) != 0;
}


void SimplexProblem::initValues(){
	// equations coef and RHS to 0.0
	for(unsigned int i=0; i<nRow*nCol; i++)
		eqs[i] = 0.0;

	for(int i=0; i<nCol; i++){
		objFunc[i] = 0.0;
		objFuncAux[i] = 0.0;
		bounds[i] = LOWER_BOUND;
		xVal[i] = 0.0;
		xInd[i] = i;
		xBounds[i] = make_float2(0.0, FLT_MAX);
		xBoundsP1[i] = xBounds[i];
		if(scaled)
			xScaling[i] = 0.0;
	}

	if(scaled)
		xScaling[nCol-1] = 1.0;

	for(unsigned int i=0; i<nRow; i++){
		basis[i] 		= i+nVar;
		bVal[i] 		= 0.0;
		eqsType[i]		= 'L';
		bBounds[i] 		= make_float2(0.0, FLT_MAX);
		bBoundsP1[i]	= bBounds[i];
	}

}


void SimplexProblem::resizeElements(unsigned int nRowOld){

	// Resize (and copy) equation array
	resize_2D<VAR_TYPE>(&eqs, nRowOld, nCol, nRow, nCol, COLUMN_WISE);
	// Resize (and copy) equation type array
	resize_1D<char>(&eqsType, nRowOld, nRow);
	// Resize (and copy) basis array
	resize_1D<int>(&basis, nRowOld, nRow);
	// Resize (and copy) basis value array
	resize_1D<VAR_TYPE>(&bVal, nRowOld, nRow);
	// Resize (and copy) basis value array
	resize_1D<float2>(&bBounds, nRowOld, nRow);
	// Resize (and copy) basis value array
	resize_1D<float2>(&bBoundsP1, nRowOld, nRow);
}

void SimplexProblem::initNewElements(unsigned int nRowOld){
	for(unsigned int i=nRowOld; i<nRow; i++){
		eqsType[i] 		= 'L'; 							// Type of equation (lower than)
		basis[i] 		= nVar+i;						// Variable index
		bVal[i] 		= eqs[IDX2C(i, nCol-1, nRow)];	// Basis value
		bBounds[i]		= make_float2(0.0, FLT_MAX);	// Variable bounds
		bBoundsP1[i]	= make_float2(0.0, FLT_MAX);	// Variable bounds for phase 1
	}
}

void SimplexProblem::copyRows(unsigned int nRowOld, vector<Inequality*> *rows){
	vector<Inequality*>::iterator it;
	unsigned int rowCnt = nRowOld;

	// Memcpy can't be used since rows are stored as 1d array ("row-wise")
	// and eqs is store column wise
	for(it = rows->begin(); it != rows->end(); it++) {
		// Copy coefficient
		for(unsigned int i=0; i<(*it)->nLHS; i++){
			eqs[IDX2C(rowCnt, i, nRow)] = (*it)->coeffLHS[i];
		}
		// Copy rhs
		eqs[IDX2C(rowCnt, nCol-1, nRow)] = (*it)->rhs;
		rowCnt++;
	}
}

void SimplexProblem::addRows(vector<Inequality*> * rows){
	vector<Inequality*>::iterator it;
	unsigned int nRowOld;

	if(rows->empty()) return; // Return if rows is empty
	for(it = rows->begin(); it != rows->end(); it++) {
		// Raise an error if rows don't have the amount of variables
		assert((*it)->nLHS == (unsigned int)this->nVar);
	}

	nRowOld = nRow;				// Memorize the old nb of rows
	nRow += rows->size();		// increment the number of rows
	nSlack = nRow;				// Update the number of slack variables

	this->resizeElements(nRowOld);
	this->copyRows(nRowOld, rows);
	this->initNewElements(nRowOld);

}

void SimplexProblem::saveSolution(string fName, bool bounds){

	FILE *resFile = fopen(fName.c_str(), "w");

	fprintf(resFile, "Optimal \n");
	fprintf(resFile, "Objective value \t %f \n", objVal);

	for(unsigned int ind=0; ind<columns.size(); ind++){
		if(fabs(xVal[ind]) > 1e-8){
			fprintf(resFile, " \t %d %s \t %10f", ind, columns.at(ind).c_str(), xVal[ind]);
			if(bounds){
				fprintf(resFile, " \t");
				if(xBounds[ind].x == -FLT_MAX)
					fprintf(resFile, "-inf");
				else
					fprintf(resFile, "%f > ", xBounds[ind].x);
				fprintf(resFile, "%f > ", xVal[ind]);
				if(xBounds[ind].y == FLT_MAX)
					fprintf(resFile, "inf");
				else
					fprintf(resFile, "%f", xBounds[ind].y);
			}
			fprintf(resFile, "\n");}
	}

	fclose(resFile);
}




