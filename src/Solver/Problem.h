//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Problem.h
 *
 *  Created on: Feb 20, 2012
 *      Author: meyerx
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#define FREE_VAR_STR			"FR"
#define UPPER_BOUND_STR			"UP"
#define LOWER_BOUND_STR 		"LO"
#define BOTH_BOUND_STR 			"UL"
#define FIXED_VAR_STR 			"FX"
#define INTEGER_BOUND_STR 		"I"
#define LOWER_INT_BOUND_STR 	"LI"
#define UPPER_INT_BOUND_STR 	"UI"
#define BOTH_INT_BOUND_STR 		"BI"
#define BIN_BOUND_STR			"BV"
#define FREE_INT_VAR_STR 		"FI"


#include <string>
#include <vector>
#include <map>
#include "Types.h"
#include <typeinfo>
using namespace std;

class Problem {
public:

	typedef enum {/* Float variables : <0x80  */
				  FREE_VAR 			= 0x00,
				  UPPER_BOUND 		= 0x01,
				  LOWER_BOUND 		= 0x02,
				  BOTH_BOUND  		= 0x03,
				  FIXED_VAR   		= 0x04,
				  /* Integral variables : >=0x80*/
				  INTEGER_BOUND	  	= 0x80,
				  LOWER_INT_BOUND 	= 0x81,
				  UPPER_INT_BOUND 	= 0x82,
				  BOTH_INT_BOUND  	= 0x83,
				  BIN_BOUND 		= 0x84,
				  FREE_INT_VAR		= 0x85
	} bound_t;

	Problem(unsigned int inNRow, unsigned int inNVar);
	Problem(unsigned int inNRow, unsigned int inNVar, bool init);
	Problem(unsigned int inNRow, unsigned int inNVar, unsigned int inSlack,  bool init);
	virtual ~Problem();

	bool isBoundInteger(unsigned int index);

	unsigned int nRow, nVar, nSlack, nNoNull;

	int nInf;

	VAR_TYPE objVal;

	/*
	 * Name of the problem
	 */
	string name;

	bound_t* boundsType;

	/*
	 * Type of each equation
	 */
	char* eqsType;

	typedef enum { PRIMAL, AUX } mode_t ;
	mode_t mode;

	typedef enum { MAX, MIN}  optiType_t;
	optiType_t optiType;

};



#endif /* PROBLEM_H_ */
