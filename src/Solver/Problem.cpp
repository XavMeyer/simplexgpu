//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Problem.cpp
 *
 *  Created on: Feb 20, 2012
 *      Author: meyerx
 */

#include "Problem.h"

Problem::Problem(unsigned int inNRow, unsigned int inNVar){
	nRow = inNRow;
	nVar = inNVar;
	nSlack = 0;
	nNoNull = 0;
	objVal = 0.0;
	nInf = 0;

	boundsType =  new bound_t[nVar+nRow];
	eqsType = new char[nRow];
}

Problem::Problem(unsigned int inNRow, unsigned int inNVar, bool init){
	nRow = inNRow;
	nVar = inNVar;
	nSlack = 0;
	nNoNull = 0;
	objVal = 0.0;
	nInf = 0;

	if(init){
		boundsType =  new bound_t[nVar+nRow];
		eqsType = new char[nRow];
	} else {
		boundsType =  NULL;
		eqsType = NULL;
	}
}

Problem::Problem(unsigned int inNRow, unsigned int inNVar, unsigned int inNSlack, bool init){
	nRow = inNRow;
	nVar = inNVar;
	nSlack = inNSlack;
	nNoNull = 0;
	objVal = 0.0;
	nInf = 0;

	if(init){
		boundsType =  new bound_t[nVar+nRow];
		eqsType = new char[nRow];
	} else {
		boundsType =  NULL;
		eqsType = NULL;
	}
}


Problem::~Problem(){
	if(boundsType != NULL){
		delete [] boundsType;
		delete [] eqsType;
	}
}


bool Problem::isBoundInteger(unsigned int index){
	if(index >= nVar) return false; // Then it is a slack
	else return (this->boundsType[index] & INTEGER_BOUND) != 0;
}
