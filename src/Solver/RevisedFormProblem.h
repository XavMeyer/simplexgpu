//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedFormProblem.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#ifndef REVISEDFORMPROBLEM_H_
#define REVISEDFORMPROBLEM_H_

#include <string.h>

#include "Problem.h"
#include "SimplexProblem.h"


class RevisedFormProblem : public Problem {
public:

	RevisedFormProblem(SimplexProblem *inP);
	RevisedFormProblem(unsigned int inNRow, unsigned int inNVar, unsigned int inNSlack);
	RevisedFormProblem(unsigned int inNRow, unsigned int inNVar, unsigned int inNSlack, bool init);
	virtual ~RevisedFormProblem();

	RevisedFormProblem * clone();

	void print();
	void printEquations();
	void printBasis();


	/*
	 * Problem size
	 */
	unsigned int nBasic, nNonBasic;

	/*
	 * Matrix containing the equations
	 */
	VAR_TYPE *Ab, *An, *B;

	/*
	 * Vector containing the LHS (b)
	 */
	VAR_TYPE *lhs;

	/*
	 * Vector containing the objective function
	 */
	VAR_TYPE *Cb, *Cn, *CbP1, *CnP1;

	/*
	 * Index of basic and non basic variables
	 */
	int *bInd, *nInd;

	/*
	 * Variable values
	 */
	VAR_TYPE *bVal, *nVal;

	/*
	 * Bound for each variables U/L
	 * xBounds.x ==> lower bound
	 * xBounds.y ==> upper bound
	 */
	float2* nBounds;
	float2* nBoundsP1;

	float2*	bBounds;
	float2*	bBoundsP1;


	/*
	 * Two phase simplex important value
	 */
	unsigned int nInf;

	/*
	 * Objective function value
	 */
	VAR_TYPE objVal, objAuxVal;


private:
	void initPtr();
	void initStructure();
	void createFromSimplexProblem(SimplexProblem *inP);

};

#endif /* REVISEDFORMPROBLEM_H_ */
