//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SimplexSolution.cpp
 *
 *  Created on: May 23, 2011
 *      Author: meyerx
 */

#include "SimplexSolution.h"

SimplexSolution::SimplexSolution(int inNVar, int inNSlack) {

	nVar = inNVar;
	nSlack = inNSlack;
	xVal = new VAR_TYPE[nVar];
	sVal = new VAR_TYPE[nSlack];
}

SimplexSolution::~SimplexSolution() {
	delete [] xVal;
	delete [] sVal;
}

void SimplexSolution::copy(SimplexSolution *source){

	if(this->nVar != source->nVar || this->nSlack != source->nSlack){
		this->nVar = source->nVar;
		this->nSlack = source->nSlack;
		// Create new xVal
		delete [] xVal;
		delete [] sVal;
		xVal = new VAR_TYPE[this->nVar];
		sVal = new VAR_TYPE[this->nSlack];
	}

	this->objVal = source->objVal;

	for(int i=0; i<nVar; i++)
		this->xVal[i] = source->xVal[i];

	for(int i=0; i<nSlack; i++)
		this->sVal[i] = source->sVal[i];

}


SimplexSolution * SimplexSolution::clone(){

	SimplexSolution * clone = new SimplexSolution(nVar, nSlack);

	clone->copy(this);

	return clone;

}

string SimplexSolution::toString(){

	ostringstream oss(ostringstream::out);
	oss << endl;
	oss << "<---------- Solution ------------>" << endl;
	oss << "Value : " << fixed << this->objVal << endl;

	if(this->nVar < MAX_RES){
		oss << "Variable values :" << endl;
		for(int i=0; i<this->nVar; i++){
			if(fabs(this->xVal[i]) > EPS1){
				oss << "x" << i << "\t:\t" << fixed << this->xVal[i];
				oss << endl;
			}
		}
		oss << "Slack values :" << endl;
			for(int i=0; i<this->nSlack; i++){
				if(fabs(this->sVal[i]) > EPS1){
					oss << "s" << i << "\t:\t" << fixed << this->sVal[i];
					oss << endl;
				}
			}
	}

	oss << "<---------- End solution ------------>" << endl;

	return oss.str();

}

bool SimplexSolution::isIntegral(int index){
	return fabs(round(xVal[index]) - xVal[index]) < EPS1;
}

bool SimplexSolution::isIntegral(){

	for(int i=0; i<nVar; i++){
		if(!isIntegral(i)){
			return false;
		}
	}

	return true;
}
