//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Inequality.h
 *
 *  Created on: Jul 25, 2011
 *      Author: meyerx
 */

#ifndef INEQUALITY_H_
#define INEQUALITY_H_

#include "cblas.h"
#include "Types.h"
#include "SimplexSolution.h"

#include <string.h>
#include <iostream>
#include <sstream>

class Inequality {
public:

	unsigned int nLHS;
	VAR_TYPE* coeffLHS;
	VAR_TYPE rhs;


	Inequality(unsigned int inNLHS){
		nLHS = inNLHS;
		coeffLHS = new VAR_TYPE[nLHS];
	}

	virtual ~Inequality(){
		delete [] coeffLHS;
	}

	bool DBG_check(SimplexSolution *sol){
		VAR_TYPE lhs = 0.0;

		// Process the lhs (decision variable)
		lhs = cblas_ddot(sol->nVar, coeffLHS, 1, sol->xVal, 1);
		/*for(int i=0; i<sol->nVar; i++)
			lhs += (cut->coeffLHS[i]*sol->xVal[i]);*/

		// Process the lhs (slack variable)
		//lhs += cblas_ddot(sol->nSlack, &coeffLHS[sol->nVar], 1, sol->sVal, 1);
		/*for(int i=0; i<sol->nSlack; i++)
			lhs += (cut->coeffLHS[i+sol->nVar]*sol->sVal[i]);*/


		if(lhs > rhs) {
			cout << "Infeasibility : " << rhs -lhs << endl;
			return false;
		} else {
			return true;
		}
	}

	string toString(){
		ostringstream oss(ostringstream::out);
		for(unsigned int i=0; i<this->nLHS; i++)
			oss << coeffLHS[i] << "\t";
		oss << rhs;

		return oss.str();
	}

};

#endif /* INEQUALITY_H_ */
