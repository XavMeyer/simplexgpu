//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LocalSProblem.h
 *
 *  Created on: Nov 2, 2010
 *      Author: meyerx
 */

#ifndef LOCALSPROBLEM_H_
#define LOCALSPROBLEM_H_

#include <string>
#include <vector>
#include <map>
#include <stdio.h>
//#include "cutil_math.h"
#include "../Types.h"
#include "../Config.h"
#include "../SimplexProblem.h"
using namespace std;

/* Defines the bounds types */
/*! no bounds */
#define FREE_VAR 	0
/*! upper bounded variable */
#define UPPER_BOUND 1
/*! lower bounded variable */
#define LOWER_BOUND 2
/*! upper and lower bounded variable */
#define BOTH_BOUND 	3
/*! Fixed bound variable */
#define FIXED_VAR	4



class LocalSProblem {
public:
	LocalSProblem();
	virtual ~LocalSProblem();

	void 			Print();
	void 			PrintEquations();
	void 			PrintEquations(int nbRows, int nbCols, VAR_TYPE* eqs);
	void 			PrintObjFunction();
	void 			PrintObjFunctionAux();
	void 			PrintX();

	/*
	 * Number of row and columns
	 */
	int nRow, nCol;

	/*
	 * Matrix containing the equations
	 */
	VAR_TYPE* eqs;

	/*
	 * Vector containing the objective function
	 */
	VAR_TYPE* objFunc;
	VAR_TYPE* objFuncAux;

	/*
	 * Bound for each variables U/L
	 */
	VAR_TYPE* 	xVal; 	//variable value (1..nCol)
	int* 		xInd;
	float2*		xBounds;
	float2* 	xBoundsP1;

	/*
	 * Index for each non-basic variable
	 */


	/*
	 * Variable for ROW selection
	 * Most of this structures are redundant but necessary to optimize cuda
	 * processing
	 */
	VAR_TYPE* 	bVal; 	//basis variable value (1..nRow)
	int* 		basis;
	float2*		bBounds;
	float2*		bBoundsP1;

	/*
	 * Two phase simplex important value
	 */
	int nVar;
	int nEquality;
	int nNegRHS;
	int nInf;
	int nInfX;
	int nInfB;
	int nSlack;

	VAR_TYPE objVal, objAuxVal;

	bool scaled;
	VAR_TYPE* xScaling;

	// Multi GPU
	bool RHS;

	SimplexProblem::mode_t mode;

};

#endif /* LOCALSPROBLEM_H_ */
