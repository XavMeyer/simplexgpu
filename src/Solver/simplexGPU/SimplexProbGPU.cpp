//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SimplexProbGPU.cpp
 *
 *  Created on: Sep 7, 2011
 *      Author: meyerx
 */

#include "SimplexProbGPU.h"


SimplexProbGPU::SimplexProbGPU(SimplexProblem *inP){

}


SimplexProbGPU::~SimplexProbGPU(){

}



void SimplexProbGPU::initProblemStructure(SimplexProblem *inP){

#if COL_CHOICE_METHOD == NESTED_STD
	initNested(inP);
#endif
	initResult(inP);
	initPivoting(inP);
	initSwap();

	//Eqs
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_eqs, &pitch, inP->nRow*sizeof(*d_eqs), inP->nCol ));

	//Obj
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFunc, inP->nCol*sizeof(*d_objFunc)));

	//ObjAux
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFuncAux, inP->nCol*sizeof(*d_objFuncAux)));

	//Basis
	cutilSafeCallNoSync( cudaMalloc((void**) &d_basis, inP->nRow*sizeof(*d_basis)));

	// x index (non-basic)
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xInd, inP->nCol*sizeof(*d_xInd)));

	//xVal, upper, lower
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xVal, inP->nCol*sizeof(*d_xVal)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBounds, inP->nCol*sizeof(*d_xBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBoundsP1, inP->nCol*sizeof(*d_xBoundsP1)));


	//bVal, bLo, bUp
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bVal, inP->nRow*sizeof(*d_bVal)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBounds, inP->nRow*sizeof(*d_bBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBoundsP1, inP->nRow*sizeof(*d_bBoundsP1)));

	if(inP->scaled){
		cutilSafeCallNoSync( cudaMalloc((void**) &d_xScaling, inP->nCol*sizeof(*d_xScaling)));
	} else {
		d_xScaling = NULL;
	}

}

void SimplexProbGPU::deInitProblemStructure(){

	cutilSafeCallNoSync(cudaFree(d_eqs));
	cutilSafeCallNoSync(cudaFree(d_objFunc));
	cutilSafeCallNoSync(cudaFree(d_objFuncAux));
	cutilSafeCallNoSync(cudaFree(d_basis));
	cutilSafeCallNoSync(cudaFree(d_xInd));

	cutilSafeCallNoSync(cudaFree(d_xVal));
	cutilSafeCallNoSync(cudaFree(d_xBounds));
	cutilSafeCallNoSync(cudaFree(d_xBoundsP1));

	cutilSafeCallNoSync(cudaFree(d_bVal));
	cutilSafeCallNoSync(cudaFree(d_bBounds));
	cutilSafeCallNoSync(cudaFree(d_bBoundsP1));

	if(d_xScaling != NULL)
		cutilSafeCallNoSync(cudaFree(d_xScaling));

	deInitPivoting();
	deInitResult();
	deInitSwap();

}



void SimplexProbGPU::initNested(SimplexProblem *inP){
	cutilSafeCallNoSync(cudaMalloc((void**) &d_mask, (inP->nCol-1)*sizeof(*d_mask)));
}

void SimplexProbGPU::deInitNested(){
	cutilSafeCallNoSync(cudaFree(d_mask));
}


/*!
 * Define the number of blocks and threads for CUDA
 * Create the result array on CPU and Device
 */
void SimplexProbGPU::initResult(SimplexProblem *inP){
	int nEl = MAX(inP->nRow, inP->nCol);
	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
#if MAPPED_RESULT && PINNED
	int flags = cudaHostAllocMapped;
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dRes, (void *)hRes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dERes, (void *)hERes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dIRes, (void *)hIRes, 0));
#elif PINNED
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#else
	hRes = (red_result_t*) malloc(MAX_BLOCKS*sizeof(red_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	hERes = (exp_result_t*) malloc(MAX_BLOCKS*sizeof(exp_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	hIRes = (int*) malloc(MAX_BLOCKS*sizeof(int));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#endif
}

void SimplexProbGPU::deInitResult(){
#if MAPPED_RESULT || PINNED
	cutilSafeCall(cudaFreeHost(hRes));
	cutilSafeCall(cudaFreeHost(hERes));
	cutilSafeCall(cudaFreeHost(hIRes));
#else
	free(hRes);
	free(hERes);
	free(hIRes)
#endif
	cutilSafeCallNoSync(cudaFree(dRes));
	cutilSafeCallNoSync(cudaFree(dERes));
	cutilSafeCallNoSync(cudaFree(dIRes));

}


/*!
 * \brief Initializing CUBLAS
 */
bool SimplexProbGPU::initPivoting(SimplexProblem *inP){

	cublasStatus stat;

	stat = cublasAlloc(inP->nRow, sizeof(*x), (void**)&x);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	stat = cublasAlloc(inP->nCol, sizeof(*y), (void**)&y);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}

	//d_zCol
	stat = cublasAlloc(inP->nRow, sizeof(*d_zCol), (void**)&d_zCol);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_zCol
	VAR_TYPE *tmpZ = new VAR_TYPE[inP->nRow];
	for(unsigned int i=0; i<inP->nRow; i++)
		tmpZ[i] = 0.0;
	cublasSetVector(inP->nRow, sizeof(VAR_TYPE), tmpZ, 1, d_zCol, 1);
	delete [] tmpZ;

	// d_one
	stat = cublasAlloc(1, sizeof(*d_one), (void**)&d_one);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_one
	VAR_TYPE tmpOne = 1.0;
	cublasSetVector(1, sizeof(VAR_TYPE), &tmpOne, 1, d_one, 1);



	return true;
}

/*!
 * \brief De-initializing CUBLAS
 */
void SimplexProbGPU::deInitPivoting(){
	cublasFree (y);
	cublasFree (x);
	cublasFree (d_zCol);
}

/*!
 * \brief Initializing swap
 */
void SimplexProbGPU::initSwap(){

	cutilSafeCallNoSync( cudaMalloc((void**) &swpInt, 1*sizeof(int)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt, 1*sizeof(float)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt2, 1*sizeof(float2)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpDbl, 1*sizeof(double)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpTyp, 1*sizeof(VAR_TYPE)) );
}

/*!
 * \brief De-initializing CUBLAS
 */
void SimplexProbGPU::deInitSwap(){

	cutilSafeCallNoSync(cudaFree(swpInt));
	cutilSafeCallNoSync(cudaFree(swpFlt));
	cutilSafeCallNoSync(cudaFree(swpFlt2));
	cutilSafeCallNoSync(cudaFree(swpDbl));
	cutilSafeCallNoSync(cudaFree(swpTyp));

}

unsigned int SimplexProbGPU::nextPow2( unsigned int x ) {
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

inline void SimplexProbGPU::getNumBlocksAndThreads( int n, int &blocks, int &threads)
{
	threads = (n < MAX_THREADS*2) ? nextPow2((n + 1)/ 2) : MAX_THREADS;
    blocks = MIN(MAX_BLOCKS,(n + (threads * 2 - 1)) / (threads * 2));
}
