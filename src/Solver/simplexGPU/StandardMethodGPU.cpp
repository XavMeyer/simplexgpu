//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file StandardMethodGPU.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 9, 2011
 *
 */

#include "StandardMethodGPU.h"

StandardMethodGPU::StandardMethodGPU(Logger *inL, int inDevice) {
	logger = inL;
	kw = new KernelWrapperMono(inL, inDevice);
	p = NULL;
	deltaK = EXPAND_DELTA_0;
}

StandardMethodGPU::~StandardMethodGPU() {
	p = NULL;
	delete kw;
}

void StandardMethodGPU::checkInfeasabilities(){
	kw->checkInfeasWS(p);
}

void StandardMethodGPU::updateInfeasabilities(){
	kw->updateInfeasWS(p);
}

red_result_t StandardMethodGPU::findEnteringVariableP1(){
	return kw->seFindColP1(p);
}

row_result_t StandardMethodGPU::findLeavingVariableP1(red_result_t enteringVar){
	row_result_t res;

	res = kw->expandFindRowP1(enteringVar.index, enteringVar.value, deltaK, EXPAND_TAU, p);
	deltaK += EXPAND_TAU;

	//cout << "IN : " << enteringVar.index << " - OUT : " << res.index << endl;
	//getchar();

	return res;

}

solver_res_t StandardMethodGPU::pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	if(leavingVar.index == -1){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Infeasible aux.";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Unbounded aux";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return UNBOUNDED_AUX;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){
		kw->nonBasicToBound(enteringVar.index, leavingVar.alpha, leavingVar.absP, p);
	} else if(leavingVar.mode == M_PIVOT) {
		kw->updatingBasisWS(leavingVar.index, enteringVar.index, leavingVar.alpha, p);
		kw->pivotingWS(leavingVar.index, enteringVar.index, p);
		ip->update(leavingVar.index, enteringVar.index);
	} else if(leavingVar.mode == M_INF_TO_BOUND) {
		kw->nonBasicToBound(enteringVar.index, leavingVar.alpha, leavingVar.absP, p);
	} else if(leavingVar.mode == M_INF_PIVOT) {
		kw->updatingBasisWS(leavingVar.index, enteringVar.index, leavingVar.alpha, p);
		kw->pivotingWS(leavingVar.index, enteringVar.index, p);
		ip->update(leavingVar.index, enteringVar.index);
	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex auxiliary phase).");
		}
		return ERROR;
	}

	return SUCCESS;
}

red_result_t StandardMethodGPU::findEnteringVariable(){
	return kw->seFindCol(p);
}

row_result_t StandardMethodGPU::findLeavingVariable(red_result_t enteringVar){
	row_result_t res;

	res = kw->expandFindRow(enteringVar.index, enteringVar.value, deltaK, EXPAND_TAU, p);
	deltaK += EXPAND_TAU;

	//cout << "IN : " << enteringVar.index << " - OUT : " << res.index << endl;
	//getchar();

	return res;
}

solver_res_t StandardMethodGPU::pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	if(leavingVar.index == -1){ // No row => infeasible
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Infeasible");
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){ // Unbounded
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Unbounded");
		}
		return UNBOUNDED;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){ // Bounded by non-basic
		kw->nonBasicToBound(enteringVar.index, leavingVar.alpha, leavingVar.absP, p);
	} else if(leavingVar.mode == M_PIVOT) { // Pivot
		kw->updatingBasisWS(leavingVar.index, enteringVar.index, leavingVar.alpha, p);
		kw->pivotingWS(leavingVar.index, enteringVar.index, p);
		ip->update(leavingVar.index, enteringVar.index);

	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex std phase).");
		}

		return ERROR;
	}

	return SUCCESS;
}

void StandardMethodGPU::setDenseProblem(SimplexProblem *inP){


	if(p == NULL) { // no problem in memory
		p = inP;
		kw->initProblemStructure(p);
	} else {
		p = inP;
		kw->deInitProblemStructure();
		kw->initProblemStructure(p);
	}

	// Copy the data
	kw->loadProblem(p);

}

void StandardMethodGPU::setSparseProblem(SparseRevisedProblem *inP){

	throw(NOT_IMPLEMENTED);

}

void StandardMethodGPU::setSparseProblem(SparseRevisedFormProblem *inP){

	throw(NOT_IMPLEMENTED);

}

void StandardMethodGPU::reinitProblem(){
	kw->reinitPWS(p);
}

void StandardMethodGPU::getProblem(SimplexProblem *outP){
	if(outP == NULL){
		outP = p->clone();
	}

	kw->getDeviceData(outP);
}

void StandardMethodGPU::getSolution(SimplexSolution *outSol){
	if(outSol == NULL){
		outSol = new SimplexSolution(p->nVar, p->nSlack);
	}

	kw->getSolution(p, outSol);
}

void StandardMethodGPU::changeBound(int index, float2 bound, IndexPivoting *ip){
	p->xBounds[index] = bound;
	kw->changeBoundWK(p, ip, index, bound);
}

void StandardMethodGPU::processOptimum(){
	kw->processOptimumWS(p);
}

void StandardMethodGPU::resetExpand(){

	// Reset deltaK
	deltaK = EXPAND_DELTA_0;

	kw->resetBoundsWS(p);

	// reprocessing obj value
	kw->processOptimumWS(p);

	// check inf
	kw->checkInfeasWS(p);

	if(logger->isActive()){
		ostringstream oss (ostringstream::out);
		oss << p->nInf << " infeasability after resetExpand.";
		logger->log(Logger::LOG_INFO, oss.str());
	}
}

void StandardMethodGPU::saveWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL)
		delete wStart;

	// Read the GPU problem state
	SimplexProblem *gpuProb = p->clone();
	this->getProblem(gpuProb);

	// Save the counter status, the GPU problem state and the pivoting index
	wStart = new SimplexWarmstart<SimplexProblem>(counter, gpuProb, ip->clone());

}

void StandardMethodGPU::loadWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL){
		// Set the counter to the nb of iteration of the warmstart
		counter =  wStart->getCounter();

		// Remove the current pivoting index and replace it by the new one
		delete ip;
		ip = wStart->getIndexPivoting()->clone();

		// Load the old problem state
		kw->loadProblem(wStart->getProblem());
		// TODO check the state of the current problem (p) bounds (they should be probably reinit by the BNB
		// PROBABLY THE SOLUTION : Reinitialize the bounds one by one... not neat but whatever
		for(int i=0; i<p->nVar; i++)
			this->changeBound(i, p->xBounds[i], ip);


	}
}

