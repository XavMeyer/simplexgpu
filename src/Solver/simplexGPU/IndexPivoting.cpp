//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * IndexPivoting.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: meyerx
 */

#include "IndexPivoting.h"

IndexPivoting::IndexPivoting(int inNVar, int inNRow) {
	nVar = inNVar;
	nRow = inNRow;

	basis = new int[nRow];
	xInd = new int[nVar];
	xLoc = new int[nVar];

	initVal();
}

IndexPivoting::~IndexPivoting() {
	delete [] basis;
	delete [] xInd;
	delete [] xLoc;
}

IndexPivoting * IndexPivoting::clone(){

	IndexPivoting * copy = new IndexPivoting(nVar, nRow);

	memcpy(copy->basis, basis, nRow*sizeof(int));
	memcpy(copy->xInd, xInd, nVar*sizeof(int));
	memcpy(copy->xLoc, xLoc, nVar*sizeof(int));

	return copy;

}

void IndexPivoting::initVal(){

	for(int i=0; i<nVar; i++){
		xInd[i] = i;
		xLoc[i] = i;
	}

	for(int i=0; i<nRow; i++){
		basis[i] = i+nVar;
	}


}

void IndexPivoting::update(int row, int col){
	int temp = xInd[col];
	xInd[col] = basis[row];
	basis[row] = temp;

	if(xInd[col] < nVar) // Is it a decision variable ?
		xLoc[xInd[col]] = col; // Updating variable location

	if(basis[row] < nVar) // Is it a decision variable ?
		xLoc[basis[row]] = nVar + row; // Updating variable location

}

void IndexPivoting::updateForFullA(int row, int col){

	int inIndex = col;
	int outIndex = basis[row];


	if(inIndex < nVar){ // Is it a decision variable ?
		xLoc[inIndex] = row + nVar;
	}

	if(outIndex < nVar){ // Is it a decision variable ?
		xLoc[outIndex] = col;
	}

	basis[row] = inIndex;

}


