//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file StandardMethodSpecP1GPU.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 9, 2011
 *
 */

#ifndef STANDARDMETHODSPECP1GPU_H_
#define STANDARDMETHODSPECP1GPU_H_

#include "../simplex/Method.h"
#include "cuda/KernelWrapperMono.h"

class StandardMethodSpecP1GPU: public Method {
public:

	/*
	 * Constructor / Destructor
	 *
	 */
	StandardMethodSpecP1GPU(Logger *inL, int inDevice);
	virtual ~StandardMethodSpecP1GPU();

	/*
	 * Phase 1 methods
	 *
	 */
	// Check for infeasabilities and update the phase 1 objective function
	void checkInfeasabilities();

	// Update the phase 1 objective function
	void updateInfeasabilities();

	// Find the entering variable
	red_result_t findEnteringVariableP1();

	// Find the leaving variable
	row_result_t findLeavingVariableP1(red_result_t enteringVar);

	// Do the pivoting steps / upgrade the basis
	solver_res_t pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip);

	/*
	 * Phase 2 methods
	 *
	 */
	// Find the entering variable
	 red_result_t findEnteringVariable();

	// Find the leaving variable
	row_result_t findLeavingVariable(red_result_t enteringVar);

	// Do the pivoting steps / upgrade the basis
	solver_res_t pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip);

	/*
	 * Problem related methods
	 *
	 */

	// Set the starting problem
	// Warning : must be called prior to reinitProblem call
	void setDenseProblem(SimplexProblem *inP);
	void setSparseProblem(SparseRevisedProblem *inP);
	void setSparseProblem(SparseRevisedFormProblem *inP);

	// The problem is reinit'd but the current value of the solution is kept
	// Warning :  setProblem must have been called prior to this
	void reinitProblem();

	// Return the current state of the problem in the object pointed by outP
	void getProblem(SimplexProblem *outP);

	// Return the current solution of the problem
	 void getSolution(SimplexSolution *outSol);

	// Change a bound of the problem
	void changeBound(int index, float2 bound, IndexPivoting *ip);

	// Process the value of the optimum function
	void processOptimum();

	// Reset the perturbation of the expand method
	void resetExpand();

	// Warmstart management
	void saveWarmstart(unsigned int &counter, IndexPivoting *ip);
	void loadWarmstart(unsigned int &counter, IndexPivoting *ip);

private:

	int cntUpdateInf;
	double deltaK;
	SimplexWarmstart<SimplexProblem> *wStart;
	Logger *logger;
	SimplexProblem *p;
	KernelWrapperMono *kw;

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
	bool firstInVar;
#endif
};

#endif /* STANDARDMETHODSPECP1GPU_H_ */
