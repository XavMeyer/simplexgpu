//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SimplexProbGPU.h
 *
 *  Created on: Sep 7, 2011
 *      Author: meyerx
 */

#ifndef SIMPLEXPROBGPU_H_
#define SIMPLEXPROBGPU_H_

#include "../SimplexProblem.h"
//#include "cutil_math.h"
#include "cutil_inline.h"
#include "cublas.h"
#include "cuda/KernelWrapper.h"

class SimplexProbGPU {
public:
	SimplexProbGPU(SimplexProblem *inProb);
	virtual ~SimplexProbGPU();

	// array pitch
	size_t			pitch;

	// Res variables
	int 			nBlocks;
	int 			nThreads;

	// Temp result
	int*			hIRes;
	int*			dIRes;

	red_result_t* 	hRes;
	red_result_t* 	dRes;

	exp_result_t* 	hERes ;
	exp_result_t* 	dERes;

	// Matrix variables
	//int _nbRow, _nbCol;
	VAR_TYPE* 		x;
	VAR_TYPE* 		y;
	VAR_TYPE*		d_one;
	VAR_TYPE*		d_zCol;
	int*			d_mask;

	// Swap
	int*			swpInt;
	float*			swpFlt;
	float2*			swpFlt2;
	double*			swpDbl;
	VAR_TYPE*		swpTyp;

	// Global memory pointers
	VAR_TYPE* 		d_eqs;
	VAR_TYPE* 		d_objFunc;
	VAR_TYPE*		d_objFuncAux;
	int* 			d_basis;
	int*			d_xInd;
	VAR_TYPE*		d_xScaling;

	VAR_TYPE*		d_xVal;
	float2*			d_xBounds;
	float2*			d_xBoundsP1;

	VAR_TYPE* 		d_bVal;
	float2*			d_bBounds;
	float2*			d_bBoundsP1;

private:

	void    initProblemStructure(SimplexProblem *inP);
	void 	deInitProblemStructure();

	void 	initNested(SimplexProblem *inP);
	void 	deInitNested();

	void	initResult(SimplexProblem *inP);
	void 	deInitResult();

	bool 	initPivoting(SimplexProblem *inP);
	void 	deInitPivoting();

	void 	initSwap();
	void 	deInitSwap();

	inline void getNumBlocksAndThreads( int n, int &blocks, int &threads);
	unsigned int nextPow2( unsigned int x );

};

#endif /* SIMPLEXPROBGPU_H_ */
