//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * Solver.h
 *
 *  Created on: March 10, 2010
 *      Author: meyerx
 */
#ifndef SOLVERMONO_H_
#define SOLVERMONO_H_

#include <iostream>
#include <sstream>
using namespace std;

#include "../simplex/Solver.h"
#include "cuda/KernelWrapperMono.h"

/*!
 * \class Solver
 * \brief Solve a simplex problem given as an array
 */
class SolverMono : public Solver{
public:

	SolverMono(Logger *inL); //!< constructor

	/*!
	 * \brief Constructor with the device number
	 */
	SolverMono(Logger *inL, int device);

	/*!
	 * \brief Constructor with no cuda init
	 */
	SolverMono(Logger *inL, bool noInit);

	virtual ~SolverMono(); 	//!< destructor


	/*!
	 * \brief Solve the given equation matrix.
	 * \param debug : Mode debug on/off (print the stages)
	 */
	solver_res_t solve();

	/*!
	 * \brief Solve the given equation matrix.
	 * \param debug : Mode debug on/off (print the stages)
	 */
	solver_res_t solve(unsigned int inMaxIter);

	//void minimize();
	//void maximize();

	void setProblem(SimplexProblem *inP);
	void reloadProblem();
	void getProblem(SimplexProblem *inP);

	void getSolution(SimplexSolution *inSol);

	void changeBound(int index, float2 bound);

	int getCounter();

	void saveWarmstart();
	void loadWarmstart();
	void enableWarmstart();
	void disableWarmstart();

	SimplexProblemStats * getStats();

private:


	enum mode_t {NORMAL, SMALLEST_SUBSCRIPT};
	enum mode_t mode;

	// Variables
	IndexPivoting *ip;
	SimplexProblem *p;
	KernelWrapperMono *kw;
	SimplexProblemStats *pStats;
	SimplexWarmstart<SimplexProblem> *wStart;

	VAR_TYPE	prevObjVal;
	int			degenerateCount;
	VAR_TYPE	sig;
	//bool		debug;

	//Expand
	unsigned int iterLimit, iterCnt;
	int			iterK;
	double 		deltaK;
	int			counter;
	int			counterTotal;
	int			nTobound, nPivot;

	// Methods

	// Initialization methods
	void 			initVariables();

	// Solver methods
	solver_res_t		SolveSimplex();
	solver_res_t		SolveAuxiliary();

	// Expand
	void 			resetExpand();

	// Debug functions
	void 			printDebug();
	void 			printDebug(red_result_t pRow, red_result_t pCol);
	void 			printDebug(row_result_t pRow, red_result_t pCol);


	void			initTracker();
	//void			trackDensity(int it);
	void			deInitTracker();
	FILE*			fichier;

};
#endif /* SOLVER_H_ */
