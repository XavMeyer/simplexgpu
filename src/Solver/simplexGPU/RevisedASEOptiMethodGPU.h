//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedMethodSEGPU.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#ifndef REVISEDMETHODASEOPTIGPU_H_
#define REVISEDMETHODASEOPTIGPU_H_

#include "../simplex/Method.h"
#include "../RevisedFormProblem.h"
#include "cuda/KernelWrapperRevised.h"
//#include <type_traits>

#define MAX_UPDATE_SE_COEFF_ASE 		200

class RevisedASEOptiMethodGPU: public Method {
public:
	RevisedASEOptiMethodGPU(Logger *inL, int inDevice);
	virtual ~RevisedASEOptiMethodGPU();

	/*
	 * Phase 1 methods
	 *
	 */
	// Check for infeasabilities and update the phase 1 objective function
	void checkInfeasabilities();

	// Update the phase 1 objective function
	void updateInfeasabilities();

	// Find the entering variable
	red_result_t findEnteringVariableP1();

	// Find the leaving variable
	row_result_t findLeavingVariableP1(red_result_t enteringVar);

	// Do the pivoting steps / upgrade the basis
	solver_res_t pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip);

	/*
	 * Phase 2 methods
	 *
	 */
	// Find the entering variable
	 red_result_t findEnteringVariable();

	// Find the leaving variable
	row_result_t findLeavingVariable(red_result_t enteringVar);

	// Do the pivoting steps / upgrade the basis
	solver_res_t pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip);

	/*
	 * Problem related methods
	 *
	 */

	// Set the starting problem
	// Warning : must be called prior to reinitProblem call
	void setDenseProblem(SimplexProblem *inP);
	void setSparseProblem(SparseRevisedProblem *inP);
	void setSparseProblem(SparseRevisedFormProblem *inP);

	// The problem is reinit'd but the current value of the solution is kept
	// Warning :  setProblem must have been called prior to this
	void reinitProblem();

	// Return the current state of the problem in the object pointed by outP
	void getProblem(SimplexProblem *outP);

	// Return the current solution of the problem
	 void getSolution(SimplexSolution *outSol);

	// Change a bound of the problem
	void changeBound(int index, float2 bound, IndexPivoting *ip);

	// Process the value of the optimum function
	void processOptimum();

	// Reset the perturbation of the expand method
	void resetExpand();

	// Warmstart management
	void saveWarmstart(unsigned int &counter, IndexPivoting *ip);
	void loadWarmstart(unsigned int &counter, IndexPivoting *ip);

private:

	bool basisUpdated;

	VAR_TYPE Zq;
	red_result_t inVar;
	row_result_t outVar;

	bool refreshRC, refreshRCP1;
	unsigned int cntUpdateSE, cntUpdateRedCost;

	double deltaK;

	SimplexWarmstart<RevisedFormProblem> *wStart;
	Logger *logger;
	SimplexProblem *p;
	RevisedFormProblem *rp;
	KernelWrapperRevised *kw;

#if PRINT_DEBUG
	RevisedFormProblem *currentRP;
#endif

};

#endif /* REVISEDMETHODASEOPTIGPU_H_ */
