//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * Solver.cpp
 *
 *  Created on: March 10, 2010
 *      Author: meyerx
 */

#include "SolverMulti.h"
#include <float.h>
#include <stdio.h>

/*! Default constructor */
SolverMulti::SolverMulti() {
	SolverMulti::initVariables();
}

/*! Constructor with the equations matrix */
SolverMulti::SolverMulti(bool maximize, SimplexProblem *inP, multiGPU_t *inMGPU) {

	p = inP;
	mGPU = inMGPU;

	if(maximize){
		sig = 1.0;
	} else{
		sig = -1.0;
	}
	initVariables();
}

/* Destructor */
SolverMulti::~SolverMulti() {
	p = NULL;
	mGPU = NULL;
	//delete &lP;
}

/*!
 * \brief This function initialize the private variables
 *
 */
void SolverMulti::initVariables() {
	mode = NORMAL;
	prevObjVal = 0;
	deltaK = EXPAND_DELTA_0;
	iterK = 0;
	counter = 0;
	nTobound = 0;
	nPivot = 0;
	debug = false;
}

void SolverMulti::solve(int inDevice, bool inDebug) {

	device = inDevice;
	debug = inDebug;
	kw = KernelWrapperMulti(device);

	pthread_create(&m_thread, NULL, SolverMulti::run, this);
}

void SolverMulti::initLocalProb(){

	lP = LocalSProblem();

	int col;

	lP.nCol = floor((float)p->nCol/mGPU->nbGPU);
	if(device < (p->nCol % mGPU->nbGPU))
		lP.nCol++;

	lP.nRow = p->nRow;

	if(device >= (p->nCol % mGPU->nbGPU))
		col = device * lP.nCol + (p->nCol % mGPU->nbGPU);
	else
		col = device * lP.nCol;

	lP.eqs = &p->eqs[IDX2C(0, col, p->nRow)];

	lP.bVal = p->bVal;
	lP.basis = p->basis;
	lP.bBounds = p->bBounds;
	lP.bBoundsP1 = p->bBoundsP1;

	lP.xVal = &p->xVal[col];
	lP.xInd = &p->xInd[col];
	lP.xBounds = &p->xBounds[col];
	lP.xBoundsP1 = &p->xBoundsP1[col];

	lP.objFunc = &p->objFunc[col];
	lP.objFuncAux = &p->objFuncAux[col];

	lP.scaled = p->scaled;
	if(p->scaled)
		lP.xScaling = &p->xScaling[col];


	lP.RHS = device == (mGPU->nbGPU-1);

	lP.nVar = lP.nCol - lP.RHS;

	printf("Device [%d] - Row : %d - Col : %d - offset Col : %d - nVar : %d\n", device, lP.nRow, lP.nCol, col, lP.nVar);
}

/*!
 * This function launch the simplex algorithm
 */
void * SolverMulti::run(void * arg) {

	SolverMulti * s = (SolverMulti*)arg;
	s->execute();

	return NULL;
}

Solver::result_t SolverMulti::execute() {

	int r = 0;
	int lastCnt;
	result_t result = SUCCESS;

	initLocalProb();
	//lP.Print();

	// Init cuda with the problem
	kw.initCuda(device, &lP);

	// Check if the problem is infeasible
	//kw.checkInfeasWS(&lP);
	checkInfeasibility();
	//printf("nInf start : %d\n", p->nInf);

	while(r < EXPAND_T_RESET){

		lastCnt = counter;

		if(p->mode == p->AUX)
			result = SolveAuxiliary();

		if(result == SUCCESS)
			result = SolveSimplex();

		if(lastCnt == counter)
			break;

		if(result != GOTO_PHASE1){
			resetExpand();
			if(counter > 100000)
				kw.reinitPWS(&lP);
			//checkInfeasWS(p);

			r++;
		}
	}

	//reinitPWS(p);
	processOptimum();

	if(device == 0){
		if(result == SUCCESS)
			printf("Success!\nOptimum : %f \n", sig*p->objVal);
		else if(result == UNBOUNDED_AUX)
			printf("Aux infeasible : Unbounded problem.\n");
		else if(result == UNBOUNDED)
			printf("Problem is not bounded!\n");
		else if(result == NO_SOLUTION)
			printf("Dual infeasible : No initial dictionary found!\n");
		else if(INFEASIBLE)
			printf("Problem is infeasible!\n");
		else if(MAX_ITER_REACHED)
			printf("Max iteration (%d) reached without founding an optimum.\n", MAX_ITER);
	}

	kw.getDeviceData(&lP);
	kw.deInitCuda(true);

	return result;
}


/*!
 * This function solve the simplex problem
 * from a feasible dictionary.
 */
Solver::result_t SolverMulti::SolveSimplex() {

	int	colDev;
	int toB, piv;
	red_result_t pColResult;
	row_result_t pRowResult;

	toB = 0;
	piv = 0;

	if(device == 0){
		printf("Phase 2 : std Simplex \n");
	}

	// While max_iter not reached
	while(counter < MAX_ITER) {

		if(debug){
			kw.getDeviceData(&lP);
			processOptimum();
			if(device == 0)
				printDebug();
		}

		pColResult = findCol(colDev);

		// If there is a potential column
		if(pColResult.index >= 0) {

			pRowResult = findRow(pColResult, colDev);

			if(pRowResult.index == -1){ // No row => infeasible
				printf("(Infeasible) Stopped at iteration %d.\n", counter);
				return INFEASIBLE;
			} else if(pRowResult.mode == M_UNBOUNDED){ // Unbounded
				printf("(Unbounded) Stopped at iteration %d.\n", counter);
				return UNBOUNDED;
			} else if(pRowResult.mode == M_BASIC_TO_BOUND){ // Bounded by non-basic
				toBound(pColResult, pRowResult, colDev); toB++;
			} else if(pRowResult.mode == M_PIVOT) { // Pivot
				pivoting(pColResult, pRowResult, colDev); piv++;
			} else {
				printf("Not good : unknown operation (simplex std)\n");
			}

			// Debug msg
			if(debug){
				kw.getDeviceData(&lP);
				processOptimum();
				pthread_barrier_wait(&mGPU->barrier);
				if(device == 0)
					printDebug(pRowResult, pColResult, colDev);
			}

			// optimum state and counter
			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){
				processOptimum();
				if(device == 0){
					printf("Iteration : %d - Optimum : %e.\n", counter, sig*p->objVal);
					printf("Pivoting steps : %d - To bound steps : %d \n", piv, toB);
					toB = 0;
					piv = 0;
				}
			}
			counter++;

			// expand state, does we need a reset ?
			if(iterK == EXPAND_K) {
				iterK = 0;
				resetExpand();
				if(lP.nInf > 0)
					return GOTO_PHASE1;
			} else {
				iterK++;
			}

		} else {

			processOptimum();
			if(device == 0){
				printf("(Success) Last iteration : %d - Optimum : %e.\n", counter, sig*p->objVal);
			}
			return SUCCESS;
		}
	}

	processOptimum();
	if(device == 0)
		printf("(Max iter)Last iteration : %d - Optimum : %e.\n", counter, sig*p->objVal);

	return MAX_ITER_REACHED;
}


/*!
 * \brief This function solve the Auxiliary problem
 */
Solver::result_t SolverMulti::SolveAuxiliary() {

	int	colDev;
	red_result_t pColResult;
	row_result_t pRowResult;

	if(device == 0){
		printf("Phase 1 : Auxiliary problem \n");
	}

	if(debug){
		processOptimum();
		kw.getDeviceData(&lP);
		if(device == 0)
			printDebug();
	}

	while(counter < MAX_ITER){ // while MAX_ITER not reached

		pColResult = findColP1(colDev);

		if(pColResult.index >= 0) {
			// Get the pivot row
			pRowResult = findRowP1(pColResult, colDev);

			if(pRowResult.index == -1){
				printf("(Infeasible aux) Stopped at iteration %d.\n", counter);
				return INFEASIBLE;
			} else if(pRowResult.mode == M_UNBOUNDED){
				printf("(Unbounded aux) Stopped at iteration %d.\n", counter);
				return UNBOUNDED_AUX;
			} else if(pRowResult.mode == M_BASIC_TO_BOUND){
				toBound(pColResult, pRowResult, colDev);
			} else if(pRowResult.mode == M_PIVOT) {
				pivoting(pColResult, pRowResult, colDev);
			} else if(pRowResult.mode == M_INF_TO_BOUND) {
				toBound(pColResult, pRowResult, colDev);
			} else if(pRowResult.mode == M_INF_PIVOT) {
				pivoting(pColResult, pRowResult, colDev);
			} else {
				printf("*******************not good!\n");
			}

			// Debug msg
			if(debug){
				processOptimum();
				kw.getDeviceData(&lP);
				if(device == 0)
					printDebug(pRowResult, pColResult, colDev);
			}

			if(iterK == EXPAND_K) {
				iterK = 0;
				resetExpand();
			} else {
				iterK++;

				//if(counter % 400 == 0)
				//	checkInfeasWS(&lP);
				//else
				updateInfeasibility(); //kw.updateInfeasWS(&lP);
			}

			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){

				processOptimum();
				if(device == 0){
					printf("Iteration : %d - Infeasibility : %e (%d) - Optimum : %e.\n", counter, -1.0*p->objAuxVal, p->nInf, sig*p->objVal);
				}
			}
			counter++;

			if(lP.nInf == 0)
				return SUCCESS;

		} else {

			processOptimum();
			if(device == 0){
				printf("Last iteration : %d - Infeasibility : %e (%d) - Optimum : %e.\n", counter, -1.0*p->objAuxVal, p->nInf, sig*p->objVal);
			}
			return NO_SOLUTION;
		}
	}


	processOptimum();
	if(device == 0){
		printf("(Max iter) Last iteration : %d - Optimum : %e.\n", counter, sig*p->objVal);
	}

	return MAX_ITER_REACHED;

}


// Reset expand value and bounds
void SolverMulti::resetExpand() {

	// Reset deltaK
	deltaK = EXPAND_DELTA_0;

	//kw.resetBoundsWS(&lP);
	resetVal();

	// reprocessing obj value
	processOptimum();

	// check inf
	checkInfeasibility();
	if(device == 0)
		printf("%d infeasability after resetExpand.\n", p->nInf);
}


void SolverMulti::printDebug(){
	printf("*\n");
	printf("obj : %e (%d : %e)\n", sig*	p->objVal, p->nInf, p->objAuxVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->PrintEquations();
		p->PrintObjFunction();
		p->PrintObjFunctionAux();
		p->PrintX();
	}

	//getchar();
}


void SolverMulti::printDebug(red_result_t pRow, red_result_t pCol){
	printf("*\n");
	printf("pCol idx : %d - val : %e \n", pCol.index, pCol.value);
	printf("pRow idx : %d - val : %e \n", pRow.index, pRow.value);
	//printf("&lP value : %e - ", p->eqs[IDX2C(pRow.index, pCol.index, p->nRow)]);
	printf("obj : %e \n", sig*p->objVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->PrintEquations();
		p->PrintObjFunction();
		p->PrintObjFunctionAux();
		p->PrintX();
	}

	//getchar();
}

void SolverMulti::printDebug(row_result_t pRow, red_result_t pCol){
	string s;
	switch(pRow.mode){
	case M_PIVOT : s = "Pivot"; break;
	case M_INF_PIVOT : s = "Infeasible Pivot (p1)"; break;
	case M_UNBOUNDED : s = "Unbounded"; break;
	case M_BASIC_TO_BOUND : s = "Non-basic to bound"; break;
	case M_INF_TO_BOUND	: s = "Infeasible to bound (p1)"; break;
	case M_INFEASIBLE	: s = "Infeasible"; break;
	default : s = "Unknown move";
	}

	printf("*** %s \n", s.c_str());
	printf("pCol idx : %d - val : %e \n", pCol.index, pCol.value);
	printf("pRow idx : %d - pivot : %e - alpha : %e \n", pRow.index, pRow.absP, pRow.alpha);
	//printf("&lP value : %e - ", p->eqs[IDX2C(pRow.index, pCol.index, p->nRow)]);
	printf("obj : %e (%d : %e)\n", sig*p->objVal, p->nInf, p->objAuxVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->PrintEquations();
		p->PrintObjFunction();
		p->PrintObjFunctionAux();
		p->PrintX();
	}

		//getchar();

}

void SolverMulti::printDebug(row_result_t pRow, red_result_t pCol, int colDev){
	string s;
	switch(pRow.mode){
	case M_PIVOT : s = "Pivot"; break;
	case M_INF_PIVOT : s = "Infeasible Pivot (p1)"; break;
	case M_UNBOUNDED : s = "Unbounded"; break;
	case M_BASIC_TO_BOUND : s = "Non-basic to bound"; break;
	case M_INF_TO_BOUND	: s = "Infeasible to bound (p1)"; break;
	case M_INFEASIBLE	: s = "Infeasible"; break;
	default : s = "Unknown move";
	}

	printf("*** Device(%d) %s \n", colDev, s.c_str());
	printf("pCol idx : %d (%d) - val : %e \n", pCol.index, colDev, pCol.value);
	printf("pRow idx : %d - pivot : %e - alpha : %e \n", pRow.index, pRow.absP, pRow.alpha);
	//printf("&lP value : %e - ", p->eqs[IDX2C(pRow.index, pCol.index, p->nRow)]);
	printf("obj : %e (%d : %e)\n", sig*p->objVal, p->nInf, p->objAuxVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->PrintEquations();
		p->PrintObjFunction();
		p->PrintObjFunctionAux();
		p->PrintX();
	}

	getchar();

}

pthread_t SolverMulti::getPThread(){
	return m_thread;
}


red_result_t SolverMulti::findCol(int &colDev){
	red_result_t myCol;
	red_result_t resCol;
	// Get the pivot column
	#if COL_CHOICE_METHOD == STEEPEST_EDGE
		myCol = kw.seFindCol(&lP);//seFindCol(&lP);
	#elif COL_CHOICE_METHOD == NESTED_STD
		myCol = kw.nestedFindCol(&lP);
	#else
		myCol = kw.stdFindCol(&lP);
	#endif

	// Put result in shared CPU memory
	SET(mGPU->exSE[device], myCol);
	// Synchro barrier
	pthread_barrier_wait(&mGPU->barrier);
	colDev = -1;
	resCol.index = -1;
	resCol.value = 0.0;
	for(int i=0; i<mGPU->nbGPU; i++){
		//printf("Device %d - index : %d - value : %f - resCol value : %f\n", i, mGPU->exSE[i].index, mGPU->exSE[i].value, resCol.value);
		if(fabs(resCol.value) < fabs(mGPU->exSE[i].value)){
			resCol.index = mGPU->exSE[i].index;
			resCol.value = mGPU->exSE[i].value;
			colDev = i;
		}
	}

	//printf("max SE device : %d \n", colDev);

	return resCol;
}

red_result_t SolverMulti::findColP1(int &colDev){
	red_result_t myCol;
	red_result_t resCol;
	// Get the pivot column
	#if COL_CHOICE_METHOD == STEEPEST_EDGE
		myCol = kw.seFindColP1(&lP);//seFindColP1(&lP);
	#elif COL_CHOICE_METHOD == NESTED_STD
		myCol = kw.nestedFindColP1(&lP);
	#else
		myCol = kw.stdFindCol(&lP);
	#endif

	// Put result in shared CPU memory
	SET(mGPU->exSE[device], myCol);
	// Synchro barrier
	pthread_barrier_wait(&mGPU->barrier);
	colDev = -1;
	resCol.index = -1;
	resCol.value = 0.0;
	for(int i=0; i<mGPU->nbGPU; i++){

		if(fabs(resCol.value) < fabs(mGPU->exSE[i].value)){
			resCol.index = mGPU->exSE[i].index;
			resCol.value = mGPU->exSE[i].value;
			colDev = i;
		}
		//printf("[%d]exSE : %d : %f - device : %d \n", device, mGPU->exSE[i].index, mGPU->exSE[i].value, i);
		//printf("[%d]resCol : %d : %f - max SE device : %d \n", device, resCol.index, resCol.value, colDev);
	}

	//printf("max SE device : %d \n", colDev);

	return resCol;
}

row_result_t SolverMulti::findRow(red_result_t pColResult, int colDev){

	row_result_t expandRowRes;

	// Get the pivot row (only 1 thread)
	if(device == colDev){
		expandRowRes = kw.expandFindRow(pColResult.index, pColResult.value, deltaK, EXPAND_TAU, &lP);
		SET_R(mGPU->exRow, expandRowRes);
		if(expandRowRes.mode == M_BASIC_TO_BOUND){
			kw.planToBound(pColResult.index, &lP, mGPU);
		} else if(expandRowRes.mode == M_PIVOT){
			kw.planPivoting(expandRowRes.index, pColResult.index, &lP, mGPU);
		}
	}
	deltaK += EXPAND_TAU;

	// Synchro
	pthread_barrier_wait(&mGPU->barrier);

	return mGPU->exRow;
}

row_result_t SolverMulti::findRowP1(red_result_t pColResult, int colDev){

	row_result_t expandRowRes;

	// Get the pivot row (only 1 thread)
	if(device == colDev){
		expandRowRes = kw.expandFindRowP1(pColResult.index, pColResult.value, deltaK, EXPAND_TAU, &lP);
		SET_R(mGPU->exRow, expandRowRes);
		if(expandRowRes.mode == M_BASIC_TO_BOUND || expandRowRes.mode == M_INF_TO_BOUND){
			kw.planToBound(pColResult.index, &lP, mGPU);
		} else if(expandRowRes.mode == M_PIVOT || expandRowRes.mode == M_INF_PIVOT){
			kw.planPivoting(expandRowRes.index, pColResult.index, &lP, mGPU);
		}
	}
	deltaK += EXPAND_TAU;

	// Synchro
	pthread_barrier_wait(&mGPU->barrier);

	return mGPU->exRow;
}

void SolverMulti::pivoting(red_result_t pColResult, row_result_t pRowResult, int colDev){
	if(device == colDev){
		kw.updatingBasisMGPU1(pRowResult.index, pColResult.index, &lP, mGPU);
		kw.pivotingMGPU1(pRowResult.index, pColResult.index, &lP, mGPU);
	} else {
		kw.updatingBasisMGPU2(pRowResult.index, &lP, mGPU);
		kw.pivotingMGPU2(pRowResult.index, &lP, mGPU);
	}
}

void SolverMulti::toBound(red_result_t pColResult, row_result_t pRowResult, int colDev) {
	if(device == colDev){
		kw.toBound1(pColResult.index, pRowResult.alpha, pRowResult.absP, &lP);
	} else {
		kw.toBound2(pRowResult.alpha, &lP, mGPU);
	}
}

void SolverMulti::processOptimum(){

	kw.processOptimumWS(&lP);
	mGPU->exLocOpti[device] = lP.objVal;
	if(p->mode == p->AUX)
		mGPU->exLocOptiAux[device] = lP.objAuxVal;
	pthread_barrier_wait(&mGPU->barrier);
	if(device == 0){
		p->objVal = 0.0;
		p->objAuxVal = 0.0;
		for(int i=0; i<mGPU->nbGPU; i++){
			p->objVal += mGPU->exLocOpti[i];
			p->objAuxVal += mGPU->exLocOptiAux[i];
		}
	}

}

void SolverMulti::checkInfeasibility(){

	kw.checkInfeasWS(&lP);
	mGPU->exInfX[device] = lP.nInfX;
	mGPU->exInfB[device] = lP.nInfB;
	pthread_barrier_wait(&mGPU->barrier);
	if(device == 0){
		p->nInf = 0;
		p->mode = p->PRIMAL;
		for(int i=0; i<mGPU->nbGPU; i++){
			//printf("[%d] inf X : %d - inf B : %d \n", i, mGPU->exInfX[i], mGPU->exInfB[i]);
			p->nInf += mGPU->exInfX[i];
		}
		p->nInf += mGPU->exInfB[0];
		if(p->nInf > 0)
			p->mode = p->AUX;
	}
	pthread_barrier_wait(&mGPU->barrier);
	lP.nInf = p->nInf;
	lP.mode = p->mode;
}

void SolverMulti::updateInfeasibility(){

	//int iNext;

	kw.updateInfeasWS(&lP);
	mGPU->exInfX[device] = lP.nInfX;
	mGPU->exInfB[device] = lP.nInfB;
	pthread_barrier_wait(&mGPU->barrier);
	if(device == 0){
		for(int i=0; i<mGPU->nbGPU; i++){
			p->nInf -= mGPU->exInfX[i];
			//iNext = (i+1)%mGPU->nbGPU;
			//if(mGPU->exInfB[i] != mGPU->exInfB[iNext])
			//printf("Basis infeasability GPU[%d] : %d vs GPU[%d] : %d \n", i, mGPU->exInfB[i], iNext, mGPU->exInfB[iNext]);
		}
		p->nInf -= mGPU->exInfB[0];
	}
	pthread_barrier_wait(&mGPU->barrier);
	lP.nInf = p->nInf;

}

void SolverMulti::resetVal(){

	int nNtriv = 0;

	// Reset xVal and count non trivial move
	mGPU->exNTriv[device] = kw.resetXValMGPU(&lP);
	pthread_barrier_wait(&mGPU->barrier);
	// reduce non trivial count
	for(int i=0; i<mGPU->nbGPU; i++)
		nNtriv += mGPU->exNTriv[i];

	// If non trivial locally process bVal
	if(nNtriv > 0)
		kw.resetBValMGPU(device, &lP, mGPU);
	pthread_barrier_wait(&mGPU->barrier);

	if(device == 0){
		for(int j=0; j<p->nRow; j++)
			p->bVal[j] = mGPU->exBVal[j];

		for(int i=1; i<mGPU->nbGPU; i++)
			for(int j=0; j<p->nRow; j++)
				p->bVal[j] += mGPU->exBVal[i*lP.nRow + j];
	}

	pthread_barrier_wait(&mGPU->barrier);

	kw.setBVal(&lP);
}

