//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedSEMethodGPU.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "RevisedSEOptiMethodGPU.h"

RevisedSEOptiMethodGPU::RevisedSEOptiMethodGPU(Logger *inL, int inDevice) {
	logger = inL;
	kw = new KernelWrapperRevised(inL, inDevice);
	p = NULL;
	deltaK = EXPAND_DELTA_0;
	rp = NULL;
#if PRINT_DEBUG
	currentRP = NULL;
#endif
	basisUpdated = true;
	cntUpdateSE = 0;
	cntUpdateRedCost = 0;
	refreshRC = refreshRCP1 = true;
}

RevisedSEOptiMethodGPU::~RevisedSEOptiMethodGPU() {
	p = NULL;
	delete rp;
	delete kw;
#if PRINT_DEBUG
	if(currentRP != NULL)
		delete currentRP;
#endif
}

void RevisedSEOptiMethodGPU::checkInfeasabilities(){
#if INF_CHECK_TYPE == INF_CHECK_OLD
	int oldNInf = p->nInf;
	kw->checkInfeasibility(p);
	if(p->nInf != oldNInf){
		refreshRCP1 = true;
	}
#elif INF_CHECK_TYPE == INF_CHECK_NEW
	kw->checkInfeasibilityV2(p);
	cntUpdateInf = 0;
#endif

}

void RevisedSEOptiMethodGPU::updateInfeasabilities(){
#if INF_CHECK_TYPE == INF_CHECK_OLD
	int oldNInf = p->nInf;
	kw->checkInfeasibility(p);
	if(p->nInf != oldNInf){
		refreshRCP1 = true;
	}
#elif INF_CHECK_TYPE == INF_CHECK_NEW
	int prevNInf = p->nInf;
	if(cntUpdateInf < MAX_UPDATE_INFEASABILITY){
		kw->updateInfeasibilityV2(p);
		cntUpdateInf++;
		if(p->nInf <= prevNInf){
			cntUpdateInf++;
		} else {
			cntUpdateInf = MAX_UPDATE_INFEASABILITY;
		}
	} else {
		kw->checkInfeasibilityV2(p);
		cntUpdateInf = 0;
	}
#endif
}

red_result_t RevisedSEOptiMethodGPU::findEnteringVariableP1(){
	red_result_t enteringCol;

#if PRINT_DEBUG
	//this->rp->print();
	kw->getDeviceData(currentRP);
	currentRP->print();
#endif

	if(!basisUpdated){
		// Column remains the same, no need to update columns norm
		kw->processReducedCostP1();
		enteringCol = kw->selectSECoeffP1();
	} else if(cntUpdateSE == 0){
		refreshRC = true;
		refreshRCP1 = false;
		// Process the reduced cost vector
		kw->processReducedCostP1();
		// Once in a while process the true columns norm
		// This operation is quite costly
		kw->processSECoeff();
		enteringCol = kw->selectSECoeffP1();
		cntUpdateSE = 1;
	} else if(refreshRCP1) {
		refreshRC = true;
		refreshRCP1 = false;
		kw->processReducedCostP1();
		enteringCol = kw->updateSECoeffP1(inVar, outVar);
		cntUpdateSE = (cntUpdateSE + 1) % MAX_UPDATE_SE_COEFF;
	} else {
		// Update columns norm
		// If the P1 objective has been updated we have to process
		// the reduced cost again
		if(cntUpdateInf == 0){
			kw->processReducedCostP1();
			enteringCol = kw->updateSECoeffP1(inVar, outVar);
		} else { // else we just update the SE coefficient and reduced costs
			enteringCol = kw->updateInVarP1(inVar, outVar, Zq);
		}

		cntUpdateSE = (cntUpdateSE + 1) % MAX_UPDATE_SE_COEFF;
	}

	// Memorize the entering variable
	inVar = enteringCol;

	return enteringCol;
}

row_result_t RevisedSEOptiMethodGPU::findLeavingVariableP1(red_result_t enteringVar){
	row_result_t res;

	kw->processColumn(enteringVar);
	Zq = kw->getZqP1(enteringVar);
	res = kw->expandFindRowP1(enteringVar, deltaK, EXPAND_TAU);
	deltaK += EXPAND_TAU;

	//cout << "IN : " << enteringVar.index << " - OUT : " << res.index << " (var gain : " <<  res.alpha << ")" << endl;
	//getchar();

	outVar = res;

	return res;

}

solver_res_t RevisedSEOptiMethodGPU::pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	basisUpdated = false;

	if(leavingVar.index == -1){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Infeasible aux.";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Unbounded aux";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return UNBOUNDED_AUX;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){
		kw->updateVariables(enteringVar, leavingVar);
		//cout << "Non basic to bound" << endl;
	} else if(leavingVar.mode == M_PIVOT) {
		basisUpdated = true;
		kw->processBeta();
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariablesP1(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);
		kw->processAlpha(leavingVar);
	} else if(leavingVar.mode == M_INF_TO_BOUND) {
		kw->updateVariables(enteringVar, leavingVar);
		//cout << "Infeasible to bound (" << leavingVar.alpha << ")" << endl;
	} else if(leavingVar.mode == M_INF_PIVOT) {
		basisUpdated = true;
		kw->processBeta();
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariablesP1(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);
		kw->processAlpha(leavingVar);
	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex auxiliary phase).");
		}
		return ERROR;
	}

	return SUCCESS;
}

red_result_t RevisedSEOptiMethodGPU::findEnteringVariable(){
	red_result_t enteringCol;

#if PRINT_DEBUG
	//this->rp->print();
	kw->getDeviceData(currentRP);
	currentRP->print();
#endif
	//kw->processReducedCost();

	if(!basisUpdated){
		// Column remains the same, no need to update columns norm
		kw->processReducedCost();
		enteringCol = kw->selectSECoeff();
	} else if(cntUpdateSE == 0){
		refreshRC = false;
		refreshRCP1 = true;
		// Once in a while process the true columns norm
		// This operation is quite costly
		kw->processReducedCost();
		kw->processSECoeff();
		enteringCol = kw->selectSECoeff();
		cntUpdateSE = 1;
	} else if(refreshRC) {
		refreshRC = false;
		refreshRCP1 = true;
		kw->processReducedCost();
		enteringCol = kw->updateSECoeff(inVar, outVar);
		cntUpdateSE = (cntUpdateSE + 1) % MAX_UPDATE_SE_COEFF;
	} else {
		// Update columns norm
		enteringCol = kw->updateInVar(inVar, outVar, Zq);
		//kw->processReducedCost();
		//kw->updateRedCost(inVar, outVar, Zq);
		//enteringCol = kw->updateSECoeff(inVar, outVar);
		cntUpdateSE = (cntUpdateSE + 1) % MAX_UPDATE_SE_COEFF;
	}

	// Memorize the entering variable
	inVar = enteringCol;

	 //kw->checkInVar(inVar);
	//cout << "In col index : " << enteringCol.index <<  endl;

	return enteringCol;
}

row_result_t RevisedSEOptiMethodGPU::findLeavingVariable(red_result_t enteringVar){
	row_result_t res;

	kw->processColumn(enteringVar);
	Zq = kw->getZq(enteringVar);
	res = kw->expandFindRow(enteringVar, deltaK, EXPAND_TAU);
	deltaK += EXPAND_TAU;

	//cout << "IN : " <<  enteringVar.index << " - OUT : " << res.index << " (absRedCost : " <<  enteringVar.value << ")" << endl;
	//getchar();

	outVar = res;

	return res;
}

solver_res_t RevisedSEOptiMethodGPU::pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	//cout << "IN : " <<  ip->xInd[enteringVar.index] << " - OUT : " << leavingVar.index << " (absRedCost : " <<  enteringVar.value << ")" << endl;

	basisUpdated = false;

	if(leavingVar.index == -1){ // No row => infeasible
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Infeasible");
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){ // Unbounded
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Unbounded");
		}
		return UNBOUNDED;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){ // Bounded by non-basic
		kw->updateVariables(enteringVar, leavingVar);
	} else if(leavingVar.mode == M_PIVOT) { // Pivot
		basisUpdated = true;
		kw->processBeta();
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariables(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);
		kw->processAlpha(leavingVar);
	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex std phase).");
		}

		return ERROR;
	}

	return SUCCESS;
}

void RevisedSEOptiMethodGPU::setDenseProblem(SimplexProblem *inP){
	if(p == NULL) { // no problem in memory
		p = inP;
		rp = new RevisedFormProblem(p);
		kw->initProblemStructure(rp);
	} else {
		p = inP;
		delete rp;
		rp = new RevisedFormProblem(p);
		kw->deInitProblemStructure();
		kw->initProblemStructure(rp);
	}

#if PRINT_DEBUG
	currentRP = rp->clone();
#endif

	// Copy the data
	kw->loadProblem(rp, true);

	// Reinit update SE counter
	cntUpdateSE = 0;


}

void RevisedSEOptiMethodGPU::setSparseProblem(SparseRevisedProblem *inP){

	throw(NOT_IMPLEMENTED);

}

void RevisedSEOptiMethodGPU::setSparseProblem(SparseRevisedFormProblem *inP){

	throw(NOT_IMPLEMENTED);

}


void RevisedSEOptiMethodGPU::reinitProblem(){
	kw->reinitProblem(rp);

	// Reinit update SE counter
	cntUpdateSE = 0;
}

void RevisedSEOptiMethodGPU::getProblem(SimplexProblem *outP){
	if(outP == NULL){
		outP = p->clone();
	}

	kw->getDeviceData(outP);
}

void RevisedSEOptiMethodGPU::getSolution(SimplexSolution *outSol){
	if(outSol == NULL){
		outSol = new SimplexSolution(p->nVar, p->nSlack);
	}

	kw->getSolution(outSol);
}

void RevisedSEOptiMethodGPU::changeBound(int index, float2 bound, IndexPivoting *ip){
	p->xBounds[index] = bound;
	kw->changeBoundWK(ip, index, bound);
}

void RevisedSEOptiMethodGPU::processOptimum(){
	kw->processOptimum(p);
}

void RevisedSEOptiMethodGPU::resetExpand(){

	// Force a SE coefficient processing
	basisUpdated = true;
	cntUpdateSE = 0;

	// Reset deltaK
	deltaK = EXPAND_DELTA_0;

	kw->resetBounds();

	// reprocessing obj value
	kw->processOptimum(p);

	// check inf
#if INF_CHECK_TYPE == INF_CHECK_OLD
	kw->checkInfeasibility(p);
#elif INF_CHECK_TYPE == INF_CHECK_NEW
	kw->checkInfeasibilityV2(p);
#endif


	if(logger->isActive()){
		ostringstream oss (ostringstream::out);
		oss << p->nInf << " infeasability after resetExpand.";
		logger->log(Logger::LOG_INFO, oss.str());
	}
}

void RevisedSEOptiMethodGPU::saveWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL)
		delete wStart;

	// Read the GPU problem state
	RevisedFormProblem *gpuProb = rp->clone();
	kw->getDeviceData(gpuProb);

	// Save the counter status, the GPU problem state and the pivoting index
	wStart = new SimplexWarmstart<RevisedFormProblem>(counter, gpuProb, ip->clone());

}

void RevisedSEOptiMethodGPU::loadWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL){
		// Set the counter to the nb of iteration of the warmstart
		counter =  wStart->getCounter();

		// Remove the current pivoting index and replace it by the new one
		delete ip;
		ip = wStart->getIndexPivoting()->clone();

		// Load the old problem state
		kw->loadProblem(wStart->getProblem(), false);
		// TODO check the state of the current problem (p) bounds (they should be probably reinit by the BNB
		// PROBABLY THE SOLUTION : Reinitialize the bounds one by one... not neat but whatever
		for(unsigned int i=0; i<p->nVar; i++)
			this->changeBound(i, p->xBounds[i], ip);


	}
}

