//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LocalSProblem.cpp
 *
 *  Created on: Nov 2, 2010
 *      Author: meyerx
 */

#include "LocalSProblem.h"

LocalSProblem::LocalSProblem() {

}

LocalSProblem::~LocalSProblem() {
	eqs = NULL;
	objFunc = NULL;
	objFuncAux = NULL;

	xVal = NULL;
	xInd = NULL;
	xBounds = NULL;
	xBoundsP1 = NULL;

	bVal = NULL;
	basis = NULL;
	bBounds = NULL;
	bBoundsP1 = NULL;

	xScaling = NULL;
}


void LocalSProblem::Print() {


	if(nRow > 20 || nCol > 20) return;

	PrintEquations();
	PrintObjFunction();
	if(objFuncAux != NULL)
		PrintObjFunctionAux();
	PrintX();
	printf("----------------------------------------------\n");
	printf("\n");
}

/*!
 * \brief Format and print the equations
 */
void LocalSProblem::PrintEquations() {

	printf("[xInd] - ");

	for(int i=0; i<nCol-1; i++){
		printf("[x%d]  \t\t", xInd[i]);
	}

	printf("\n");

	for(int i=0; i<nRow; i++) {

		printf("[x%d] - ", basis[i]);

		for(int j=0; j<nCol; j++) {
			printf("[%d][%d]%4.3f\t", i, j, eqs[IDX2C(i, j, nRow)]);
		}


		printf("[bVal] ");

		if(bBounds[i].x == -FLT_MAX)
			printf("-inf");
		else
			printf("%4.3f", bBounds[i].x);

		if(bBounds[i].x != bBoundsP1[i].x){
			if(bBoundsP1[i].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)", bBoundsP1[i].x);
		}

		printf(" > %4.3f > ", bVal[i]);

		if(bBounds[i].y == FLT_MAX)
			printf("inf");
		else
			printf("%4.3f", bBounds[i].y);

		if(bBounds[i].y != bBoundsP1[i].y){
			if(bBoundsP1[i].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)", bBoundsP1[i].y);
		}

		printf("\n");
	}

}

/*!
 * \brief print the objective function
 */
void LocalSProblem::PrintObjFunction() {

	printf("[-z] - ");

	for(int j=0; j<nCol; j++)
		printf("   [%d]%4.3f\t", j, objFunc[j]);

	printf("\n");
}


/*!
 * \brief print the objective function
 */
void LocalSProblem::PrintObjFunctionAux() {

	printf("[AUX]- ");

	for(int j=0; j<nCol; j++)
		printf("   [%d]%4.3f\t", j, objFuncAux[j]);

	printf("\n");
}

/*!
 * \brief Format and print the equations
 */
void LocalSProblem::PrintEquations(int nbRows, int nbCols, VAR_TYPE* eqs) {

	for(int i=0; i<nbRows; i++) {
		for(int j=0; j<nbCols; j++) {
			printf("[%d][%d]%4.3f\t", i, j, eqs[IDX2C(i, j, nbRows)]);
		}
		printf("\n");
	}
}

/*!
 * \brief Format and print the equations
 */
void LocalSProblem::PrintX() {

	printf("[xLo] - ");

	for(int j=0; j<nVar; j++) {

		if(xBounds[j].x == -FLT_MAX)
			printf("[%d]  -inf   ", j);
		else
			printf("[%d]%4.3f", j, xBounds[j].x);

		if(xBounds[j].x != xBoundsP1[j].x){
			if(xBoundsP1[j].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)",xBoundsP1[j].x);
		}
		printf("\t");
	}
	printf("\n");

	printf("[xVal] - ");

	for(int j=0; j<nCol; j++) {
		printf("[%d]%4.3f\t", j, xVal[j]);
	}
	printf("\n");

	printf("[xUp] - ");

	for(int j=0; j<nCol-1; j++) {
		if(xBounds[j].y == FLT_MAX)
			printf("[%d]   inf  ", j);
		else
			printf("[%d]%4.3f", j, xBounds[j].y);

		if(xBounds[j].y != xBoundsP1[j].y){
			if(xBoundsP1[j].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)",xBoundsP1[j].y);
		}
		printf("\t");
	}
	printf("\n");

}
