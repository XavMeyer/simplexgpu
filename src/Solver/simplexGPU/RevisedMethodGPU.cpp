//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedMethodGPU.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "RevisedMethodGPU.h"

RevisedMethodGPU::RevisedMethodGPU(Logger *inL, int inDevice) {
	logger = inL;
	kw = new KernelWrapperRevised(inL, inDevice);
	p = NULL;
	deltaK = EXPAND_DELTA_0;
#if PRINT_DEBUG
	currentRP = NULL;
#endif
}

RevisedMethodGPU::~RevisedMethodGPU() {
	p = NULL;
	delete kw;
#if PRINT_DEBUG
	if(currentRP != NULL)
		delete currentRP;
#endif
}

void RevisedMethodGPU::checkInfeasabilities(){
	kw->checkInfeasibility(p);
}

void RevisedMethodGPU::updateInfeasabilities(){
	kw->updateInfeasibility(p);
}

red_result_t RevisedMethodGPU::findEnteringVariableP1(){
	kw->processReducedCostP1();
	return kw->findCol();
}

row_result_t RevisedMethodGPU::findLeavingVariableP1(red_result_t enteringVar){
	row_result_t res;

	cout << "Entering variable : " << enteringVar.index << endl;

	kw->processColumn(enteringVar);
	res = kw->expandFindRowP1(enteringVar, deltaK, EXPAND_TAU);
	deltaK += EXPAND_TAU;

	cout << "Leaving variable : " << res.index << endl;

	return res;

}

solver_res_t RevisedMethodGPU::pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	if(leavingVar.index == -1){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Infeasible aux.";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Unbounded aux";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		return UNBOUNDED_AUX;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){
		kw->updateVariables(enteringVar, leavingVar);
	} else if(leavingVar.mode == M_PIVOT) {
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariablesP1(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);
	} else if(leavingVar.mode == M_INF_TO_BOUND) {
		kw->updateVariables(enteringVar, leavingVar);
	} else if(leavingVar.mode == M_INF_PIVOT) {
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariablesP1(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);
	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex auxiliary phase).");
		}
		return ERROR;
	}

	return SUCCESS;
}

red_result_t RevisedMethodGPU::findEnteringVariable(){

#if PRINT_DEBUG
	//this->rp->print();
	kw->getDeviceData(currentRP);
	currentRP->print();
#endif

	kw->processReducedCost();
	return kw->findCol();
}

row_result_t RevisedMethodGPU::findLeavingVariable(red_result_t enteringVar){
	row_result_t res;

	cout << "Entering variable : " << enteringVar.index << endl;

	kw->processColumn(enteringVar);
	res = kw->expandFindRow(enteringVar, deltaK, EXPAND_TAU);
	deltaK += EXPAND_TAU;

	cout << "Leaving variable : " << res.index << endl;


	return res;
}

solver_res_t RevisedMethodGPU::pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip){

	if(leavingVar.index == -1){ // No row => infeasible
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Infeasible");
		}
		return INFEASIBLE;
	} else if(leavingVar.mode == M_UNBOUNDED){ // Unbounded
		if(logger->isActive()){
			logger->log(Logger::LOG_DEBUG, "Unbounded");
		}
		return UNBOUNDED;
	} else if(leavingVar.mode == M_BASIC_TO_BOUND){ // Bounded by non-basic
		kw->updateVariables(enteringVar, leavingVar);
	} else if(leavingVar.mode == M_PIVOT) { // Pivot
		kw->updateBasis(leavingVar);
		kw->updateVariables(enteringVar, leavingVar);
		kw->swapVariables(enteringVar, leavingVar);
		ip->update(leavingVar.index, enteringVar.index);

	} else {
		if(logger->isActive()){
			logger->log(Logger::LOG_ERROR, "Unknown operation (simplex std phase).");
		}

		return ERROR;
	}

	return SUCCESS;
}

void RevisedMethodGPU::setDenseProblem(SimplexProblem *inP){

	if(p == NULL) { // no problem in memory
		p = inP;
		rp = new RevisedFormProblem(p);
		kw->initProblemStructure(rp);
	} else {
		p = inP;
		delete rp;
		rp = new RevisedFormProblem(p);
		kw->deInitProblemStructure();
		kw->initProblemStructure(rp);
	}

#if PRINT_DEBUG
	currentRP = rp->clone();
#endif

	// Copy the data
	kw->loadProblem(rp, true);


}

void RevisedMethodGPU::setSparseProblem(SparseRevisedProblem *inP){

	throw(NOT_IMPLEMENTED);

}

void RevisedMethodGPU::setSparseProblem(SparseRevisedFormProblem *inP){

	throw(NOT_IMPLEMENTED);

}


void RevisedMethodGPU::reinitProblem(){
	kw->reinitProblem(rp);
}

void RevisedMethodGPU::getProblem(SimplexProblem *outP){
	if(outP == NULL){
		outP = p->clone();
	}

	kw->getDeviceData(outP);
}

void RevisedMethodGPU::getSolution(SimplexSolution *outSol){
	if(outSol == NULL){
		outSol = new SimplexSolution(p->nVar, p->nSlack);
	}

	kw->getSolution(outSol);
}

void RevisedMethodGPU::changeBound(int index, float2 bound, IndexPivoting *ip){
	p->xBounds[index] = bound;
	kw->changeBoundWK(ip, index, bound);
}

void RevisedMethodGPU::processOptimum(){
	kw->processOptimum(p);
}

void RevisedMethodGPU::resetExpand(){

	// Reset deltaK
	deltaK = EXPAND_DELTA_0;

	kw->resetBounds();

	// reprocessing obj value
	kw->processOptimum(p);

	// check inf
	kw->checkInfeasibility(p);

	if(logger->isActive()){
		ostringstream oss (ostringstream::out);
		oss << p->nInf << " infeasability after resetExpand.";
		logger->log(Logger::LOG_INFO, oss.str());
	}
}

void RevisedMethodGPU::saveWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL)
		delete wStart;

	// Read the GPU problem state
	RevisedFormProblem *gpuProb = rp->clone();
	kw->getDeviceData(gpuProb);

	// Save the counter status, the GPU problem state and the pivoting index
	wStart = new SimplexWarmstart<RevisedFormProblem>(counter, gpuProb, ip->clone());

}

void RevisedMethodGPU::loadWarmstart(unsigned int &counter, IndexPivoting *ip){
	if(wStart != NULL){
		// Set the counter to the nb of iteration of the warmstart
		counter =  wStart->getCounter();

		// Remove the current pivoting index and replace it by the new one
		delete ip;
		ip = wStart->getIndexPivoting()->clone();

		// Load the old problem state
		kw->loadProblem(wStart->getProblem(), false);
		// TODO check the state of the current problem (p) bounds (they should be probably reinit by the BNB
		// PROBABLY THE SOLUTION : Reinitialize the bounds one by one... not neat but whatever
		for(unsigned int i=0; i<p->nVar; i++)
			this->changeBound(i, p->xBounds[i], ip);


	}
}
