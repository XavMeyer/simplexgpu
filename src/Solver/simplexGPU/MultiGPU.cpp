//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MultiGPU.cpp
 *
 *  Created on: Oct 31, 2010
 *      Author: meyerx
 */

#include "MultiGPU.h"

MultiGPU::MultiGPU() {
	mGPU.exSE = NULL;
	mGPU.exColumn = NULL;
	mGPU.exBVal = NULL;
	mGPU.exLocOpti = NULL;
	mGPU.exInfX = NULL;
	mGPU.exInfB = NULL;
	mGPU.exNTriv = NULL;
}

MultiGPU::MultiGPU(int nGPU, SimplexProblem *inP) {

	p = inP;

	mGPU.nbGPU = nGPU;
	mGPU.exSE = new red_result_t[nGPU];
	mGPU.exColumn = new VAR_TYPE[p->nRow];
	mGPU.exBVal = new VAR_TYPE[nGPU*p->nRow];
	mGPU.exLocOpti = new VAR_TYPE[nGPU];
	mGPU.exLocOptiAux = new VAR_TYPE[nGPU];
	mGPU.exInfX = new int[nGPU];
	mGPU.exInfB = new int[nGPU];
	mGPU.exNTriv = new int[nGPU];
	pthread_barrier_init(&mGPU.barrier, NULL, nGPU);

}

MultiGPU::~MultiGPU() {

	if(mGPU.exSE != NULL)
		delete[] mGPU.exSE;

	if(mGPU.exColumn != NULL)
		delete[] mGPU.exColumn;

	if(mGPU.exBVal != NULL)
		delete[] mGPU.exBVal;

	if(mGPU.exLocOpti != NULL)
		delete[] mGPU.exLocOpti;

	if(mGPU.exLocOptiAux != NULL)
		delete[] mGPU.exLocOptiAux;

	if(mGPU.exInfX != NULL)
		delete[] mGPU.exInfX;

	if(mGPU.exInfB != NULL)
		delete[] mGPU.exInfB;

	if(mGPU.exNTriv != NULL)
		delete[] mGPU.exNTriv;
}

void MultiGPU::solve(bool maximize, bool debug) {

	cpu_set_t 	*cpusetp;
	cpusetp = CPU_ALLOC(1024);

	if(!maximize)
		p->changeToMinimize();

	solvers = new SolverMulti[mGPU.nbGPU];
	for(int i=0; i<mGPU.nbGPU; i++){
		solvers[i] = SolverMulti(maximize, p, &mGPU);
		solvers[i].solve(i, debug);
		CPU_ZERO(cpusetp);
		CPU_SET(i+4, cpusetp);
		pthread_setaffinity_np(solvers[i].getPThread(), sizeof(cpu_set_t), cpusetp);
	}

	for(int i=0; i<mGPU.nbGPU; i++){
		pthread_join (solvers[i].getPThread(), NULL);
	}

	CPU_FREE(cpusetp);

	delete[] solvers;
}
