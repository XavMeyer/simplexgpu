//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * Solver.cpp
 *
 *  Created on: March 10, 2010
 *      Author: meyerx
 */

#include "SolverMono.h"
#include <float.h>
#include <stdio.h>


/*! Default constructor */
SolverMono::SolverMono(Logger *inL) {
	initVariables();
	setLogger(inL);
	kw = new KernelWrapperMono(inL);
}

/*! Constructor with the equations matrix */
SolverMono::SolverMono(Logger *inL, int device) {
	initVariables();
	setLogger(inL);
	kw = new KernelWrapperMono(inL, device);
}

/*! Constructor with the equations matrix */
SolverMono::SolverMono(Logger *inL, bool noInit) {
	initVariables();
	setLogger(inL);
	kw = new KernelWrapperMono(inL, noInit);
}

/* Destructor */
SolverMono::~SolverMono(){

	logger = NULL;
	p = NULL;

	delete kw;

	if(ip != NULL)
		delete ip;

	if(pStats != NULL)
		delete pStats;

	if(wStart != NULL)
		delete wStart;

}

/*!
 * \brief This function initialize the private variables
 *
 */
void SolverMono::initVariables() {
	mode = NORMAL;
	prevObjVal = 0;
	degenerateCount = 0;
	deltaK = EXPAND_DELTA_0;
	iterK = 0;
	counter = 0;
	counterTotal = 0;
	nTobound = 0;
	nPivot = 0;
	//debug = false;
	p = NULL;
	ip = NULL;
	wStart = NULL;
	pStats = NULL;

#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif
}


void SolverMono::setProblem(SimplexProblem *inP){

	if(p == NULL) { // no problem in memory
		p = inP;
		kw->initProblemStructure(p);
	} else {
		p = inP;
		kw->deInitProblemStructure();
		kw->initProblemStructure(p);
		delete ip;
	}

	ip = new IndexPivoting(p->nVar, p->nRow);

	// Copy the data
	kw->loadProblem(p);

	if(p->optiType == SimplexProblem::MAX)
		sig = 1.0;
	else
		sig = -1.0;

	if(pStats == NULL)
		pStats = new SimplexProblemStats();
	else
		pStats->init();

}

void SolverMono::reloadProblem(){
	// Copy the data
	kw->loadProblem(p);
	ip->initVal();
}


void SolverMono::getProblem(SimplexProblem *inP){
	// Copy GPU data in CPU problem
	kw->getDeviceData(p);
	// return pointer to CPU problem
	inP = p;
}

void SolverMono::getSolution(SimplexSolution *inSol){
	kw->getSolution(p, inSol);
	kw->processOptimumWS(p);
	inSol->objVal = sig * p->objVal;
}

void SolverMono::changeBound(int index, float2 bound){
	p->xBounds[index] = bound;
	kw->changeBoundWK(p, ip, index, bound);
	//kw->changeBoundWS(p, index, bound);
}

int SolverMono::getCounter(){
	return iterCnt;
}

SimplexProblemStats * SolverMono::getStats(){
	return pStats;
}

void SolverMono::saveWarmstart(){
	if(wStart != NULL)
		delete wStart;

	// Read the GPU problem state
	SimplexProblem *gpuProb = p->clone();
	kw->getDeviceData(gpuProb);

	// Save the counter status, the GPU problem state and the pivoting index
	wStart = new SimplexWarmstart<SimplexProblem>(counter, gpuProb, ip->clone());

	cout << "Saving warmstart : " << endl;
	cout << wStart->toString().c_str() << endl;
}

void SolverMono::loadWarmstart(){
	if(wStart != NULL){
		// Set the counter to the nb of iteration of the warmstart
		counter =  wStart->getCounter();

		// Remove the current pivoting index and replace it by the new one
		delete ip;
		ip = wStart->getIndexPivoting()->clone();

		// Load the old problem state
		kw->loadProblem(wStart->getProblem());
		// TODO check the state of the current problem (p) bounds (they should be probably reinit by the BNB
		// PROBABLY THE SOLUTION : Reinitialize the bounds one by one... not neat but whatever
		for(int i=0; i<p->nVar; i++)
			this->changeBound(i, p->xBounds[i]);


	}
}

void SolverMono::enableWarmstart(){
	if(wStart != NULL) wStart->enableWarmstart();
}
void SolverMono::disableWarmstart(){
	if(wStart != NULL) wStart->disableWarmstart();
}



/*!
 * This function launch the simplex algorithm
 */
solver_res_t SolverMono::solve(unsigned int inMaxIter) {
	int r = 0;
	int lastCnt;
	solver_res_t result = SUCCESS;

	// Init expand reset counter
	iterK = 0;
	iterLimit = inMaxIter;
	iterCnt = 0;

	// Check if the problem is infeasible
	_START_EVENT(prof,"INITIAL_INF_CHECK")
	kw->checkInfeasWS(p);
	_END_EVENT(prof,"INITIAL_INF_CHECK")

	while(r < EXPAND_T_RESET){

		lastCnt = counter;

		if(p->mode == p->AUX){
			_START_EVENT(prof,"SOLVE_AUXILIARY")
			result = SolveAuxiliary();
			_END_EVENT(prof,"SOLVE_AUXILIARY")
		}

		if(result == SUCCESS){
			_START_EVENT(prof, "SOLVE_SIMPLEX")
			result = SolveSimplex();
			_END_EVENT(prof, "SOLVE_SIMPLEX")
		}

		if(lastCnt == counter)
			break;

		if(result != GOTO_PHASE1){
			_START_EVENT(prof,"RESET")
			resetExpand();
			if(counter > NB_ITER_REINIT){//100000){
				_START_EVENT(prof, "PROBLEM_RESET")
				pStats->nbReset++;
				if(wStart != NULL && wStart->isEnabled()){
					loadWarmstart();
					cout << "Starting from warmStart" << endl;
				} else {
					kw->reinitPWS(p);
					ip->initVal();
				}
				counterTotal += counter;
				counter = 0;
				logger->log(Logger::LOG_DEBUG, "Pb reinit!");
				_END_EVENT(prof, "PROBLEM_RESET")
			}

			r++;
			_END_EVENT(prof,"RESET")
		}
	}

	//reinitPWS(p);
	kw->processOptimumWS(p);

	pStats->addRelaxation(iterCnt, result == SUCCESS);

	if(logger->isActive()){
		if(result == SUCCESS){
			ostringstream oss(ostringstream::out);
			oss << "Success - result : " << fixed << sig*p->objVal;
			logger->log(Logger::LOG_NOTHING, oss.str());
		} else if(result == UNBOUNDED_AUX){
			logger->log(Logger::LOG_NOTHING, "Aux infeasible : Unbounded problem.");
		} else if(result == UNBOUNDED){
			logger->log(Logger::LOG_NOTHING, "Problem is not bounded!");
		} else if(result == NO_SOLUTION){
			logger->log(Logger::LOG_NOTHING, "Dual infeasible : No initial dictionary found!");
		} else if(result == INFEASIBLE){
			logger->log(Logger::LOG_NOTHING, "Problem is infeasible!");
		} else if(result == MAX_ITER_REACHED){
			ostringstream oss(ostringstream::out);
			oss << "Max iteration " << MAX_ITER << " reached without founding an optimum." << endl;
			logger->log(Logger::LOG_NOTHING, oss.str());
		}
	}

	//kw->reinitPWS(p);
	//ip->initVal();
	//kw->getDeviceData(p);
	//kw->deInitCuda(true);
	//deInitTracker();

	return result;
}


solver_res_t SolverMono::solve(){
	return solve(UINT_MAX);
}


/*!
 * This function solve the simplex problem
 * from a feasible dictionary.
 */
solver_res_t SolverMono::SolveSimplex() {

	red_result_t pColResult;
	row_result_t expandRowRes;

	if(logger->isActive())
		logger->log(Logger::LOG_INFO, "Phase 2 : std Simplex.");

	// While max_iter not reached
	while((counter < MAX_ITER) && (iterCnt < iterLimit)) {

		_START_EVENT(prof,"FIND_ENTERING")
		// Get the pivot column
		#if COL_CHOICE_METHOD == STEEPEST_EDGE
			pColResult = kw->seFindCol(p);//seFindCol2(p);
		#elif COL_CHOICE_METHOD == NESTED_STD
			pColResult = nestedFindCol(p);
		#else
			pColResult = stdFindCol(p);
		#endif
		_END_EVENT(prof,"FIND_ENTERING")

		// If there is a potential column
		if(pColResult.index >= 0) {

			_START_EVENT(prof,"FIND_LEAVING")
			// Get the pivot row
			expandRowRes = kw->expandFindRow(pColResult.index, pColResult.value, deltaK, EXPAND_TAU, p);
			deltaK += EXPAND_TAU;
			_END_EVENT(prof,"FIND_LEAVING")

			_START_EVENT(prof,"PIVOTING")
			if(expandRowRes.index == -1){ // No row => infeasible
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "(Infeasible) Stopped at iteration " << counter << ".";
					logger->log(Logger::LOG_DEBUG, oss.str());
				}
				_END_EVENT(prof, "PIVOTING")
				return INFEASIBLE;
			} else if(expandRowRes.mode == M_UNBOUNDED){ // Unbounded
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "(Unbounded) Stopped at iteration " << counter << ".";
					logger->log(Logger::LOG_DEBUG, oss.str());
				}
				_END_EVENT(prof, "PIVOTING")
				return UNBOUNDED;
			} else if(expandRowRes.mode == M_BASIC_TO_BOUND){ // Bounded by non-basic
				kw->nonBasicToBound(pColResult.index, expandRowRes.alpha, expandRowRes.absP, p);
				nTobound++;
			} else if(expandRowRes.mode == M_PIVOT) { // Pivot
				kw->updatingBasisWS(expandRowRes.index, pColResult.index, expandRowRes.alpha, p);
				kw->pivotingWS(expandRowRes.index, pColResult.index, p);
				ip->update(expandRowRes.index, pColResult.index);
				nPivot++;

			} else {
				if(logger->isActive()){
					logger->log(Logger::LOG_ERROR, "Unknown operation (simplex std phase).");
				}
			}
			_END_EVENT(prof, "PIVOTING")

			// Debug msg
//			if(debug){
//				kw->processOptimumWS(p);
//				kw->getDeviceData(p);
//				printDebug(expandRowRes, pColResult);
//			}

			// optimum state and counter
			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){
				//trackDensity(counter);
				kw->processOptimumWS(p);
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "Iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
					logger->log(Logger::LOG_INFO, oss.str());
				}
			}
			counter++;
			iterCnt++;

			// expand state, does we need a reset ?
			if(iterK == EXPAND_K) {
				iterK = 0;
				resetExpand();
				if(p->nInf > 0)
					return GOTO_PHASE1;
			} else {
				iterK++;
			}

		} else {
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "(Sucess) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
				logger->log(Logger::LOG_INFO, oss.str());
			}
			return SUCCESS;
		}
	}

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "(Max iter) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
		logger->log(Logger::LOG_INFO, oss.str());
	}

	return MAX_ITER_REACHED;
}


/*!
 * \brief This function solve the Auxiliary problem
 */
solver_res_t SolverMono::SolveAuxiliary() {
	red_result_t pColResult;
	row_result_t expandRowRes;

	if(logger->isActive())
		logger->log(Logger::LOG_INFO, "Phase 1 : Auxiliary problem.");

//	if(debug){
//		kw->getDeviceData(p);
//		printDebug();
//	}

	while((counter < MAX_ITER) && (iterCnt < iterLimit)){ // while MAX_ITER not reached

		_START_EVENT(prof, "FIND_ENTERING")
		// Get the pivot column
		#if COL_CHOICE_METHOD == STEEPEST_EDGE
			pColResult = kw->seFindColP1(p);//seFindCol2P1(p);
		#elif COL_CHOICE_METHOD == NESTED_STD
			pColResult = nestedFindColP1(p);
		#else
			pColResult = stdFindCol(p);
		#endif
		_END_EVENT(prof, "FIND_ENTERING")

		if(pColResult.index >= 0) {
			// Get the pivot row
			_START_EVENT(prof, "FIND_LEAVING")
			expandRowRes = kw->expandFindRowP1(pColResult.index, pColResult.value, deltaK, EXPAND_TAU, p);
			deltaK += EXPAND_TAU;
			_END_EVENT(prof, "FIND_LEAVING")

			_START_EVENT(prof, "PIVOTING")
			if(expandRowRes.index == -1){
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "(Infeasible aux) Stopped at iteration " << counter << ".";
					logger->log(Logger::LOG_DEBUG, oss.str());
				}
				_END_EVENT(prof, "PIVOTING")
				return INFEASIBLE;
			} else if(expandRowRes.mode == M_UNBOUNDED){
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "(Unbounded aux) Stopped at iteration " << counter << ".";
					logger->log(Logger::LOG_DEBUG, oss.str());
				}
				_END_EVENT(prof, "PIVOTING")
				return UNBOUNDED_AUX;
			} else if(expandRowRes.mode == M_BASIC_TO_BOUND){
				kw->nonBasicToBound(pColResult.index, expandRowRes.alpha, expandRowRes.absP, p);
				nTobound++;
			} else if(expandRowRes.mode == M_PIVOT) {
				kw->updatingBasisWS(expandRowRes.index, pColResult.index, expandRowRes.alpha, p);
				kw->pivotingWS(expandRowRes.index, pColResult.index, p);
				ip->update(expandRowRes.index, pColResult.index);
				nPivot++;
			} else if(expandRowRes.mode == M_INF_TO_BOUND) {
				kw->nonBasicToBound(pColResult.index, expandRowRes.alpha, expandRowRes.absP, p);
				nTobound++;
			} else if(expandRowRes.mode == M_INF_PIVOT) {
				kw->updatingBasisWS(expandRowRes.index, pColResult.index, expandRowRes.alpha, p);
				kw->pivotingWS(expandRowRes.index, pColResult.index, p);
				ip->update(expandRowRes.index, pColResult.index);
				nPivot++;
			} else {
				if(logger->isActive()){
					logger->log(Logger::LOG_ERROR, "Unknown operation (simplex auxiliary phase).");
				}
			}
			_END_EVENT(prof, "PIVOTING")

			// Debug msg
//			if(debug){
//				kw->processOptimumWS(p);
//				kw->getDeviceData(p);
//				printDebug(expandRowRes, pColResult);
//			}


			_START_EVENT(prof,"UPDATE_INF")
			if(iterK == EXPAND_K) {
				iterK = 0;
				resetExpand();
			} else {
				iterK++;
				kw->updateInfeasWS(p);
			}
			_END_EVENT(prof, "UPDATE_INF")

			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){
				//trackDensity(counter);
				kw->processOptimumWS(p);
				if(logger->isActive()){
					ostringstream oss(ostringstream::out);
					oss << "Iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << "(Inf. var : " << p->nInf << ").";
					logger->log(Logger::LOG_INFO, oss.str());
				}
			}
			counter++;
			iterCnt++;

			if(p->nInf == 0)
				return SUCCESS;

		} else {
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
				logger->log(Logger::LOG_INFO, oss.str());
			}
			if(p->nInf > 0) { // we have not found the optiumum : 0
				return NO_SOLUTION;
			} else { // we have found the optimum : 0 and so we create the new starting dictionary
				return SUCCESS;
			}
		}
	}

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "(Max iter) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
		logger->log(Logger::LOG_INFO, oss.str());
	}


	return MAX_ITER_REACHED;

}


// Reset expand value and bounds
void SolverMono::resetExpand() {

	// Reset deltaK
	deltaK = EXPAND_DELTA_0;

	kw->resetBoundsWS(p);

	// reprocessing obj value
	kw->processOptimumWS(p);

	// check inf
	kw->checkInfeasWS(p);

	if(logger->isActive()){
		ostringstream oss (ostringstream::out);
		oss << p->nInf << " infeasability after resetExpand.";
		logger->log(Logger::LOG_INFO, oss.str());
	}
}


void SolverMono::printDebug(){
	printf("*\n");
	kw->processOptimumWS(p);
	printf("obj : %e (%d : %e)\n", sig*p->objVal, p->nInf, p->objAuxVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->printEquations();
		p->printObjFunction();
		p->printObjFunctionAux();
		p->printX();
	}

	//getchar();
}


void SolverMono::printDebug(red_result_t pRow, red_result_t pCol){
	printf("*\n");
	printf("pCol idx : %d - val : %e \n", pCol.index, pCol.value);
	printf("pRow idx : %d - val : %e \n", pRow.index, pRow.value);
	//printf("P value : %e - ", p->eqs[IDX2C(pRow.index, pCol.index, p->nRow)]);
	kw->processOptimumWS(p);
	printf("obj : %e \n", sig*p->objVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->printEquations();
		p->printObjFunction();
		p->printObjFunctionAux();
		p->printX();
	}

	//getchar();
}

void SolverMono::printDebug(row_result_t pRow, red_result_t pCol){
	string s;
	switch(pRow.mode){
	case M_PIVOT : s = "Pivot"; break;
	case M_INF_PIVOT : s = "Infeasible Pivot (p1)"; break;
	case M_UNBOUNDED : s = "Unbounded"; break;
	case M_BASIC_TO_BOUND : s = "Non-basic to bound"; break;
	case M_INF_TO_BOUND	: s = "Infeasible to bound (p1)"; break;
	case M_INFEASIBLE	: s = "Infeasible"; break;
	default : s = "Unknown move";
	}

	printf("*** %s \n", s.c_str());
	printf("pCol idx : %d - val : %e \n", pCol.index, pCol.value);
	printf("pRow idx : %d - pivot : %e - alpha : %e \n", pRow.index, pRow.absP, pRow.alpha);
	//printf("P value : %e - ", p->eqs[IDX2C(pRow.index, pCol.index, p->nRow)]);
	kw->processOptimumWS(p);
	printf("obj : %e (%d : %e)\n", sig*p->objVal, p->nInf, p->objAuxVal);
	if(p->nCol < 20 && p->nRow < 20){
		printf("Matrix : \n");
		p->printEquations();
		p->printObjFunction();
		p->printObjFunctionAux();
		p->printX();
	}

	//getchar();

}

void SolverMono::initTracker(){
	fichier = fopen("density.csv", "w");
	fprintf(fichier, "Iterations, density \n");

}

/*void SolverMono::trackDensity(int it){
	float density = 0.0;

	kw->getDeviceData(p);

	for(int i=0; i<p->nRow*p->nCol; i++){
		if(p->eqs[i] > EPS1){
			density++;
		}
	}

	density /= p->nRow*p->nCol;

	fprintf(fichier, "%d, %f \n", it, density);
}*/


void SolverMono::deInitTracker(){
	fclose(fichier);

}
