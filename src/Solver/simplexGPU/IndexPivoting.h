//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * IndexPivoting.h
 *
 *  Created on: Jun 15, 2011
 *      Author: meyerx
 */

#ifndef INDEXPIVOTING_H_
#define INDEXPIVOTING_H_

#include <string.h>

class IndexPivoting {
public:

	int nVar, nRow;

	int *basis;
	int *xInd;
	int *xLoc;

	IndexPivoting(int inNVar, int inNRow);
	virtual ~IndexPivoting();

	IndexPivoting * clone();

	// Internal basis, variable index management
	void initVal();
	void update(int row, int col);
	void updateForFullA(int row, int col);

};

#endif /* INDEXPIVOTING_H_ */
