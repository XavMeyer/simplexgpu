//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * Solver.h
 *
 *  Created on: March 10, 2010
 *      Author: meyerx
 */
#ifndef SOLVERMULTI_H_
#define SOLVERMULTI_H_

#include "../Types.h"
#include "../Config.h"
#include "../SimplexProblem.h"
#include "../simplex/Solver.h"
#include "LocalSProblem.h"
#include "cuda/KernelWrapperMulti.h"

#include <pthread.h>

/*!
 * \class Solver
 * \brief Solve a simplex problem given as an array
 */
class SolverMulti {
public:


	SolverMulti(); //!< constructor

	/*!
	 * \brief Constructor with the equation matrix
	 * \param inNRow : the number of rows
	 * \param inNCol : the number of columns
	 * \param inEquations : a pointer to the array representing the equations
	 */
	SolverMulti(bool maximize, SimplexProblem *inP,  multiGPU_t *inMGPU);

	virtual ~SolverMulti(); 	//!< destructor


	/*!
	 * \brief Solve the given equation matrix.
	 * \param debug : Mode debug on/off (print the stages)
	 */
	void 		solve(int device, bool debug);

	pthread_t 	getPThread();

protected:

	static void * run(void * arg);
	solver_res_t execute();

private:


	enum mode_t {NORMAL, SMALLEST_SUBSCRIPT};
	enum mode_t mode;

	// Variables
	SimplexProblem *p;
	LocalSProblem lP;
	bool RHS;

	KernelWrapperMulti kw;

	multiGPU_t	*mGPU;

	VAR_TYPE	prevObjVal;
	VAR_TYPE	sig;

	bool		debug;

	//Expand
	int			iterK;
	double 		deltaK;
	int			counter;
	int			nTobound, nPivot;
	int			device;

	pthread_t	m_thread;

	// Methods

	// Initialization methods
	void 			initVariables(void);
	void 			initLocalProb(void);

	// Solver methods
	solver_res_t		SolveSimplex();
	solver_res_t		SolveAuxiliary();

	// Expand
	void 			resetExpand();

	//Multi GPU function
	red_result_t	findCol(int &colDev);
	red_result_t 	findColP1(int &colDev);
	row_result_t 	findRow(red_result_t pColResult, int colDev);
	row_result_t 	findRowP1(red_result_t pColResult, int colDev);
	void 			pivoting(red_result_t pColResult, row_result_t pRowResult, int colDev);
	void 			toBound(red_result_t pColResult, row_result_t pRowResult, int colDev);
	void 			processOptimum();
	void 			checkInfeasibility();
	void 			updateInfeasibility();
	void 			resetVal();

	// Debug functions
	void 			printDebug();
	void 			printDebug(red_result_t pRow, red_result_t pCol);
	void 			printDebug(row_result_t pRow, red_result_t pCol);
	void 			printDebug(row_result_t pRow, red_result_t pCol, int colDev);

};
#endif /* SOLVER_H_ */
