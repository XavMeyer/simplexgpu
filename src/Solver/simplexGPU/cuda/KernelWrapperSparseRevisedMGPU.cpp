//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file KernelWrapperRevised.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "KernelWrapperSparseRevisedMGPU.h"

#if CUSPARSE_SUPPORTED

KernelWrapperSparseRevisedMGPU::KernelWrapperSparseRevisedMGPU(Logger *inL) {

	logger = inL;
	device = 0;
	init();
	initCuda();
}

KernelWrapperSparseRevisedMGPU::KernelWrapperSparseRevisedMGPU(Logger *inL, int inDevice) {
	logger = inL;
	device = inDevice;
	init();
	initCuda();
}

KernelWrapperSparseRevisedMGPU::KernelWrapperSparseRevisedMGPU(Logger *inL, bool noInit) {
	logger = inL;
	if(!noInit){
		device = 0;
		init();
		initCuda();
	}
}


KernelWrapperSparseRevisedMGPU::~KernelWrapperSparseRevisedMGPU() {
	deInitProblemStructure();
	deInitCuda();
}



void KernelWrapperSparseRevisedMGPU::init(){
	devexInit = NULL;
#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif
}

/*!
 * Call the init functions
 */
void KernelWrapperSparseRevisedMGPU::initCuda(){

	// TODO Not safe, device properties should be checked (for p2p)

	// Init device
	cutilSafeCallNoSync(cudaGetDeviceProperties(&devProp, device));
	cutilSafeCallNoSync(cudaSetDevice(device));

	if(cublasInit() != CUBLAS_STATUS_SUCCESS){
		logger->log(Logger::LOG_ERROR, "Cublas error at init.");
		throw 11;
	}
	checkOp( cusparseCreate(&handleCUSP));
	cutilSafeCallNoSync(cudaDeviceEnablePeerAccess(device+1, 0));

	// Init device+1
	cutilSafeCallNoSync(cudaSetDevice(device+1));
	if(cublasInit() != CUBLAS_STATUS_SUCCESS){
		logger->log(Logger::LOG_ERROR, "Cublas error at init.");
		throw 11;
	}
	cutilSafeCallNoSync(cudaDeviceEnablePeerAccess(device, 0));
	cudaStreamCreate(&stream);

	// Go back to device
	cutilSafeCallNoSync(cudaSetDevice(device));

}

void KernelWrapperSparseRevisedMGPU::deInitCuda(){

	// Deinit device + 1
	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync(cudaDeviceDisablePeerAccess(device+1));
	cudaStreamDestroy(stream);
	cublasShutdown();
	cudaThreadExit();

	// Deinit device
	cutilSafeCallNoSync(cudaSetDevice(device));
	cutilSafeCallNoSync(cudaDeviceDisablePeerAccess(device));
	checkOp( cusparseDestroy(handleCUSP));
	cublasShutdown();
	cudaThreadExit();
}


void KernelWrapperSparseRevisedMGPU::initProblemStructure(SparseRevisedFormProblem *inP) {

	// Create the revised form problem that will be on GPU
	pGPU = new SparseRevisedFormProblem(inP->nRow, inP->nVar, inP->nSlack, inP->nNoNull, false);

	// Create the structure in GPU memory

	// Matrix
	// A (Col wise)
	checkOp( cusparseCreateMatDescr(&descrA));
	checkOp( cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL));
	checkOp( cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->sparseA, inP->nNoNull*sizeof(*pGPU->sparseA)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->rowIndA, inP->nNoNull*sizeof(*pGPU->rowIndA)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->colIndA, inP->nNoNull*sizeof(*pGPU->colIndA)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_colPtrA, (inP->nCol+1)*sizeof(*d_colPtrA)));
	h_colPtrA = new int[inP->nCol+1];


	// A (Row wise)
	/*checkOp( cusparseCreateMatDescr(&descrAT));
	checkOp( cusparseSetMatType(descrAT, CUSPARSE_MATRIX_TYPE_GENERAL));
	checkOp( cusparseSetMatIndexBase(descrAT, CUSPARSE_INDEX_BASE_ZERO));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_sparseAT, inP->nNoNull*sizeof(*d_sparseAT)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_rowPtrAT, (1+inP->nRow)*sizeof(*d_rowPtrAT)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_colIndAT, inP->nNoNull*sizeof(*d_colIndAT)));*/


	// B
#if B_STORAGE == B_NORMAL
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->B, &pitch_B, pGPU->nBasic*sizeof(*pGPU->B), pGPU->nBasic));
#elif B_STORAGE == B_NORMAL_AND_TRANS

#if B_PADDING == B_PAD_16
	nBasicPad = 16*ceil((float)pGPU->nBasic/16);
#elif B_PADDING == B_PAD_32
	nBasicPad = 32*ceil((float)pGPU->nBasic/32);
#endif
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->B, &pitch_B, nBasicPad*sizeof(*pGPU->B), nBasicPad));
#endif

	//Vectors
	// lhs
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->lhs, inP->nRow*sizeof(*pGPU->lhs)));

	// Basic variable bounds
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bBounds, inP->nBasic*sizeof(*pGPU->bBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1)));

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bounds, inP->nCol*sizeof(*pGPU->bounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->boundsP1, inP->nCol*sizeof(*pGPU->boundsP1)));

	// Variable values
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->val, inP->nCol*sizeof(*pGPU->val)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bVal, inP->nBasic*sizeof(*pGPU->bVal)));

	// Variable index
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bInd, inP->nBasic*sizeof(*pGPU->bInd)));

	// Objective function
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->Cb, inP->nBasic*sizeof(*pGPU->Cb)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->CbP1, inP->nBasic*sizeof(*pGPU->CbP1)));

	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->C, inP->nCol*sizeof(*pGPU->C)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->CP1, inP->nCol*sizeof(*pGPU->CP1)));

	this->initMethodStructure();
	this->initResult();
	//this->initSwap();
#if DBG_PRINT
	this->initDbg(pGPU);
#endif

}

void KernelWrapperSparseRevisedMGPU::deInitProblemStructure(){

	// Matrix
	// A (Col wise)
	checkOp( cusparseDestroyMatDescr(descrA));
	cutilSafeCallNoSync( cudaFree(pGPU->sparseA));
	pGPU->sparseA = NULL;
	cutilSafeCallNoSync( cudaFree(pGPU->rowIndA));
	cutilSafeCallNoSync( cudaFree(pGPU->colIndA));
	cutilSafeCallNoSync( cudaFree(d_colPtrA));
	delete [] h_colPtrA;

	// A (Row wise)
/*	checkOp( cusparseDestroyMatDescr(descrAT));
	cutilSafeCallNoSync( cudaFree(d_sparseAT));
	cutilSafeCallNoSync( cudaFree(d_rowPtrAT));
	cutilSafeCallNoSync( cudaFree(d_colIndAT));*/

	// B
	cutilSafeCallNoSync(cudaFree(pGPU->B));

	// Vectors
	cutilSafeCallNoSync(cudaFree(pGPU->lhs));

	cutilSafeCallNoSync(cudaFree(pGPU->bBounds));
	cutilSafeCallNoSync(cudaFree(pGPU->bBoundsP1));

	cutilSafeCallNoSync(cudaFree(pGPU->bounds));
	cutilSafeCallNoSync(cudaFree(pGPU->boundsP1));

	cutilSafeCallNoSync(cudaFree(pGPU->bVal));
	cutilSafeCallNoSync(cudaFree(pGPU->val));

	cutilSafeCallNoSync(cudaFree(pGPU->bInd));

	cutilSafeCallNoSync(cudaFree(pGPU->Cb));
	cutilSafeCallNoSync(cudaFree(pGPU->CbP1));

	cutilSafeCallNoSync(cudaFree(pGPU->C));
	cutilSafeCallNoSync(cudaFree(pGPU->CP1));


	this->deInitMethodStructure();
	this->deInitResult();
	//this->deInitSwap();
#if DBG_PRINT
	this->deInitDbg();
#endif

	if(devexInit != NULL)
		delete [] devexInit;

}

void KernelWrapperSparseRevisedMGPU::initMethodStructure(){

	// Tmp vectors
	//cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, pGPU->nRow*sizeof(*d_tmpCol)));
	//cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, pGPU->nBasic*sizeof(*d_tmpBasic)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpRow, pGPU->nCol*sizeof(*d_tmpRow)));

	// Reduced cost vector
	cutilSafeCallNoSync( cudaMalloc((void**) &d_redCost, pGPU->nCol*sizeof(*d_redCost)));

	// Steepest edge coefficient
	cutilSafeCallNoSync( cudaMalloc((void**) &d_seCoeff, pGPU->nCol*sizeof(*d_seCoeff)));
	// Steepest edge vector alpha and beta
	cutilSafeCallNoSync( cudaMalloc((void**) &d_alpha, pGPU->nCol*sizeof(*d_alpha)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_beta, pGPU->nCol*sizeof(*d_beta)));

#if B_STORAGE == B_NORMAL_AND_TRANS
	// Entering variable column
	cutilSafeCallNoSync( cudaMalloc((void**) &d_enterCol, nBasicPad*sizeof(*d_enterCol)));
	// Tmp vectors
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, nBasicPad*sizeof(*d_tmpCol)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, nBasicPad*sizeof(*d_tmpBasic)));

	// BTrans
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_BTrans, &pitch_B, nBasicPad*sizeof(*d_BTrans), nBasicPad));
	#elif B_STORAGE == B_NORMAL
	// Entering variable column
	cutilSafeCallNoSync( cudaMalloc((void**) &d_enterCol, pGPU->nRow*sizeof(*d_enterCol)));
	// Tmp vectors
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, pGPU->nRow*sizeof(*d_tmpCol)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, pGPU->nBasic*sizeof(*d_tmpBasic)));
#endif

	// Vector 0
	VAR_TYPE zeros[pGPU->nBasic];
	// Init 0 vector and replace basis row l by 0 vector
	for(unsigned int i=0; i<pGPU->nBasic; i++){
		zeros[i] = 0.0;
	}
	cutilSafeCallNoSync(cudaMalloc((void **)&d_z, pGPU->nBasic*sizeof(VAR_TYPE)));
	cutilSafeCallNoSync(cudaMemcpy( d_z, zeros, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyHostToDevice));

	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d2_B, &pitch_B, pGPU->nBasic*sizeof(*pGPU->B), pGPU->nBasic));
	cutilSafeCallNoSync( cudaMalloc((void**) &d2_R, pGPU->nBasic*sizeof(*d2_R)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d2_D, pGPU->nBasic*sizeof(*d2_D)));
	cutilSafeCallNoSync(cudaSetDevice(device));

}

void KernelWrapperSparseRevisedMGPU::deInitMethodStructure(){

	cutilSafeCallNoSync(cudaFree(d_tmpCol));
	cutilSafeCallNoSync(cudaFree(d_tmpBasic));
	cutilSafeCallNoSync(cudaFree(d_tmpRow));

	cutilSafeCallNoSync(cudaFree(d_redCost));

	cutilSafeCallNoSync(cudaFree(d_enterCol));

	cutilSafeCallNoSync(cudaFree(d_seCoeff));
	cutilSafeCallNoSync(cudaFree(d_alpha));
	cutilSafeCallNoSync(cudaFree(d_beta));

	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync(cudaFree(d2_B));
	cutilSafeCallNoSync(cudaFree(d2_R));
	cutilSafeCallNoSync(cudaFree(d2_D));
	cutilSafeCallNoSync(cudaSetDevice(device));


#if B_STORAGE == B_NORMAL_AND_TRANS
	// BTrans
	cutilSafeCallNoSync(cudaFree(d_BTrans));
#endif
}

/*!
 * \brief Initializing swap
 */
void KernelWrapperSparseRevisedMGPU::initSwap(){

	cutilSafeCallNoSync( cudaMalloc((void**) &swpInt, 1*sizeof(int)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt, 1*sizeof(float)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt2, 1*sizeof(float2)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpDbl, 1*sizeof(double)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpTyp, 1*sizeof(VAR_TYPE)) );
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperSparseRevisedMGPU::deInitSwap(){

	cutilSafeCallNoSync(cudaFree(swpInt));
	cutilSafeCallNoSync(cudaFree(swpFlt));
	cutilSafeCallNoSync(cudaFree(swpFlt2));
	cutilSafeCallNoSync(cudaFree(swpDbl));
	cutilSafeCallNoSync(cudaFree(swpTyp));

}

/*!
 * Define the number of blocks and threads for CUDA
 * Create the result array on CPU and Device
 */
void KernelWrapperSparseRevisedMGPU::initResult(){
#if MAPPED_RESULT && PINNED
	int flags = cudaHostAllocMapped;
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), flags));
	cutilSafeCallNoSync(cudaHostGetDevicePointer((void **)&dRes, (void *)hRes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), flags));
	cutilSafeCallNoSync(cudaHostGetDevicePointer((void **)&dERes, (void *)hERes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), flags));
	cutilSafeCallNoSync(cudaHostGetDevicePointer((void **)&dIRes, (void *)hIRes, 0));
#elif PINNED
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#else
	hRes = (red_result_t*) malloc(MAX_BLOCKS*sizeof(red_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	hERes = (exp_result_t*) malloc(MAX_BLOCKS*sizeof(exp_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	hIRes = (int*) malloc(MAX_BLOCKS*sizeof(int));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#endif
}

void KernelWrapperSparseRevisedMGPU::deInitResult(){
#if MAPPED_RESULT || PINNED
	cutilSafeCallNoSync(cudaFreeHost(hRes));
	cutilSafeCallNoSync(cudaFreeHost(hERes));
	cutilSafeCallNoSync(cudaFreeHost(hIRes));
#else
	free(hRes);
	free(hERes);
	free(hIRes)
#endif
	cutilSafeCallNoSync(cudaFree(dRes));
	cutilSafeCallNoSync(cudaFree(dERes));
	cutilSafeCallNoSync(cudaFree(dIRes));
}




void KernelWrapperSparseRevisedMGPU::loadProblem(SparseRevisedFormProblem *inP, bool isBasisID) {

	// Matrix
	// A
	cutilSafeCallNoSync( cudaMemcpy(pGPU->sparseA, inP->sparseA, inP->nNoNull*sizeof(*pGPU->sparseA), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->rowIndA, inP->rowIndA, inP->nNoNull*sizeof(*pGPU->rowIndA), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->colIndA, inP->colIndA, inP->nNoNull*sizeof(*pGPU->colIndA), cudaMemcpyHostToDevice) );

	// Process CSC A
	checkOp( cusparseXcoo2csr(handleCUSP, pGPU->colIndA, pGPU->nNoNull, pGPU->nCol, d_colPtrA, CUSPARSE_INDEX_BASE_ZERO));
	cutilSafeCallNoSync( cudaMemcpy(h_colPtrA, d_colPtrA, (pGPU->nCol+1)*sizeof(*d_colPtrA), cudaMemcpyDeviceToHost));

	// Process CSR A
	/*checkOp(cusparseDcsr2csc(handleCUSP, pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
							 pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
							 d_sparseAT, d_colIndAT, d_rowPtrAT,
							 CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO));*/


	// B
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->B, pitch_B, inP->B, inP->nRow*sizeof(*pGPU->B), inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyHostToDevice) );

	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync( cudaMemcpy2DAsync(d2_B, pitch_B, inP->B, inP->nRow*sizeof(*pGPU->B), inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyHostToDevice, stream));
	cutilSafeCallNoSync(cudaSetDevice(device));


#if B_STORAGE == B_NORMAL_AND_TRANS
	for(unsigned int i=0; i<pGPU->nBasic; i++){
		cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(0, i, P2S(pitch_B))], 1, &d_BTrans[IDX2C(i, 0, P2S(pitch_B))], P2S(pitch_B));
	}
#endif

	// Vectors
	// lhs
	cutilSafeCallNoSync( cudaMemcpy(pGPU->lhs, inP->lhs, inP->nRow*sizeof(*pGPU->lhs), cudaMemcpyHostToDevice) );
	// Basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBounds, inP->bBounds, inP->nBasic*sizeof(*pGPU->bBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBoundsP1, inP->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1), cudaMemcpyHostToDevice) );

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bounds, inP->bounds, inP->nCol*sizeof(*pGPU->bounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->boundsP1, inP->boundsP1, inP->nCol*sizeof(*pGPU->boundsP1), cudaMemcpyHostToDevice) );

	// Variable values
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bVal, inP->bVal, inP->nBasic*sizeof(*pGPU->bVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->val, inP->val, inP->nCol*sizeof(*pGPU->val), cudaMemcpyHostToDevice) );

	// Variable index
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bInd, inP->bInd, inP->nBasic*sizeof(*pGPU->bInd), cudaMemcpyHostToDevice) );

	// Objective function
	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cb, inP->Cb, inP->nBasic*sizeof(*pGPU->Cb), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CbP1, inP->CbP1, inP->nBasic*sizeof(*pGPU->CbP1), cudaMemcpyHostToDevice) );

	cutilSafeCallNoSync( cudaMemcpy(pGPU->C, inP->C, inP->nCol*sizeof(*pGPU->C), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CP1, inP->CP1, inP->nCol*sizeof(*pGPU->CP1), cudaMemcpyHostToDevice) );

	// If the basis is set the Identity matrix, we just have to copy the matrix An
	// and so Eqs are up to date, else they are not and they will be processed when needed
	eqsUpToDate = isBasisID;

	///////////////////////////////////////////////////////////////////////////////////////////
	VAR_TYPE row[pGPU->nCol], C[pGPU->nCol];
	cutilSafeCallNoSync(cudaMemcpy(row, d_tmpRow, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	cutilSafeCallNoSync(cudaMemcpy(C, pGPU->C, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));


	for(unsigned int i=0; i<pGPU->nCol; i++){
		if(isnan(row[i]) || isnan(C[i])){
			cout << i << " - val : " << row[i] << " - C : " << C[i] << " vs " << inP->C[i] << endl;
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////

}

void KernelWrapperSparseRevisedMGPU::checkOp(cusparseStatus_t status){

	if (status != CUSPARSE_STATUS_SUCCESS) {
		cout << "Cusparse error!" << endl;
		throw(CUSPARSE_ERROR);
	}

}

void KernelWrapperSparseRevisedMGPU::initSECoeff(){

	int nBlocks, nThreads, nColByBlock;

	// Number of threads is at least nCol and at most MAX_THREADS
	nThreads = (pGPU->nRow < MAX_THREADS) ? nextPow2((pGPU->nRow + 1)) : MAX_THREADS;
	// Number of blocks is from 1 to MAX_BLOCK at most
	nBlocks = pGPU->nCol > MAX_BLOCKS ? MAX_BLOCKS : pGPU->nCol;
	// Number of column to process by block
	nColByBlock = ceil((float)pGPU->nCol / nBlocks);

	/*cout << "Threads : " << nThreads << " \t nBlocks : " << nBlocks << " \t nColByBlock : " << nColByBlock << endl;

	unsigned int cnt = 0;
	int h_colPtr[pGPU->nCol+1], h_colInd[pGPU->nNoNull], h_rowInd[pGPU->nNoNull];
	VAR_TYPE h_A[pGPU->nNoNull];
	cutilSafeCallNoSync( cudaMemcpy(h_colPtr, d_colPtrA, (pGPU->nCol+1)*sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(h_colInd, pGPU->colIndA, pGPU->nNoNull*sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(h_rowInd, pGPU->rowIndA, pGPU->nNoNull*sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(h_A, pGPU->sparseA, pGPU->nNoNull*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );


	cout << " colPtr \t colInd \t rowInd \t val " << endl;

	for(unsigned int i=0; i<pGPU->nNoNull; i++){

		if(h_colPtr[cnt] == (int)i ) {
			cout << h_colPtr[cnt] << "\t\t";
			cnt++;
		} else {
			cout <<  "-\t\t";
		}

		cout << h_colInd[i] << "\t\t" << h_rowInd[i] << "\t\t" << h_A[i] << endl;


	}
	cout << h_colPtr[cnt] << "\t\t" << endl;*/


	processSECoeffRevSparse(nThreads, nBlocks, pGPU->nCol, nColByBlock,
								 d_colPtrA, pGPU->sparseA,
								 d_seCoeff);
}


void KernelWrapperSparseRevisedMGPU::getColAq(red_result_t enteringVar){

	int startPtr = h_colPtrA[enteringVar.index];
	int endPtr = h_colPtrA[enteringVar.index+1];
	int size = endPtr - startPtr;

	//cout << "Size : " << size << endl;

	cutilSafeCallNoSync( cudaMemcpy(d_tmpCol, d_z, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	checkOp(cusparseDsctr(handleCUSP, size, &pGPU->sparseA[startPtr], &pGPU->rowIndA[startPtr], d_tmpCol, CUSPARSE_INDEX_BASE_ZERO));
}


void KernelWrapperSparseRevisedMGPU::checkInfeasibilityV2(SimplexProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = inP->PRIMAL;

	infB = checkFeasBasicV2W();
	infX = checkFeasNonBasicV2W();

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		inP->mode = inP->AUX;
	}
	_SYNC_KERNEL
}

void KernelWrapperSparseRevisedMGPU::updateInfeasibilityV2(SimplexProblem *inP) {

	inP->nInf = updateFeasV2W();

	if(inP->nInf>0){
		inP->mode = inP->AUX;
	} else {
		inP->mode = inP->PRIMAL;
	}

	_SYNC_KERNEL
}


/* Process using pGPU values
 *
 * y = Cb * B
 * RedCost = Cn - y * An
 *
 * Store the result in d_redCost
 */
void KernelWrapperSparseRevisedMGPU::processReducedCost(){

	double alpha = -1.0;
	double beta = 1.0;

	#if TYPE == USE_DOUBLE
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
#endif
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->C, pGPU->nCol*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	checkOp( cusparseDcsrmv(handleCUSP, CUSPARSE_OPERATION_NON_TRANSPOSE,
						    pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
						    &alpha, descrA, pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
						    d_tmpBasic, &beta, d_redCost));


#elif TYPE == USE_FLOAT
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
	cublasSgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->Cn, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	cublasSgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif


}


/* Process using pGPU values
 *
 * y = Cb * B
 * RedCost = Cn - y * An
 *
 * Store the result in d_redCost
 */
void KernelWrapperSparseRevisedMGPU::processReducedCostP1(){

	double alpha = -1.0;
	double beta = 1.0;

#if TYPE == USE_DOUBLE
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->CbP1, 1, 0.0, d_tmpBasic, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), pGPU->CbP1, 1, 0.0, d_tmpBasic, 1);
#endif
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->CP1, pGPU->nCol*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	checkOp( cusparseDcsrmv(handleCUSP, CUSPARSE_OPERATION_NON_TRANSPOSE,
						    pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
						    &alpha, descrA, pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
						    d_tmpBasic, &beta, d_redCost));
#elif TYPE == USE_FLOAT
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
	cublasSgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->Cn, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	cublasSgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif

}

/* Process using pGPU values
 *
 * d_e = B * a_e
 *
 * Store the result in d_enterCol
 */
void KernelWrapperSparseRevisedMGPU::processColumn(red_result_t enteringVar){

	//VAR_TYPE Zq, ZqPrime;

	// Aq (sparse) =====> d_tmpCol (dense)
	// d_e = B * a_e ===> d_enterCol = B * d_tmpCol + 0.0 * d_enterCol
#if TYPE == USE_DOUBLE
	// Scatter sparse Aq to d_tmpCol
	getColAq(enteringVar);
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), d_tmpCol, 1, 0.0, d_enterCol, 1);
#elif TYPE == USE_FLOAT
	cublasSgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), &pGPU->An[IDX2C(0, enteringVar.index, P2S(pitch_An))], 1, 0.0, d_enterCol, 1);
#endif


	/*VAR_TYPE h_col[pGPU->nRow], h_colB[pGPU->nRow];
	cutilSafeCallNoSync( cudaMemcpy(h_col, d_tmpCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(h_colB, d_enterCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	for(unsigned int i=0; i<pGPU->nRow; i++){
		cout << h_col[i] << "\t\t" << h_colB[i] << endl;
	}

	getchar();*/
}

void KernelWrapperSparseRevisedMGPU::copyEnterColToDevice2(){
	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync(cudaMemcpyAsync(d2_D, d_enterCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDefault, stream));
	cutilSafeCallNoSync(cudaSetDevice(device));
}


/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperSparseRevisedMGPU::selectSECoeff(){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);

	/*//////////////////////////////////////////////////////////////
	double h_seCoeff[pGPU->nCol];
	double h_redCost[pGPU->nCol];

	cutilSafeCallNoSync( cudaMemcpy( h_seCoeff, d_seCoeff, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_redCost, d_redCost, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	cout << "index\t seCoeff\t redCost" << endl;

	for(unsigned int i=0; i<pGPU->nCol; i++){
		cout << i << "\t\t" << h_seCoeff[i] << "\t\t" << h_redCost[i] << endl;
	}
	getchar();
	//////////////////////////////////////////////////////////////*/


	selectSECoeffRevised(pGPU->nCol, nThreads, nBlocks,
						 d_seCoeff, pGPU->val, pGPU->bounds, d_redCost, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperSparseRevisedMGPU::selectSECoeffP1(){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);


	/*double h_seCoeff[pGPU->nCol];
	double h_redCost[pGPU->nCol];

	cutilSafeCallNoSync( cudaMemcpy( h_seCoeff, d_seCoeff, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_redCost, d_redCost, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );


	cout << "index\t seCoeff\t redCost" << endl;

	for(unsigned int i=0; i<pGPU->nCol; i++){
		cout << i << "\t\t" << h_seCoeff[i] << "\t\t" << h_redCost[i] << endl;
	}*/


	selectSECoeffRevised(pGPU->nCol, nThreads, nBlocks,
						 d_seCoeff, pGPU->val, pGPU->boundsP1, d_redCost, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperSparseRevisedMGPU::updateSECoeff(red_result_t enteringVar, row_result_t leavingVar, unsigned int outVarIdx){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);

	seCoeffQ = getEnteringSECoeff();
	alphaQ = getPivotValue(leavingVar);
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);

	updateSECoeffAqRevised(pGPU->nCol, nThreads, nBlocks,
						 outVarIdx, seCoeffQ, newSeCoeffQ, alphaQ,
			             d_seCoeff, d_alpha, d_beta,
			             pGPU->val, pGPU->bounds, d_redCost,
			             dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

red_result_t KernelWrapperSparseRevisedMGPU::updateSECoeffP1(red_result_t enteringVar, row_result_t leavingVar, unsigned int outVarIdx){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);


	seCoeffQ = getEnteringSECoeff();
	alphaQ = getPivotValue(leavingVar);
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);

	updateSECoeffAqRevised(pGPU->nCol, nThreads, nBlocks,
						 outVarIdx, seCoeffQ, newSeCoeffQ, alphaQ,
			             d_seCoeff, d_alpha, d_beta,
			             pGPU->val, pGPU->boundsP1, d_redCost,
			             dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;


	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperSparseRevisedMGPU::updateInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq, unsigned int outVarIdx){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);


	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	updateInVarAqRevised(pGPU->nCol, nThreads, nBlocks,
					   outVarIdx, seCoeffQ, newSeCoeffQ,
			           redCostQ, newRedCostQ,  alphaQ,
			           d_seCoeff, d_alpha, d_beta,
			           pGPU->val, pGPU->bounds, d_redCost,
			           dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperSparseRevisedMGPU::updateInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq, unsigned int outVarIdx){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);

	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	//cout << "Nouvel valeur redCost Q[" <<  enteringVar.index <<"] = " << newRedCostQ << endl;
	//getchar();

	updateInVarAqRevised(pGPU->nCol, nThreads, nBlocks,
					   outVarIdx, seCoeffQ, newSeCoeffQ,
			           redCostQ, newRedCostQ, alphaQ,
			           d_seCoeff, d_alpha, d_beta,
			           pGPU->val, pGPU->boundsP1, d_redCost,
			           dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Return the current pivot value from the d_enterCol vector
 */
VAR_TYPE KernelWrapperSparseRevisedMGPU::getPivotValue(row_result_t leavingVar){
	VAR_TYPE dl;
	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	return dl;
}

/**
 * Process, afresh, the steepest edge coefficient of the entering variable
 */
VAR_TYPE KernelWrapperSparseRevisedMGPU::getEnteringSECoeff(){
	VAR_TYPE nrm = cublasDnrm2(pGPU->nRow, d_enterCol, 1);
	cudaThreadSynchronize();
	return (nrm*nrm + 1.0);
}

/**
 * Process, afresh, the reduced cost of the entering variable
 */
VAR_TYPE KernelWrapperSparseRevisedMGPU::getZq(red_result_t enteringVar){
	VAR_TYPE zq, cq;
	cutilSafeCallNoSync( cudaMemcpy( &cq, &pGPU->C[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	zq = -cublasDdot(pGPU->nRow, pGPU->Cb, 1, d_enterCol, 1);
	cudaThreadSynchronize();
	zq += cq;


	//VAR_TYPE tmpq;
	//cutilSafeCallNoSync( cudaMemcpy( &tmpq, &d_redCost[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	return zq;
}

/**
 * Process, afresh, the reduced cost of the entering variable
 */
VAR_TYPE KernelWrapperSparseRevisedMGPU::getZqP1(red_result_t enteringVar){
	VAR_TYPE zq, cq;
	cutilSafeCallNoSync( cudaMemcpy( &cq, &pGPU->CP1[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	zq = -cublasDdot(pGPU->nRow, pGPU->CbP1, 1, d_enterCol, 1);
	cudaThreadSynchronize();
	zq += cq;


	return zq;
}


row_result_t KernelWrapperSparseRevisedMGPU::expandFindRow(red_result_t enteringVar, double delta, double tau){
	red_result_t res1;
	exp_result_t res2;
	row_result_t res;
	VAR_TYPE xVal, objVal;
	float2 xBound;
	float lo, up;
	VAR_TYPE alpha1, alpha2;
	VAR_TYPE alphaMin;
	int objSign;// = (enteringVar.value > 0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT_MEM")
	cutilSafeCallNoSync( cudaMemcpy( &objVal, &d_redCost[enteringVar.index], 1 * sizeof(objVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &pGPU->val[enteringVar.index], 1 * sizeof(xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &pGPU->bounds[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT_MEM")

	objSign = (objVal > 0.0 ? 1 : -1);

	lo = xBound.x;
	up = xBound.y;

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(pGPU->nBasic, pGPU->bBounds, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	if(objSign == 1){
		if(up < FLT_MAX){
			alpha1 = (up + delta) - xVal;
		} else {
			alpha1 = MAX_TYPE;
		}
	} else {
		if(lo > -FLT_MAX) {
			alpha1 = xVal - (lo - delta);
		} else {
			alpha1 = MAX_TYPE;
		}
	}

	//cout << "Res1 value :  " << res1.value << " - alpha1 : " << alpha1 << endl;

	alpha1 = MIN(alpha1, res1.value);

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		res.mode = M_UNBOUNDED;
		printf("We went there(unbounded)!");
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(up < FLT_MAX)
			alpha2 = up - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha2 = xVal - lo;
		else
			alpha2 = MAX_TYPE;
	}

	//cout << "alpha2 : " << alpha2 << " -  alpha1 : " << alpha1 << endl;

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
	    alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		res2 = expandP2W(pGPU->nBasic, pGPU->bBounds, alpha1, objSign);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1)
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);

	return res;
}



row_result_t KernelWrapperSparseRevisedMGPU::expandFindRowP1(red_result_t enteringVar, double delta, double tau){
	row_result_t res;
	red_result_t resRed;
	red_result_t res1;
	exp_result_t res2;
	VAR_TYPE xVal, objVal;
	float2 xBound, xBoundP1;
	float lo, up, loP1, upP1;
	VAR_TYPE alpha1, alpha2, alphaI, teta;
	VAR_TYPE alphaMin;
	int objSign; // = (enteringVar.value > 0.0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT")
	cutilSafeCallNoSync( cudaMemcpy( &objVal, &d_redCost[enteringVar.index], 1 * sizeof(objVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &pGPU->val[enteringVar.index], 1 * sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &pGPU->bounds[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBoundP1, &pGPU->boundsP1[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT")

	objSign = (objVal > 0.0 ? 1 : -1);

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(pGPU->nBasic, pGPU->bBoundsP1, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	 lo = xBound.x;
	 up = xBound.y;
	 loP1 = xBoundP1.x;
	 upP1 = xBoundP1.y;

	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha1 = (upP1 + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha1 = xVal - (loP1 - delta);
		else
			alpha1 = MAX_TYPE;
	}
	alpha1 = MIN(alpha1, res1.value);

	// Special step - trying to put a infeasible variable to it's feasible bound
	// Getting the max absolute pivot
	_START_EVENT(prof, "REDUCTION_MAX")
	resRed = argMaxAbsW(pGPU->nRow, d_enterCol, 1);
	_END_EVENT(prof, "REDUCTION_MAX")
	// Tolerance on the pivot
	teta = fabs(resRed.value) * EXPAND_W;
	// Expand special Phase 1 step
	_START_EVENT(prof, "SPECIAL_P1_PHASE")
	res2 = expandSpecW(pGPU->nBasic, teta, objSign);
	_END_EVENT(prof, "SPECIAL_P1_PHASE")
	SET_E(res, res2);
	res.mode = M_INF_PIVOT;

	// Will proabaly not work
	if(up != upP1){
		alphaI = xVal - up;
	} else if (lo != loP1) {
		alphaI = lo - xVal;
	} else {
		alphaI = 0;
	}


	if(alphaI > res.alpha){
		res.index = -2;
		res.alpha = alphaI;
		res.absP = xVal;
		res.mode = M_INF_TO_BOUND;
	}

	if(res.alpha <= alpha1 && res.index != -1) {
		res.alpha *= (double)objSign;
		return res;
	}

	// if the pivoting step is limited by the bound of the variable
	// then we don't have to pivot

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		printf("We went there(unbounded) [P1]!");
		res.mode = M_UNBOUNDED;
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha2 = upP1 - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha2 = xVal - loP1;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
		alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		res2 = expandP2W(pGPU->nBasic, pGPU->bBoundsP1, alpha1, objSign);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1){
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);
	}

	return res;

}

void KernelWrapperSparseRevisedMGPU::updateBasisDevice2(row_result_t leavingVar){
	VAR_TYPE dl;
	cutilSafeCallNoSync(cudaSetDevice(device+1));
	// Nb of threads is pow2 nearest nBasic and lower or equals to MAX_THREADS
	int threads = (pGPU->nBasic < MAX_THREADS*2) ? nextPow2((pGPU->nBasic + 1)/ 2) : MAX_THREADS;
	// Nb of blocks is the smallest between nBasic and MAX_BLOCKS
	int blocks = pGPU->nBasic;//MIN(pGPU->nBasic, MAX_BLOCKS);

	cutilSafeCallNoSync( cudaMemcpy( &dl, &d2_D[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	updateBasisKernel(stream, threads, blocks, pGPU->nBasic, leavingVar.index, dl, pGPU->B, P2S(pitch_B), d2_D);
	cutilSafeCallNoSync(cudaSetDevice(device));

	_SYNC_KERNEL

}

void KernelWrapperSparseRevisedMGPU::copyBasisFromDevice2(){

	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync(cudaMemcpy2DAsync(pGPU->B, pitch_B, d2_B, pitch_B, pGPU->nRow*sizeof(*pGPU->B), pGPU->nBasic, cudaMemcpyDefault, stream));
	cutilSafeCallNoSync(cudaSetDevice(device));

	_SYNC_KERNEL

}

void KernelWrapperSparseRevisedMGPU::waitCopyDone(){

	cutilSafeCallNoSync(cudaSetDevice(device+1));
	cutilSafeCallNoSync(cudaStreamSynchronize(stream));
	cutilSafeCallNoSync(cudaSetDevice(device));

	_SYNC_KERNEL

}

void KernelWrapperSparseRevisedMGPU::updateBasis(row_result_t leavingVar){

	// since the basis has been updated, equations are no longer up to date
	eqsUpToDate = false;

#if B_STORAGE == B_NORMAL
	this->updateBasisW(leavingVar);
	//this->updateBasisCublas(leavingVar);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	this->updateBasisV2W(leavingVar, pGPU->B);
#endif

}

/*
 * Process the Beta vector for SE update
 */
void KernelWrapperSparseRevisedMGPU::processBeta(){

	double alpha = 1.0;
	double beta = 0.0;

	// B * inCol = K
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), d_enterCol, 1, 0.0, d_tmpCol, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), d_enterCol, 1, 0.0, d_tmpCol, 1);
#endif

	// sparseA * K = Beta
	checkOp( cusparseDcsrmv(handleCUSP, CUSPARSE_OPERATION_NON_TRANSPOSE,
						    pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
						    &alpha, descrA, pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
						    d_tmpCol, &beta, d_beta));
}

/**
 * Process the pivot row alpha
 */
void KernelWrapperSparseRevisedMGPU::processAlpha(row_result_t leavingVar){

	double alpha = 1.0;
	double beta = 0.0;

#if B_STORAGE == B_NORMAL
	// Get row_P of leaving variable P
	cublasDcopy(pGPU->nBasic, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
	// sparseA * row_P = Alpha
	checkOp( cusparseDcsrmv(handleCUSP, CUSPARSE_OPERATION_NON_TRANSPOSE,
						    pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
						    &alpha, descrA, pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
						    d_tmpCol, &beta, d_alpha));
#elif B_STORAGE == B_NORMAL_AND_TRANS
	// Get row_P of leaving variable P
	VAR_TYPE *ptrB = &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))];
	checkOp( cusparseDcsrmv(handleCUSP, CUSPARSE_OPERATION_NON_TRANSPOSE,
						    pGPU->nCol, pGPU->nRow, pGPU->nNoNull,
						    &alpha, descrA, pGPU->sparseA, d_colPtrA, pGPU->rowIndA,
						    ptrB, &beta, d_alpha));
#endif

}

void KernelWrapperSparseRevisedMGPU::updateVariables(red_result_t enteringVar, row_result_t leavingVar){

	VAR_TYPE xVal;

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(pGPU->nRow, -leavingVar.alpha, d_enterCol, 1, pGPU->bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -leavingVar.alpha, d_enterCol, 1, PU->bVal, 1);
#endif

	// Updating entering var. value
	// Read xVal TODO xVal is probably into leavingVar.absP (to check)
	cutilSafeCallNoSync(cudaMemcpy( &xVal, &pGPU->val[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	// Add alpha
	xVal += leavingVar.alpha;
	// Store xVal
	cutilSafeCallNoSync(cudaMemcpy(&pGPU->val[enteringVar.index], &xVal, sizeof(VAR_TYPE), cudaMemcpyHostToDevice));
}

/*void KernelWrapperSparseRevisedMGPU::swapVariables(red_result_t enteringVar, row_result_t leavingVar){

	int outNBIdx;

	// Get the "non-basic index" of the leaving variable
	cutilSafeCallNoSync( cudaMemcpy( &outNBIdx, &pGPU->bInd[leavingVar.index], sizeof(int), cudaMemcpyDeviceToHost) );

	this->swapVariables(enteringVar, leavingVar, outNBIdx);

	this->checkPivoting(enteringVar, leavingVar, outNBIdx);


}*/

void KernelWrapperSparseRevisedMGPU::swapVariables(red_result_t enteringVar, row_result_t leavingVar, unsigned int outNBIdx){

	int inV = enteringVar.index;
	int outV = leavingVar.index;
	VAR_TYPE erase = NAN;

	/*/////////////////////////////////////////////////////////////
	int h_basis[pGPU->nRow], h_basis2[pGPU->nRow];
	double h_val[pGPU->nCol], h_bVal[pGPU->nRow];
	double h_val2[pGPU->nCol], h_bVal2[pGPU->nRow];

	cutilSafeCallNoSync( cudaMemcpy( h_basis, pGPU->bInd, pGPU->nRow*sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_val, pGPU->val, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_bVal, pGPU->bVal, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	/////////////////////////////////////////////////////////////*/

	// Get the "non-basic index" of the leaving variable (bInd)
	//cutilSafeCallNoSync( cudaMemcpy( &outNBIdx, &pGPU->bInd[outV], sizeof(int), cudaMemcpyDeviceToHost) );

	// Leaving variable : Overwrite non-basic with basic values ( val, bounds, C)
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->val[outNBIdx], &pGPU->bVal[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bounds[outNBIdx], &pGPU->bBounds[outV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->C[outNBIdx], &pGPU->Cb[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	// Entering variable :
	// Replace basic values by those of inVar. ( bInd, bVal, bBounds, Cb)
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bInd[outV], &inV, sizeof(int), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bVal[outV], &pGPU->val[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bBounds[outV], &pGPU->bounds[inV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->Cb[outV], &pGPU->C[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	// Set non-basic value of C, val to NaN
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->val[inV], &erase, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
	//cutilSafeCallNoSync( cudaMemcpy( &pGPU->C[inV], &erase, sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	/*//////////////////////////////////////////
	cutilSafeCallNoSync( cudaMemcpy( h_basis2, pGPU->bInd, pGPU->nRow*sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_val2, pGPU->val, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_bVal2, pGPU->bVal, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	cout << "basis" << endl;
	cout << "ind \t\t" << "before \t\t" << "after \t\t" << endl;
	for(unsigned int i=0; i<pGPU->nRow; i++){
		cout << i << " : " << h_basis[i] << "(" << h_basis2[i] << ")\t" << h_bVal[i] << "\t\t" << h_bVal2[i] << endl;
	}

	cout << "val" << endl;
	cout << "ind \t\t" << "before \t\t" << "after \t\t" << endl;
	for(unsigned int i=0; i<pGPU->nCol; i++){
		cout << i << "\t\t" << h_val[i] << "\t\t" << h_val2[i] << endl;
	}

	getchar();
	//////////////////////////////////////////*/

}

void KernelWrapperSparseRevisedMGPU::swapVariablesP1(red_result_t enteringVar, row_result_t leavingVar, unsigned int outNBIdx){

	//int outNBIdx;
	int inV = enteringVar.index;
	int outV = leavingVar.index;
	//VAR_TYPE erase = NAN;

	// Get the "non-basic index" of the leaving variable
	//cutilSafeCallNoSync( cudaMemcpy( &outNBIdx, &pGPU->bInd[outV], sizeof(int), cudaMemcpyDeviceToHost) );

	// Same as phase 1 swap
	this->swapVariables(enteringVar, leavingVar, outNBIdx);

	// Leaving variable : Overwrite non-basic with basic values (BoundsP1, CP1)
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->boundsP1[outNBIdx], &pGPU->bBoundsP1[outV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->CP1[outNBIdx], &pGPU->CbP1[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	// Entering variable :
	// Replace basic values by those of inVar. ( bBoundsP1, CbP1)
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bBoundsP1[outV], &pGPU->boundsP1[inV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->CbP1[outV], &pGPU->CP1[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	// Set non-basic value of CP1 to NaN
	//cutilSafeCallNoSync( cudaMemcpy( &pGPU->CP1[inV], &erase, sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
}


void KernelWrapperSparseRevisedMGPU::reinitProblem(SparseRevisedFormProblem *inP){

	bool inSite = true;

	// Matrix
	// B
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->B, pitch_B, inP->B, inP->nRow*sizeof(*pGPU->B), inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyHostToDevice) );

	reinitBasisW(inSite);

	// Vectors
	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bounds, inP->bounds, inP->nNonBasic*sizeof(*pGPU->bounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->boundsP1, inP->boundsP1, inP->nNonBasic*sizeof(*pGPU->boundsP1), cudaMemcpyHostToDevice) );

	// Basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBounds, inP->bBounds, inP->nBasic*sizeof(*pGPU->bBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBoundsP1, inP->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1), cudaMemcpyHostToDevice) );

	// Objective function

	cutilSafeCallNoSync( cudaMemcpy(pGPU->C, inP->C, inP->nNonBasic*sizeof(*pGPU->C), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CP1, inP->CP1, inP->nNonBasic*sizeof(*pGPU->CP1), cudaMemcpyHostToDevice) );

	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cb, inP->Cb, inP->nBasic*sizeof(*pGPU->Cb), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CbP1, inP->CbP1, inP->nBasic*sizeof(*pGPU->CbP1), cudaMemcpyHostToDevice) );

}


void KernelWrapperSparseRevisedMGPU::getSolution(SimplexSolution *inSol){
	// Order the vals as they were at beginning (x1..xn, bn+1...bm)
	// Ordered values are put in y (xVal => 1..nCol) and x (bVal => 1..nRow)
	bool inSite = false;

	reinitBasisW(inSite);
	cutilSafeCallNoSync(cudaMemcpy(inSol->xVal, d_tmpRow, pGPU->nVar*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	cutilSafeCallNoSync(cudaMemcpy(inSol->sVal, d_tmpCol, pGPU->nSlack*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
}


void KernelWrapperSparseRevisedMGPU::processOptimum(SimplexProblem *inP){

	// Trick to get xVal without NAN in it.
	bool inSite = false;
	reinitBasisW(inSite);


	/*///////////////////////////////////////////////////////////////////////////////////////////
	VAR_TYPE row[pGPU->nCol], C[pGPU->nCol];
	cutilSafeCallNoSync(cudaMemcpy(row, d_tmpRow, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	cutilSafeCallNoSync(cudaMemcpy(C, pGPU->C, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));


	for(unsigned int i=0; i<pGPU->nCol; i++){
		if(isnan(row[i]) || isnan(C[i])){
			cout << i << " - val : " << row[i] << " - C : " << C[i] << endl;
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////*/


	#if TYPE == USE_DOUBLE
		inP->objVal = cublasDdot(pGPU->nCol, d_tmpRow, 1, pGPU->C, 1);
		cudaThreadSynchronize();
	#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->Cb, 1);
		cudaThreadSynchronize();
		inP->objVal += cublasSdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->Cn, 1);
		cudaThreadSynchronize();
		throw(NOT_IMPLEMENTED);
	#endif

	if(inP->mode == inP->AUX) {
		#if TYPE == USE_DOUBLE
		inP->objAuxVal = cublasDdot(pGPU->nCol, d_tmpRow, 1, pGPU->CP1, 1);
		cudaThreadSynchronize();
		#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->CbP1, 1);
		inP->objVal += cublasSdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->CnP1, 1);
		throw(NOT_IMPLEMENTED);
		#endif
	}
	_SYNC_KERNEL
}

void KernelWrapperSparseRevisedMGPU::changeBoundWK(IndexPivoting *inI, int index, float2 bound){

	unsigned int loc = inI->xLoc[index];
	cutilSafeCallNoSync(cudaMemcpy(&pGPU->bounds[index], &bound, sizeof(float2), cudaMemcpyHostToDevice));

	if(loc > pGPU->nVar){
		cutilSafeCallNoSync(cudaMemcpy(&pGPU->bBounds[loc-pGPU->nVar], &bound, sizeof(float2), cudaMemcpyHostToDevice));
	}
	_SYNC_KERNEL
}

void KernelWrapperSparseRevisedMGPU::resetBounds(){

	// Reset Non basic values to their bounds after EXPAND
	// nTriv contains the number of non-trivial reset
	VAR_TYPE error[pGPU->nRow];
	row_result_t rowIdx;

	int nTriv = resetXValW();

	// Really heavy processing then
	if(nTriv > 0){

		// Trick to get xVal without NAN in it.
		bool inSite = false;
		reinitBasisW(inSite);

		for(unsigned int i=0; i<pGPU->nRow; i++){
			rowIdx.index = i;
			this->processAlpha(rowIdx);
			error[i] = cublasDdot(pGPU->nCol, d_alpha, 1, d_tmpRow, 1);
		}

		/*////////////////////////////////////////////////////////////////////
		cout << "errors : " << endl;
		for(unsigned int i=0; i< pGPU->nRow; i++){
			cout << error[i] << endl;
		}
		getchar();
		////////////////////////////////////////////////////////////////////*/

		cutilSafeCallNoSync(cudaMemcpy(d_tmpCol, error, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyHostToDevice));

		// TODO check sign correctness
		cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, -1.0, pGPU->B, P2S(pitch_B), pGPU->lhs, 1, 1.0, d_tmpCol, 1);

		/*/////////////////////////////////////////////////////////////////////
		cutilSafeCallNoSync(cudaMemcpy(error, d_tmpCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
		cout << "-B*lhs + RHS =  delta : " << endl;
		for(unsigned int i=0; i< pGPU->nRow; i++){
			cout << error[i] << endl;
		}
		getchar();
		////////////////////////////////////////////////////////////////////*/


		cublasDaxpy(pGPU->nRow, 1.0, d_tmpCol, 1, pGPU->bVal, 1);

		//this->resetSECoeff();
	}
	_SYNC_KERNEL
}

void KernelWrapperSparseRevisedMGPU::getDeviceData(SimplexProblem *inP){

	unsigned int cnt = 0;
	red_result_t colIdx;
	VAR_TYPE val[pGPU->nCol], objF[pGPU->nCol], objFP1[pGPU->nCol];
	float2 bounds[pGPU->nCol], boundsP1[pGPU->nCol];

	cutilSafeCallNoSync( cudaMemcpy( val, pGPU->val, inP->nCol * sizeof(*pGPU->val), cudaMemcpyDeviceToHost) );

	this->processReducedCost();
	cutilSafeCallNoSync( cudaMemcpy( objF, d_redCost, inP->nCol * sizeof(*d_redCost), cudaMemcpyDeviceToHost) );

	this->processReducedCostP1();
	cutilSafeCallNoSync( cudaMemcpy( objFP1, d_redCost, inP->nCol * sizeof(*d_redCost), cudaMemcpyDeviceToHost) );


	cutilSafeCallNoSync( cudaMemcpy( bounds, pGPU->bounds, inP->nCol * sizeof(*pGPU->bounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( boundsP1, pGPU->boundsP1, inP->nCol * sizeof(*pGPU->boundsP1), cudaMemcpyDeviceToHost) );


	// Shooting variable in basis and taking true non-basic
	for(unsigned int i=0; i<pGPU->nCol; i++){
		if(!isnan(val[i])){
			colIdx.index = i;
			this->processColumn(colIdx);
			inP->xVal[cnt] = val[i];
			inP->objFunc[cnt] = objF[i];
			inP->objFuncAux[cnt] =  objFP1[i];
			inP->xBounds[cnt] = bounds[i];
			inP->xBoundsP1[cnt] = boundsP1[i];
			inP->xInd[cnt] = i;
			cutilSafeCallNoSync( cudaMemcpy(&inP->eqs[IDX2C(0, cnt, inP->nRow)], d_enterCol, inP->nRow * sizeof(*d_enterCol), cudaMemcpyDeviceToHost) );
			cnt++;
		}
	}

	// Basic value
	cutilSafeCallNoSync( cudaMemcpy( inP->bVal, pGPU->bVal, inP->nRow * sizeof(*pGPU->bVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBounds, pGPU->bBounds, inP->nRow * sizeof(*pGPU->bBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBoundsP1, pGPU->bBoundsP1, inP->nRow * sizeof(*pGPU->bBoundsP1), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->basis, pGPU->bInd, inP->nRow * sizeof(*pGPU->bInd), cudaMemcpyDeviceToHost) );

}

// Get the problem
void KernelWrapperSparseRevisedMGPU::getDeviceData(SparseRevisedFormProblem *inP){

	_SYNC_KERNEL

	// Matrix
	// B
	cutilSafeCallNoSync( cudaMemcpy2D(inP->B, inP->nRow*sizeof(*pGPU->B), pGPU->B, pitch_B, inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyDeviceToHost) );

	// Vectors
	// lhs
	cutilSafeCallNoSync( cudaMemcpy(inP->lhs, pGPU->lhs, pGPU->nRow*sizeof(*inP->lhs), cudaMemcpyDeviceToHost) );
	// Basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(inP->bBounds, pGPU->bBounds, pGPU->nBasic*sizeof(*inP->bBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->bBoundsP1, pGPU->bBoundsP1, pGPU->nBasic*sizeof(*inP->bBoundsP1), cudaMemcpyDeviceToHost) );

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(inP->bounds, pGPU->bounds, pGPU->nCol*sizeof(*inP->bounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->boundsP1, pGPU->boundsP1, pGPU->nCol*sizeof(*inP->boundsP1), cudaMemcpyDeviceToHost) );

	// Variable values
	cutilSafeCallNoSync( cudaMemcpy(inP->bVal, pGPU->bVal, pGPU->nBasic*sizeof(*inP->bVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->val, pGPU->val, pGPU->nCol*sizeof(*inP->val), cudaMemcpyDeviceToHost) );

	// Variable index
	cutilSafeCallNoSync( cudaMemcpy(inP->bInd, pGPU->bInd, pGPU->nBasic*sizeof(*inP->bInd), cudaMemcpyDeviceToHost) );

	// Objective function
	cutilSafeCallNoSync( cudaMemcpy(inP->Cb, pGPU->Cb, pGPU->nBasic*sizeof(*inP->Cb), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->CbP1, pGPU->CbP1, pGPU->nBasic*sizeof(*inP->CbP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy(inP->C, pGPU->C, pGPU->nNonBasic*sizeof(*inP->C), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->CP1, pGPU->CP1, inP->nNonBasic*sizeof(*inP->CP1), cudaMemcpyHostToDevice) );

}

/***********************************************************************************************/
/*****************************      Kernel Wrappers     ****************************************/
/***********************************************************************************************/


int KernelWrapperSparseRevisedMGPU::checkFeasNonBasicV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);
	checkFeasXV2(pGPU->nCol, nThreads, nBlocks, pGPU->val, pGPU->bounds, pGPU->boundsP1, pGPU->CP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperSparseRevisedMGPU::checkFeasBasicV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nBasic, nBlocks, nThreads);
	checkFeasRevisedBV2(pGPU->nBasic, nThreads, nBlocks, pGPU->bVal, pGPU->bBounds, pGPU->bBoundsP1, pGPU->CbP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperSparseRevisedMGPU::updateFeasV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);
	updateFeasV2(pGPU->nCol + pGPU->nBasic, nThreads, nBlocks, pGPU->nCol, pGPU->val, pGPU->bVal, pGPU->bounds, pGPU->bBounds, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

red_result_t KernelWrapperSparseRevisedMGPU::argMaxPosW(int nEl, VAR_TYPE* p1, int incP1){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxPos(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
	SET(gpu_result, MAX_R(gpu_result, hRes[i]));

	if(F_EQUAL_Z(gpu_result.value)) gpu_result.index = -1;

	return gpu_result;
}

red_result_t KernelWrapperSparseRevisedMGPU::argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbs(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


red_result_t KernelWrapperSparseRevisedMGPU::expandP1W(int nEl, float2 *bounds, double delta, int objSign){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = MAX_TYPE;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP1(nEl, nThreads, nBlocks, d_enterCol, pGPU->bVal, bounds, delta, objSign, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET(gpu_result, MIN_R(gpu_result, hRes[i]));
	}

	return gpu_result;

}

exp_result_t KernelWrapperSparseRevisedMGPU::expandP2W(int nEl, float2* bounds, double alpha, int objSign){
	int nBlocks, nThreads;
	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.absP = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP2(nEl, nThreads, nBlocks, d_enterCol, pGPU->bVal, bounds, alpha, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET_E_P(gpu_result, MAX_E_P(gpu_result, hERes[i]));
	}

	return gpu_result;

}

exp_result_t KernelWrapperSparseRevisedMGPU::expandSpecW(int nEl, double teta, int objSign){
	int nBlocks, nThreads;
	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.alpha = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandSpec(nEl, nThreads, nBlocks, d_enterCol,  pGPU->bVal, pGPU->bBounds, pGPU->bBoundsP1, teta, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
		SET_E_A(gpu_result, MAX_E_A(gpu_result, hERes[i]));


	return gpu_result;

}


void KernelWrapperSparseRevisedMGPU::updateBasisW(row_result_t leavingVar){
	VAR_TYPE dl;

	// Nb of threads is pow2 nearest nBasic and lower or equals to MAX_THREADS
	int threads = (pGPU->nBasic < MAX_THREADS*2) ? nextPow2((pGPU->nBasic + 1)/ 2) : MAX_THREADS;
	// Nb of blocks is the smallest between nBasic and MAX_BLOCKS
	int blocks = pGPU->nBasic;//MIN(pGPU->nBasic, MAX_BLOCKS);

	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	updateBasisKernel(threads, blocks, pGPU->nBasic, leavingVar.index, dl, pGPU->B, P2S(pitch_B), d_enterCol);

	_SYNC_KERNEL
}

void KernelWrapperSparseRevisedMGPU::updateBasisCublas(row_result_t leavingVar){
	VAR_TYPE dl, dlBis, negOne;

	// Copy dl value
	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	// Change it for -1
	negOne = -1;
	cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
	// Copy basis row l
	cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
	// replace it by zero vector
	cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

	/* Process Bij' = -(1/dl)*di*rj + Bij
	 * With d_enterCol as d, d_tmpCol as r
	 * Row Blj is changed to 0 and dl to -1 such as when i=l :
	 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
	 */
	dlBis = -(1/dl);
	cublasDger(pGPU->nBasic, pGPU->nBasic, dlBis, d_enterCol, 1, d_tmpCol, 1, pGPU->B, P2S(pitch_B));

	///////////////////////////////////////

/*	VAR_TYPE *tmpD = new VAR_TYPE[pGPU->nBasic];
	VAR_TYPE *tmpR = new VAR_TYPE[pGPU->nBasic];
	VAR_TYPE *tmpB = new VAR_TYPE[pGPU->nBasic*pGPU->nBasic];
	//cutilSafeCallNoSync( cudaMemcpy( tmpB, pGPU->B, pGPU->nBasic*pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(tmpD, d_enterCol, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(tmpR, d_tmpCol, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy2D(tmpB, pGPU->nRow*sizeof(*tmpB), pGPU->B, pitch_B, pGPU->nRow*sizeof(*pGPU->B), pGPU->nBasic, cudaMemcpyDeviceToHost) );

	cout << endl;
	cout << "Alpha : " << dlBis << endl;

	cout << "Col (D)" << endl;
	for(unsigned int j=0; j<pGPU->nBasic; j++){
		if(fabs(tmpD[j]) > 1e-20){
			cout << j << "\t\t" << tmpD[j] << endl;
		}
	}

	cout << endl;

	cout << "Col (R)" << endl;
	for(unsigned int j=0; j<pGPU->nBasic; j++){
		if(fabs(tmpR[j]) > 1e-20){
			cout << j << "\t\t" << tmpR[j] << endl;
		}
	}


	cout << endl;

	for(unsigned int i=0; i<pGPU->nBasic; i++){
		for(unsigned int j=0; j<pGPU->nBasic; j++){
			if(fabs(tmpB[IDX2C(i,j, pGPU->nBasic)]) > 1e-20){
				cout << j << "\t\t" << i << "\t\t" << tmpB[IDX2C(i,j, pGPU->nBasic)] << endl;
			}
		}
	}
	delete [] tmpB;
	delete [] tmpD;
	delete [] tmpR;

	cout << "**********************************************************" << endl;
*/
	//////////////////////////////////////





	// Change back enter column to its orinigal value (might be used later for other processing)
	cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
}

void KernelWrapperSparseRevisedMGPU::updateBasisV2W(row_result_t leavingVar, VAR_TYPE *bBis){
#if B_STORAGE == B_NORMAL_AND_TRANS

	VAR_TYPE dl;

	if(devProp.major >= 2){
		int blocks = ceil((float)pGPU->nBasic/32);

#ifdef SIMPLE
			VAR_TYPE dlBis, negOne;
			// Copy dl value
			cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
			// Change it for -1
			negOne = -1;
			cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
			// Copy basis row l
			cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
			// replace it by 0 row
			cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

			/* Process Bij' = -(1/dl)*di*rj + Bij
			 * With d_enterCol as d, d_tmpCol as r
			 * Row Blj is changed to 0 and dl to -1 such as when i=l :
			 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
			 */
			dlBis = -(1/dl);
			//cublasDger(pGPU->nBasic, pGPU->nBasic, dlBis, d_enterCol, 1, d_tmpCol, 1, pGPU->B, P2S(pitch_B));
			updateBasisSimpleV3Kernel(blocks, dlBis, pGPU->B, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);

			// Change back enter column to its orinigal value (might be used later for other processing)
			cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
#else
		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		//cublasDcopy(pGPU->nRow, &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))], 1, d_tmpCol, 1);
		cublasDcopy(pGPU->nRow, &bBis[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);

		updateBasisV3Kernel(blocks, pGPU->nBasic, leavingVar.index, dl, bBis, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);
		cudaThreadSynchronize();
#endif
	} else {

#ifdef SIMPLE

		int threads = 16;
		int blocks = ceil((float)pGPU->nBasic/32);

		VAR_TYPE dlBis, negOne;
		// Copy dl value
		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		// Change it for -1
		negOne = -1;
		cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		// Copy basis row l
		cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
		// replace it by 0 row
		cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

		/* Process Bij' = -(1/dl)*di*rj + Bij
		 * With d_enterCol as d, d_tmpCol as r
		 * Row Blj is changed to 0 and dl to -1 such as when i=l :
		 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
		 */
		dlBis = -(1/dl);
		updateBasisSTrKernel(threads, blocks, dlBis, pGPU->B, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);

		// Change back enter column to its orinigal value (might be used later for other processing)
		cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

#else
		int threads = 16;
		int blocks = ceil((float)pGPU->nBasic/threads);

		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		cublasDcopy(pGPU->nRow, &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))], 1, d_tmpCol, 1);

		updateBasisV2Kernel(threads, blocks, pGPU->nBasic, leavingVar.index, dl, bBis, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);
#endif
	}

#endif
	_SYNC_KERNEL

}


void KernelWrapperSparseRevisedMGPU::reinitBasisW(bool inSite) {
	int nThreads = MAX_THREADS;
	int nBlocks = ceil((float)pGPU->nBasic/nThreads);

	if(inSite){
		reinitBasis(nThreads, nBlocks, inSite, pGPU->nBasic, pGPU->nVar, pGPU->val, pGPU->bInd, pGPU->bVal, pGPU->val, pGPU->bVal);
	} else {

		/*////////////////////////////////////////////////////////////////////////
		int h_basis[pGPU->nRow];
		double h_val[pGPU->nCol], h_bVal[pGPU->nRow];
		double h_val2[pGPU->nCol], h_bVal2[pGPU->nRow];

		cutilSafeCallNoSync( cudaMemcpy( h_basis, pGPU->bInd, pGPU->nRow*sizeof(int), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( h_val, pGPU->val, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( h_bVal, pGPU->bVal, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		////////////////////////////////////////////////////////////////////////*/

		cutilSafeCallNoSync( cudaMemcpy( d_tmpRow, pGPU->val, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( d_tmpCol, pGPU->bVal, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
		reinitBasis(nThreads, nBlocks, inSite, pGPU->nBasic, pGPU->nVar, pGPU->val, pGPU->bInd, pGPU->bVal, d_tmpRow, d_tmpCol);

		/*////////////////////////////////////////////////////////////////////////
		cutilSafeCallNoSync( cudaMemcpy( h_val2, d_tmpRow, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( h_bVal2, d_tmpCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

		cout << "basis" << endl;
		cout << "ind \t\t" << "before \t\t" << "after \t\t" << endl;
		for(unsigned int i=0; i<pGPU->nRow; i++){
			cout << h_basis[i] << "\t\t" << h_bVal[i] << "\t\t" << h_bVal2[i] << endl;
		}

		cout << "val" << endl;
		cout << "ind \t\t" << "before \t\t" << "after \t\t" << endl;
		for(unsigned int i=0; i<pGPU->nCol; i++){
			cout << i << "\t\t" << h_val[i] << "\t\t" << h_val2[i] << endl;
		}

		getchar();
		////////////////////////////////////////////////////////////////////////*/

	}

}


int KernelWrapperSparseRevisedMGPU::resetXValW(){
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nCol, nBlocks, nThreads);
	resetXVal(pGPU->nCol, nThreads, nBlocks, pGPU->val, pGPU->bounds, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

unsigned int KernelWrapperSparseRevisedMGPU::getLeavingVarIndex(row_result_t leavingVar){

	unsigned int outIdx;
	cutilSafeCallNoSync( cudaMemcpy( &outIdx, &pGPU->bInd[leavingVar.index], sizeof(unsigned int), cudaMemcpyDeviceToHost) );
	return outIdx;

}


void KernelWrapperSparseRevisedMGPU::checkInVar(red_result_t inVar){

	VAR_TYPE  redCost, seCoeff;

	cutilSafeCallNoSync( cudaMemcpy( &redCost, &d_redCost[inVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &seCoeff, &d_seCoeff[inVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	cout << "Var x"<< inVar.index << " ==> Red cost : " << redCost << " - Se coeff : " << seCoeff << endl;

}

void KernelWrapperSparseRevisedMGPU::checkPivoting(red_result_t inVar, row_result_t outVar, int outIdx){

	VAR_TYPE val, redCost, seCoeff;

	cutilSafeCallNoSync( cudaMemcpy( &val, &pGPU->val[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &redCost, &d_redCost[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &seCoeff, &d_seCoeff[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	cout << "out x"<< outIdx << " ==> Rc : " << redCost << " - SE : " << seCoeff << "- Val : "<< val << endl;
}

/*
void KernelWrapperSparseRevisedMGPU::resetSECoeff(){
	red_result_t idxCol;
	VAR_TYPE res[pGPU->nCol];

	for(unsigned int i=0; i<pGPU->nCol; i++){
		idxCol.index = i;
		this->processColumn(idxCol);
		res[i] = cublasDnrm2(pGPU->nRow, d_enterCol, 1) + 1.0;
	}

	cutilSafeCallNoSync( cudaMemcpy(d_seCoeff, res, pGPU->nCol*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

}
*/

#endif
