//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * expandP2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEINVARREVISED_KERNEL_H_
#define _UPDATEINVARREVISED_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"


#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define OLD_PROCESSING				1
#define NEW_PROCESSING				2
#define PROCESSING_METHOD 			OLD_PROCESSING


extern "C"
bool isPow2(unsigned int x);

struct SharedMemory
{
	
	//carefull here
    __device__ inline operator       red_result_t*()
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }

    __device__ inline operator const red_result_t*() const
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }
};


template <unsigned int blockSize>
__global__ void
updateInVarRevised(int size, int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta, VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost, red_result_t *result)
{
	
	volatile red_result_t *sdata = SharedMemory();
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x * blockSize + threadIdx.x;
    unsigned int gridSize = blockSize*gridDim.x;
    
    // custom bool
    int isElligible, isBest, isIequalQ, isT1GreaterThanT2;
    // Term 1 and Term 2
    VAR_TYPE t1, t2;
    // Alpha beta, gamma ( <==> equiv. seCoeff)
    VAR_TYPE a, b, g;
    // xVal, objVal, bounds
    VAR_TYPE redCostVal, xVal, upperDist, lowerDist;
    float2 bound;
    // SE values
    VAR_TYPE seVal;
    
/*    
#if REDUCTION_TYPE == REDUCTION_LOGICAL
    int tmp, isActiveThread, isAlreadyMax;
#endif
*/
    
    // init result
    red_result_t myMax;
    myMax.index = -1;
    myMax.value = 0.0;
              
    while (i < size) {    

    	/*
    	 * First part : updating gamma and redCost
    	 */    	
    	// Load values
    	a = alpha[i];
    	redCostVal = redCost[i];
    	
    	// Check if the current index is equal to index
    	isIequalQ = (i == indexQ);
    	// Store i temporarily to free a register
    	sdata[tid].index = i;
    	
    	// Process the new redCost val
      	// redCostVal might be NAN.
      	// It must then return NaN. Which is the case. Must remain that way.
    	redCostVal = isIequalQ*newRedCostQ + (1-isIequalQ)*(redCostVal-(a*redCostQ));
    	
    	// Load i for the following operation
    	i = sdata[tid].index;
    	
    	// Store into global memory the new redCost value
    	redCost[i] = redCostVal;
    	
    	// Store redCost Val to free some memory
    	sdata[tid].value = redCostVal;
    	
    	// Load more val
    	b = beta[i];
    	g = seCoeff[i];    	
    	
    	// store i to free some registers
    	sdata[tid].index = i;
    	
#if PROCESSING_METHOD == OLD_PROCESSING

    	// Processing b = 2*a*b
    	b = 2*a*b;
    	// Processing  a = a^2
    	a = a*a;
    	
    	// Processing t1 = g - 2*a*b + a^2*g_q
    	t1 = g - b + a * seCoeffQ;
    	// Processing t2 = a^2 + 1;
    	t2 = a + 1.0;
    	    
#elif PROCESSING_METHOD == NEW_PROCESSING

    	/*t1 = 0;
    	t1 += -2*b;
    	t1 += a*seCoeffQ;
    	t1 *= a;
    	t1 += g;*/
    	t1 = g + a *(-2*b + a*seCoeffQ);

    	t2 = 10^-4;

#endif

    	// Check if the the term 1 is greater than the term t2
    	isT1GreaterThanT2 = (t1 > t2);
    	
    	/* Arithmetical "if" as follow 
    	 * if(isIequalQ){
    	 * 		seVal = newSEQ;
    	 * else
    	 * 		if(isT1GreaterThanT2)
    	 *			seVal = t1;
    	 *		else
    	 *			seVal = t2; 
    	 */
    	seVal = (isIequalQ*newSEQ) + ((1-isIequalQ)*(isT1GreaterThanT2*t1 + (1-isT1GreaterThanT2)*t2));
    	   
    	// Memorize seVal into shared data to free registers
    	redCostVal = sdata[tid].value;
    	sdata[tid].value = seVal;
    	
    	// Load i value for shared memory
    	i = sdata[tid].index;
    	
    	// Memorize seVal into the global memory    	
    	seCoeff[i] = seVal;
    	
    	/*
    	 * Second part : Getting the best SE coefficient
    	 */    	
    	// Load value required
      	xVal = x[i];
      	bound = bounds[i];
      	
      	// Store i into shared memory to free registers
      	sdata[tid].index = i;
    	
      	// Distance between upper bound and current variable value
      	upperDist = ((VAR_TYPE)bound.y) - xVal;
      	// Distance between lower bound and current variable value
      	lowerDist = xVal - ((VAR_TYPE)bound.x);
      	
      	// Is this variable elligible as entering variable ?     
      	// redCostVal and xVal (so upperDist and lowerDist) might be NAN.
      	// It must then return !isElligible. Which is the case. Must remain that way.
      	isElligible = ((redCostVal>EPS1) && (upperDist > EXPAND_DELTA_K)) || 
      			      ((redCostVal<-EPS1) && (lowerDist > EXPAND_DELTA_K));
      	
      	/* Arithmetical instruction corresponding to
      	 * if (isElligible)
      	 *  	seVal = abs(redCost)/sqrt(sdata[tid].value);
      	 * else
      	 * 		seVal = 0;
      	 */
      	seVal = isElligible * fabs(redCostVal)*rsqrt(sdata[tid].value);
      	
      	// Is this steepest edge coefficient the best found ?
      	// if (!elligible ==> seVal = 0) and can't be greater than myMax.value
      	isBest = (seVal > myMax.value);
      	
    	// Load i value for shared memory
    	i = sdata[tid].index;
      	
      	/* Arithmetical instruction corresponding to
      	 * if(isBest){
      	 * 		myMax.index = sdata[tid].index; // current index
      	 *  	myMax.value = seVal; 
      	 * } else { // not usefull, but we have to do it since we use arithmetical logic 
      	 * 		myMax.index = myMax.index;
      	 * 		myMax.value = myMax.value;
      	 * }
      	 */      	      	
      	myMax.index = isBest*i + (1-isBest)*myMax.index;
      	myMax.value = isBest*seVal + (1-isBest)*myMax.value;
      	   	
    	// Update the loop index        
      	i += gridSize;
    } 
    
    // Synchronize threads previous to applying the reduction
    SET(sdata[tid], myMax);
    __syncthreads();
   
/*
#if REDUCTION_TYPE == REDUCTION_LOGICAL
    tmp = blockSize;    
    while(tmp > 1){
    	tmp = tmp >> 1;
    	isActiveThread = tid < tmp;
    	isAlreadyMax = myMax.value > sdata[tid+tmp].value;
    	
    	myMax.index = isActiveThread*(isAlreadyMax*myMax.index + (1-isAlreadyMax)*sdata[tid+tmp].index); + (1-isActiveThread)*myMax.index;
    	myMax.value = isActiveThread*(isAlreadyMax*myMax.value + (1-isAlreadyMax)*sdata[tid+tmp].value) + (1-isActiveThread)*myMax.value;
    	
    	sdata[tid].index = myMax.index;
    	sdata[tid].value = myMax.value;
    }
    
#elif REDUCTION_TYPE == REDUCTION_STANDARD
*/
    
    // do reduction in shared mem
     if (blockSize >= 512) { if (tid < 256) { SET(myMax, MAX_R(myMax, sdata[tid + 256])); SET(sdata[tid], myMax);} __syncthreads(); } 
     if (blockSize >= 256) { if (tid < 128) { SET(myMax, MAX_R(myMax, sdata[tid + 128])); SET(sdata[tid], myMax);} __syncthreads(); } 
     if (blockSize >= 128) { if (tid <  64) { SET(myMax, MAX_R(myMax, sdata[tid + 64])); SET(sdata[tid], myMax);} __syncthreads(); } 
     
 #ifndef __DEVICE_EMULATION__
     if (tid < 32)
 #endif
     {
         if (blockSize >=  64) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 32])); EMUSYNC; }
         if (blockSize >=  32) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 16])); EMUSYNC; }
         if (blockSize >=  16) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 8])); EMUSYNC; }
         if (blockSize >=   8) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 4])); EMUSYNC; }
         if (blockSize >=   4) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 2])); EMUSYNC; }
         if (blockSize >=   2) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 1])); EMUSYNC; }
     }
     
//#endif
     
     // write result for this block to global mem 
     if (tid == 0) {
         SET(result[blockIdx.x], sdata[0]);
     }
       
  
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateInVarRevised(int size, int threads, int blocks, int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta, VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost, red_result_t *result)
{
	dim3 dimBlock(threads, 1, 1);
	dim3 dimGrid(blocks, 1, 1);
	int smemSize = (threads <= 32) ? (2 * threads) * sizeof(red_result_t) : threads * sizeof(red_result_t);

	switch (threads)
	{
	case 512:
		updateInVarRevised< 512><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case 256:
		updateInVarRevised< 256><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case 128:
		updateInVarRevised< 128><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case 64:
		updateInVarRevised<  64><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case 32:
		updateInVarRevised<  32><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case 16:
		updateInVarRevised<  16><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case  8:
		updateInVarRevised<   8><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case  4:
		updateInVarRevised<   4><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case  2:
		updateInVarRevised<   2><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	case  1:
		updateInVarRevised<   1><<< dimGrid, dimBlock, smemSize >>>(size, indexQ, seCoeffQ, newSEQ, redCostQ, newRedCostQ, seCoeff, alpha, beta, x, bounds, redCost, result); break;
	}
    
}

#endif // #ifndef _UPDATEINVARREVISED_KERNEL_H_
