//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASISSIMPLE_KERNEL_H_
#define _UPDATEBASISSIMPLE_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define UB_BLOCK_DIM			32
#define UB_HALF_BLOCK_DIM		(32/2)

__global__ void
updateBasisSimpleK(VAR_TYPE alpha, VAR_TYPE *B, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_x = blockIdx.x;//unsigned int blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
	unsigned int blockIdx_y = blockIdx.y;//unsigned int blockIdx_y = blockIdx.x;
	
	__shared__ VAR_TYPE block[(2*UB_BLOCK_DIM)+1];
	
    unsigned int xIndex = blockIdx_x * UB_BLOCK_DIM + threadIdx.x;
    unsigned int offsetY = blockIdx_y * UB_BLOCK_DIM;
    unsigned int yIndex = offsetY + threadIdx.y;
    unsigned int inIndex = yIndex * pitch_B + xIndex;
    
    VAR_TYPE Bij_1;
    VAR_TYPE Bij_2;
    VAR_TYPE Bij_3;
    VAR_TYPE Bij_4;

    inIndex = yIndex * pitch_B + xIndex;
	Bij_1 = B[inIndex];
	inIndex = yIndex * pitch_B + (xIndex+UB_HALF_BLOCK_DIM);
	Bij_2 = B[inIndex];
	inIndex = (yIndex+UB_HALF_BLOCK_DIM) * pitch_B + xIndex;
	Bij_3 = B[inIndex];
	inIndex = (yIndex+UB_HALF_BLOCK_DIM) * pitch_B + (xIndex+UB_HALF_BLOCK_DIM);
	Bij_4 = B[inIndex];
    
	if(threadIdx.y == 0){
		block[threadIdx.x] = r[offsetY + threadIdx.x];
	} else if (threadIdx.y == 1) {
		block[threadIdx.x + UB_HALF_BLOCK_DIM] = r[offsetY + UB_HALF_BLOCK_DIM + threadIdx.x];
	} else if (threadIdx.y == 2){
		block[UB_BLOCK_DIM + 1 + threadIdx.x] = d[xIndex];	
	} else if (threadIdx.y == 3){
		block[UB_BLOCK_DIM + UB_HALF_BLOCK_DIM + 1 + threadIdx.x] = d[xIndex+UB_HALF_BLOCK_DIM];	
	}
	__syncthreads();
	
	Bij_1 += alpha * block[UB_BLOCK_DIM + 1 + threadIdx.x]*block[threadIdx.y];
	Bij_2 += alpha * block[UB_BLOCK_DIM + 1 + threadIdx.x + UB_HALF_BLOCK_DIM]*block[threadIdx.y];
	Bij_3 += alpha * block[UB_BLOCK_DIM + 1 + threadIdx.x]*block[threadIdx.y + UB_HALF_BLOCK_DIM];
	Bij_4 += alpha * block[UB_BLOCK_DIM + 1 + threadIdx.x + UB_HALF_BLOCK_DIM]*block[threadIdx.y + UB_HALF_BLOCK_DIM];
	
	
    inIndex = yIndex * pitch_B + xIndex;
	B[inIndex] = Bij_1;
	inIndex = yIndex * pitch_B + (xIndex+UB_HALF_BLOCK_DIM);
	B[inIndex] = Bij_2;
	inIndex = (yIndex+UB_HALF_BLOCK_DIM) * pitch_B + xIndex;
	B[inIndex] = Bij_3;
	inIndex = (yIndex+UB_HALF_BLOCK_DIM) * pitch_B + (xIndex+UB_HALF_BLOCK_DIM);
	B[inIndex] = Bij_4;
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisSimpleKernel(int threads, int blocks, 
						VAR_TYPE alpha, VAR_TYPE *B, unsigned int pitch_B, 
						VAR_TYPE *d, VAR_TYPE *r)
{

    dim3 dimBlock(threads, threads, 1);
    dim3 dimGrid(blocks, blocks, 1);
    int smemSize = ((2*UB_BLOCK_DIM)+1)*sizeof(VAR_TYPE);
    
	updateBasisSimpleK<<< dimGrid, dimBlock, smemSize >>>(alpha, B, pitch_B, d, r); 
  
}

#endif // #ifndef _UPDATEBASISSIMPLE_KERNEL_H_
