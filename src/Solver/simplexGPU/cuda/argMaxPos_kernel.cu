//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * argMaxPos_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _ARGMAXPOS_KERNEL_H_
#define _ARGMAXPOS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
    __device__ inline operator       red_result_t*()
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }

    __device__ inline operator const red_result_t*() const
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }
};


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
argMaxPos(int inc_g_idata, VAR_TYPE *g_idata, red_result_t *g_odata, unsigned int n)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
    
    volatile red_result_t *sdata = SharedMemory();

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    
    red_result_t myMax;
    myMax.index = -1;
    myMax.value = 0+EPS1;

    // we reduce multiple elements per thread.  The number is determined by the 
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n)
    {         
    	if(myMax.value < g_idata[i*inc_g_idata]){
    		myMax.index = i;
    		myMax.value = g_idata[i*inc_g_idata];
    	}
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n){
        	if(myMax.value < g_idata[(i+blockSize)*inc_g_idata]){
        		myMax.index = i+blockSize;
        		myMax.value = g_idata[(i+blockSize)*inc_g_idata];
        	}
        }
        i += gridSize;
    } 

    // each thread puts its local sum into shared memory 
    SET(sdata[tid], myMax);
    __syncthreads();


    // do reduction in shared mem
    if (blockSize >= 512) { if (tid < 256) { SET(myMax, MAX_R(myMax, sdata[tid + 256])); SET(sdata[tid], myMax);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
    if (blockSize >= 256) { if (tid < 128) { SET(myMax, MAX_R(myMax, sdata[tid + 128])); SET(sdata[tid], myMax);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
    if (blockSize >= 128) { if (tid <  64) { SET(myMax, MAX_R(myMax, sdata[tid + 64])); SET(sdata[tid], myMax);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid +  64]; } __syncthreads(); }
    
#ifndef __DEVICE_EMULATION__
    if (tid < 32)
#endif
    {
        if (blockSize >=  64) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 32])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
        if (blockSize >=  32) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 16])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
        if (blockSize >=  16) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 8])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
        if (blockSize >=   8) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 4])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
        if (blockSize >=   4) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 2])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
        if (blockSize >=   2) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 1])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
    }
    
    // write result for this block to global mem 
    if (tid == 0) {
        SET(g_odata[blockIdx.x], sdata[0]);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
argMaxPos(int size, int threads, int blocks, VAR_TYPE *d_idata, int inc_d_idata, red_result_t *d_odata)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(red_result_t) : threads * sizeof(red_result_t);

    // choose which of the optimized versions of reduction to launch
	
    if (isPow2(size))
    {
		switch (threads)
		{
		case 512:
			argMaxPos<512, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 256:
			argMaxPos<256, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 128:
			argMaxPos< 128, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 64:
			argMaxPos<  64, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 32:
			argMaxPos<  32, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 16:
			argMaxPos<  16, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  8:
			argMaxPos<   8, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  4:
			argMaxPos<   4, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  2:
			argMaxPos<   2, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  1:
			argMaxPos<   1, true><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		}
	}
	else
	{
		switch (threads)
		{
		case 512:
			argMaxPos< 512, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 256:
			argMaxPos< 256, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 128:
			argMaxPos< 128, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 64:
			argMaxPos<  64, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 32:
			argMaxPos<  32, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case 16:
			argMaxPos<  16, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  8:
			argMaxPos<   8, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  4:
			argMaxPos<   4, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  2:
			argMaxPos<   2, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		case  1:
			argMaxPos<   1, false><<< dimGrid, dimBlock, smemSize >>>(inc_d_idata, d_idata, d_odata, size); break;
		}
	}     
    
}


template <unsigned int blockSize, bool nIsPow2>
__global__ void
argMaxPosNextSteps(red_result_t *g_idata, red_result_t *g_odata, unsigned int n)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
	//extern __shared__ red_result_t __smem_d[]; 
    //volatile red_result_t *sdata = (red_result_t*)__smem_d;
    red_result_t *sdata = SharedMemory();
    
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    
    red_result_t myMax;
    myMax.index = -1;
    myMax.value = 0;

    // we reduce multiple elements per thread.  The number is determined by the 
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n)
    {         
    	SET(myMax, MAX_R(myMax, g_idata[i]));
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n){
        	SET(myMax, MAX_R(myMax, g_idata[i+blockSize]));
        }
        i += gridSize;
    }

    // each thread puts its local sum into shared memory 
    SET(sdata[tid], myMax);
    __syncthreads();


    // do reduction in shared mem
    if (blockSize >= 512) { if (tid < 256) { SET(myMax, MAX_R(myMax, sdata[tid + 256])); SET(sdata[tid], myMax);} __syncthreads(); } 
    if (blockSize >= 256) { if (tid < 128) { SET(myMax, MAX_R(myMax, sdata[tid + 128])); SET(sdata[tid], myMax);} __syncthreads(); } 
    if (blockSize >= 128) { if (tid <  64) { SET(myMax, MAX_R(myMax, sdata[tid + 64])); SET(sdata[tid], myMax);} __syncthreads(); }
    
#ifndef __DEVICE_EMULATION__
    if (tid < 32)
#endif
    {
        if (blockSize >=  64) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 32])); EMUSYNC; }
        if (blockSize >=  32) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 16])); EMUSYNC; }
        if (blockSize >=  16) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 8])); EMUSYNC; }
        if (blockSize >=   8) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 4])); EMUSYNC; }
        if (blockSize >=   4) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 2])); EMUSYNC; }
        if (blockSize >=   2) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 1])); EMUSYNC; }
    }
    
    // write result for this block to global mem 
    if (tid == 0) 
        SET(g_odata[blockIdx.x], sdata[0]);
}


////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
argMaxPosNextSteps(int size, int threads, int blocks, red_result_t *d_idata, red_result_t *d_odata)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(red_result_t) : threads * sizeof(red_result_t);

    // choose which of the optimized versions of reduction to launch
    
    if (isPow2(size))
    {
		switch (threads)
		{
		case 512:
			argMaxPosNextSteps<512, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 256:
			argMaxPosNextSteps<256, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 128:
			argMaxPosNextSteps< 128, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 64:
			argMaxPosNextSteps<  64, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 32:
			argMaxPosNextSteps<  32, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 16:
			argMaxPosNextSteps<  16, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  8:
			argMaxPosNextSteps<   8, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  4:
			argMaxPosNextSteps<   4, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  2:
			argMaxPosNextSteps<   2, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  1:
			argMaxPosNextSteps<   1, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		}
	}
	else
	{
		switch (threads)
		{
		case 512:
			argMaxPosNextSteps< 512, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 256:
			argMaxPosNextSteps< 256, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 128:
			argMaxPosNextSteps< 128, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 64:
			argMaxPosNextSteps<  64, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 32:
			argMaxPosNextSteps<  32, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case 16:
			argMaxPosNextSteps<  16, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  8:
			argMaxPosNextSteps<   8, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  4:
			argMaxPosNextSteps<   4, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  2:
			argMaxPosNextSteps<   2, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		case  1:
			argMaxPosNextSteps<   1, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size); break;
		}
	}     
    
}

#endif // #ifndef _ARGMAXPOS_KERNEL_H_
