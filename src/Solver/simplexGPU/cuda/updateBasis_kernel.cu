//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasis_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASIS_KERNEL_H_
#define _UPDATEBASIS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

struct SharedMemory
{
	
	//carefull here
    __device__ inline operator       VAR_TYPE*()
    {
        extern __shared__ VAR_TYPE __smem[];
        return (VAR_TYPE*)__smem;
    }

    __device__ inline operator const VAR_TYPE*() const
    {
        extern __shared__ VAR_TYPE __smem[];
        return (VAR_TYPE*)__smem;
    }
};

/*
	Updating the basis
*/
template <unsigned int blockSize>
__global__ void
updateBasisKernel(int nBasic, unsigned int leave, VAR_TYPE dl, VAR_TYPE *B, unsigned int pitch_B, VAR_TYPE *d)
{
    volatile VAR_TYPE *sdata = SharedMemory();        
	unsigned int bId = blockIdx.x;
	unsigned int tId = threadIdx.x;
	unsigned int colStart = bId*pitch_B;
	unsigned int isNotLeaveRow;
	
	VAR_TYPE Bij, di, B2ij;
	
	// unroll first loop for better performance
	if(tId < nBasic){
		
		// load di and Bij
		di = d[tId];
		Bij = B[colStart+tId];
			
		// First thread load Blj so it can be broadcasted via shared memory to each threads
		if(tId == 0){
			sdata[0] = B[colStart+leave] / dl;		
		}
		__syncthreads();
		
		// 1 if tId != leave else 0
		isNotLeaveRow = (tId != leave);
		// load Blj / dl
		B2ij = sdata[0];
		// multiply by -di if "is not leave row" else by 1.0
		B2ij *= (-di*isNotLeaveRow + 1.0*(1-isNotLeaveRow));
		// Add Bij if "is not leave row"
		B2ij += isNotLeaveRow * Bij;
		/*if(fabs(B2ij) < 1e-13)
			B2ij = 0;*/
		
		// store into Bij 
		B[colStart+tId] = B2ij;
		
		// tId += nbThread per block, to process a new data
		tId += blockSize;
	}	
				
	while(tId < nBasic){
		
		// load di and Bij
		di = d[tId];
		Bij = B[colStart+tId];
		
		// 1 if tId != leave else 0
		isNotLeaveRow = (tId != leave);
		// load Blj / dl
		B2ij = sdata[0];
		// multiply by -di if "is not leave row" else by 1.0
		B2ij *= (-di*isNotLeaveRow + 1.0*(1-isNotLeaveRow));
		// Add Bij if "is not leave row"
		B2ij += isNotLeaveRow * Bij;			
		/*if(fabs(B2ij) < 1e-13)
			B2ij = 0;*/
		// store into Bij if "is not leave row" else store Bij
		B[colStart+tId] = B2ij;

		
		// tId += nbThread per block, to process a new data
		tId += blockSize;		
	}				
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisKernel(int threads, int blocks, 
				  int nBasic, unsigned int leave, 
				  VAR_TYPE dl, VAR_TYPE *B, 
				  unsigned int pitch_B, VAR_TYPE *d)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? (1 + 2 * threads) * sizeof(VAR_TYPE) : (1 + threads) * sizeof(VAR_TYPE);

    
	switch (threads)
	{
	case 512:
	    updateBasisKernel<512><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case 256:
	    updateBasisKernel<256><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case 128:
	    updateBasisKernel<128><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case 64:
		updateBasisKernel< 64><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case 32:
		updateBasisKernel< 32><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case 16:
		updateBasisKernel< 16><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case  8:
		updateBasisKernel<  8><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case  4:
		updateBasisKernel<  4><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case  2:
		updateBasisKernel<  2><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	case  1:
		updateBasisKernel<  1><<< dimGrid, dimBlock, smemSize >>>(nBasic, leave, dl, B, pitch_B, d); 
	    break;
	}
  
}

void
updateBasisKernel(cudaStream_t stream,
				  int threads, int blocks,
				  int nBasic, unsigned int leave,
				  VAR_TYPE dl, VAR_TYPE *B,
				  unsigned int pitch_B, VAR_TYPE *d)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? (1 + 2 * threads) * sizeof(VAR_TYPE) : (1 + threads) * sizeof(VAR_TYPE);


	switch (threads)
	{
	case 512:
	    updateBasisKernel<512><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case 256:
	    updateBasisKernel<256><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case 128:
	    updateBasisKernel<128><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case 64:
		updateBasisKernel< 64><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case 32:
		updateBasisKernel< 32><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case 16:
		updateBasisKernel< 16><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case  8:
		updateBasisKernel<  8><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case  4:
		updateBasisKernel<  4><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case  2:
		updateBasisKernel<  2><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	case  1:
		updateBasisKernel<  1><<< dimGrid, dimBlock, smemSize, stream >>>(nBasic, leave, dl, B, pitch_B, d);
	    break;
	}

}

#endif // #ifndef _UPDATEBASIS_KERNEL_H_
