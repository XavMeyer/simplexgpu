//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * cleanXVal_kernel.cu
 *
 *  Created on: Fevr, 2012
 *      Author: meyerx
 */

#ifndef _CLEANXVAL_KERNEL_H_
#define _CLEANXVAL_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);

/*
	Returning a vector containing xVal value with NaN replaced by 0.
*/
template <unsigned int blockSize>
__global__ void
cleanXValK(unsigned int nCol, VAR_TYPE *val, VAR_TYPE *rowRes)
{
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
    double lVal;
    
	if(i < nCol){

		// Load slack val
		lVal = val[i];
		if(isnan(lVal)){
			lVal = 0.0;
		}
		rowRes[i] = lVal;	
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
cleanXVal(int threads, int blocks, unsigned int nCol, VAR_TYPE *val, VAR_TYPE *rowRes)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    
	switch (threads)
	{
		case 512:
			cleanXValK<512 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case 256:
			cleanXValK<256 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case 128:
			cleanXValK<128 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case 64:
			cleanXValK< 64 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case 32:
			cleanXValK< 32 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case 16:
			cleanXValK< 16 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case  8:
			cleanXValK<  8 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case  4:
			cleanXValK<  4 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case  2:
			cleanXValK<  2 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
		case  1:
			cleanXValK<  1 ><<< dimGrid, dimBlock, 0 >>>(nCol, val, rowRes); break;
	}

}

#endif // #ifndef _CLEANXVAL_KERNEL_H_
