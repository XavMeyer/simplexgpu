//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATE_FEASBWS_KERNEL_H_
#define _UPDATE_FEASBWS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
    __device__ inline operator       int*()
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }

    __device__ inline operator const int*() const
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }
};


/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
updateFeasBWS(unsigned int n, VAR_TYPE *bVal, float2 *bounds, float2 *boundsP1, VAR_TYPE *coeff, int *result)
{
	// now that we are using warp-synchronous programming (below)
	// we need to declare our shared memory volatile so that the compiler
	// doesn't reorder stores to it and induce incorrect behavior.   
	volatile int *sdata = SharedMemory();

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
	bool feasLo, feasUp, infUp, infLo;
    VAR_TYPE x, c;
    float2 bound, boundP1;
	float lo, up, loP1, upP1;
	int mySum = 0;
    
    while(i < n) {
    	
    	//Loading var
		x = bVal[i];
		bound = bounds[i];
		lo = bound.x;
		up = bound.y;
		boundP1 = boundsP1[i];
		loP1 = boundP1.x;
		upP1 = boundP1.y;
		
    	infLo = lo != loP1;
    	infUp = up != upP1;        	
    	
    	feasLo = x >= ((VAR_TYPE)lo-EXPAND_DELTA_K);
    	feasUp = x <= ((VAR_TYPE)up+EXPAND_DELTA_K);
    	
		if(feasLo && feasUp){
			mySum += (infLo || infUp);
			loP1 = lo;
			upP1 = up;
			c = - (VAR_TYPE)infUp + (VAR_TYPE)infLo;
		} else {     	// new			
			mySum -= !(infLo || infUp);
			loP1 = (float)!feasLo*-FLT_MAX + (float)feasLo*lo;
			upP1 = (float)!feasUp*FLT_MAX +  (float)feasUp*up;
			c = (VAR_TYPE)(!feasUp && !infUp) - (VAR_TYPE)(!feasLo && !infLo);
		}
		
    	boundP1.x = loP1;
    	boundP1.y = upP1;
		
		// Set the auxiliary basis coeff
		coeff[i] = c;//(feas && infLo) - (feas && infUp);
		// Set P1 bound
		boundsP1[i] = boundP1;
		
		if (nIsPow2 || i + blockSize < n)
		{
	    	//Loading var
			x = bVal[i + blockSize];
			bound = bounds[i + blockSize];
			lo = bound.x;
			up = bound.y;
			boundP1 = boundsP1[i + blockSize];
			loP1 = boundP1.x;
			upP1 = boundP1.y;
			
	    	infLo = lo != loP1;
	    	infUp = up != upP1;        	
	    	
	    	feasLo = x >= ((VAR_TYPE)lo-EXPAND_DELTA_K);
	    	feasUp = x <= ((VAR_TYPE)up+EXPAND_DELTA_K);
	    	
			if(feasLo && feasUp){
				mySum += (infLo || infUp);
				loP1 = lo;
				upP1 = up;
				c = - (VAR_TYPE)infUp + (VAR_TYPE)infLo;
			} else {     	// new			
				mySum -= !(infLo || infUp);
				loP1 = (float)!feasLo*-FLT_MAX + (float)feasLo*lo;
				upP1 = (float)!feasUp*FLT_MAX +  (float)feasUp*up;
				c = (VAR_TYPE)(!feasUp && !infUp) - (VAR_TYPE)(!feasLo && !infLo);
			}
			
	    	boundP1.x = loP1;
	    	boundP1.y = upP1;
			
			// Set the auxiliary basis coeff
			coeff[i + blockSize] = c; // (feas && infLo) - (feas && infUp);

			// Set P1 bound
			boundsP1[i + blockSize] = boundP1;
		}	
		i += gridSize;
    }
    
    // each thread puts its local sum into shared memory 
    sdata[tid] = mySum;
	__syncthreads();
	
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid + 64]; } __syncthreads(); }
		
	#ifndef __DEVICE_EMULATION__
	if (tid < 32)
	#endif
	{
		if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
		if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
		if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
		if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
		if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
		if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
	}
		
	// write result for this block to global mem 
	if (tid == 0) 
		 result[blockIdx.x] = sdata[0];
	
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateFeasBWS(int nEl, int threads, int blocks, VAR_TYPE *bVal, float2 *bounds, float2 *boundsP1, VAR_TYPE *coeff, int *result)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(int) : threads * sizeof(int);
   
    
    if (isPow2(nEl))
    {
		switch (threads)
		{
		case 512:
			updateFeasBWS<512, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 256:
			updateFeasBWS<256, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 128:
			updateFeasBWS<128, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 64:
			updateFeasBWS< 64, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 32:
			updateFeasBWS< 32, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 16:
			updateFeasBWS< 16, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  8:
			updateFeasBWS<  8, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  4:
			updateFeasBWS<  4, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  2:
			updateFeasBWS<  2, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  1:
			updateFeasBWS<  1, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		}
    } else {
		switch (threads)
		{
		case 512:
			updateFeasBWS<512, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 256:
			updateFeasBWS<256, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 128:
			updateFeasBWS<128, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 64:
			updateFeasBWS< 64, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 32:
			updateFeasBWS< 32, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 16:
			updateFeasBWS< 16, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  8:
			updateFeasBWS<  8, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  4:
			updateFeasBWS<  4, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  2:
			updateFeasBWS<  2, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  1:
			updateFeasBWS<  1, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		}
    }
  
}

#endif // #ifndef __KERNEL_H_
