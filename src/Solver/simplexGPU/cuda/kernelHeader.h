//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * parameters_kernel.h
 *
 *  Created on: Sep 4, 2010
 *      Author: meyerx
 */

#ifndef _KERNELHEADERMONO_H_
#define _KERNELHEADERMONO_H_


#include "../../utils/UniqueProfiler.h"
#include "../../SimplexProblem.h"
#include <math.h>
#include "../../Config.h"
#include "../../Types.h"

/***********************************************/
/****************** DEFINES ********************/
/***********************************************/
#define P2S(p)	(p/sizeof(VAR_TYPE))
// REDUCTIONS DEFINE

#define CUSPARSE_SUPPORTED 		1

#define MAX_THREADS 			256
#define MAX_BLOCKS				64
#define CPU_FINAL_REDUCTION		1
#define CPU_FINAL_THRESHOLD 	1

// MATRIX DEFINE
#define OPTI_CUBLAS 			3

#define NESTED_INIT 			0
#define NESTED_APPLY			1

#if PROFILING
#define _SYNC_KERNEL			cudaThreadSynchronize();
#else
#define _SYNC_KERNEL
#endif

/***********************************************/
/******** WRAPER KERNELS PROTOTYPE *************/
/***********************************************/
// This kernel returns the index of the maximum positive value of a vector
void 	argMaxPos(	int size, int threads, int blocks,
					VAR_TYPE *d_idata, int inc_d_idata,
					red_result_t *d_odata);
void 	argMaxPosNextSteps(	int size, int threads, int blocks,
							red_result_t *d_idata, red_result *d_odata);

// This kernel returns the index of the maximum absolute value of a vector
void 	argMaxAbs(	int size, int threads, int blocks,
					VAR_TYPE *d_idata, int inc_d_idata,
					red_result_t *d_odata);

void	argMaxAbsNB(	int size, int threads, int blocks,
						VAR_TYPE *obj, VAR_TYPE *x,
						float2 *bounds, red_result_t *result);

void	pSEValExpand3(	int m, int n, int threads, int blocks,
						VAR_TYPE *obj, int incObj,
						VAR_TYPE *eqs, int pitchEqs,
						VAR_TYPE *x, float2 *xBounds,
						VAR_TYPE *result);

void  	pSEValExpand(	int m, int n, int threads, int blocks,
						VAR_TYPE *obj, VAR_TYPE *eqs, int pitchEqs,
						VAR_TYPE *x, float2 *bounds,
						red_result_t *result);

void  	pSEValExpandSmall(	int m, int n, int threads, int blocks,
							VAR_TYPE *obj, VAR_TYPE *eqs, int pitchEqs,
							VAR_TYPE *x, float2 *bounds,
							red_result_t *result);

void	pSEValExpandT( 	int m, int n, int threads, int blocks,
						VAR_TYPE threshold, VAR_TYPE *obj,
						VAR_TYPE *eqs, int pitchEqs,
						VAR_TYPE *x, float2 *bounds, red_result_t *result);

// This kernel returns the index of the maximum positive value of a vector
void 	argMaxNestedInit(	int size, int threads, int blocks,
							VAR_TYPE *inData, int incInData,
							int *mask, red_result_t *d_odata);
void 	argMaxNestedApply(	int size, int threads, int blocks,
							VAR_TYPE *inData, int incInData,
							int *mask, red_result_t *d_odata);

// These kernels returns pivot row using the EXPAND method
void expandP1(	int size, int threads, int blocks,
				VAR_TYPE *pivots, VAR_TYPE *x,
				float2* bounds,
				float delta, int objSign,
				red_result_t *d_odata);

void expandP2(	int size, int threads, int blocks,
				VAR_TYPE *pivots, VAR_TYPE *x,
				float2* bounds,
				VAR_TYPE alpha,  int objSign,
				exp_result_t *d_odata);

void expandSpec(int size, int threads, int blocks,
		        VAR_TYPE *pivots, VAR_TYPE *x,
		        float2 *bounds,
		        float2 *boundsP1,
		        VAR_TYPE teta, int objSign,
		        exp_result_t *d_odata);

// WS (without slack)
void	checkFeasXWS(	int size, int threads, int blocks,
						VAR_TYPE *x, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *objAux, int *result);

void	checkFeasRevisedXWS(	int size, int threads, int blocks,
								VAR_TYPE *x, float2 *bounds,
								float2 *boundsP1,
								VAR_TYPE *objAux, int *result);

int 	checkFeasXWSW(int nEl);


void 	checkFeasBWS(	int nEl, int threads, int blocks,
						VAR_TYPE *bVal, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *coeff, int *result);

void 	checkFeasRevisedBWS(	int nEl, int threads, int blocks,
								VAR_TYPE *bVal, float2 *bounds,
								float2 *boundsP1,
								VAR_TYPE *coeff, int *result);

void 	updateFeasXWS(	int size, int threads, int blocks,
						VAR_TYPE *x, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *objAux, int *result);

void	updateFeasBWS(	int nEl, int threads, int blocks,
						VAR_TYPE *bVal, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *coeff, int *result);

void 	resetXVal(	int size, int threads, int blocks,
					VAR_TYPE *x, float2 *bounds,
					int *result);

void orderVals(	int nEl, int threads, int blocks,
				int nVar, VAR_TYPE *xVal, int *xInd,
				VAR_TYPE *bVal, int *basis,
				VAR_TYPE *xTemp, VAR_TYPE *bTemp);

void vectorMul(int nEl, int threads, int blocks, VAR_TYPE *x, VAR_TYPE *y);

void changeBound(	 int nEl, int threads, int blocks,
					 int nVar, int index, float2 bounds,
					 int *xInd, float2 *xBounds,
					 int *bInd, float2 *bBounds);


// Test

void	checkFeasXV2(	int size, int threads, int blocks,
						VAR_TYPE *x, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *objAux, int *result);

void 	checkFeasBV2(	int nEl, int threads, int blocks,
						VAR_TYPE *bVal, float2 *bounds,
						float2 *boundsP1,
						VAR_TYPE *coeff, int *result);

void    updateFeasV2( 	int nEl, int nThreads, int nBlocks,
						int nVar, VAR_TYPE *xVal, VAR_TYPE *bVal,
						float2 *xBounds, float2 *bBounds,
						int *result);

void	checkFeasRevisedXV2(	int size, int threads, int blocks,
								VAR_TYPE *x, float2 *bounds,
								float2 *boundsP1,
								VAR_TYPE *objAux, int *result);

void 	checkFeasRevisedBV2(	int nEl, int threads, int blocks,
								VAR_TYPE *bVal, float2 *bounds,
								float2 *boundsP1,
								VAR_TYPE *coeff, int *result);

void pivotingSEKernel(	int threads, int blocksX, int blocksY, int nRow, int nCol,
						VAR_TYPE alpha, VAR_TYPE *eqs, unsigned int pitch_eqs,
						VAR_TYPE *col, VAR_TYPE *row, VAR_TYPE *seCoeff);

/***********************************************************************/
/******************** Revised simplex kernels **************************/
/***********************************************************************/

void updateBasisKernel(	int threads, int blocks,
						int nBasic, unsigned int leave,
						VAR_TYPE dl, VAR_TYPE *B,
						unsigned int pitch_B, VAR_TYPE *d);

void updateBasisKernel(	cudaStream_t stream,
						int threads, int blocks,
						int nBasic, unsigned int leave,
						VAR_TYPE dl, VAR_TYPE *B,
						unsigned int pitch_B, VAR_TYPE *d);

void updateBasisV2Kernel(	int threads, int blocks,
							int nBasic, unsigned int leave,
							VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp,
							unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r);

void updateBasisV3Kernel(	int blocks,
							int nBasic, unsigned int leave,
							VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp,
							unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r);

void updateBasisSimpleKernel(	int threads, int blocks,
								VAR_TYPE alpha, VAR_TYPE *B, unsigned int pitch_B,
								VAR_TYPE *d, VAR_TYPE *r);

void updateBasisSimpleV3Kernel(	int blocks,
								VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTransp,
								unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r);

void updateBasisSTrKernel(	int threads, int blocks,
							VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTrans, unsigned int pitch_B,
							VAR_TYPE *d, VAR_TYPE *r);


void processSECoeffRevised( int m, int n, int threads, int blocks,
							VAR_TYPE *eqs, int pitchEqs, VAR_TYPE *result);

void updateSECoeffRevised( int size, int threads, int blocks,
		                   int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ,
		                   VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta,
		                   VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
		                   red_result_t *result);

void updateInVarRevised(	int size, int threads, int blocks,
							int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ,
							VAR_TYPE redCostQ, VAR_TYPE newRedCostQ,
							VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta,
							VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
							red_result_t *result);

void selectSECoeffRevised(int size, int threads, int blocks,
					      VAR_TYPE *seCoeff, VAR_TYPE *x, float2 *bounds,
					      VAR_TYPE *redCost, red_result_t *result);

void updateRCRevised(	int nEl, int threads, int blocks,
						int indQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ,
						VAR_TYPE *alpha, VAR_TYPE *redCost);

void approxInVarRevised(	int size, int threads, int blocks,
							int indexQ, VAR_TYPE pValPow2, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ,
							VAR_TYPE redCostQ, VAR_TYPE newRedCostQ,
							VAR_TYPE *seCoeff, VAR_TYPE *alpha,
							VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
							red_result_t *result);

void updateDVXInVarRevised(	int size, int threads, int blocks,
							int indexQ,  VAR_TYPE seCoeffQ, VAR_TYPE newSEQ,
							VAR_TYPE redCostQ, VAR_TYPE newRedCostQ,
							VAR_TYPE *seCoeff, VAR_TYPE *alpha,
							VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
							red_result_t *result);

/***********************************************************************/
/**************** Sparse revised simplex kernels ***********************/
/***********************************************************************/

void processSECoeffRevSparse(int threads, int blocks,
							 unsigned int nCol, unsigned int nColByBlock,
							 int *colPtrA, VAR_TYPE *sparseA,
							 VAR_TYPE *result);

void reinitBasis(	int threads, int blocks,
					bool inSite, unsigned int nBasic, unsigned int nVar,
					VAR_TYPE *val, unsigned int *bInd, VAR_TYPE *bVal,
					VAR_TYPE *rowRes, VAR_TYPE *colRes);

void cleanXVal(	int threads, int blocks,
			    unsigned int nCol, VAR_TYPE *val, VAR_TYPE *rowRes);

/********************************************************************************************/
/********** MultiGPU kernels (kernels with alpha processed before basis update) *************/
/********************************************************************************************/

void updateRCAqRevised(	int nEl, int threads, int blocks,
						int indQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE alphaQ,
						VAR_TYPE *alpha, VAR_TYPE *redCost);


void updateSECoeffAqRevised( 	int size, int threads, int blocks,
								int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ, VAR_TYPE alphaQ,
								VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta,
								VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
								red_result_t *result);

void updateInVarAqRevised(	int size, int threads, int blocks,
							int indexQ, VAR_TYPE seCoeffQ, VAR_TYPE newSEQ,
							VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE alphaQ,
							VAR_TYPE *seCoeff, VAR_TYPE *alpha, VAR_TYPE *beta,
							VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost,
							red_result_t *result);


#endif /* _KERNEL_H_ */
