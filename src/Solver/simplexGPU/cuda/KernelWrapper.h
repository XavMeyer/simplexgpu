//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * KernelWrapper.h
 *
 *  Created on: May 18, 2011
 *      Author: meyerx
 */

#ifndef KERNELWRAPPER_H_
#define KERNELWRAPPER_H_

#include <math.h>
#include "../IndexPivoting.h"
#include "../../SimplexSolution.h"
#include "../../SimplexProblem.h"
#include "../../Config.h"
#include "../../Types.h"
#include "../../utils/Logger.h"
#include "../../Error.h"

#include "../../simplex/SolverConfig.h"

#include "../SimplexProbGPU.h"

#include "kernelHeader.h"

class KernelWrapper {
public:

protected:
	Logger *logger;

	inline unsigned int nextPow2( unsigned int x ) {
	    --x;
	    x |= x >> 1;
	    x |= x >> 2;
	    x |= x >> 4;
	    x |= x >> 8;
	    x |= x >> 16;
	    return ++x;
	}

	inline void getNumBlocksAndThreads( int n, int &blocks, int &threads)
	{
		threads = (n < MAX_THREADS*2) ? nextPow2((n + 1)/ 2) : MAX_THREADS;
	    blocks = MIN(MAX_BLOCKS,(n + (threads * 2 - 1)) / (threads * 2));
	}
};

#endif /* KERNELWRAPPER_H_ */
