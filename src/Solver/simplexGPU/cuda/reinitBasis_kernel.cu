//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * reinitBasis_kernel.cu
 *
 *  Created on: Fevr, 2012
 *      Author: meyerx
 */

#ifndef _REINITBASIS_KERNEL_H_
#define _REINITBASIS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);

/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize, bool inSite>
__global__ void
reinitBasisK(unsigned int nBasic, unsigned int nVar, VAR_TYPE *val, unsigned int *bInd, VAR_TYPE *bVal, VAR_TYPE *rowRes, VAR_TYPE *colRes)
{
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
	VAR_TYPE lVal, lBVal;
	int lBInd;
    
	if(i < nBasic){

		// Load slack val
		lVal = val[i+nVar];

		// Load basis bVal, bInd
		lBInd = bInd[i];
		lBVal = bVal[i];

		// Store slack val and ind into basis
		colRes[i] = lVal;
		if(inSite){
			bInd[i] = i+nVar;
		}

		// Store NaN into slackVar
		/*if(!inSite){
			//rowRes[i+nVar] = 0;
		} else*/ 
		if(inSite) {
			rowRes[i+nVar] = lVal;
		}
		__syncthreads();

		// Store basis val into slack
		rowRes[lBInd] = lBVal;
		
		// Test
		// Store NaN into slackVar
		/*if(!inSite){
			rowRes[i+nVar] = 0;
			__syncthreads();
			if(lBInd < nVar){
				rowRes[lBInd] = lBVal;
			} else {
				colRes[lBInd-nVar] = lBVal;
			}
		} else {
			rowRes[i+nVar] = lVal;
			__syncthreads();
			rowRes[lBInd] = lBVal;

		}*/
	}

}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
reinitBasis(int threads, int blocks, bool inSite, unsigned int nBasic, unsigned int nVar, VAR_TYPE *val, unsigned int *bInd, VAR_TYPE *bVal, VAR_TYPE *rowRes, VAR_TYPE *colRes)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    
    if(inSite) {
		switch (threads)
		{
			case 512:
				reinitBasisK<512, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 256:
				reinitBasisK<256, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 128:
				reinitBasisK<128, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 64:
				reinitBasisK< 64, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 32:
				reinitBasisK< 32, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 16:
				reinitBasisK< 16, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  8:
				reinitBasisK<  8, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  4:
				reinitBasisK<  4, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  2:
				reinitBasisK<  2, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  1:
				reinitBasisK<  1, true><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
		}
    } else {

		switch (threads)
		{
			case 512:
				reinitBasisK<512, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 256:
				reinitBasisK<256, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 128:
				reinitBasisK<128, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 64:
				reinitBasisK< 64, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 32:
				reinitBasisK< 32, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case 16:
				reinitBasisK< 16, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  8:
				reinitBasisK<  8, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  4:
				reinitBasisK<  4, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  2:
				reinitBasisK<  2, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
			case  1:
				reinitBasisK<  1, false><<< dimGrid, dimBlock, 0 >>>(nBasic, nVar, val, bInd, bVal, rowRes, colRes); break;
		}
	}
}

#endif // #ifndef _REINITBASIS_KERNEL_H_
