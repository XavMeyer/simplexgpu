//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _PIVOTINGSE_KERNEL_H_
#define _PIVOTINGSE_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define PSE_BLOCK_DIM			32
#define PSE_HALF_BLOCK_DIM		(32/2)

__global__ void
pivotingSE_1_3(int nTile, int nRow, int nCol,  VAR_TYPE alpha, VAR_TYPE *eqs, unsigned int pitch_eqs, VAR_TYPE *col, VAR_TYPE *row, VAR_TYPE *seCoeff)
{
	unsigned int blockIdx_x = blockIdx.x;
	
	__shared__ VAR_TYPE block[PSE_HALF_BLOCK_DIM+6][PSE_HALF_BLOCK_DIM+1];
	
    unsigned int xIndex = threadIdx.x;
    unsigned int offsetY = blockIdx_x * PSE_BLOCK_DIM;
    unsigned int yIndex = offsetY + threadIdx.y;
    unsigned int inIndex;
    
    VAR_TYPE Bij_1, Bij_2, Bij_3, Bij_4;

	// Load Row_j
	if(threadIdx.y == 0){
		block[PSE_HALF_BLOCK_DIM+0][threadIdx.x] = alpha * row[offsetY + threadIdx.x];
	} else if (threadIdx.y == 1) {
		block[PSE_HALF_BLOCK_DIM+1][threadIdx.x] = alpha * row[offsetY + PSE_HALF_BLOCK_DIM + threadIdx.x];
	} else if(threadIdx.y == 2){
		block[PSE_HALF_BLOCK_DIM+4][threadIdx.x] = 0.0;
	} else if(threadIdx.y == 3){
		block[PSE_HALF_BLOCK_DIM+5][threadIdx.x] = 0.0;
	}
	__syncthreads();

	for(int i=0; i<nTile; i++){

		// Load Bij_1 and Bij_2
		inIndex = yIndex * pitch_eqs + xIndex;
		Bij_1 = eqs[inIndex];
		inIndex += PSE_HALF_BLOCK_DIM;
		Bij_2 = eqs[inIndex];

		// Load Col_i
		if (threadIdx.y == 0){
			block[PSE_HALF_BLOCK_DIM+2][threadIdx.x] = col[xIndex];
		} else if (threadIdx.y == 1){
			block[PSE_HALF_BLOCK_DIM+3][threadIdx.x] =  col[xIndex+PSE_HALF_BLOCK_DIM];
		}
		__syncthreads();

		// Process Bij_1' and store it
		Bij_1 += block[PSE_HALF_BLOCK_DIM+2][threadIdx.x]*block[PSE_HALF_BLOCK_DIM+0][threadIdx.y];		
		block[threadIdx.y][threadIdx.x] = Bij_1*Bij_1;
		inIndex = yIndex * pitch_eqs + xIndex;
		eqs[inIndex] = Bij_1;
		__syncthreads();
				
		// Process the SE Coefficient for 1st tile
		if(threadIdx.y == 0){
			for(int j=0; j<PSE_HALF_BLOCK_DIM; j++){
				block[PSE_HALF_BLOCK_DIM+4][threadIdx.x] += block[threadIdx.x][j];
			}
		}
		__syncthreads();

		// Load Bij_3
		inIndex = (PSE_HALF_BLOCK_DIM + yIndex) * pitch_eqs + xIndex;
		Bij_3 = eqs[inIndex];
		
		// Process Bij_2' and store it
		Bij_2 += block[PSE_HALF_BLOCK_DIM+3][threadIdx.x]*block[PSE_HALF_BLOCK_DIM+0][threadIdx.y];
		block[threadIdx.y][threadIdx.x] = Bij_2*Bij_2;
		inIndex = yIndex * pitch_eqs + xIndex + PSE_HALF_BLOCK_DIM;
		eqs[inIndex] = Bij_2;
		__syncthreads();

		// Process the SE Coefficient for 2nd tile
		if(threadIdx.y == 0){
			for(int j=0; j<PSE_HALF_BLOCK_DIM; j++){
				block[PSE_HALF_BLOCK_DIM+4][threadIdx.x] += block[threadIdx.x][j];
			}
		}
		__syncthreads();
		
		// Load Bij_4
		inIndex = (PSE_HALF_BLOCK_DIM + yIndex) * pitch_eqs + xIndex + PSE_HALF_BLOCK_DIM;
		Bij_4 = eqs[inIndex];	
		
		// Process Bij_3' and store it
		Bij_3 += block[PSE_HALF_BLOCK_DIM+2][threadIdx.x]*block[PSE_HALF_BLOCK_DIM+1][threadIdx.y];
		block[threadIdx.y][threadIdx.x] = Bij_3*Bij_3;
		inIndex = (PSE_HALF_BLOCK_DIM + yIndex) * pitch_eqs + xIndex;
		eqs[inIndex] = Bij_3;
		__syncthreads();

		// Process the SE Coefficient for 3rd tile
		if(threadIdx.y == 0){
			for(int j=0; j<PSE_HALF_BLOCK_DIM; j++){
				block[PSE_HALF_BLOCK_DIM+5][threadIdx.x] += block[threadIdx.x][j];
			}
		}
		__syncthreads();


		// Process Bij_4' and store it
		Bij_4 += block[PSE_HALF_BLOCK_DIM+3][threadIdx.x]*block[PSE_HALF_BLOCK_DIM+1][threadIdx.y];
		block[threadIdx.y][threadIdx.x] = Bij_4*Bij_4;
		inIndex += PSE_HALF_BLOCK_DIM;
		eqs[inIndex] = Bij_4;
		__syncthreads();
		
		// Process the SE Coefficient for 4th tile
		if(threadIdx.y == 0){
			for(int j=0; j<PSE_HALF_BLOCK_DIM; j++){
				block[PSE_HALF_BLOCK_DIM+5][threadIdx.x] += block[threadIdx.x][j];
			}
		}
		__syncthreads();

		xIndex += PSE_BLOCK_DIM;
		
	}

	if(threadIdx.y == 0){
		seCoeff[offsetY + threadIdx.x] = block[PSE_HALF_BLOCK_DIM+4][threadIdx.x]+1.0;
	} else if (threadIdx.y == 1 ){
		seCoeff[offsetY + PSE_HALF_BLOCK_DIM + threadIdx.x] = block[PSE_HALF_BLOCK_DIM+5][threadIdx.x]+1.0;
	}
}


__global__ void
pivotingSE_1_3_V2(VAR_TYPE alpha, VAR_TYPE *eqs, unsigned int pitch_eqs, VAR_TYPE *col, VAR_TYPE *row)
{
		unsigned int blockIdx_x = blockIdx.x;//unsigned int blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
		unsigned int blockIdx_y = blockIdx.y;//unsigned int blockIdx_y = blockIdx.x;

		__shared__ VAR_TYPE block[(2*PSE_BLOCK_DIM)+1];

	    unsigned int xIndex = blockIdx_x * PSE_BLOCK_DIM + threadIdx.x;
	    unsigned int offsetY = blockIdx_y * PSE_BLOCK_DIM;
	    unsigned int yIndex = offsetY + threadIdx.y;
	    unsigned int inIndex;// = yIndex * pitch_eqs + xIndex;

	    VAR_TYPE Bij_1;
	    VAR_TYPE Bij_2;
	    VAR_TYPE Bij_3;
	    VAR_TYPE Bij_4;

	    inIndex = yIndex * pitch_eqs + xIndex;
		Bij_1 = eqs[inIndex];
		inIndex = yIndex * pitch_eqs + (xIndex+PSE_HALF_BLOCK_DIM);
		Bij_2 = eqs[inIndex];
		inIndex = (yIndex+PSE_HALF_BLOCK_DIM) * pitch_eqs + xIndex;
		Bij_3 = eqs[inIndex];
		inIndex = (yIndex+PSE_HALF_BLOCK_DIM) * pitch_eqs + (xIndex+PSE_HALF_BLOCK_DIM);
		Bij_4 = eqs[inIndex];

		if(threadIdx.y == 0){
			block[threadIdx.x] = alpha*row[offsetY + threadIdx.x];
		} else if (threadIdx.y == 1) {
			block[threadIdx.x + PSE_HALF_BLOCK_DIM] = alpha*row[offsetY + PSE_HALF_BLOCK_DIM + threadIdx.x];
		} else if (threadIdx.y == 2){
			block[PSE_BLOCK_DIM + 1 + threadIdx.x] = col[xIndex];
		} else if (threadIdx.y == 3){
			block[PSE_BLOCK_DIM + PSE_HALF_BLOCK_DIM + 1 + threadIdx.x] = col[xIndex+PSE_HALF_BLOCK_DIM];
		}
		__syncthreads();

		Bij_1 += block[PSE_BLOCK_DIM + 1 + threadIdx.x]*block[threadIdx.y];
		Bij_2 += block[PSE_BLOCK_DIM + 1 + threadIdx.x + PSE_HALF_BLOCK_DIM]*block[threadIdx.y];
		Bij_3 += block[PSE_BLOCK_DIM + 1 + threadIdx.x]*block[threadIdx.y + PSE_HALF_BLOCK_DIM];
		Bij_4 += block[PSE_BLOCK_DIM + 1 + threadIdx.x + PSE_HALF_BLOCK_DIM]*block[threadIdx.y + PSE_HALF_BLOCK_DIM];


	    inIndex = yIndex * pitch_eqs + xIndex;
		eqs[inIndex] = Bij_1;
		inIndex = yIndex * pitch_eqs + (xIndex+PSE_HALF_BLOCK_DIM);
		eqs[inIndex] = Bij_2;
		inIndex = (yIndex+PSE_HALF_BLOCK_DIM) * pitch_eqs + xIndex;
		eqs[inIndex] = Bij_3;
		inIndex = (yIndex+PSE_HALF_BLOCK_DIM) * pitch_eqs + (xIndex+PSE_HALF_BLOCK_DIM);
		eqs[inIndex] = Bij_4;


}





////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
pivotingSEKernel(int threads, int blocksX, int blocksY, int nRow, int nCol,
					 VAR_TYPE alpha, VAR_TYPE *eqs, unsigned int pitch_eqs,
					 VAR_TYPE *col, VAR_TYPE *row, VAR_TYPE *seCoeff)
{

    dim3 dimBlock(threads, threads, 1);
    dim3 dimGrid(blocksY, 1, 1);
    int smemSize = (PSE_HALF_BLOCK_DIM+6) * (PSE_HALF_BLOCK_DIM+1) * sizeof(VAR_TYPE);
    
    pivotingSE_1_3<<< dimGrid, dimBlock, smemSize >>>(blocksX, nRow, nCol, alpha, eqs, pitch_eqs, col, row, seCoeff);
    //pivotingSE_1_3_V2<<< dimGrid, dimBlock, smemSize >>>(alpha, eqs, pitch_eqs, col, row);
}

#endif // #ifndef _PIVOTINGSE_KERNEL_H_
