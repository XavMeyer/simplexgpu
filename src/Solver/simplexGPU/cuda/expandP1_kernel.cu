//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * expandP1_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _EXPANDP1_KERNEL_H_
#define _EXPANDP1_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

struct SharedMemory
{
    __device__ inline operator       red_result_t*()
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }

    __device__ inline operator const red_result_t*() const
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }
};

extern "C"
bool isPow2(unsigned int x);


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize, bool nIsPow2, int objSign>
__global__ void
expandP1(int size, VAR_TYPE *pivots, VAR_TYPE *x, float2 *bounds, float delta, red_result_t *d_odata)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
	//extern __shared__ red_result_t __smem_d[]; 
   //volatile red_result_t *sdata = (red_result_t*)__smem_d;
    
	volatile red_result_t *sdata = SharedMemory();

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    
    bool case1, case2;
    VAR_TYPE p, xVal;//ratio, xVal;
    float2 bound;
   // float up, lo;
    red_result_t myRatio;
    myRatio.index = -1;
    myRatio.value = MAX_TYPE;

    // we reduce multiple elements per thread.  The number is determined by the 
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < size)
    {         
    	bound = bounds[i];
    	xVal = x[i];
    	p = (VAR_TYPE)objSign * pivots[i];
    	
    	case1 = (p >  EPS2) && (bound.x > -FLT_MAX);// && (xVal > (lo-delta));
       	case2 = (p < -EPS2) && (bound.y < FLT_MAX);// && (xVal < (up+delta));
    	
    	if(case1)
    		xVal = (xVal-(VAR_TYPE)(bound.x - delta))/p;
    	else if(case2)
    		xVal = (xVal-(VAR_TYPE)(bound.y + delta))/p;
    	
    	if(xVal < myRatio.value && (case1 || case2)){
    		myRatio.index = i;
    		myRatio.value = xVal;
    	}
    	
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < size){
        	bound = bounds[i + blockSize];
        	xVal = x[i + blockSize];
            p = (VAR_TYPE)objSign * pivots[i + blockSize];
            	
           	case1 = (p >  EPS2) && (bound.x > -FLT_MAX);// && (xVal > (lo-delta));
           	case2 = (p < -EPS2) && (bound.y < FLT_MAX);// && (xVal < (up+delta));
            	
           	if(case1)
           		xVal = (xVal - (VAR_TYPE)(bound.x - delta))/p;
           	else if(case2)
           		xVal = (xVal - (VAR_TYPE)(bound.y + delta))/p;
            	
           	if(xVal < myRatio.value && (case1 || case2)){
        		myRatio.index = i + blockSize;
        		myRatio.value = xVal;
        	}
        }
        i += gridSize;
    } 

    // each thread puts its local sum into shared memory 
    SET(sdata[tid], myRatio);
    __syncthreads();


    // do reduction in shared mem
    if (blockSize >= 512) { if (tid < 256) { SET(myRatio, MIN_R(myRatio, sdata[tid + 256])); SET(sdata[tid], myRatio);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
    if (blockSize >= 256) { if (tid < 128) { SET(myRatio, MIN_R(myRatio, sdata[tid + 128])); SET(sdata[tid], myRatio);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
    if (blockSize >= 128) { if (tid <  64) { SET(myRatio, MIN_R(myRatio, sdata[tid + 64])); SET(sdata[tid], myRatio);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid +  64]; } __syncthreads(); }
    
#ifndef __DEVICE_EMULATION__
    if (tid < 32)
#endif
    {
        if (blockSize >=  64) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 32])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
        if (blockSize >=  32) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 16])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
        if (blockSize >=  16) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 8])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
        if (blockSize >=   8) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 4])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
        if (blockSize >=   4) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 2])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
        if (blockSize >=   2) { SET(sdata[tid], MIN_R(sdata[tid], sdata[tid + 1])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
    }
    
    // write result for this block to global mem 
    if (tid == 0) {
        SET(d_odata[blockIdx.x], sdata[0]);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
expandP1(int size, int threads, int blocks, VAR_TYPE *pivots, VAR_TYPE *x, float2 *bounds, float delta, int objSign, red_result_t *d_odata)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(red_result_t) : threads * sizeof(red_result_t);

    // choose which of the optimized versions of reduction to launch
    if(objSign == -1)
    {
		if (isPow2(size))
		{
			switch (threads)
			{
			case 512:
				expandP1<512, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 256:
				expandP1<256, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 128:
				expandP1< 128, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 64:
				expandP1<  64, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 32:
				expandP1<  32, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 16:
				expandP1<  16, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  8:
				expandP1<   8, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  4:
				expandP1<   4, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  2:
				expandP1<   2, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  1:
				expandP1<   1, true, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			}
		}
		else
		{
			switch (threads)
			{
			case 512:
				expandP1< 512, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 256:
				expandP1< 256, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 128:
				expandP1< 128, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 64:
				expandP1<  64, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 32:
				expandP1<  32, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 16:
				expandP1<  16, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  8:
				expandP1<   8, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  4:
				expandP1<   4, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  2:
				expandP1<   2, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  1:
				expandP1<   1, false, -1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			}
		}     
    } else if(objSign == 1){
		if (isPow2(size))
		{
			switch (threads)
			{
			case 512:
				expandP1<512, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 256:
				expandP1<256, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 128:
				expandP1< 128, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 64:
				expandP1<  64, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 32:
				expandP1<  32, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 16:
				expandP1<  16, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  8:
				expandP1<   8, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  4:
				expandP1<   4, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  2:
				expandP1<   2, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  1:
				expandP1<   1, true, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			}
		}
		else
		{
			switch (threads)
			{
			case 512:
				expandP1< 512, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 256:
				expandP1< 256, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 128:
				expandP1< 128, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 64:
				expandP1<  64, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 32:
				expandP1<  32, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case 16:
				expandP1<  16, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  8:
				expandP1<   8, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  4:
				expandP1<   4, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  2:
				expandP1<   2, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			case  1:
				expandP1<   1, false, 1><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, delta, d_odata); break;
			}
		} 
    }
    
    
}

#endif // #ifndef _EXPANDP1_KERNEL_H_
