//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file KernelWrapperRevised.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#ifndef KERNELWRAPPERREVISED_H_
#define KERNELWRAPPERREVISED_H_

#define DBG_PRINT 				0

#define AN_ROW_WISE  			1
#define AN_COL_WISE  			2
#define AN_STORAGE				AN_ROW_WISE

#define SIMPLE
#define B_NORMAL					1
#define B_NORMAL_AND_TRANS			2
#define B_STORAGE					B_NORMAL


#define B_PAD_16				1
#define B_PAD_32				2
#define B_PADDING				B_PAD_32

#include "KernelWrapper.h"
#include "../../RevisedFormProblem.h"

class KernelWrapperRevised : public KernelWrapper {
public:
	KernelWrapperRevised(Logger *inL);
	KernelWrapperRevised(Logger *inL, int inDevice);
	KernelWrapperRevised(Logger *inL, bool noInit);
	virtual ~KernelWrapperRevised();


	// Init functions
	void 	init();

	void 	initCuda();
	void 	deInitCuda();

	void    initProblemStructure(RevisedFormProblem *inP);
	void 	deInitProblemStructure();

	void 	initMethodStructure();
	void 	deInitMethodStructure();

	void 	initSwap();
	void 	deInitSwap();

	void	initResult();
	void	deInitResult();

	void	loadProblem(RevisedFormProblem *inP, bool isBasisID);

	// "Reverse processing"
	void 	processReducedCost();
	//void 	processReducedCost(red_result_t inVar);
	void 	processReducedCostP1();
	void 	processColumn(red_result_t enteringVar);

	// find column
	red_result_t findCol();

	// find leaving
	row_result_t expandFindRow(red_result_t enteringVar, double delta, double tau);
	row_result_t expandFindRowP1(red_result_t enteringVar, double delta, double tau);

	// Pivoting
	void 	updateVariables(red_result_t enteringVar, row_result_t leavingVar);
	void	updateBasis(row_result_t leavingVar);
	void	swapVariables(red_result_t enteringVar, row_result_t leavingVar);
	void	swapVariablesP1(red_result_t enteringVar, row_result_t leavingVar);

	// Infeasibility check
	void checkInfeasibility(SimplexProblem *inP);
	void updateInfeasibility(SimplexProblem *inP);

	void checkInfeasibilityV2(SimplexProblem *inP);
	void updateInfeasibilityV2(SimplexProblem *inP);

	// Reinit problems
	void reinitProblem(RevisedFormProblem *inP);

	// Changing bounds
	void changeBoundWK(IndexPivoting *inI, int index, float2 bound); // No kernel involved (replace only)

	// Process optimum value (P1 too if necessary)
	void processOptimum(SimplexProblem *inP);

	// Resets bVal and nVal after expand
	void resetBounds();

	// Get the solution
	void getSolution(SimplexSolution *inSol);

	// Get the problem in its standard form
	void getDeviceData(SimplexProblem *inP);
	// Get the problem in its revised form
	void getDeviceData(RevisedFormProblem *inP);

	// return the current pivot value (should be called after expand)
	VAR_TYPE getPivotValue(row_result_t leavingVar);

	/* Steepest edge
	 * see " A new steepest edge approximation for the simplex method for linear programming"
	 * Artur Swietanowski, 1998 Computational Optimization and Applications vol 10
	 */
	void processSECoeff();
	void initDevexCoeff();
	void processBeta();
	VAR_TYPE getEnteringSECoeff();
	VAR_TYPE getZq(red_result_t enteringVar);
	VAR_TYPE getZqP1(red_result_t enteringVar);
	void processAlpha(row_result_t leavingVar);
	red_result_t updateSECoeff(red_result_t enteringVar, row_result_t leavingVar);
	red_result_t updateSECoeffP1(red_result_t enteringVar, row_result_t leavingVar);
	red_result_t selectSECoeff();
	red_result_t selectSECoeffP1();
	red_result_t updateInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);
	red_result_t updateInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);
	red_result_t approxInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);
    red_result_t approxInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);
    red_result_t devexInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);
    red_result_t devexInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);

	void updateRedCost(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq);

	void checkInVar(red_result_t inVar);
	void checkPivoting(red_result_t inVar, row_result_t outVar);

private:

	typedef KernelWrapper super;

#if PROFILING
	UniqueProfiler *prof;
#endif

	// Memorize if eqs is up to date
	bool eqsUpToDate, limitedMem;

	// Device number
	unsigned int device;
	// Device properties
	cudaDeviceProp devProp;

	// Revised form problem on GPU
	RevisedFormProblem *pGPU;

	// Matrices pitch
	size_t pitch_Ab, pitch_An, pitch_B, pitch_eqs;

	// Temporary structure on GPU
	VAR_TYPE *d_tmpCol, *d_tmpBasic, *d_tmpNonBasic, *d_z;
	// reduced cost and entering column vectors
	VAR_TYPE *d_redCost, *d_enterCol;
	// Steepest edge vectors
	VAR_TYPE *d_seCoeff, *d_beta, *d_alpha;

	// Full matrix buffer
	VAR_TYPE *d_eqs;

#if AN_STORAGE == AN_ROW_WISE
	unsigned int nBasicPad;
	VAR_TYPE *h_transpAn;
#endif

#if B_STORAGE == B_NORMAL_AND_TRANS
	VAR_TYPE *d_BTrans;
#endif

	// Swap
	int*			swpInt;
	float*			swpFlt;
	float2*			swpFlt2;
	double*			swpDbl;
	VAR_TYPE*		swpTyp;

	// Kernel results
	int*			hIRes;
	int*			dIRes;

	red_result_t* 	hRes;
	red_result_t* 	dRes;

	exp_result_t* 	hERes ;
	exp_result_t* 	dERes;

	// Init vector
	VAR_TYPE *devexInit;

#if DBG_PRINT
	VAR_TYPE *dbg_D_An, *dbg_D_Ab;
	VAR_TYPE *dbg_An, *dbg_Ab, *dbg_B;
	int *dbg_nInd, *dbg_bInd;
	size_t dbg_p_Ab, dbg_p_An;

	void initDbg(RevisedFormProblem *inP);
	void deInitDbg();
#endif

	void processColumns();

	/***********************************************/
	/********* KERNELS WRAPPER PROTOTYPE ***********/
	/***********************************************/
	// Kernel updating the basis (V2 should require less read on vector d)
	void updateBasisW(row_result_t leavingVar);
	void updateBasisV2W(row_result_t leavingVar, VAR_TYPE *bBis);
	void updateBasisCublas(row_result_t leavingVar);
	// These kernels returns pivot row using the EXPAND method
	red_result_t expandP1W(int nEl, float2* bounds, double delta, int objSign);
	exp_result_t expandP2W(int nEl, float2* bounds, double alpha, int objSign);
	exp_result_t expandSpecW(int nEl, double teta, int objSign);

	// Misc
	red_result_t argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1);
	red_result_t argMaxPosW(int nEl, VAR_TYPE* p1, int incP1);

	// Check feasability
	/* First version :
	 * These functions must be called at each iteration.
	 * It create a new objective function and change the P1 bounds such as
	 *
	 * 	If variable is Infeasible then
	 *		If lower infeasible then
	 *			objFunc = 1.0
	 *			boundP1 = [-inf, upper bound]
	 *		else
	 *			objFunc = -1.0
	 *			boundP1 = [lower bound, +inf]
	 *		end
	 *	end
	 */
	int checkFeasNonBasicW();
	int checkFeasBasicW();

	/** Second version :
	 * These functions (checkXXXX) must be called first.
	 * It create a new objective function and change the p1 bounds such as
	 *	If variable is Infeasible then
	 *		If lower infeasible then
	 *			objFunc = 1.0
	 *			boundP1 = [-inf, lower bound]
	 *		else
	 *			objFunc = -1.0
	 *			boundP1 = [upper bound, +inf]
	 *		end
	 *	end
	 *
	 * Then the updateFeasV2 function has to be called to check when there are no inf. variable.
	 * >>>>> This is done to prevent the objective function to change frequently <<<<<
	 */
	int checkFeasNonBasicV2W();
	int checkFeasBasicV2W();
	int updateFeasV2W();

	// Reinit
	void orderValsW();

	// Process the full array
	void processEqs();

	// Reset Non basic values after expand
	int resetXValW();

};

#endif /* KERNELWRAPPERREVISED_H_ */
