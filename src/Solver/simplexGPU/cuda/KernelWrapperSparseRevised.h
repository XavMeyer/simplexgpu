//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file KernelWrapperRevised.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#ifndef KERNELWRAPPERSPARSEREVISED_H_
#define KERNELWRAPPERSPARSEREVISED_H_

#include "KernelWrapper.h"

#if CUSPARSE_SUPPORTED
#define DBG_PRINT 				0

#define SIMPLE
#define B_NORMAL					1
#define B_NORMAL_AND_TRANS			2
#define B_STORAGE					B_NORMAL


#define B_PAD_16				1
#define B_PAD_32				2
#define B_PADDING				B_PAD_32

#include "../../SparseRevisedFormProblem.h"
#include <cusparse_v2.h>

class KernelWrapperSparseRevised : public KernelWrapper {
public:
	KernelWrapperSparseRevised(Logger *inL);
	KernelWrapperSparseRevised(Logger *inL, int inDevice);
	KernelWrapperSparseRevised(Logger *inL, bool noInit);
	virtual ~KernelWrapperSparseRevised();

	void    initProblemStructure(SparseRevisedFormProblem *inP);
	void 	deInitProblemStructure();

	void	loadProblem(SparseRevisedFormProblem *inP, bool isBasisID);

	void 	reinitProblem(SparseRevisedFormProblem *inP);
	void 	reinitProblemV2(SparseRevisedFormProblem *inP);

	void 	changeBoundWK(IndexPivoting *inI, int index, float2 bound);

	// Put variable to bounds and correct basis Val
	void 	resetBounds();
	void 	resetBoundsV2();

	// Get the solution
	void 	getSolution(SimplexSolution *inSol);

	// Process optimum value (P1 too if necessary)
	void 	processOptimum(SimplexProblem *inP);

	// Infeasability check
	void 	checkInfeasibilityV2(SimplexProblem *inP);
	void 	updateInfeasibilityV2(SimplexProblem *inP);

	// Projection through B^-1
	void 	processReducedCost();
	void 	processReducedCostP1();
	void 	processColumn(red_result_t enteringVar);

	// SE, Basis, etc update
	red_result_t selectSECoeff();
	red_result_t selectSECoeffP1();
	red_result_t updateSECoeff(red_result_t enteringVar, row_result_t leavingVar, unsigned int outVarIdx);
	red_result_t updateSECoeffP1(red_result_t enteringVar, row_result_t leavingVar, unsigned int outVarIdx);
	red_result_t updateInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq, unsigned int outVarIdx);
	red_result_t updateInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq, unsigned int outVarIdx);

	// return the current pivot value (should be called after expand)
	VAR_TYPE getPivotValue(row_result_t leavingVar);

	// return the SE Coeff of the entering column
	VAR_TYPE getEnteringSECoeff();

	// Process Zq
	VAR_TYPE getZq(red_result_t enteringVar);
	VAR_TYPE getZqP1(red_result_t enteringVar);

	// find leaving
	row_result_t expandFindRow(red_result_t enteringVar, double delta, double tau);
	row_result_t expandFindRowP1(red_result_t enteringVar, double delta, double tau);

	// Update basis kernel
	void	updateBasis(row_result_t leavingVar);

	// Process Row Alpha (leaving variable row post-update of B^-1)
	void processAlpha(row_result_t leavingVar);

	// Process Row Beta (for SE update)
	void processBeta();

	// Update the basic variable and the entering (non-basic, soon to be basic) variable
	void updateVariables(red_result_t enteringVar, row_result_t leavingVar);

	// Put enteringVar into the basis and remove it from the non basic.
	// Put back leavingVar into the non basic.
	//void	swapVariables(red_result_t enteringVar, row_result_t leavingVar);
	void 	swapVariables(red_result_t enteringVar, row_result_t leavingVar, unsigned int outNBIdx);
	void	swapVariablesP1(red_result_t enteringVar, row_result_t leavingVar, unsigned int outNBIdx);

	void getDeviceData(SparseRevisedFormProblem *inP);
	void getDeviceData(SimplexProblem *inP);

	// Init SE coefficient using matrix sparse A
	void initSECoeff();

	unsigned int getLeavingVarIndex(row_result_t leavingVar);


	void resetSECoeff();
	void processSECoeff();

	void checkInVar(red_result_t inVar);
	void checkPivoting(red_result_t inVar, row_result_t outVar, int outIdx);

private:

	typedef KernelWrapper super;

#if PROFILING
	UniqueProfiler *prof;
#endif

	// Memorize if eqs is up to date
	bool eqsUpToDate, limitedMem;

	// Device number
	unsigned int device;
	// Device properties
	cudaDeviceProp devProp;

	// Revised form problem on GPU
	SparseRevisedFormProblem *pGPU;

	// Matrices pitch
	size_t pitch_B;

	// Temporary structure on GPU
	VAR_TYPE *d_tmpCol, *d_tmpBasic, *d_tmpRow, *d_z;
	// reduced cost and entering column vectors
	VAR_TYPE *d_redCost, *d_enterCol;
	// Steepest edge vectors
	VAR_TYPE *d_seCoeff, *d_beta, *d_alpha;

	// Matrix sparseA in CSC format
	int *d_colPtrA;
	int *h_colPtrA;

	// Transpose of sparseA (row wise)
	/*VAR_TYPE *d_sparseAT;
	int *d_rowPtrAT;
	int *d_colIndAT;*/

	// cusparse related variable
	cusparseStatus_t status;
	cusparseHandle_t handleCUSP;
	cusparseMatDescr_t descrA;
	//cusparseMatDescr_t descrAT;

	//cusparseHybMat_t hybA;

#if B_STORAGE == B_NORMAL_AND_TRANS
	VAR_TYPE *d_BTrans;
#endif

	// Swap
	int*			swpInt;
	float*			swpFlt;
	float2*			swpFlt2;
	double*			swpDbl;
	VAR_TYPE*		swpTyp;

	// Kernel results
	int*			hIRes;
	int*			dIRes;

	red_result_t* 	hRes;
	red_result_t* 	dRes;

	exp_result_t* 	hERes ;
	exp_result_t* 	dERes;

	// Init vector
	VAR_TYPE *devexInit;

#if DBG_PRINT
	VAR_TYPE *dbg_D_An, *dbg_D_Ab;
	VAR_TYPE *dbg_An, *dbg_Ab, *dbg_B;
	int *dbg_nInd, *dbg_bInd;
	size_t dbg_p_Ab, dbg_p_An;

	void initDbg(RevisedFormProblem *inP);
	void deInitDbg();
#endif

	// Init functions
	void 	init();

	void 	initCuda();
	void 	deInitCuda();

	void 	initMethodStructure();
	void 	deInitMethodStructure();

	void 	initSwap();
	void 	deInitSwap();

	void	initResult();
	void	deInitResult();


	// Check correctness of a cusparse kernel execution
	void checkOp(cusparseStatus_t status);
	// Scatter the column Aq from matrix sparse A
	void getColAq(red_result_t enteringVar);

	/** Second version :
	 * These functions (checkXXXX) must be called first.
	 * It create a new objective function and change the p1 bounds such as
	 *	If variable is Infeasible then
	 *		If lower infeasible then
	 *			objFunc = 1.0
	 *			boundP1 = [-inf, lower bound]
	 *		else
	 *			objFunc = -1.0
	 *			boundP1 = [upper bound, +inf]
	 *		end
	 *	end
	 *
	 * Then the updateFeasV2 function has to be called to check when there are no inf. variable.
	 * >>>>> This is done to prevent the objective function to change frequently <<<<<
	 */
	int checkFeasNonBasicV2W();
	int checkFeasBasicV2W();
	int updateFeasV2W();

	// These kernels returns pivot row using the EXPAND method
	red_result_t expandP1W(int nEl, float2* bounds, double delta, int objSign);
	exp_result_t expandP2W(int nEl, float2* bounds, double alpha, int objSign);
	exp_result_t expandSpecW(int nEl, double teta, int objSign);

	// Misc
	red_result_t argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1);
	red_result_t argMaxPosW(int nEl, VAR_TYPE* p1, int incP1);

	// Kernel updating the basis (V2 should require less read on vector d)
	void updateBasisW(row_result_t leavingVar);
	void updateBasisV2W(row_result_t leavingVar, VAR_TYPE *bBis);
	void updateBasisCublas(row_result_t leavingVar);

	// Set back the original values of variable : val(1..nVar) ==> valX_i || val(nVar..nVar+nSlack) = NaN || bVal(1..nRow) = valX_(i+nVar)
	// If inSite is set to true, then the pGPU->xVal, pGPU->bVal, pGPU->bInd are modified
	// Else ordered val are only put into d_tmpRow and d_tmpCol without updating bInd. And val(nVar..nVar+nSlack) = valX_(i+nVar)
	void reinitBasisW(bool inSite);

	// Kernel that reset the xVal (set them into their bounds)
	int resetXValW();

	// Set xVal with NaN replaced by 0.0 into d_tmpRow
	void cleanXValW();

};

#endif
#endif /* KERNELWRAPPERSPARSEREVISED_H_ */
