//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * expandP2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _SELECTCOEFFREVISED_KERNEL_H_
#define _SELECTCOEFFREVISED_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"


#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define OPTI 		1

extern "C"
bool isPow2(unsigned int x);

struct SharedMemory
{
	
	//carefull here
    __device__ inline operator       red_result_t*()
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }

    __device__ inline operator const red_result_t*() const
    {
        extern __shared__ red_result_t __smem[];
        return (red_result_t*)__smem;
    }
};


template <unsigned int blockSize>
__global__ void
selectSECoeffRevised(int size, VAR_TYPE *seCoeff, VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost, red_result_t *result)
{
	
	volatile red_result_t *sdata = SharedMemory();
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x * blockSize + threadIdx.x;
    unsigned int gridSize = blockSize*gridDim.x;
    
    // custom bool
    int isElligible, isBest;
    // xVal, objVal, bounds
    VAR_TYPE redCostVal, xVal, upperDist, lowerDist;
    float2 bound;
    // SE values
    VAR_TYPE seVal;
   
    // init result
    red_result_t myMax;
    myMax.index = -1;
    myMax.value = 0.0;
              
    while (i < size) {    
    
    	/*
    	 * Processing the SE coefficient and keeping the best one
    	 */    	
    	// Load value required    	
    	redCostVal = redCost[i];
      	xVal = x[i];
      	bound = bounds[i];
      	sdata[tid].value  = seCoeff[i];
      	
    	// store i to free some registers
    	sdata[tid].index = i;
	
      	// Distance between upper bound and current variable value
      	upperDist = ((VAR_TYPE)bound.y) - xVal;
      	// Distance between lower bound and current variable value
      	lowerDist = xVal - ((VAR_TYPE)bound.x);
      	      	
#if 	OPTI      	
      	// Is this variable elligible as entering variable ?
      	// redCostVal and xVal (so upperDist and lowerDist) might be NAN.
      	// It must then return !isElligible. Which is the case. Must remain that way.
      	isElligible = ((redCostVal>EPS1) && (upperDist > EXPAND_DELTA_K)) || 
      			      ((redCostVal<-EPS1) && (lowerDist > EXPAND_DELTA_K));
      	
      	/* Arithmetical instruction corresponding to
      	 * if (isElligible)
      	 *  	seVal = abs(redCost)/sdata[tid].value;
      	 * else
      	 * 		seVal = 0;
      	 */
      	seVal = isElligible * fabs(redCostVal) * rsqrt(sdata[tid].value);
      	
      	// Is this steepest edge coefficient the best found ?
      	// if (!elligible ==> seVal = 0) and can't be greater than myMax.value
      	isBest = seVal > myMax.value;
      	
    	// Load i value for shared memory
    	i = sdata[tid].index;
      	
      	/* Arithmetical instruction corresponding to
      	 * if(isBest){
      	 * 		myMax.index = sdata[tid].index; // current index
      	 *  	myMax.value = seVal; 
      	 * } else { // not usefull, but we have to do it since we use arithmetical logic 
      	 * 		myMax.index = myMax.index;
      	 * 		myMax.value = myMax.value;
      	 * }
      	 */      	      	
      	myMax.index = isBest*i + (1-isBest)*myMax.index;
      	myMax.value = isBest*seVal + (1-isBest)*myMax.value;
#else      	   	
		//i = sdata[tid].index;
		if(((redCostVal>EPS1) && (upperDist > EXPAND_DELTA_K)) || 
		   ((redCostVal<-EPS1) && (lowerDist > EXPAND_DELTA_K))){
			seVal = fabs(redCostVal)*rsqrt(sdata[tid].value);
			if(seVal > myMax.value){				
				myMax.index = i;
				myMax.value = seVal;
			}
		}
#endif 
    	// Update the loop index        
      	i += gridSize;
    } 
    
    // Synchronize threads previous to applying the reduction

    SET(sdata[tid], myMax);
    __syncthreads();
    
    // do reduction in shared mem
     if (blockSize >= 512) { if (tid < 256) { SET(myMax, MAX_R(myMax, sdata[tid + 256])); SET(sdata[tid], myMax);} __syncthreads(); } 
     if (blockSize >= 256) { if (tid < 128) { SET(myMax, MAX_R(myMax, sdata[tid + 128])); SET(sdata[tid], myMax);} __syncthreads(); } 
     if (blockSize >= 128) { if (tid <  64) { SET(myMax, MAX_R(myMax, sdata[tid + 64])); SET(sdata[tid], myMax);} __syncthreads(); } 
     
 #ifndef __DEVICE_EMULATION__
     if (tid < 32)
 #endif
     {
         if (blockSize >=  64) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 32])); EMUSYNC; }
         if (blockSize >=  32) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 16])); EMUSYNC; }
         if (blockSize >=  16) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 8])); EMUSYNC; }
         if (blockSize >=   8) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 4])); EMUSYNC; }
         if (blockSize >=   4) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 2])); EMUSYNC; }
         if (blockSize >=   2) { SET(sdata[tid], MAX_R(sdata[tid], sdata[tid + 1])); EMUSYNC; }
     }
     
     // write result for this block to global mem 
     if (tid == 0) {
         SET(result[blockIdx.x], sdata[0]);
     }
       
  
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
selectSECoeffRevised(int size, int threads, int blocks, VAR_TYPE *seCoeff, VAR_TYPE *x, float2 *bounds, VAR_TYPE *redCost, red_result_t *result)
{
	dim3 dimBlock(threads, 1, 1);
	dim3 dimGrid(blocks, 1, 1);
	int smemSize = (threads <= 32) ? (2 * threads) * sizeof(red_result_t) : threads * sizeof(red_result_t);

	switch (threads)
	{
	case 512:
		selectSECoeffRevised< 512><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case 256:
		selectSECoeffRevised< 256><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case 128:
		selectSECoeffRevised< 128><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case 64:
		selectSECoeffRevised<  64><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case 32:
		selectSECoeffRevised<  32><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case 16:
		selectSECoeffRevised<  16><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case  8:
		selectSECoeffRevised<   8><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case  4:
		selectSECoeffRevised<   4><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case  2:
		selectSECoeffRevised<   2><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	case  1:
		selectSECoeffRevised<   1><<< dimGrid, dimBlock, smemSize >>>(size, seCoeff, x, bounds, redCost, result); break;
	}
    
}

#endif // #ifndef _SELECTCOEFFREVISED_KERNEL_H_
