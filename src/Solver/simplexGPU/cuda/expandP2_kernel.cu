//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * expandP2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _EXPANDP2_KERNEL_H_
#define _EXPANDP2_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

struct SharedMemory
{
    __device__ inline operator       exp_result_t*()
    {
        extern __shared__ exp_result_t __smem[];
        return (exp_result_t*)__smem;
    }

    __device__ inline operator const exp_result_t*() const
    {
        extern __shared__ exp_result_t __smem[];
        return (exp_result_t*)__smem;
    }
};

extern "C"
bool isPow2(unsigned int x);


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
expandP2(int size, VAR_TYPE *pivots, VAR_TYPE *x, float2 *bounds, VAR_TYPE alpha, int objSign, exp_result_t *d_odata)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
	//extern __shared__ red_result __smem_d[]; 
   //volatile red_result *sdata = (red_result*)__smem_d;
    
	volatile exp_result_t *sdata = SharedMemory();

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    
    
    bool case1, case2;
    VAR_TYPE p, ratio, xVal;
    float up, lo;
    float2 bound;
    exp_result_t myPivot;
    myPivot.index = -1;
    myPivot.absP = 0.0;
    myPivot.alpha = 0.0;
    
    // we reduce multiple elements per thread.  The number is determined by the 
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < size)
    {         
    	bound = bounds[i];
    	up = bound.y;//upper[i];
    	lo = bound.x;//lower[i];
    	xVal = x[i];
    	p = (VAR_TYPE)objSign * pivots[i];
    	
    	case1 = (p >  EPS2) && (lo > -FLT_MAX);// && (xVal > lo);
    	case2 = (p < -EPS2) && (up < FLT_MAX);// && (xVal < up);
    	
    	if(case1)
    		ratio = (xVal-(VAR_TYPE)lo)/p;
    	else if(case2)
    		ratio = (xVal-(VAR_TYPE)up)/p;
    	else
    		ratio = MAX_TYPE;
    	
    	if(ratio <= alpha && fabs(p) > myPivot.absP){
    		myPivot.index = i;
    	    myPivot.absP = abs(p);
    	    myPivot.alpha = ratio;
    	}
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < size){
        	bound = bounds[i + blockSize];
           	up = bound.y;//upper[i+blockSize];
            lo = bound.x;//lower[i+blockSize];
            xVal = x[i+blockSize];
            p = (VAR_TYPE)objSign * pivots[i+blockSize];
            	
           	case1 = (p >  EPS2) && (lo > -FLT_MAX);// && (xVal > lo);
           	case2 = (p < -EPS2) && (up < FLT_MAX);// && (xVal < up);
            	
           	if(case1)
           		ratio = (xVal-(VAR_TYPE)lo)/p;
           	else if(case2)
           		ratio = (xVal-(VAR_TYPE)up)/p;
           	else
           		ratio = MAX_TYPE;
            	
           	if(ratio <= alpha && abs(p) > myPivot.absP){
          		myPivot.index = i+blockSize;
            	myPivot.absP = abs(p);
            	myPivot.alpha = ratio;
        	}
        }
        i += gridSize;
    } 

    // each thread puts its local sum into shared memory 
    SET_E_P(sdata[tid], myPivot);
    __syncthreads();


    // do reduction in shared mem
    if (blockSize >= 512) { if (tid < 256) { SET_E_P(myPivot, MAX_E_P(myPivot, sdata[tid + 256])); SET_E_P(sdata[tid], myPivot);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
    if (blockSize >= 256) { if (tid < 128) { SET_E_P(myPivot, MAX_E_P(myPivot, sdata[tid + 128])); SET_E_P(sdata[tid], myPivot);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
    if (blockSize >= 128) { if (tid <  64) { SET_E_P(myPivot, MAX_E_P(myPivot, sdata[tid + 64])); SET_E_P(sdata[tid], myPivot);} __syncthreads(); } //sdata[tid] = mySum = mySum + sdata[tid +  64]; } __syncthreads(); }
    
#ifndef __DEVICE_EMULATION__
    if (tid < 32)
#endif
    {
        if (blockSize >=  64) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 32])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
        if (blockSize >=  32) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 16])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
        if (blockSize >=  16) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 8])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
        if (blockSize >=   8) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 4])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
        if (blockSize >=   4) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 2])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
        if (blockSize >=   2) { SET_E_P(sdata[tid], MAX_E_P(sdata[tid], sdata[tid + 1])); EMUSYNC; }//sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
    }
    
    // write result for this block to global mem 
    if (tid == 0) {
        SET_E_P(d_odata[blockIdx.x], sdata[0]);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
expandP2(int size, int threads, int blocks, VAR_TYPE *pivots, VAR_TYPE *x, float2 *bounds, VAR_TYPE alpha, int objSign, exp_result_t *d_odata)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(exp_result_t) : threads * sizeof(exp_result_t);

    // choose which of the optimized versions of reduction to launch
    if (isPow2(size))
    {
		switch (threads)
		{
		case 512:
			expandP2<512, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 256:
			expandP2<256, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 128:
			expandP2< 128, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 64:
			expandP2<  64, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 32:
			expandP2<  32, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 16:
			expandP2<  16, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  8:
			expandP2<   8, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  4:
			expandP2<   4, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  2:
			expandP2<   2, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  1:
			expandP2<   1, true><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		}
	}
	else
	{
		switch (threads)
		{
		case 512:
			expandP2< 512, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 256:
			expandP2< 256, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 128:
			expandP2< 128, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 64:
			expandP2<  64, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 32:
			expandP2<  32, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case 16:
			expandP2<  16, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  8:
			expandP2<   8, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  4:
			expandP2<   4, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  2:
			expandP2<   2, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		case  1:
			expandP2<   1, false><<< dimGrid, dimBlock, smemSize >>>(size, pivots, x, bounds, alpha, objSign, d_odata); break;
		}
	}     
    
}

#endif // #ifndef _EXPANDP2_KERNEL_H_
