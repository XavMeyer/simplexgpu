//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _CHECKFEASBW_KERNEL_H_
#define _CHECKFEASBW_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
    __device__ inline operator       int*()
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }

    __device__ inline operator const int*() const
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }
};


/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
checkFeasBWS(unsigned int n, VAR_TYPE *bVal, float2 *bounds, float2 *boundsP1, VAR_TYPE *coeff, int *result)
{

	// now that we are using warp-synchronous programming (below)
	// we need to declare our shared memory volatile so that the compiler
	// doesn't reorder stores to it and induce incorrect behavior.   
	volatile int *sdata = SharedMemory();

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
	bool infUp, infLo;
    VAR_TYPE b;
    float2 bound, boundP1; 
	float lo, up, loP1, upP1;
	int mySum = 0;
    
    
    while(i < n) {
    	
    	//Loading var
		b = bVal[i];
		bound = bounds[i];
		lo = bound.x;//bLo[i];
		up = bound.y;//bUp[i];
		
		// infeasible up
		infUp = b > ((VAR_TYPE)up+EXPAND_DELTA_K);
		// infeasible lo
		infLo = b < ((VAR_TYPE)lo-EXPAND_DELTA_K);
		
		// change the P1 lower bound if needed
		loP1 = lo*(!infLo) - FLT_MAX*(infLo);
		// change the P1 upper bound if needed
		upP1 = up*(!infUp) + FLT_MAX*(infUp);
		
		mySum += (infLo || infUp);
		
		// Set the auxiliary basis coeff
		coeff[i] = 1.0*(infUp) - 1.0*(infLo);
		
		// Set P1 bound
		boundP1.x = loP1;
		boundP1.y = upP1;
		boundsP1[i] = boundP1;
		//bLoP1[i] = loP1;
		//bUpP1[i] = upP1;
		
		if (nIsPow2 || i + blockSize < n)
		{
			//Loading var
			b = bVal[i + blockSize];
			bound = bounds[i+ blockSize];
			lo = bound.x;//bLo[i+ blockSize];
			up = bound.y;//bUp[i+ blockSize];
			
			// infeasible up
			infUp = b > ((VAR_TYPE)up+EXPAND_DELTA_K);
			// infeasible lo
			infLo = b < ((VAR_TYPE)lo-EXPAND_DELTA_K);
			
			// change the P1 lower bound if needed
			loP1 = lo*(!infLo) - FLT_MAX*(infLo);
			// change the P1 upper bound if needed
			upP1 = up*(!infUp) + FLT_MAX*(infUp);
			
			mySum += (infLo || infUp);
			
			// Set the auxiliary basis coeff
			coeff[i + blockSize] = 1.0*(infUp) - 1.0*(infLo);
			
			// Set P1 bound
			boundP1.x = loP1;
			boundP1.y = upP1;
			boundsP1[i + blockSize] = boundP1;
			//bLoP1[i + blockSize] = loP1;
			//bUpP1[i + blockSize] = upP1;
			
		}		
		i += gridSize;
    }
    
    // each thread puts its local sum into shared memory 
    sdata[tid] = mySum;
	__syncthreads();
	
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid + 64]; } __syncthreads(); }
		
	#ifndef __DEVICE_EMULATION__
	if (tid < 32)
	#endif
	{
		if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
		if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
		if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
		if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
		if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
		if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
	}
		
	// write result for this block to global mem 
	if (tid == 0) 
		 result[blockIdx.x] = sdata[0];
	
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
checkFeasBWS(int nEl, int threads, int blocks, VAR_TYPE *bVal, float2 *bounds, float2 *boundsP1, VAR_TYPE *coeff, int *result)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(int) : threads * sizeof(int);
   
    
    if (isPow2(nEl))
    {
		switch (threads)
		{
		case 512:
			checkFeasBWS<512, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 256:
			checkFeasBWS<256, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 128:
			checkFeasBWS<128, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 64:
			checkFeasBWS< 64, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 32:
			checkFeasBWS< 32, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 16:
			checkFeasBWS< 16, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  8:
			checkFeasBWS<  8, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  4:
			checkFeasBWS<  4, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  2:
			checkFeasBWS<  2, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  1:
			checkFeasBWS<  1, true><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		}
    } else {
		switch (threads)
		{
		case 512:
			checkFeasBWS<512, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 256:
			checkFeasBWS<256, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 128:
			checkFeasBWS<128, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 64:
			checkFeasBWS< 64, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 32:
			checkFeasBWS< 32, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case 16:
			checkFeasBWS< 16, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  8:
			checkFeasBWS<  8, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  4:
			checkFeasBWS<  4, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  2:
			checkFeasBWS<  2, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		case  1:
			checkFeasBWS<  1, false><<< dimGrid, dimBlock, smemSize >>>(nEl, bVal, bounds, boundsP1, coeff, result); break;
		}
    }
  
}

#endif // #ifndef __KERNEL_H_
