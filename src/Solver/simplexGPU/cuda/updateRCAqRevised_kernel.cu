//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateRCRevised_kernel.cu
 *
 *  Created on: Jan, 2012
 *      Author: meyerx
 */

#ifndef _UPDATERCAQREVISED_KERNEL_H_
#define _UPDATERCAQREVISED_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

template <unsigned int blockSize>
__global__ void
updateRCAqRevised(int nEl, int indQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE alphaQ, VAR_TYPE *alpha, VAR_TYPE *redCost)
{	
	
	VAR_TYPE alphaVal;
	VAR_TYPE redCostVal;
	
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
    unsigned int gridSize = blockSize * gridDim.x; 
    
    while(i < nEl){
    
    	alphaVal = alpha[i];
    	redCostVal = redCost[i];
    	
    	if(i != indQ){
    		// If i != q then we process the new value ==> Zi = Zi - ai*Zq
    		redCost[i] = redCostVal - ((alphaVal/alphaQ)*redCostQ);
    	} else {
    		// If i == q then when memorize the new value (newRedCostQ)
    		redCost[i] = newRedCostQ;
    	}
    	
    	i+=gridSize;
    }
    
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateRCAqRevised(int nEl, int threads, int blocks, int indQ, VAR_TYPE redCostQ, VAR_TYPE newRedCostQ, VAR_TYPE alphaQ, VAR_TYPE *alpha, VAR_TYPE *redCost)
{
	
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
   
    
	switch (threads)
	{
	case 512:
		updateRCAqRevised<512><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case 256:
		updateRCAqRevised<256><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case 128:
		updateRCAqRevised<128><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case 64:
		updateRCAqRevised< 64><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case 32:
		updateRCAqRevised< 32><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case 16:
		updateRCAqRevised< 16><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case  8:
		updateRCAqRevised<  8><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case  4:
		updateRCAqRevised<  4><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case  2:
		updateRCAqRevised<  2><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	case  1:
		updateRCAqRevised<  1><<< dimGrid, dimBlock, 0 >>>(nEl, indQ, redCostQ, newRedCostQ, alphaQ, alpha, redCost); break;
	}
  
}

#endif // #ifndef _UPDATERCAQREVISED_KERNEL_H_
