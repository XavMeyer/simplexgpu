//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _CHANGEBOUND_KERNEL_H_
#define _CHANGEBOUND_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);

/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize>
__global__ void
changeBound(int nEl, int nVar, int index, float2 bounds, int *xInd, float2 *xBounds, int *bInd, float2 *bBounds)
{
	
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
    
	while(i < nEl){
		if(i < nVar){
			if(xInd[i] == index){
				xBounds[i] = bounds;
			}
		} else {
			if(bInd[i-nVar] == index){
				bBounds[i-nVar] = bounds;
			}
		}		
		
		i += blockSize;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
changeBound(int nEl, int threads, int blocks, int nVar, int index, float2 bounds, int *xInd, float2 *xBounds, int *bInd, float2 *bBounds)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    
	switch (threads)
	{
	case 512:
	    changeBound<512><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case 256:
	    changeBound<256><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case 128:
	    changeBound<128><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case 64:
		changeBound< 64><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case 32:
		changeBound< 32><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case 16:
		changeBound< 16><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case  8:
		changeBound<  8><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case  4:
		changeBound<  4><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case  2:
		changeBound<  2><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	case  1:
		changeBound<  1><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, index, bounds, xInd, xBounds, bInd, bBounds); break;
	}
  
}

#endif // #ifndef _CHANGEBOUND_KERNEL_H_
