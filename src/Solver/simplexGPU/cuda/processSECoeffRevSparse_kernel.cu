//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * SECoeffRevSparse_kernel.cu
 *
 *  Created on: Fev, 2012
 *      Author: meyerx
 */

#ifndef _PROCESSSECOEFFREVSPARSE_KERNEL_H
#define _PROCESSSECOEFFREVSPARSE_KERNEL_H

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
	
	//carefull here
    __device__ inline operator       double*()
    {
        extern __shared__ double __smem[];
        return (double*)__smem;
    }

    __device__ inline operator const double*() const
    {
        extern __shared__ double __smem[];
        return (double*)__smem;
    }
};


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize>
__global__ void
processSECoeffRevSparse(const unsigned int nCol, const unsigned int nColByBlock, int *colPtrA, VAR_TYPE *sparseA, VAR_TYPE *result)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
    //volatile double *sdata = SharedMemory();
    __shared__ volatile double sdata[blockSize+1];
    __shared__ volatile int sptr[blockSize+1];
    
    int start, end, pos;
    unsigned int colId = 0;
    unsigned int tid = threadIdx.x;
    unsigned int colStart = blockIdx.x*nColByBlock;
    volatile double mySum;
    volatile double coeff1, coeff2, coeff3, coeff4;


	// First we load colPtr
	if((tid < (nColByBlock+1)) && ((colStart+tid) < nCol+1)){
		sptr[tid] = colPtrA[colStart+tid];
		if((tid == 0) && (nColByBlock == blockSize)){
			sptr[blockSize] = colPtrA[colStart+blockSize];
		}
	}
	__syncthreads();

	// We process column one by one
	while((colId < nColByBlock) && ((colId+colStart) < nCol)){

		mySum = 0;
		start = sptr[colId];
		end = sptr[colId+1];
		pos = start + tid;

		while(pos < end){

			if(pos + 3*blockSize < end){
				coeff1 = sparseA[pos];
				coeff2 = sparseA[pos+blockSize];
				coeff3 = sparseA[pos+2*blockSize];
				coeff4 = sparseA[pos+3*blockSize];
				mySum += coeff1*coeff1;
				mySum += coeff2*coeff2;
				mySum += coeff3*coeff3;
				mySum += coeff4*coeff4;
				pos += 4*blockSize;

			}

			if(pos + blockSize < end){
				coeff1 = sparseA[pos];
				coeff2 = sparseA[pos+blockSize];
				mySum += coeff1*coeff1;
				mySum += coeff2*coeff2;
				pos += 2*blockSize;

			}

			if(pos < end){
				coeff1 = sparseA[pos];
				mySum += coeff1*coeff1;
				pos += blockSize;
			}
		}

		sdata[tid] = mySum;
		__syncthreads();

		if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid +  64]; } __syncthreads(); }

		#ifndef __DEVICE_EMULATION__
		if (tid < 32)
		#endif
		{
			if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
			if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
			if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
			if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
			if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
			if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
		}

		// write result for this block to global mem
		if (tid == 0){
			result[colStart+colId] = mySum+1.0;
		}
		__syncthreads();

		colId++;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
processSECoeffRevSparse(int threads, int blocks, unsigned int nCol, unsigned int nColByBlock, int *colPtrA, VAR_TYPE *sparseA, VAR_TYPE *result) {
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? (2 * threads + 1) * (sizeof(double) + sizeof(int)) : (threads+1) * (sizeof(double) + sizeof(int));


    // choose which of the optimized versions of reduction to launch
	switch (threads)
	{
	case 512:
		processSECoeffRevSparse<512><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case 256:
		processSECoeffRevSparse<256><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case 128:
		processSECoeffRevSparse<128><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case 64:
		processSECoeffRevSparse< 64><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case 32:
		processSECoeffRevSparse< 32><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case 16:
		processSECoeffRevSparse< 16><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case  8:
		processSECoeffRevSparse<  8><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case  4:
		processSECoeffRevSparse<  4><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case  2:
		processSECoeffRevSparse<  2><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	case  1:
		processSECoeffRevSparse<  1><<< dimGrid, dimBlock, smemSize >>>(nCol, nColByBlock, colPtrA, sparseA, result); break;
	}
}

#endif

