//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASISV2_KERNEL_H_
#define _UPDATEBASISV2_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define UB_BLOCK_DIM		16

/*
	Updating the basis
*/
__global__ void
updateBasisV2K(int nBasic, unsigned int leave, VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_y = blockIdx.y;//blockIdx.x;
	unsigned int blockIdx_x = blockIdx.x;//(blockIdx.x+blockIdx.y)%gridDim.x;
	
	__shared__ VAR_TYPE block[UB_BLOCK_DIM+2][UB_BLOCK_DIM+1];
	
    unsigned int xIndex = threadIdx.x;
    unsigned int yIndex = blockIdx_y * UB_BLOCK_DIM + threadIdx.y;
    unsigned int xIndex2 = blockIdx_y * UB_BLOCK_DIM + threadIdx.x;
    unsigned int yIndex2 = threadIdx.y;
    
    int isNotLeaveRow;
    VAR_TYPE Bij, B2ij;
	unsigned int index_in = yIndex * pitch_B + xIndex;
	unsigned int index_out = yIndex2 * pitch_B + xIndex2;
	
	if(yIndex < nBasic)
	{
		if(threadIdx.y == 0){
			// Since tIdx.y == 0 then yIndex is the base address y wise then yIndex + tIdx.x give us the right element
			block[UB_BLOCK_DIM][threadIdx.x] = r[yIndex + threadIdx.x]/dl;
		// If we are the second row of threads, we load Ddi into shared memory
		}
		__syncthreads();
		
		
		while(xIndex < nBasic){
			
			// Load the Bij value
			Bij = B[index_in];
					
			if(threadIdx.y == 0) {
					// xIndex has already the right values
					block[UB_BLOCK_DIM+1][threadIdx.x] = d[xIndex];
			}		
			__syncthreads();
			// Thread sync prior to processing B2ij

		    // Check if we are the leaving row
			isNotLeaveRow = (xIndex != leave);
			// load Blj / dl
			B2ij = block[UB_BLOCK_DIM][threadIdx.y];
			// multiply by -di if "is not leave row" else by 1.0
			B2ij *= (-block[UB_BLOCK_DIM+1][threadIdx.x]*isNotLeaveRow + 1.0*(1-isNotLeaveRow));
			// Add Bij if "is not leave row"
			B2ij += isNotLeaveRow * Bij;
			
			// Store into B[i,j]
			B[index_in] = B2ij;
			
			// store into Bij if "is not leave row" else store Bij
			block[threadIdx.y][threadIdx.x] = B2ij;
			__syncthreads();
						
			// Transpose B
			//if(index_out < nBasic*pitch_B){
			BTransp[index_out] = block[threadIdx.x][threadIdx.y];
			//}

			//blockIdx_x++;
			xIndex += UB_BLOCK_DIM;
			index_in += UB_BLOCK_DIM;
			index_out += UB_BLOCK_DIM*pitch_B;
		}

	}
	
}

__global__ void
updateBasisV2K_2(int nIter, int nBasic, unsigned int leave, VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	//unsigned int blockIdx_x = blockIdx.x;//(blockIdx.x+blockIdx.y)%gridDim.x;
	//unsigned int blockIdx_y = blockIdx.y;//blockIdx.x;
	
	__shared__ VAR_TYPE block[UB_BLOCK_DIM+2][UB_BLOCK_DIM+1];
	
    unsigned int xIndex = threadIdx.x;
    unsigned int yIndex = blockIdx.y * UB_BLOCK_DIM + threadIdx.y;
    unsigned int xIndex2 = blockIdx.y * UB_BLOCK_DIM + threadIdx.x;
    unsigned int yIndex2 = threadIdx.y;
    unsigned int indexIn = yIndex * pitch_B + xIndex;
    unsigned int indexOut = yIndex2 * pitch_B + xIndex2;
    
    int isNotLeaveRow;
    VAR_TYPE Bij, B2ij;
	//unsigned int index_in = yIndex * pitch_B + xIndex;
	//unsigned int index_out = yIndex2 * pitch_B + xIndex2;

	if(threadIdx.y == 0){// && yIndex < nBasic){
		// Since tIdx.y == 0 then yIndex is the base address y wise then yIndex + tIdx.x give us the right element
		block[UB_BLOCK_DIM][threadIdx.x] = r[yIndex + threadIdx.x]/dl;
	// If we are the second row of threads, we load Ddi into shared memory
	}
	__syncthreads();
	
	while(nIter--){
		
		if(threadIdx.y == 0){// && xIndex < nBasic) {
				// xIndex has already the right values
				block[UB_BLOCK_DIM+1][threadIdx.x] = d[xIndex];
		}
		// Thread sync prior to processing B2ij
		__syncthreads();
				
		
		// Load the Bij value
		//Bij = B[yIndex * pitch_B + xIndex];
		Bij = B[indexIn];

		// Check if we are the leaving row
		isNotLeaveRow = (xIndex != leave);
		// load Blj / dl
		B2ij = block[UB_BLOCK_DIM][threadIdx.y];
		// multiply by -di if "is not leave row" else by 1.0
		B2ij *= (-block[UB_BLOCK_DIM+1][threadIdx.x]*isNotLeaveRow + 1.0*(1-isNotLeaveRow));
		// Add Bij if "is not leave row"
		B2ij += isNotLeaveRow * Bij;
		
		// store into Bij if "is not leave row" else store Bij
		block[threadIdx.y][threadIdx.x] = B2ij;

		// Store into B[i,j]
		//B[yIndex * pitch_B + xIndex] = B2ij;
		B[indexIn] = B2ij;
		
		__syncthreads();
		
		// Transpose B
		//BTransp[yIndex2 * pitch_B + xIndex2] = block[threadIdx.x][threadIdx.y];
		BTransp[indexOut] = block[threadIdx.x][threadIdx.y];
		
		//blockIdx_x++;
		xIndex += UB_BLOCK_DIM;
		indexIn += UB_BLOCK_DIM;
		indexOut += UB_BLOCK_DIM*pitch_B;
		//yIndex2 += UB_BLOCK_DIM;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisV2Kernel(int threads, int blocks, 
					int nBasic, unsigned int leave, 
					VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp,
					unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{

    dim3 dimBlock(threads, threads, 1);
    dim3 dimGrid(1, blocks, 1);
    int smemSize = (threads+1)*(threads+2)*sizeof(VAR_TYPE);
    
	updateBasisV2K_2<<< dimGrid, dimBlock, smemSize >>>(blocks, nBasic, leave, dl, B, BTransp, pitch_B, d, r); 
  
}

#endif // #ifndef _UPDATEBASISV2_KERNEL_H_
