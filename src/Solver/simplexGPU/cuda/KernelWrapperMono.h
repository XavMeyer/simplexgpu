//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * KernelWrapper.h
 *
 *  Created on: Dec 20, 2010
 *      Author: meyerx
 */

#ifndef KERNELWRAPPER_MONO_H_
#define KERNELWRAPPER_MONO_H_

/*#include <math.h>
#include "../../SimplexProblem.h"
#include "../../Config.h"
#include "../../Types.h"
#include "kernelHeader.h"
*/

#include "KernelWrapper.h"

#define NO_PADDING					1
#define PAD_32						2

#if PIVOTING_METHOD == PIVOTING_NORMAL
	#define PADDING_TYPE				NO_PADDING
#elif PIVOTING_METHOD == PIVOTING_OPTIMISED
	#define PADDING_TYPE				PAD_32
#endif


class KernelWrapperMono : public KernelWrapper {
public:
	KernelWrapperMono(Logger *inL);
	KernelWrapperMono(Logger *inL, int inDevice);
	KernelWrapperMono(Logger *inL, bool noInit);
	virtual ~KernelWrapperMono();

	/***********************************************/
	/*********** FUNCTION PROTOTYPE ****************/
	/***********************************************/


	// Init functions
	void 	init();
	void 	initCuda();
	void    initProblemStructure(SimplexProblem *inP);
	void 	deInitProblemStructure();

	void	loadProblem(SimplexProblem *inP);

	void 	deInitCuda();

	void 	initNested(SimplexProblem *inP);
	void 	deInitNested();

	void	initResult(SimplexProblem *inP);
	void 	deInitResult();

	bool 	initPivoting(SimplexProblem *inP);
	void 	deInitPivoting();

	void 	initSwap();
	void 	deInitSwap();



	// Standard simplex functions
	// Normal mode
	red_result_t	stdFindCol(SimplexProblem *inP);

	// Steepest edge
	red_result_t 	seFindCol(SimplexProblem *inP);
	red_result_t	seFindColP1(SimplexProblem *inP);
	red_result_t 	seFindCol2(SimplexProblem *inP);
	red_result_t	seFindCol2P1(SimplexProblem *inP);
	red_result_t 	seFindCol3(SimplexProblem *inP);
	red_result_t 	seFindCol3P1(SimplexProblem *inP);

	red_result_t 	selectSECoeff(SimplexProblem *inP);
	red_result_t 	selectSECoeffP1(SimplexProblem *inP);

	// Nested std
	red_result_t 	nestedFindCol(SimplexProblem *inP);
	red_result_t 	nestedFindColP1(SimplexProblem *inP);

	//Expand row choice
	row_result_t 	expandFindRow(int col, VAR_TYPE seVal, double delta, double tao, SimplexProblem *inP);
	row_result_t 	expandFindRowP1(int col, VAR_TYPE seVal, double delta, double tau, SimplexProblem *inP);

	void 	nonBasicToBound(int col, VAR_TYPE alpha, VAR_TYPE xVal,  SimplexProblem *inP);

	// useful function (at least for me...)
	// return the optimum value (last element of obj function)
	//VAR_TYPE 	getOptimum(SimplexProblem *inP);
	void		getDeviceBasis(SimplexProblem *inP);
	void 		getDeviceObj(SimplexProblem *inP);
	void 		getDeviceObjAux(SimplexProblem *inP);
	void 		getDeviceEqs(SimplexProblem *inP);
	// Move device data to the SimplexProblem given at init
	void 		getDeviceData(SimplexProblem *inP);
	// Place the current solution into the *inSol vector but doesn't change inP
	void 		getSolution(SimplexProblem *inP, SimplexSolution *inSol);



	// Without slack version
	void pivotingWS(int row, int col, SimplexProblem *inP);
	void updatingBasisWS(int row, int col, VAR_TYPE alpha, SimplexProblem *inP);
	void checkInfeasWS(SimplexProblem *inP);
	void updateInfeasWS(SimplexProblem *inP);
	void processOptimumWS(SimplexProblem *inP);
	void resetBoundsWS(SimplexProblem *inP);
	void orderValsWS(SimplexProblem *inP);
	void changeBoundWK(SimplexProblem *inP, IndexPivoting *inI, int index, float2 bound); // No kernel involved (replace only)
	void changeBoundWS(SimplexProblem *inP, int index, float2 bound); // Kernel involved (search + replace)
	void reinitPWS(SimplexProblem *inP);

	void checkInfeasV2(SimplexProblem *inP);
	void updateInfeasV2(SimplexProblem *inP);

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
	void pivotingSE(int row, int col, SimplexProblem *inP);
#endif

private:

	typedef KernelWrapper super;

	/***********************************************/
	/***************** Variables *******************/
	/***********************************************/

	// Res variables
	int 			nBlocks;
	int 			nThreads;

	int*			hIRes;
	int*			dIRes;

	red_result_t* 	hRes;
	red_result_t* 	dRes;

	exp_result_t* 	hERes ;
	exp_result_t* 	dERes;

	// Matrix variables
	//int _nbRow, _nbCol;
	VAR_TYPE* 		x;
	VAR_TYPE* 		y;
	VAR_TYPE*		d_one;
	VAR_TYPE*		d_zCol;
	int*			d_mask;

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
	VAR_TYPE*		d_seCoeff;
#endif

	// Swap
	int*			swpInt;
	float*			swpFlt;
	float2*			swpFlt2;
	double*			swpDbl;
	VAR_TYPE*		swpTyp;

	// Global memory pointers
	VAR_TYPE* 		d_eqs;
	VAR_TYPE* 		d_objFunc;
	VAR_TYPE*		d_objFuncAux;
	int* 			d_basis;
	int*			d_xInd;
	VAR_TYPE*		d_xScaling;

	VAR_TYPE*		d_xVal;
	float2*			d_xBounds;
	float2*			d_xBoundsP1;


	VAR_TYPE* 		d_bVal;
	float2*			d_bBounds;
	float2*			d_bBoundsP1;

	size_t			pitch;

	//misc
	bool			firstNested;

	int dev;

#if PROFILING
	UniqueProfiler *prof;
#endif

	/***********************************************/
	/*******************   MISC  *******************/
	/***********************************************/

	inline void getNumBlocksAndThreads( int n, int &blocks, int &threads);

	/***********************************************/
	/******** WRAPER KERNELS PROTOTYPE *************/
	/***********************************************/
	// This kernel returns the index of the maximum positive value of a vector
	void 	argMaxPosNextSteps(	int size, int threads, int blocks,
								red_result_t *d_idata, red_result *d_odata);
	red_result_t 	argMaxPosW(int nEl, VAR_TYPE* p1, int incP1); // Wrapper

	// This kernel returns the index of the maximum absolute value of a vector
	red_result_t 	argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1); // Wrapper

	red_result_t argMaxAbsNBW(int nEl, VAR_TYPE* obj, float2* bounds);

	void	pSEValExpand3W(	int m, int n,
							VAR_TYPE *obj, int incObj,
							VAR_TYPE *eqs, int pitchEqs,
							VAR_TYPE *x, float2 *xBounds,
							VAR_TYPE *result);

	red_result_t pSEValExpandW( int m, int n, VAR_TYPE *obj, float2 *xBounds);

	red_result_t pSEValExpandTW( int m, int n, VAR_TYPE *obj, float2 *xBounds, VAR_TYPE threshold);

	// This kernel returns the index of the maximum positive value of a vector
	red_result_t 	argMaxNestedW(int nEl, VAR_TYPE* p1, int incP1, int* mask, int type); // Wrapper

	// These kernels returns pivot row using the EXPAND method
	red_result_t expandP1W(int nEl, VAR_TYPE* pivots, float2* bounds, double delta, int objSign);

	exp_result_t expandP2W(int nEl, VAR_TYPE* pivots, float2* bounds, double alpha, int objSign);

	exp_result_t expandSpecW(int nEl, VAR_TYPE* pivots, double teta, int objSign);

	// WS (without slack
	int 	checkFeasXWSW(int nEl);
	int 	checkFeasBWSW(int nEl);

	int 	updateFeasXWSW(int nEl);
	int 	updateFeasBWSW(int nEl);

	int 	resetXValW(int nEl);

	void orderValsW(int nRow, int nCol);

	void changeBoundW(int nRow, int nCol, int index, float2 bound);

	void DGER(	int m, int n, int threads, int blocks,
				double alpha, double *row, double *col,
				double *eqs, int pitch);

	void DGERW(	int m, int n, double alpha,
				double *row, double *col,
				double *eqs, int pitch);

	void vectorMulW(int nEl, VAR_TYPE *inX, VAR_TYPE *inY);

	bool isPow2(unsigned int x);

	unsigned int nextPow2( unsigned int x );

	// Test
	int 	checkFeasXV2W(int nEl);
	int 	checkFeasBV2W(int nEl);

	int updateFeasV2W(int nVar, int nSlack);

};

#endif /* KERNELWRAPPER_H_ */
