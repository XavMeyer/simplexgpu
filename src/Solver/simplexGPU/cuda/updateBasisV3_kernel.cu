//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV3_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASISV3_KERNEL_H_
#define _UPDATEBASISV3_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define UB_TILE_DIM				32
#define UB_BLOCKS_ROW			8

/*
	Updating the basis
*/
__global__ void
updateBasisV3K(int nIter, int nBasic, unsigned int leave, VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	__shared__ VAR_TYPE block[UB_TILE_DIM+2][UB_TILE_DIM+1];
	
    unsigned int xIndex = threadIdx.x;
    unsigned int yIndex = blockIdx.y * UB_TILE_DIM + threadIdx.y;
    unsigned int xIndex2 = blockIdx.y * UB_TILE_DIM + threadIdx.x;
    unsigned int yIndex2 = threadIdx.y;
    unsigned int indexIn = yIndex * pitch_B + xIndex;
    unsigned int indexOut = yIndex2 * pitch_B + xIndex2;
    
    int isNotLeaveRow;
    VAR_TYPE Bij, B2ij;

    
    // Compute the leaving row coeffs and store them into shared memory
	if(threadIdx.y == 0){
		block[UB_TILE_DIM][threadIdx.x] = r[yIndex + threadIdx.x]/dl;
	}
	__syncthreads();
	
	// Process a full vertical band of the matrix 
	while(nIter--){
		
		// Load entering column values and store them into shared memory
		if(threadIdx.y == 0){
			block[UB_TILE_DIM+1][threadIdx.x] = d[xIndex];
		}
		__syncthreads();
				
		// Check if we are the leaving row
		isNotLeaveRow = (xIndex != leave);
		
		for(int i=0; i<UB_TILE_DIM; i+=UB_BLOCKS_ROW){
			// Load the Bij value
			Bij = B[indexIn+i*pitch_B];
			// load Blj / dl
			B2ij = block[UB_TILE_DIM][threadIdx.y+i];
			// multiply by -di if "is not leave row" else by 1.0
			B2ij *= (-block[UB_TILE_DIM+1][threadIdx.x]*isNotLeaveRow + 1.0*(1-isNotLeaveRow));
			// Add Bij if "is not leave row"
			B2ij += isNotLeaveRow * Bij;
			
			// store into Bij if "is not leave row" else store Bij
			block[threadIdx.y+i][threadIdx.x] = B2ij;

			// Store into B[i,j]
			B[indexIn+i*pitch_B] = B2ij;
		}
		__syncthreads();
		
		for(int i=0; i<UB_TILE_DIM; i+=UB_BLOCKS_ROW){
			// Transpose B		
			BTransp[indexOut+i*pitch_B] = block[threadIdx.x][threadIdx.y+i];
		}
		
		xIndex += UB_TILE_DIM;
		indexIn += UB_TILE_DIM;
		indexOut += UB_TILE_DIM*pitch_B;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisV3Kernel(int blocks, 
					int nBasic, unsigned int leave, 
					VAR_TYPE dl, VAR_TYPE *B, VAR_TYPE *BTransp,
					unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{

    dim3 dimBlock(UB_TILE_DIM, UB_BLOCKS_ROW, 1);
    dim3 dimGrid(1, blocks, 1);
    int smemSize = (UB_TILE_DIM+1)*(UB_TILE_DIM+2)*sizeof(VAR_TYPE);
    
	updateBasisV3K<<< dimGrid, dimBlock, smemSize >>>(blocks, nBasic, leave, dl, B, BTransp, pitch_B, d, r); 
  
}

#endif // #ifndef _UPDATEBASISV3_KERNEL_H_
