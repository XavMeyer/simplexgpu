//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * pSEValExpand2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _PROCSEV_KERNEL_H_
#define _PROCSEV_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
	
	//carefull here
    __device__ inline operator       VAR_TYPE*()
    {
        extern __shared__ VAR_TYPE __smem[];
        return (VAR_TYPE*)__smem;
    }

    __device__ inline operator const VAR_TYPE*() const
    {
        extern __shared__ VAR_TYPE __smem[];
        return (VAR_TYPE*)__smem;
    }
};


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize>
__global__ void
pSEValExpand(int gridSize, int m, int n, VAR_TYPE *obj, VAR_TYPE *eqs, int pitchEqs, VAR_TYPE *x, float2 *bounds, red_result_t *result)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
    
    volatile VAR_TYPE *sdata = SharedMemory();

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i;//blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int bid = blockIdx.x;
    float2 bound;
    volatile VAR_TYPE mySum = 0.0;
    VAR_TYPE objVal;
    VAR_TYPE xVal;
    VAR_TYPE seVal;
    volatile VAR_TYPE coeff1, coeff2, coeff3, coeff4, coeff5, coeff6, coeff7, coeff8;
    red_result_t mySe;
    mySe.index = -1;
    mySe.value = 0.0;
    
    while(bid < n){
		
    	i = tid;
		mySum = 0.0;
		
		if(tid == 0){
			objVal = obj[bid];
			bound = bounds[bid];
			xVal = x[bid];
			// upper
			if(objVal > EPS1 && ((VAR_TYPE)bound.y - xVal) > EXPAND_DELTA_K ) {
				sdata[0] = 1.0;
				sdata[blockSize] = objVal;
			// lower
			} else if(objVal < -EPS1 && (xVal - (VAR_TYPE)bound.x) > EXPAND_DELTA_K ) {
				sdata[0] = 1.0;
				sdata[blockSize] = objVal;
			} else {
				sdata[0] = 0.0;
			}
		}
		
		__syncthreads();
		if(sdata[0] == 1.0){
			while(i+7*blockSize < m){
				coeff1 = eqs[IDX2C(i, bid, pitchEqs)];
				coeff2 = eqs[IDX2C(i+blockSize, bid, pitchEqs)];
				coeff3 = eqs[IDX2C(i+blockSize*2, bid, pitchEqs)];
				coeff4 = eqs[IDX2C(i+blockSize*3, bid, pitchEqs)];
				coeff5 = eqs[IDX2C(i+blockSize*4, bid, pitchEqs)];
				coeff6 = eqs[IDX2C(i+blockSize*5, bid, pitchEqs)];
				coeff7 = eqs[IDX2C(i+blockSize*6, bid, pitchEqs)];
				coeff8 = eqs[IDX2C(i+blockSize*7, bid, pitchEqs)];
				mySum += coeff1*coeff1;
				mySum += coeff2*coeff2;
				mySum += coeff3*coeff3;
				mySum += coeff4*coeff4;
				mySum += coeff5*coeff5;
				mySum += coeff6*coeff6;
				mySum += coeff7*coeff7;
				mySum += coeff8*coeff8;
				i+=8*blockSize;
			}
			
			if(i+3*blockSize < m){
				coeff1 = eqs[IDX2C(i, bid, pitchEqs)];
				coeff2 = eqs[IDX2C(i+blockSize, bid, pitchEqs)];
				coeff3 = eqs[IDX2C(i+blockSize*2, bid, pitchEqs)];
				coeff4 = eqs[IDX2C(i+blockSize*3, bid, pitchEqs)];
				mySum += coeff1*coeff1;
				mySum += coeff2*coeff2;
				mySum += coeff3*coeff3;
				mySum += coeff4*coeff4;
				i+=4*blockSize;
			}
			
			if(i+blockSize < m){
				coeff1 = eqs[IDX2C(i, bid, pitchEqs)];
				coeff2 = eqs[IDX2C(i+blockSize, bid, pitchEqs)];
				mySum += coeff1*coeff1;
				mySum += coeff2*coeff2;
				i+=2*blockSize;
			}
			
			if(i < m){
				coeff1 = eqs[IDX2C(i, bid, pitchEqs)];
				mySum += coeff1*coeff1;
			}
		
			sdata[tid] = mySum;
			__syncthreads();
			
			if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
			if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
			if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid +  64]; } __syncthreads(); }
				
			#ifndef __DEVICE_EMULATION__
			if (tid < 32)
			#endif
			{
				if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
				if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
				if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
				if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
				if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
				if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
			}
				
			// write result for this block to global mem 
			if (tid == 0){
				seVal = sdata[blockSize]*rsqrt(mySum+1.0);
				if(fabs(mySe.value) < fabs(seVal)){
					mySe.index = bid;
					mySe.value = seVal;
				}
			}
			__syncthreads();
		}
		bid += gridSize;
    }
    
	if (tid == 0)
		SET(result[blockIdx.x], mySe);
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
pSEValExpand(int m, int n, int threads, int blocks, VAR_TYPE *obj, VAR_TYPE *eqs, int pitchEqs, VAR_TYPE *x, float2 *bounds, red_result_t *result) {
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? (1 + 2 * threads) * sizeof(VAR_TYPE) : (1 + threads) * sizeof(VAR_TYPE);

    // choose which of the optimized versions of reduction to launch
	switch (threads)
	{
	case 512:
		pSEValExpand<512><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case 256:
		pSEValExpand<256><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case 128:
		pSEValExpand<128><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case 64:
		pSEValExpand< 64><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case 32:
		pSEValExpand< 32><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case 16:
		pSEValExpand< 16><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case  8:
		pSEValExpand<  8><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case  4:
		pSEValExpand<  4><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case  2:
		pSEValExpand<  2><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	case  1:
		pSEValExpand<  1><<< dimGrid, dimBlock, smemSize >>>(blocks, m, n, obj, eqs, pitchEqs, x, bounds, result); break;
	}
}

#endif
