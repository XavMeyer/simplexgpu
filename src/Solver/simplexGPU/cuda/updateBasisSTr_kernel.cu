//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV2_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASISSTR_KERNEL_H_
#define _UPDATEBASISSTR_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define UB_BLOCK_DIM			32
#define UB_HALF_BLOCK_DIM		(32/2)

__global__ void
updateBasisSTrK(VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTrans, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_x = blockIdx.x;//(blockIdx.x+blockIdx.y)%gridDim.x;
	unsigned int blockIdx_y =  blockIdx.y;//blockIdx.x
	
	__shared__ VAR_TYPE block[UB_HALF_BLOCK_DIM+4][UB_HALF_BLOCK_DIM+1];
	
    unsigned int xIndex = blockIdx_x * UB_BLOCK_DIM + threadIdx.x;
    unsigned int offsetY = blockIdx_y * UB_BLOCK_DIM;
    unsigned int yIndex = offsetY + threadIdx.y;
    unsigned int inIndex;
    
    unsigned int xIndex2 = offsetY + threadIdx.x;
    unsigned int yIndex2 = blockIdx_x * UB_BLOCK_DIM + threadIdx.y;
    unsigned int outIndex;
    
    VAR_TYPE Bij, Bij_2, Bij_3, Bij_4;
    
    // Load Bij_1, 2 and 4
    inIndex = yIndex * pitch_B + xIndex;
	Bij = B[inIndex];
	inIndex += UB_HALF_BLOCK_DIM;
	Bij_2 = B[inIndex];
	inIndex += UB_HALF_BLOCK_DIM * pitch_B;
	Bij_4 = B[inIndex];
	
	// Load di and rj
	if(threadIdx.y == 0){
		block[UB_HALF_BLOCK_DIM+0][threadIdx.x] = r[offsetY + threadIdx.x];
	} else if (threadIdx.y == 1) {
		block[UB_HALF_BLOCK_DIM+1][threadIdx.x] = r[offsetY + UB_HALF_BLOCK_DIM + threadIdx.x];
	} else if (threadIdx.y == 2){
		block[UB_HALF_BLOCK_DIM+2][threadIdx.x] = alpha * d[xIndex];	
	} else if (threadIdx.y == 3){
		block[UB_HALF_BLOCK_DIM+3][threadIdx.x] = alpha * d[xIndex+UB_HALF_BLOCK_DIM];	
	}
	__syncthreads();
	
    // Load Bij_3
	inIndex -= UB_HALF_BLOCK_DIM;
	Bij_3 = B[inIndex];
	
	// Process and update Bij_1
	Bij += block[UB_HALF_BLOCK_DIM+2][threadIdx.x]*block[UB_HALF_BLOCK_DIM+0][threadIdx.y];
    inIndex = yIndex * pitch_B + xIndex;
	B[inIndex] = Bij;
	block[threadIdx.y][threadIdx.x] = Bij;
	__syncthreads();
	
	outIndex = yIndex2 * pitch_B + xIndex2;
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();
	
	// Process and update Bij_2
	Bij_2 += block[UB_HALF_BLOCK_DIM+3][threadIdx.x]*block[UB_HALF_BLOCK_DIM+0][threadIdx.y];
	inIndex += UB_HALF_BLOCK_DIM;
	B[inIndex] = Bij_2;
	block[threadIdx.y][threadIdx.x] = Bij_2;
	__syncthreads();
			
	outIndex = (yIndex2+UB_HALF_BLOCK_DIM) * pitch_B + xIndex2;
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();
	
	// Process and update Bij_4	
	Bij_4 += block[UB_HALF_BLOCK_DIM+3][threadIdx.x]*block[UB_HALF_BLOCK_DIM+1][threadIdx.y];
	inIndex += UB_HALF_BLOCK_DIM * pitch_B;
	B[inIndex] = Bij_4;
	block[threadIdx.y][threadIdx.x] = Bij_4;
	__syncthreads();
			
	outIndex = (yIndex2+UB_HALF_BLOCK_DIM) * pitch_B + (xIndex2+UB_HALF_BLOCK_DIM);
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];	
	__syncthreads();
		
	// Process and update Bij_3	
	Bij_3 += block[UB_HALF_BLOCK_DIM+2][threadIdx.x]*block[UB_HALF_BLOCK_DIM+1][threadIdx.y];
	inIndex -= UB_HALF_BLOCK_DIM;
	B[inIndex] = Bij_3;
	block[threadIdx.y][threadIdx.x] = Bij_3;
	__syncthreads();
			
	outIndex = yIndex2 * pitch_B + (xIndex2+UB_HALF_BLOCK_DIM);
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	
}

__global__ void
updateBasisSTrK_2(VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTrans, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_x = blockIdx.x;//unsigned int blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
	unsigned int blockIdx_y = blockIdx.y;//unsigned int blockIdx_y = blockIdx.x;
	
	__shared__ VAR_TYPE block[UB_HALF_BLOCK_DIM+4][UB_HALF_BLOCK_DIM+1];
	
    unsigned int xIndex = blockIdx_x * UB_BLOCK_DIM + threadIdx.x;
    unsigned int offsetY = blockIdx_y * UB_BLOCK_DIM;
    unsigned int yIndex = offsetY + threadIdx.y;
    unsigned int inIndex = yIndex * pitch_B + xIndex;
    
    unsigned int xIndex2 = offsetY + threadIdx.x;
    unsigned int yIndex2 = blockIdx_x * UB_BLOCK_DIM + threadIdx.y;
    unsigned int outIndex;
    
    VAR_TYPE Bij_1, Bij_2, Bij_3, Bij_4;

    inIndex = yIndex * pitch_B + xIndex;
	Bij_1 = B[inIndex];
	inIndex += UB_HALF_BLOCK_DIM;
	Bij_2 = B[inIndex];


	if(threadIdx.y == 0){
		block[UB_HALF_BLOCK_DIM+0][threadIdx.x] = r[offsetY + threadIdx.x];
	} else if (threadIdx.y == 1) {
		block[UB_HALF_BLOCK_DIM+1][threadIdx.x] = r[offsetY + UB_HALF_BLOCK_DIM + threadIdx.x];
	} else if (threadIdx.y == 2){
		block[UB_HALF_BLOCK_DIM+2][threadIdx.x] = alpha * d[xIndex];	
	} else if (threadIdx.y == 3){
		block[UB_HALF_BLOCK_DIM+3][threadIdx.x] = alpha * d[xIndex+UB_HALF_BLOCK_DIM];
	}
	__syncthreads();
	
	Bij_1 += block[UB_HALF_BLOCK_DIM+2][threadIdx.x]*block[UB_HALF_BLOCK_DIM+0][threadIdx.y];	
	Bij_2 += block[UB_HALF_BLOCK_DIM+3][threadIdx.x]*block[UB_HALF_BLOCK_DIM+0][threadIdx.y];	
    inIndex = yIndex * pitch_B + xIndex;
	B[inIndex] = Bij_1;
	inIndex += UB_HALF_BLOCK_DIM;
	B[inIndex] = Bij_2;
	
    // Load Bij_3
	inIndex += UB_HALF_BLOCK_DIM * pitch_B;
	Bij_4 = B[inIndex];
	inIndex -= UB_HALF_BLOCK_DIM;
	Bij_3 = B[inIndex];

	
	block[threadIdx.y][threadIdx.x] = Bij_1;
	__syncthreads();
	outIndex = yIndex2 * pitch_B + xIndex2;
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();	
	
	block[threadIdx.y][threadIdx.x] = Bij_2;
	__syncthreads();	
	outIndex = (yIndex2+UB_HALF_BLOCK_DIM) * pitch_B + xIndex2;
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();
	
	
	
	Bij_4 += block[UB_HALF_BLOCK_DIM+3][threadIdx.x]*block[UB_HALF_BLOCK_DIM+1][threadIdx.y];	
	Bij_3 += block[UB_HALF_BLOCK_DIM+2][threadIdx.x]*block[UB_HALF_BLOCK_DIM+1][threadIdx.y];	
	B[inIndex] = Bij_3;	
	inIndex -= UB_HALF_BLOCK_DIM;
	B[inIndex] = Bij_4;	
	

	block[threadIdx.y][threadIdx.x] = Bij_3;
	__syncthreads();
	outIndex = yIndex2 * pitch_B + (xIndex2+UB_HALF_BLOCK_DIM);
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();
	
	block[threadIdx.y][threadIdx.x] = Bij_4;
	__syncthreads();
	outIndex = (yIndex2+UB_HALF_BLOCK_DIM) * pitch_B + (xIndex2+UB_HALF_BLOCK_DIM);
	BTrans[outIndex] = block[threadIdx.x][threadIdx.y];
	__syncthreads();
	
	
	




	

}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisSTrKernel(int threads, int blocks, 
						VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTrans, unsigned int pitch_B, 
						VAR_TYPE *d, VAR_TYPE *r)
{

    dim3 dimBlock(threads, threads, 1);
    dim3 dimGrid(blocks, blocks, 1);
    int smemSize = (UB_HALF_BLOCK_DIM+4) * (UB_HALF_BLOCK_DIM+1) *sizeof(VAR_TYPE);
    
	updateBasisSTrK<<< dimGrid, dimBlock, smemSize >>>(alpha, B, BTrans, pitch_B, d, r); 
  
}

#endif // #ifndef _UPDATEBASISSTR_KERNEL_H_
