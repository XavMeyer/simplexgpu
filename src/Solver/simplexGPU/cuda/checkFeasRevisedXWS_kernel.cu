//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _CHECK_FEASREVISEDXWS_KERNEL_H_
#define _CHECK_FEASREVISEDXWS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
    __device__ inline operator       int*()
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }

    __device__ inline operator const int*() const
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }
};


/*
    This version adds multiple elements per thread sequentially.  This reduces the overall
    cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
    (Brent's Theorem optimization)
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
checkFeasRevisedXWS(unsigned int n, VAR_TYPE *x, float2 *bounds, float2 *boundsP1, VAR_TYPE *objAux, int *result)
{
    // now that we are using warp-synchronous programming (below)
    // we need to declare our shared memory volatile so that the compiler
    // doesn't reorder stores to it and induce incorrect behavior. 
    
    volatile int *sdata = SharedMemory();

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    
    VAR_TYPE xVal, obj;
    float2 bound, boundP1;
    float lo, up;
    
    int mySum = 0;

    // we reduce multiple elements per thread.  The number is determined by the 
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n)
    {     
    	xVal = x[i];
    	bound = bounds[i];
    	up = bound.y;//upper[i];
    	lo = bound.x;//lower[i];
    	obj = 0;
    	
    	if(xVal < ((VAR_TYPE)lo - EXPAND_DELTA_K)){
    		mySum++;
    		lo = -FLT_MAX;
    		obj = -1.0;
    	}
    	
    	if(xVal > ((VAR_TYPE)up + EXPAND_DELTA_K)){
    		mySum++;
    		up = FLT_MAX;
    		obj = 1.0;
    	}
    	
    	boundP1.x = lo;
    	boundP1.y = up;
    	boundsP1[i] = boundP1;
    	objAux[i] = obj;
    
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n)
        {
          	xVal = x[i + blockSize];
        	bound = bounds[i + blockSize];
        	up = bound.y;//upper[i];
        	lo = bound.x;//lower[i];
          	obj = 0;
          	
        	if(xVal < ((VAR_TYPE)lo - EXPAND_DELTA_K)){
        		mySum++;
        		lo = -FLT_MAX;
        		obj = -1.0;
        	}
        	
        	if(xVal > ((VAR_TYPE)up + EXPAND_DELTA_K)){
        		mySum++;
        		up = FLT_MAX;
        		obj = 1.0;
        	}
        	
        	//upperP1[i + blockSize] = up;
        	//lowerP1[i + blockSize] = lo;
        	boundP1.x = lo;
        	boundP1.y = up;
        	boundsP1[i + blockSize] = boundP1;
        	objAux[i + blockSize] = obj;
        	
        }
        i += gridSize;
    } 

    // each thread puts its local sum into shared memory 
    sdata[tid] = mySum;
	__syncthreads();
	
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid + 64]; } __syncthreads(); }
		
	#ifndef __DEVICE_EMULATION__
	if (tid < 32)
	#endif
	{
		if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
		if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
		if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
		if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
		if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
		if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
	}
		
	// write result for this block to global mem 
	if (tid == 0) 
		 result[blockIdx.x] = sdata[0];
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
checkFeasRevisedXWS(int size, int threads, int blocks, VAR_TYPE *x, float2 *bounds, float2 *boundsP1, VAR_TYPE *objAux, int *result)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(int) : threads * sizeof(int);

    // choose which of the optimized versions of reduction to launch
	
    if (isPow2(size))
    {
		switch (threads)
		{
		case 512:
			checkFeasRevisedXWS<512, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 256:
			checkFeasRevisedXWS<256, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 128:
			checkFeasRevisedXWS< 128, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 64:
			checkFeasRevisedXWS<  64, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 32:
			checkFeasRevisedXWS<  32, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 16:
			checkFeasRevisedXWS<  16, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  8:
			checkFeasRevisedXWS<   8, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  4:
			checkFeasRevisedXWS<   4, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  2:
			checkFeasRevisedXWS<   2, true><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  1:
			checkFeasRevisedXWS<   1, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		}
	}
	else
	{
		switch (threads)
		{
		case 512:
			checkFeasRevisedXWS< 512, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 256:
			checkFeasRevisedXWS< 256, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 128:
			checkFeasRevisedXWS< 128, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 64:
			checkFeasRevisedXWS<  64, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 32:
			checkFeasRevisedXWS<  32, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case 16:
			checkFeasRevisedXWS<  16, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  8:
			checkFeasRevisedXWS<   8, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  4:
			checkFeasRevisedXWS<   4, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  2:
			checkFeasRevisedXWS<   2, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		case  1:
			checkFeasRevisedXWS<   1, false><<< dimGrid, dimBlock, smemSize >>>(size, x, bounds, boundsP1, objAux, result); break;
		}
	}     
    
}

#endif // #ifndef _CHECK_FEASREVISEDXWS_KERNEL_H_
