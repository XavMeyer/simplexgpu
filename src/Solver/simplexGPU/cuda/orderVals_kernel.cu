//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _ORDERVALS_KERNEL_H_
#define _ORDERVALS_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);

/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize>
__global__ void
orderVals(int nEl, int nVar, VAR_TYPE *xVal, int *xInd, VAR_TYPE *bVal, int *basis, VAR_TYPE *xTemp, VAR_TYPE *bTemp)
{
	
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
	VAR_TYPE val;
	int ind;
    
    
	while(i < nEl){
		if(i < nVar){
			val = xVal[i];
			ind = xInd[i];
		} else {
			val = bVal[i-nVar];
			ind = basis[i-nVar];
		}
		
		if(ind < nVar){
			xTemp[ind] = val;
		} else {
			bTemp[ind-nVar] = val;
		}
		
		i += blockSize;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
orderVals(int nEl, int threads, int blocks, int nVar, VAR_TYPE *xVal, int *xInd, VAR_TYPE *bVal, int *basis, VAR_TYPE *xTemp, VAR_TYPE *bTemp)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    
	switch (threads)
	{
	case 512:
	    orderVals<512><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case 256:
	    orderVals<256><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case 128:
	    orderVals<128><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case 64:
		orderVals< 64><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case 32:
		orderVals< 32><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case 16:
		orderVals< 16><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case  8:
		orderVals<  8><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case  4:
		orderVals<  4><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case  2:
		orderVals<  2><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	case  1:
		orderVals<  1><<< dimGrid, dimBlock, 0 >>>(nEl, nVar, xVal, xInd, bVal, basis, xTemp, bTemp); break;
	}
  
}

#endif // #ifndef _ORDERVALS_KERNEL_H_
