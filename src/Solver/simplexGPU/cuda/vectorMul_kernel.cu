//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _VECTORMUL_KERNEL_H_
#define _VECTORMUL_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize>
__global__ void
vectorMul(int nEl, VAR_TYPE *x, VAR_TYPE *y)
{
	
    unsigned int i = blockIdx.x*blockSize + threadIdx.x;
	volatile VAR_TYPE x1, x2, x3, x4, y1, y2, y3, y4, xy1, xy2, xy3, xy4;
    
    
	/*while(i+3*blockSize < nEl) {
		x1 = x[i];
		y1 = y[i];
		x2 = x[i+1*blockSize];
		y2 = y[i+1*blockSize];
		x3 = x[i+2*blockSize];
		y3 = y[i+2*blockSize];
		x4 = x[i+3*blockSize];
		y4 = y[i+3*blockSize];
	
		y1 = x1 * y1;
		y2 = x2 * y2;
		y3 = x3 * y3;
		y4 = x4 * y4;
		
		y[i] = y1;
		y[i+1*blockSize] = y2;
		y[i+2*blockSize] = y3;
		y[i+3*blockSize] = y4;
		
		i += blockSize*4;
	}*/
		
	while(i+1*blockSize < nEl) {
		x1 = x[i];
		y1 = y[i];
		x2 = x[i+1*blockSize];
		y2 = y[i+1*blockSize];
	
		xy1 = x1 * y1;
		xy2 = x2 * y2;
		
		y[i] = xy1;
		y[i+1*blockSize] = xy2;
		
		i += blockSize*2;
	}	
	
	if(i < nEl) {
		x1 = x[i];
		y1 = y[i];
	
		xy1 = x1 * y1;
		
		y[i] = xy1;
		
		i += blockSize;
	}
		
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
vectorMul(int nEl, int threads, int blocks, VAR_TYPE *x, VAR_TYPE *y)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    
	switch (threads)
	{
	case 512:
	    vectorMul<512><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case 256:
	    vectorMul<256><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case 128:
	    vectorMul<128><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case 64:
		vectorMul< 64><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case 32:
		vectorMul< 32><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case 16:
		vectorMul< 16><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case  8:
		vectorMul<  8><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case  4:
		vectorMul<  4><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case  2:
		vectorMul<  2><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	case  1:
		vectorMul<  1><<< dimGrid, dimBlock, 0 >>>(nEl, x, y); break;
	}
  
}

#endif // #ifndef _VECTORMUL_KERNEL_H_
