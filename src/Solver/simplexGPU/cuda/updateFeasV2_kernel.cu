//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * checkFeas_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEFEASV2_KERNEL_H_
#define _UPDATEFEASV2_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif



extern "C"
bool isPow2(unsigned int x);


struct SharedMemory
{
    __device__ inline operator       int*()
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }

    __device__ inline operator const int*() const
    {
        extern __shared__ int __smem[];
        return (int*)__smem;
    }
};


/*
	Updating the basis bounds for phase 1
*/
template <unsigned int blockSize, bool nIsPow2>
__global__ void
updateFeasV2(unsigned int nEl, unsigned int nVar, VAR_TYPE *xVal, VAR_TYPE *bVal, float2 *xBounds, float2 *bBounds, int *result)
{
	// now that we are using warp-synchronous programming (below)
	// we need to declare our shared memory volatile so that the compiler
	// doesn't reorder stores to it and induce incorrect behavior.   
	volatile int *sdata = SharedMemory();

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
    VAR_TYPE val;
    float2 bound;
	float lo, up;
	int infeasLo, infeasUp;
	int mySum = 0;
    
    while(i < nEl) {
    	
    	//Loading var
    	if(i < nVar) {
    		val = xVal[i];
    		bound = xBounds[i];
    	} else {
    		val = bVal[i - nVar];
    		bound = bBounds[i - nVar];
    	}
    	  
		lo = bound.x;
		up = bound.y;
		
    	// These tests must work with val being NAN (test will be false)
    	// This must be guaranteed if changed.
		infeasLo = val < ((VAR_TYPE)lo-EXPAND_DELTA_K);
    	infeasUp = val > ((VAR_TYPE)up+EXPAND_DELTA_K);	
    	
    	mySum += infeasLo + infeasUp;
		
		if (nIsPow2 || ((i + blockSize) < nEl))
		{
	    	//Loading var
	    	if((i + blockSize) < nVar) {
	    		val = xVal[i + blockSize];
	    		bound = xBounds[i + blockSize];
	    	} else {
	    		val = bVal[i + blockSize - nVar];
	    		bound = bBounds[i + blockSize - nVar];
	    	}
	    	  
			lo = bound.x;
			up = bound.y;   	
	    	
	    	// These tests must work with val being NAN (test will be false)
	    	// This must be guaranteed if changed.
			infeasLo = val < ((VAR_TYPE)lo-EXPAND_DELTA_K);
	    	infeasUp = val > ((VAR_TYPE)up+EXPAND_DELTA_K);	
			
	    	mySum += infeasLo + infeasUp;
	    	
		}	
		i += gridSize;
    }
    
    // each thread puts its local sum into shared memory 
    sdata[tid] = mySum;
	__syncthreads();
	
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] = mySum = mySum + sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] = mySum = mySum + sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] = mySum = mySum + sdata[tid + 64]; } __syncthreads(); }
		
	#ifndef __DEVICE_EMULATION__
	if (tid < 32)
	#endif
	{
		if (blockSize >=  64) { sdata[tid] = mySum = mySum + sdata[tid + 32]; EMUSYNC; }
		if (blockSize >=  32) { sdata[tid] = mySum = mySum + sdata[tid + 16]; EMUSYNC; }
		if (blockSize >=  16) { sdata[tid] = mySum = mySum + sdata[tid +  8]; EMUSYNC; }
		if (blockSize >=   8) { sdata[tid] = mySum = mySum + sdata[tid +  4]; EMUSYNC; }
		if (blockSize >=   4) { sdata[tid] = mySum = mySum + sdata[tid +  2]; EMUSYNC; }
		if (blockSize >=   2) { sdata[tid] = mySum = mySum + sdata[tid +  1]; EMUSYNC; }
	}
		
	// write result for this block to global mem 
	if (tid == 0) 
		 result[blockIdx.x] = sdata[0];
	
}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateFeasV2( int nEl, int threads, int blocks, int nVar, VAR_TYPE *xVal, VAR_TYPE *bVal, float2 *xBounds, float2 *bBounds, int *result)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(int) : threads * sizeof(int);
   
    
    if (isPow2(nEl))
    {
		switch (threads)
		{
		case 512:
			updateFeasV2<512, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 256:
			updateFeasV2<256, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 128:
			updateFeasV2<128, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 64:
			updateFeasV2< 64, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 32:
			updateFeasV2< 32, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 16:
			updateFeasV2< 16, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  8:
			updateFeasV2<  8, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  4:
			updateFeasV2<  4, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  2:
			updateFeasV2<  2, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  1:
			updateFeasV2<  1, true><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		}
    } else {
		switch (threads)
		{
		case 512:
			updateFeasV2<512, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 256:
			updateFeasV2<256, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 128:
			updateFeasV2<128, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 64:
			updateFeasV2< 64, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 32:
			updateFeasV2< 32, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case 16:
			updateFeasV2< 16, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  8:
			updateFeasV2<  8, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  4:
			updateFeasV2<  4, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  2:
			updateFeasV2<  2, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		case  1:
			updateFeasV2<  1, false><<< dimGrid, dimBlock, smemSize >>>(nEl, nVar, xVal, bVal, xBounds, bBounds, result); break;
		}
    }
  
}

#endif // #ifndef _UPDATEFEASV2_KERNEL_H_
