//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 * KernelWrapper.cpp
 *
 *  Created on: Dec 20, 2010
 *      Author: Xavier Meyer
 */


#include "KernelWrapperMono.h"

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "cutil_inline.h"
#include "cublas.h"


KernelWrapperMono::KernelWrapperMono(Logger *inL) {
	logger = inL;
	dev = 0;
	init();
	initCuda();
}

KernelWrapperMono::KernelWrapperMono(Logger *inL, int inDevice) {
	logger = inL;
	dev = inDevice;
	init();
	initCuda();
}

KernelWrapperMono::KernelWrapperMono(Logger *inL, bool noInit) {
	logger = inL;
	if(!noInit)
		init();
		initCuda();
}


KernelWrapperMono::~KernelWrapperMono() {
	deInitProblemStructure();
	deInitCuda();
}


/***********************************************/
/************ MISC FUNCTIONS *******************/
/***********************************************/

extern "C"
bool KernelWrapperMono::isPow2(unsigned int x)
{
    return ((x&(x-1))==0);
}


/***********************************************/
/************** Init Functions *****************/
/***********************************************/

void KernelWrapperMono::init(){
#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif
}

/*!
 * Call the init functions
 */
void KernelWrapperMono::initCuda(){

#if PINNED && MAPPED_RESULT
		//cutilSafeCall(cudaChooseDevice(&dev, &devProp));
		cutilSafeCall(cudaSetDevice(dev));
		cutilSafeCall(cudaSetDeviceFlags(cudaDeviceMapHost));
#else
		cout << "Device : " << dev << endl;
		cutilSafeCall(cudaSetDevice(dev));
#endif

	//printf("Running on device %d ! \n", dev);
	if(cublasInit() != CUBLAS_STATUS_SUCCESS){
		logger->log(Logger::LOG_ERROR, "Cublas error at init.");
		throw 11;
	}

	//initProblemStructure(inP);

}

void KernelWrapperMono::deInitCuda(){

	cublasShutdown();
	cudaThreadExit();
}

void KernelWrapperMono::initProblemStructure(SimplexProblem *inP) {

	// various init
#if COL_CHOICE_METHOD == NESTED_STD
	initNested(inP);
#endif
	initResult(inP);
	initPivoting(inP);
	initSwap();

	//Eqs
#if PADDING_TYPE == NO_PADDING
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_eqs, &pitch, inP->nRow*sizeof(*d_eqs), inP->nCol ));
#elif PADDING_TYPE == PAD_32

	int nRowPad = 32*ceil((float)inP->nRow/32);
	int nColPad = 32*ceil((float)inP->nCol/32);

	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_eqs, &pitch, nRowPad*sizeof(*d_eqs), nColPad ));
#endif

	//Obj
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFunc, inP->nCol*sizeof(*d_objFunc)));

	//ObjAux
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFuncAux, inP->nCol*sizeof(*d_objFuncAux)));

	//Basis
	cutilSafeCallNoSync( cudaMalloc((void**) &d_basis, inP->nRow*sizeof(*d_basis)));

	// x index (non-basic)
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xInd, inP->nCol*sizeof(*d_xInd)));

	//xVal, upper, lower
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xVal, inP->nCol*sizeof(*d_xVal)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBounds, inP->nCol*sizeof(*d_xBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBoundsP1, inP->nCol*sizeof(*d_xBoundsP1)));


	//bVal, bLo, bUp
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bVal, inP->nRow*sizeof(*d_bVal)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBounds, inP->nRow*sizeof(*d_bBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBoundsP1, inP->nRow*sizeof(*d_bBoundsP1)));

	if(inP->scaled){
		cutilSafeCallNoSync( cudaMalloc((void**) &d_xScaling, inP->nCol*sizeof(*d_xScaling)));
	} else {
		d_xScaling = NULL;
	}

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
	cutilSafeCallNoSync( cudaMalloc((void**) &d_seCoeff, nColPad*sizeof(*d_seCoeff)));
#endif
}

void KernelWrapperMono::deInitProblemStructure(){

	/*cudaThreadSynchronize();
	cout << cudaGetErrorString(cudaGetLastError()) << endl;*/

	cutilSafeCallNoSync(cudaFree(d_eqs));
	cutilSafeCallNoSync(cudaFree(d_objFunc));
	cutilSafeCallNoSync(cudaFree(d_objFuncAux));
	cutilSafeCallNoSync(cudaFree(d_basis));
	cutilSafeCallNoSync(cudaFree(d_xInd));

	cutilSafeCallNoSync(cudaFree(d_xVal));
	cutilSafeCallNoSync(cudaFree(d_xBounds));
	cutilSafeCallNoSync(cudaFree(d_xBoundsP1));

	cutilSafeCallNoSync(cudaFree(d_bVal));
	cutilSafeCallNoSync(cudaFree(d_bBounds));
	cutilSafeCallNoSync(cudaFree(d_bBoundsP1));

	if(d_xScaling != NULL)
		cutilSafeCallNoSync(cudaFree(d_xScaling));

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
	cutilSafeCallNoSync(cudaFree(d_seCoeff));
#endif

	deInitPivoting();
	deInitResult();
	deInitSwap();


	#if COL_CHOICE_METHOD == NESTED_STD
	deInitNested();
	#endif

}


void KernelWrapperMono::loadProblem(SimplexProblem *inP) {

	//Eqs
	cutilSafeCallNoSync( cudaMemcpy2D(d_eqs, pitch, inP->eqs, inP->nRow*sizeof(*d_eqs), inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyHostToDevice) );

	//Obj
	cutilSafeCallNoSync( cudaMemcpy(d_objFunc, inP->objFunc, inP->nCol*sizeof(*d_objFunc), cudaMemcpyHostToDevice) );

	//Basis
	cutilSafeCallNoSync( cudaMemcpy(d_basis, inP->basis, inP->nRow*sizeof(*d_basis), cudaMemcpyHostToDevice) );

	// x index (non-basic)
	cutilSafeCallNoSync( cudaMemcpy(d_xInd, inP->xInd, inP->nCol*sizeof(*d_xInd), cudaMemcpyHostToDevice) );

	//xVal, upper, lower
	cutilSafeCallNoSync( cudaMemcpy(d_xVal, inP->xVal, inP->nCol*sizeof(*d_xVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(d_xBounds, inP->xBounds, inP->nCol*sizeof(*d_xBounds), cudaMemcpyHostToDevice) );


	//bVal, bLo, bUp
	cutilSafeCallNoSync( cudaMemcpy(d_bVal, inP->bVal, inP->nRow*sizeof(*d_bVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(d_bBounds, inP->bBounds, inP->nRow*sizeof(*d_bBounds), cudaMemcpyHostToDevice) );

	if(inP->scaled){
		cutilSafeCallNoSync( cudaMemcpy(d_xScaling, inP->xScaling, inP->nCol*sizeof(*d_xScaling), cudaMemcpyHostToDevice) );
	}
}



void KernelWrapperMono::initNested(SimplexProblem *inP){
	cutilSafeCallNoSync(cudaMalloc((void**) &d_mask, (inP->nCol-1)*sizeof(*d_mask)));
}

void KernelWrapperMono::deInitNested(){
	cutilSafeCallNoSync(cudaFree(d_mask));
}


/*!
 * Define the number of blocks and threads for CUDA
 * Create the result array on CPU and Device
 */
void KernelWrapperMono::initResult(SimplexProblem *inP){
	int nEl = MAX(inP->nRow, inP->nCol);
	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
#if MAPPED_RESULT && PINNED
	int flags = cudaHostAllocMapped;
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dRes, (void *)hRes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dERes, (void *)hERes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dIRes, (void *)hIRes, 0));
#elif PINNED
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#else
	hRes = (red_result_t*) malloc(MAX_BLOCKS*sizeof(red_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	hERes = (exp_result_t*) malloc(MAX_BLOCKS*sizeof(exp_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	hIRes = (int*) malloc(MAX_BLOCKS*sizeof(int));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#endif
}

void KernelWrapperMono::deInitResult(){
#if MAPPED_RESULT || PINNED
	cutilSafeCall(cudaFreeHost(hRes));
	cutilSafeCall(cudaFreeHost(hERes));
	cutilSafeCall(cudaFreeHost(hIRes));
#else
	free(hRes);
	free(hERes);
	free(hIRes)
#endif
	cutilSafeCallNoSync(cudaFree(dRes));
	cutilSafeCallNoSync(cudaFree(dERes));
	cutilSafeCallNoSync(cudaFree(dIRes));

}


/*!
 * \brief Initializing CUBLAS
 */
bool KernelWrapperMono::initPivoting(SimplexProblem *inP){

	cublasStatus stat;

	stat = cublasAlloc(inP->nRow, sizeof(*x), (void**)&x);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	stat = cublasAlloc(inP->nCol, sizeof(*y), (void**)&y);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}

	//d_zCol
	stat = cublasAlloc(inP->nRow, sizeof(*d_zCol), (void**)&d_zCol);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_zCol
	VAR_TYPE *tmpZ = new VAR_TYPE[inP->nRow];
	for(int i=0; i<inP->nRow; i++)
		tmpZ[i] = 0.0;
	cublasSetVector(inP->nRow, sizeof(VAR_TYPE), tmpZ, 1, d_zCol, 1);
	delete [] tmpZ;

	// d_one
	stat = cublasAlloc(1, sizeof(*d_one), (void**)&d_one);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_one
	VAR_TYPE tmpOne = 1.0;
	cublasSetVector(1, sizeof(VAR_TYPE), &tmpOne, 1, d_one, 1);



	return true;
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperMono::deInitPivoting(){
	cublasFree (y);
	cublasFree (x);
	cublasFree (d_zCol);
}

/*!
 * \brief Initializing swap
 */
void KernelWrapperMono::initSwap(){

	cutilSafeCallNoSync( cudaMalloc((void**) &swpInt, 1*sizeof(int)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt, 1*sizeof(float)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt2, 1*sizeof(float2)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpDbl, 1*sizeof(double)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpTyp, 1*sizeof(VAR_TYPE)) );
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperMono::deInitSwap(){

	cutilSafeCallNoSync(cudaFree(swpInt));
	cutilSafeCallNoSync(cudaFree(swpFlt));
	cutilSafeCallNoSync(cudaFree(swpFlt2));
	cutilSafeCallNoSync(cudaFree(swpDbl));
	cutilSafeCallNoSync(cudaFree(swpTyp));

}



/***********************************************/
/************** Find Functions *****************/
/***********************************************/

// Standard simplex functions
// Normal mode
red_result_t KernelWrapperMono::stdFindCol(SimplexProblem *inP){
	return argMaxPosW(inP->nCol-1, d_objFunc, 1);
}

// Steepest Edge
red_result_t KernelWrapperMono::seFindCol(SimplexProblem *inP){
	return pSEValExpandW(inP->nRow, inP->nCol-1, d_objFunc, d_xBounds);
}

red_result_t KernelWrapperMono::seFindColP1(SimplexProblem *inP){
	return pSEValExpandW(inP->nRow, inP->nCol-1, d_objFuncAux, d_xBoundsP1);
}

red_result_t KernelWrapperMono::seFindCol2(SimplexProblem *inP){
	pSEValExpand3W(inP->nRow, inP->nCol-1, d_objFunc, 1, d_eqs, P2S(pitch), d_xVal, d_xBounds, y);
	return argMaxAbsW(inP->nCol-1, y, 1);
}

red_result_t KernelWrapperMono::seFindCol2P1(SimplexProblem *inP){
	pSEValExpand3W(inP->nRow, inP->nCol-1, d_objFuncAux, 1, d_eqs, P2S(pitch), d_xVal, d_xBoundsP1, y);
	return argMaxAbsW(inP->nCol-1, y, 1);
}

red_result_t KernelWrapperMono::seFindCol3(SimplexProblem *inP){
	red_result result = argMaxAbsNBW(inP->nCol-1, d_objFunc, d_xBounds);
	return pSEValExpandTW(inP->nRow, inP->nCol-1, d_objFunc, d_xBounds, MAX(SE_TH_COEFF*fabs(result.value), EPS1));
}

red_result_t KernelWrapperMono::seFindCol3P1(SimplexProblem *inP){
	red_result result = argMaxAbsNBW(inP->nCol-1, d_objFuncAux, d_xBoundsP1);
	//printf("res : %f\n", SE_TH_COEFF*fabs(result.value));
	return pSEValExpandTW(inP->nRow, inP->nCol-1, d_objFuncAux, d_xBoundsP1, MAX(SE_TH_COEFF*fabs(result.value), EPS1));
}

red_result_t KernelWrapperMono::nestedFindCol(SimplexProblem *inP){
	red_result_t res;
	if(firstNested){ // first time we have to create the nested mask
		res = argMaxNestedW(inP->nCol-1, d_objFunc, 1, d_mask, NESTED_INIT);
		firstNested = false;
	} else { // Then we have to apply it
		res = argMaxNestedW(inP->nCol-1, d_objFunc, 1, d_mask, NESTED_APPLY);
		if(res.index == -1) // if the nested return nothing we have to create the mask again
			res = argMaxNestedW(inP->nCol-1, d_objFunc, 1, d_mask, NESTED_INIT);
	}

	if(res.index != -1){ // if we have a winner then we must unset its mask
		int zero = 0;
		cutilSafeCallNoSync( cudaMemcpy(&d_mask[res.index], &zero, sizeof(*d_mask), cudaMemcpyHostToDevice) );
	}

	return res;
}

red_result_t KernelWrapperMono::nestedFindColP1(SimplexProblem *inP){
	red_result_t res;
	if(firstNested){ // first time we have to create the nested mask
		res = argMaxNestedW(inP->nCol-1, d_objFuncAux, 1, d_mask, NESTED_INIT);
		firstNested = false;
	} else { // Then we have to apply it
		res = argMaxNestedW(inP->nCol-1, d_objFuncAux, 1, d_mask, NESTED_APPLY);
		if(res.index == -1) // if the nested return nothing we have to create the mask again
			res = argMaxNestedW(inP->nCol-1, d_objFuncAux, 1, d_mask, NESTED_INIT);
	}

	if(res.index != -1){ // if we have a winner then we must unset its mask
		int zero = 0;
		cutilSafeCallNoSync( cudaMemcpy(&d_mask[res.index], &zero, sizeof(*d_mask), cudaMemcpyHostToDevice) );
	}

	return res;
}

row_result_t KernelWrapperMono::expandFindRow(int col, VAR_TYPE seVal, double delta, double tau, SimplexProblem *inP){

	red_result_t res1;
	exp_result_t res2;
	row_result_t res;
	VAR_TYPE xVal;
	float2 xBound;
	float lo, up;
	VAR_TYPE alpha1, alpha2;
	VAR_TYPE alphaMin;
	int objSign = (seVal > 0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT_MEM")
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &d_xVal[col], 1 * sizeof(xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &d_xBounds[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT_MEM")

	lo = xBound.x;
	up = xBound.y;

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBounds, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	if(objSign == 1){
		if(up < FLT_MAX)
			alpha1 = (up + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha1 = xVal - (lo - delta);
		else
			alpha1 = MAX_TYPE;
	}

	alpha1 = MIN(alpha1, res1.value);

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		res.mode = M_UNBOUNDED;
		printf("We went there(unbounded)!");
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(up < FLT_MAX)
			alpha2 = up - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha2 = xVal - lo;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
	    alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		//printf("exp2 : %d : %f : %d\n", inP->nRow, alpha1, objSign);
		res2 = expandP2W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBounds, alpha1, objSign);
		//printf("res2 : %d : %f : %f\n", res2.index, res2.alpha, res2.absP);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1)
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);

	return res;
}



row_result_t KernelWrapperMono::expandFindRowP1(int col, VAR_TYPE seVal, double delta, double tau, SimplexProblem *inP){

	row_result_t res;
	red_result_t resRed;
	red_result_t res1;
	exp_result_t res2;
	VAR_TYPE xVal;
	float2 xBound, xBoundP1;
	float lo, up, loP1, upP1;
	VAR_TYPE alpha1, alpha2, alphaI, teta;
	VAR_TYPE alphaMin;
	int objSign = (seVal > 0.0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT")
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &d_xVal[col], 1 * sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &d_xBounds[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBoundP1, &d_xBoundsP1[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT")

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBoundsP1, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	 lo = xBound.x;
	 up = xBound.y;
	 loP1 = xBoundP1.x;
	 upP1 = xBoundP1.y;

	 //printf("Alpha : %f \n", res1.alpha);

	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha1 = (upP1 + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha1 = xVal - (loP1 - delta);
		else
			alpha1 = MAX_TYPE;
	}

	alpha1 = MIN(alpha1, res1.value);

	// Special step - trying to put a infeasible variable to it's feasible bound
	// Getting the max absolute pivot
	_START_EVENT(prof, "REDUCTION_MAX")
	resRed = argMaxAbsW(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	_END_EVENT(prof, "REDUCTION_MAX")
	// Tolerance on the pivot
	teta = fabs(resRed.value) * EXPAND_W;
	// Expand special Phase 1 step
	_START_EVENT(prof, "SPECIAL_P1_PHASE")
	res2 = expandSpecW(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], teta, objSign);
	_END_EVENT(prof, "SPECIAL_P1_PHASE")
	SET_E(res, res2);
	res.mode = M_INF_PIVOT;

	// Will proabaly not work
	if(up != upP1){
		//printf("inf var (up)\n");
		alphaI = xVal - up;
	} else if (lo != loP1) {
		//printf("inf var (lo)\n");
		alphaI = lo - xVal;
	} else {
		alphaI = 0;
	}

	/*if(alphaI != 0.0)
		printf("AlphaI : %e\n", alphaI);*/

	if(alphaI > res.alpha){
		res.index = -2;
		res.alpha = alphaI;
		res.absP = xVal;
		res.mode = M_INF_TO_BOUND;
	}

	if(res.alpha <= alpha1 && res.index != -1) {
		res.alpha *= (double)objSign;
		return res;
	}

	// if the pivoting step is limited by the bound of the variable
	// then we don't have to pivot

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		printf("We went there(unbounded) [P1]!");
		res.mode = M_UNBOUNDED;
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha2 = upP1 - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha2 = xVal - loP1;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
		alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		res2 = expandP2W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBoundsP1, alpha1, objSign);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1){
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);
	}

	return res;

}


/***********************************************/
/***************** Wrappers ********************/
/***********************************************/

red_result_t KernelWrapperMono::argMaxPosW(int nEl, VAR_TYPE* p1, int incP1){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxPos(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL

	if (CPU_FINAL_REDUCTION) { // CPU does the final reduction
#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
	    for(int i=0; i<nBlocks; i++)
	    	SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	} else { // launching kernels recursively
		int s=nBlocks;
		while(s > CPU_FINAL_THRESHOLD) {
			int threads = 0, blocks = 0;
			super::getNumBlocksAndThreads(s, blocks, threads);
			argMaxPosNextSteps(s, threads, blocks, dRes, dRes);
			s = (s + (threads*2-1)) / (threads*2);
		}
		if (s > 1) {
#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, s * sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
			for(int i=0; i < s; i++)
				SET(gpu_result, MAX_R(gpu_result, hRes[i]));
		} else {
#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( &gpu_result, &dRes[0], sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
		}
	}
	if(F_EQUAL_Z(gpu_result.value)) gpu_result.index = -1;
	return gpu_result;
}

red_result_t KernelWrapperMono::argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbs(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMono::argMaxAbsNBW(int nEl, VAR_TYPE* obj, float2* bounds){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbsNB(nEl, nThreads, nBlocks, obj, d_xVal, bounds, dRes);
	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMono::argMaxNestedW(int nEl, VAR_TYPE* p1, int incP1, int* mask, int type){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	if(type == NESTED_INIT){
		argMaxNestedInit(nEl, nThreads, nBlocks, p1, incP1, mask, dRes);
		_SYNC_KERNEL
	} else if (type == NESTED_APPLY) {
		argMaxNestedApply(nEl, nThreads, nBlocks, p1, incP1, mask, dRes);
		_SYNC_KERNEL
	}
		#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
		#endif
	    for(int i=0; i<nBlocks; i++)
	    	SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));

	return gpu_result;
}

red_result_t KernelWrapperMono::expandP1W(int nEl, VAR_TYPE* pivots, float2 *bounds, double delta, int objSign){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = MAX_TYPE;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP1(nEl, nThreads, nBlocks, pivots, d_bVal, bounds, delta, objSign, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET(gpu_result, MIN_R(gpu_result, hRes[i]));
		//printf("[%d] Index : %d - Value : %e \n", i, hERes[i].index, hERes[i].alpha);
	}

	return gpu_result;

}

exp_result_t KernelWrapperMono::expandP2W(int nEl, VAR_TYPE* pivots, float2* bounds, double alpha, int objSign){

	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.absP = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP2(nEl, nThreads, nBlocks, pivots, d_bVal, bounds, alpha, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET_E_P(gpu_result, MAX_E_P(gpu_result, hERes[i]));
		//printf("[%d] Index : %d - Value : %e \n", i, hERes[i].index, hERes[i].alpha);
	}

	return gpu_result;

}

exp_result_t KernelWrapperMono::expandSpecW(int nEl, VAR_TYPE* pivots, double teta, int objSign){

	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.alpha = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandSpec(nEl, nThreads, nBlocks, pivots, d_bVal, d_bBounds, d_bBoundsP1, teta, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
		SET_E_A(gpu_result, MAX_E_A(gpu_result, hERes[i]));


	return gpu_result;

}

void KernelWrapperMono::pSEValExpand3W( int m, int n, VAR_TYPE *obj, int incObj,
					 VAR_TYPE *eqs, int pitchEqs, VAR_TYPE *x,
					 float2 *xBounds, VAR_TYPE *result) {

	int threads = (m < MAX_THREADS*2) ? super::nextPow2((m + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(n, MAX_BLOCKS);
	pSEValExpand3(m, n, threads, blocks, obj, incObj, eqs, pitchEqs, x, xBounds, result);
	_SYNC_KERNEL
}


red_result_t KernelWrapperMono::pSEValExpandW( int m, int n, VAR_TYPE *obj, float2 *xBounds){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	int threads = (m < MAX_THREADS*2) ? super::nextPow2((m + 1)/ 2) : MAX_THREADS;

	int blocks = MIN(n, MAX_BLOCKS);

	//if(n > MAX_BLOCKS) {
		pSEValExpand(m, n, threads, blocks, obj, d_eqs, P2S(pitch), d_xVal, xBounds, dRes);
	//} else {
	//	pSEValExpandSmall(m, n, threads, blocks, obj, d_eqs, P2S(pitch), d_xVal, xBounds, dRes);
	//}
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, blocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<blocks; i++) {
		//cout << "SE coeff " << i << " : " << hRes[i].index << " : " << hRes[i].value << endl;
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMono::pSEValExpandTW( int m, int n, VAR_TYPE *obj, float2 *xBounds, VAR_TYPE threshold){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	int threads = (m < MAX_THREADS*2) ? super::nextPow2((m + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(n, MAX_BLOCKS);

	cout << "pSEValExpandTW (should not)" << endl;

	pSEValExpandT(m, n, threads, blocks, threshold, obj, d_eqs, P2S(pitch), d_xVal, xBounds, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, blocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<blocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

// WS without slack
int KernelWrapperMono::checkFeasXWSW(int nEl) {
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	// [CHANGE] 20/06/11 - replaced d_objFuncAux by y
	checkFeasXWS(nEl, nThreads, nBlocks, d_xVal, d_xBounds, d_xBoundsP1, y, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMono::checkFeasBWSW(int nEl) {
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	checkFeasBWS(nEl, nThreads, nBlocks, d_bVal, d_bBounds, d_bBoundsP1, x, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

// WS without slack
int KernelWrapperMono::checkFeasXV2W(int nEl) {
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	// [CHANGE] 20/06/11 - replaced d_objFuncAux by y
	checkFeasXV2(nEl, nThreads, nBlocks, d_xVal, d_xBounds, d_xBoundsP1, y, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMono::checkFeasBV2W(int nEl) {
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	checkFeasBV2(nEl, nThreads, nBlocks, d_bVal, d_bBounds, d_bBoundsP1, x, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMono::updateFeasBWSW(int nEl){
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	updateFeasBWS(	nEl, nThreads, nBlocks, d_bVal, d_bBounds, d_bBoundsP1, x, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}



int KernelWrapperMono::updateFeasXWSW(int nEl){
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	updateFeasXWS(nEl, nThreads, nBlocks, d_xVal,
				d_xBounds, d_xBoundsP1,
				d_objFuncAux, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMono::updateFeasV2W(int nVar, int nSlack){
	int sum = 0;

	super::getNumBlocksAndThreads(nVar+nSlack, nBlocks, nThreads);
	updateFeasV2(nVar+nSlack, nThreads, nBlocks,
				 nVar, d_xVal, d_bVal,
				 d_xBounds, d_bBounds,
				 dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++){
		sum += hIRes[i];
	}

	return sum;
}



int KernelWrapperMono::resetXValW(int nEl){
	int sum = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	resetXVal(nEl, nThreads, nBlocks, d_xVal, d_xBounds, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

void KernelWrapperMono::orderValsW(int nRow, int nCol){
	int nEl = nRow + nCol - 1;
	int threads = (nEl < MAX_THREADS) ? super::nextPow2((nEl + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(MAX_BLOCKS, ceil((float)nEl/threads));
	//printf("nEl : %d - threads : %d - blocks : %d\n", nEl, threads, blocks);
	orderVals(nEl, threads, blocks, nCol-1, d_xVal, d_xInd, d_bVal, d_basis, y, x);
	_SYNC_KERNEL
}

void KernelWrapperMono::changeBoundW(int nRow, int nCol, int index, float2 bound){
	int nEl = nRow + nCol - 1;
	int threads = (nEl < MAX_THREADS) ? super::nextPow2((nEl + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(MAX_BLOCKS, ceil((float)nEl/threads));
	//printf("nEl : %d - threads : %d - blocks : %d\n", nEl, threads, blocks);
	//printf("Index : %d - lower %f - upper %f \n", index, bound.x, bound.y);
	changeBound(nEl, threads, blocks, nCol-1, index, bound, d_xInd, d_xBounds, d_basis, d_bBounds);
	_SYNC_KERNEL
}

void KernelWrapperMono::vectorMulW(int nEl, VAR_TYPE *inX, VAR_TYPE *inY){
	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	//printf("nEl : %d - threads : %d - blocks : %d\n", nEl, threads, blocks);
	vectorMul(nEl, nThreads, nBlocks, inX, inY);
	_SYNC_KERNEL
}


/***********************************************/
/************** MATRIX PROCESSING  *************/
/***********************************************/

void KernelWrapperMono::updatingBasisWS(int row, int col, VAR_TYPE alpha,  SimplexProblem *inP) {

	VAR_TYPE xVal;

	_START_EVENT(prof, "UPDATING_BASIS")

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// Updating xVal and swaping with bVal
	cublasGetVector(1, sizeof(xVal), &d_xVal[col], 1, &xVal, 1);
	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &d_bVal[row], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cublasSetVector(1, sizeof(xVal), &xVal, 1, &d_bVal[row], 1);

	// Normal bounds
	cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBounds[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xBounds[col], &d_bBounds[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_bBounds[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

	// swap basis / non-basic index
	cutilSafeCallNoSync( cudaMemcpy( swpInt, &d_xInd[col], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xInd[col], &d_basis[row], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_basis[row], swpInt, sizeof(int), cudaMemcpyDeviceToDevice) );

	// Update obj (optionnal)
	//cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	//inP->objVal += objCoef*alpha;


	if(inP->mode == inP->AUX){
		// Phase 1 bounds
		cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBoundsP1[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_xBoundsP1[col], &d_bBoundsP1[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_bBoundsP1[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

		// Useless imo
		//cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		//inP->objAuxVal += objCoef*alpha;
	}
	_SYNC_KERNEL
	_END_EVENT(prof, "UPDATING_BASIS")

}

void KernelWrapperMono::nonBasicToBound(int col, VAR_TYPE alpha, VAR_TYPE xVal, SimplexProblem *inP) {

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// New X_j value : previous value + alpha
	//printf("Val : %e + Alpha : %e alpha \n", xVal, alpha);
	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &xVal, 1 * sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
	_SYNC_KERNEL

	//cublasGetVector(1, sizeof(tmp), &d_objFunc[col], 1, &tmp, 1);
	//inP->objVal += tmp*alpha;
	/*if(inP->mode == inP->AUX){
		cublasGetVector(1, sizeof(tmp), &d_objFuncAux[col], 1, &tmp, 1);
		inP->objAuxVal += tmp*alpha;
	}*/

}

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
void KernelWrapperMono::pivotingSE(int row, int col, SimplexProblem *inP){

	VAR_TYPE pivot, tmp, objCoef;
	VAR_TYPE zero = 0.0;

	_START_EVENT(prof, "VAR_SWAP")
	cublasGetVector(1, sizeof(pivot), &d_eqs[IDX2C(row, col, P2S(pitch))], 1, &pivot, 1);
	//printf("Pivot : %e\n", pivot);

#if TYPE == USE_DOUBLE
	// Copy pivot Col into x
	cublasDcopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasDcopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasDcopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasDcopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	_SYNC_KERNEL
	_END_EVENT(prof, "VAR_SWAP")

	_START_EVENT(prof, "OBJ_UPDATE")
	cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	_SYNC_KERNEL
	_END_EVENT(prof, "OBJ_UPDATE")

	if(inP->mode == inP->AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	_START_EVENT(prof, "cublasDGER")
	//cublasDger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));

	// Init
	/*VAR_TYPE *d_eqs2, *eqs1, *eqs2;
	size_t pitch2;
	int nRowPad = 32*ceil((float)inP->nRow/32);
	int nColPad = 32*ceil((float)inP->nCol/32);
	eqs1 = new VAR_TYPE[inP->nRow*inP->nCol];
	eqs2 = new VAR_TYPE[inP->nRow*inP->nCol];
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_eqs2, &pitch2, nRowPad*sizeof(*d_eqs2), nColPad ));
	cutilSafeCallNoSync( cudaMemcpy2D(d_eqs2, pitch2, d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToDevice) );
*/
	// custom kernel on d_eqs
	int nBlocksX = ceil((float)inP->nRow/32);
	int nBlocksY = ceil((float)inP->nCol/32);
	//cout << "x blocks : " << nBlocksX << " y blocks : " << nBlocksY << endl;
	pivotingSEKernel(16, nBlocksX, nBlocksY, inP->nRow, inP->nCol,
						 tmp, d_eqs, P2S(pitch),
						 x, y, d_seCoeff);

	// custom kernel on d_eqs2
	/*cublasDger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs2, P2S(pitch2));
	cudaThreadSynchronize();
	// Check
	cout << "*****************************" << endl;
	cout << " Alpha : " << tmp << endl;
	cutilSafeCallNoSync( cudaMemcpy2D(eqs1, inP->nRow*sizeof(*d_eqs), d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy2D(eqs2, inP->nRow*sizeof(*d_eqs2), d_eqs2, pitch2, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );

	for(int i=0; i<inP->nRow; i++){
		for(int j=0; j<inP->nCol; j++){
			if(eqs1[IDX2C(i, j, inP->nRow)] - eqs2[IDX2C(i, j, inP->nRow)] > EPS1){
				cout << i << " - " << j << " -  eq1 : " << eqs1[IDX2C(i, j, inP->nRow)] << " -  eq2 : " << eqs2[IDX2C(i, j, inP->nRow)] << endl;
			}
			//if(eqs1[IDX2C(i, j, inP->nRow)] - eqs2[IDX2C(i, j, inP->nRow)] < EPS1 && eqs1[IDX2C(i, j, inP->nRow)] != 0){
			//	cout << " EQUAL : " << i << " - " << j << " -  eq1 : " << eqs1[IDX2C(i, j, inP->nRow)] << " -  eq2 : " << eqs2[IDX2C(i, j, inP->nRow)] << endl;
			//}
		}
	}

	getchar();


	delete [] eqs1;
	delete [] eqs2;
	cudaFree(d_eqs2);*/


	_SYNC_KERNEL
	_END_EVENT(prof, "cublasDGER")
#elif TYPE == USE_FLOAT
	// Copy pivot Col into x
	cublasScopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasScopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasScopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasScopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == inP->AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasSger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#endif
	_SYNC_KERNEL
}
#endif

void KernelWrapperMono::pivotingWS(int row, int col, SimplexProblem *inP){

	VAR_TYPE pivot, tmp, objCoef;
	VAR_TYPE zero = 0.0;

	_START_EVENT(prof, "VAR_SWAP")
	cublasGetVector(1, sizeof(pivot), &d_eqs[IDX2C(row, col, P2S(pitch))], 1, &pivot, 1);
	//printf("Pivot : %e\n", pivot);

#if TYPE == USE_DOUBLE
	// Copy pivot Col into x
	cublasDcopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasDcopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasDcopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasDcopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	_SYNC_KERNEL
	_END_EVENT(prof, "VAR_SWAP")

	_START_EVENT(prof, "OBJ_UPDATE")
	cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	_SYNC_KERNEL
	_END_EVENT(prof, "OBJ_UPDATE")

	if(inP->mode == inP->AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	_START_EVENT(prof, "cublasDGER")
	cublasDger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
	_SYNC_KERNEL
	_END_EVENT(prof, "cublasDGER")
#elif TYPE == USE_FLOAT
	// Copy pivot Col into x
	cublasScopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasScopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasScopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasScopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == inP->AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasSger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#endif
	_SYNC_KERNEL
}

void KernelWrapperMono::getDeviceBasis(SimplexProblem *inP){
	printf("Get basis. \n");
	_SYNC_KERNEL
	cutilSafeCallNoSync( cudaMemcpy( inP->basis, d_basis, inP->nRow * sizeof(*d_basis), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMono::getDeviceObj(SimplexProblem *inP){
	printf("Get obj. \n");
	_SYNC_KERNEL
	cutilSafeCallNoSync( cudaMemcpy( inP->objFunc, d_objFunc, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMono::getDeviceObjAux(SimplexProblem *inP){
	printf("Get obj aux. \n");
	_SYNC_KERNEL
	cutilSafeCallNoSync( cudaMemcpy( inP->objFuncAux, d_objFuncAux, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMono::getDeviceEqs(SimplexProblem *inP){
	printf("Get eqs. \n");
	_SYNC_KERNEL
	cutilSafeCallNoSync( cudaMemcpy2D(inP->eqs, inP->nRow*sizeof(*d_eqs), d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
}

/*
VAR_TYPE* KernelWrapper::getColumn(int ind, SimplexProblem *inP){
	printf("Get col. \n");
	VAR_TYPE* column = new VAR_TYPE[inP->nRow];
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy(column, &d_eqs[IDX2C(0, ind, P2S(pitch))], inP->nRow * sizeof(*x), cudaMemcpyDeviceToHost) );
	return column;
}
*/

void KernelWrapperMono::getDeviceData(SimplexProblem *inP){

	_SYNC_KERNEL
	cutilSafeCallNoSync( cudaMemcpy2D(inP->eqs, inP->nRow*sizeof(*d_eqs), d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->objFunc, d_objFunc, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->objFuncAux, d_objFuncAux, inP->nCol * sizeof(*d_objFuncAux), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->basis, d_basis, inP->nRow * sizeof(*d_basis), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xInd, d_xInd, inP->nCol * sizeof(*d_xInd), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->xVal, d_xVal, inP->nCol * sizeof(*d_xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBounds, d_xBounds, inP->nCol * sizeof(*d_xBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBoundsP1, d_xBoundsP1, inP->nCol * sizeof(*d_xBoundsP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->bVal, d_bVal, inP->nRow * sizeof(*d_bVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBounds, d_bBounds, inP->nRow * sizeof(*d_bBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBoundsP1, d_bBoundsP1, inP->nRow * sizeof(*d_bBoundsP1), cudaMemcpyDeviceToHost) );

}

void KernelWrapperMono::resetBoundsWS(SimplexProblem *inP){

	int nTriv = resetXValW(inP->nCol-1);
	//printf("Reset non basic bounds.\n");
	if(nTriv > 0){
		//printf("Reset basic values.\n");
		#if TYPE == USE_DOUBLE
			cublasDgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
		#elif TYPE == USE_FLOAT
			cublasSgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
		#endif
	}
	_SYNC_KERNEL

}

void KernelWrapperMono::checkInfeasWS(SimplexProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = inP->PRIMAL;

	infB = checkFeasBWSW(inP->nRow);
	infX = checkFeasXWSW(inP->nCol-1);

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		VAR_TYPE zero = 0;
		cutilSafeCallNoSync( cudaMemcpy( d_objFuncAux, y, (inP->nCol-1)*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_objFuncAux[inP->nCol-1], &zero, 1*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		inP->mode = inP->AUX;

		if(infB > 0){
			// x has the infeasibility coefficient setted in previously called checkFeasBW
			// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
			#if TYPE == USE_DOUBLE
				cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#elif TYPE == USE_FLOAT
				cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#endif
		}
	}
	_SYNC_KERNEL
}

void KernelWrapperMono::checkInfeasV2(SimplexProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = inP->PRIMAL;

	infB = checkFeasBV2W(inP->nRow);
	infX = checkFeasXV2W(inP->nCol-1);

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		VAR_TYPE zero = 0;
		cutilSafeCallNoSync( cudaMemcpy( d_objFuncAux, y, (inP->nCol-1)*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_objFuncAux[inP->nCol-1], &zero, 1*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		inP->mode = inP->AUX;

		if(infB > 0){
			// x has the infeasibility coefficient setted in previously called checkFeasBW
			// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
			#if TYPE == USE_DOUBLE
				cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#elif TYPE == USE_FLOAT
				cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#endif
		}
	}
	_SYNC_KERNEL

}

void KernelWrapperMono::updateInfeasWS(SimplexProblem *inP) {

	int infB = 0;
	int infX = 0;

	_START_EVENT(prof, "CHECK_BASIC")
	infB = checkFeasBWSW(inP->nRow);
	_END_EVENT(prof, "CHECK_BASIC")
	_START_EVENT(prof, "CHECK_NON_BASIC")
	infX = checkFeasXWSW(inP->nCol-1);
	_END_EVENT(prof, "CHECK_NON_BASIC")

	// Checking the basis feasability : return the number of non feasible becoming feasible
	if(inP->nInf != (infX + infB)){
		inP->nInf = infX + infB;

		_START_EVENT(prof, "UPDATE_OBJ_FUNC")

		if(inP->nInf > 0){
			VAR_TYPE zero = 0;
			// Y has the coefficient set in previously called checkFeasXW
			cutilSafeCallNoSync( cudaMemcpy( d_objFuncAux, y, (inP->nCol-1)*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
			cutilSafeCallNoSync( cudaMemcpy( &d_objFuncAux[inP->nCol-1], &zero, 1*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

			if(infB > 0){
				_START_EVENT(prof, "DGEMV")
				// x has the infeasibility coefficient setted in previously called checkFeasBW
				// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
				#if TYPE == USE_DOUBLE
					cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
				#elif TYPE == USE_FLOAT
					cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
				#endif
				_END_EVENT(prof, "DGEMV")
			}
		} else {
			inP->mode = inP->AUX;
		}

		_END_EVENT(prof, "UPDATE_OBJ_FUNC")
	}
	_SYNC_KERNEL
}

void KernelWrapperMono::updateInfeasV2(SimplexProblem *inP) {
	inP->nInf = updateFeasV2W(inP->nVar, inP->nSlack);
	if(inP->nInf > 0){
		inP->mode = inP->AUX;
	} else {
		inP->mode = inP->PRIMAL;
	}

	/*if(inP->nInf > 0){
		VAR_TYPE zero = 0;
		cutilSafeCallNoSync( cudaMemcpy( d_objFuncAux, y, (inP->nCol-1)*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_objFuncAux[inP->nCol-1], &zero, 1*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		inP->mode = inP->AUX;

		if(infB > 0){
			// x has the infeasibility coefficient setted in previously called checkFeasBW
			// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
			#if TYPE == USE_DOUBLE
				cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#elif TYPE == USE_FLOAT
				cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#endif
		}
	}*/
	_SYNC_KERNEL
}

/* Not working as intended */
/*
void KernelWrapperMono::updateInfeasWS(SimplexProblem *inP) {

	int nInf2FeasB = updateFeasBWSW(inP->nRow);
	int nInf2FeasX = updateFeasXWSW(inP->nCol-1);

	// Checking the basis feasability : return the number of non feasible becoming feasible
	if(nInf2FeasB > 0){
		// x has the infeasibility coefficient setted in previously called checkInfBasis
		// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
		#if TYPE == USE_DOUBLE
			cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
		#elif TYPE == USE_FLOAT
			cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
		#endif

	}

	inP->nInf -= nInf2FeasX + nInf2FeasB;
}
*/

void KernelWrapperMono::processOptimumWS(SimplexProblem *inP){

	#if TYPE == USE_DOUBLE
		inP->objVal = cublasDdot(inP->nCol, d_objFunc, 1, d_xVal, 1);
		cudaThreadSynchronize();
	#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(inP->nCol, d_objFunc, 1, d_xVal, 1);
	#endif

	if(inP->mode == inP->AUX) {
		#if TYPE == USE_DOUBLE
			inP->objAuxVal = cublasDdot(inP->nCol, d_objFuncAux, 1, d_xVal, 1);
			cudaThreadSynchronize();
		#elif TYPE == USE_FLOAT
			inP->objAuxVal = cublasSdot(inP->nCol, d_objFuncAux, 1, d_xVal, 1);
		#endif
	}
	_SYNC_KERNEL

}

void KernelWrapperMono::orderValsWS(SimplexProblem *inP){
	// Order the vals as they were at beginning (x1..xn, bn+1...bm)
	// Ordered values are put in y (xVal => 1..nCol) and x (bVal => 1..nRow)
	orderValsW(inP->nRow, inP->nCol);
	cutilSafeCallNoSync( cudaMemcpy( d_xVal, y, inP->nVar*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( d_bVal, x, inP->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	_SYNC_KERNEL
}

void KernelWrapperMono::changeBoundWK(SimplexProblem *inP, IndexPivoting *inI, int index, float2 bound){
	int loc = inI->xLoc[index];
	//printf("%d \n", loc);
	if(loc >= inP->nVar) {
		cutilSafeCallNoSync(cudaMemcpy(&d_bBounds[loc-inP->nVar], &bound, sizeof(float2), cudaMemcpyHostToDevice));
	} else {
		//printf("ncol : %d \n", inP->nCol);
		cutilSafeCallNoSync(cudaMemcpy(&d_xBounds[loc], &bound, sizeof(float2), cudaMemcpyHostToDevice));
	}
	_SYNC_KERNEL
}

void KernelWrapperMono::changeBoundWS(SimplexProblem *inP, int index, float2 bound){
	changeBoundW(inP->nRow, inP->nCol, index, bound);
	_SYNC_KERNEL
}

void KernelWrapperMono::getSolution(SimplexProblem *inP, SimplexSolution *inSol){
	// Order the vals as they were at beginning (x1..xn, bn+1...bm)
	// Ordered values are put in y (xVal => 1..nCol) and x (bVal => 1..nRow)
	orderValsW(inP->nRow, inP->nCol);
	cutilSafeCallNoSync(cudaMemcpy(inSol->xVal, y, inP->nVar*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	cutilSafeCallNoSync(cudaMemcpy(inSol->sVal, x, inP->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
}

/*
 * ATTENTION READ THIS :
 * This method wont works if debug mode is on since the value of the initial problem is updated
 */
void KernelWrapperMono::reinitPWS(SimplexProblem *inP){
	// Order the val (so we begin with a nearly optimal solution)

	orderValsWS(inP);

	// Then we copy the initial problem (again) to start from fresh values
	// Eqs
	cutilSafeCallNoSync( cudaMemcpy2D(d_eqs, pitch, inP->eqs, inP->nRow*sizeof(*d_eqs), inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyHostToDevice) );

	// Obj
	cutilSafeCallNoSync( cudaMemcpy(d_objFunc, inP->objFunc, inP->nCol*sizeof(*d_objFunc), cudaMemcpyHostToDevice) );

	// x and b index
	cutilSafeCallNoSync( cudaMemcpy(d_xInd, inP->xInd, inP->nCol*sizeof(*d_xInd), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(d_basis, inP->basis, inP->nRow*sizeof(*d_basis), cudaMemcpyHostToDevice) );

	// Bounds
	cutilSafeCallNoSync( cudaMemcpy( d_xBounds, inP->xBounds, inP->nCol * sizeof(*d_xBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( d_bBounds, inP->bBounds, inP->nRow * sizeof(*d_bBounds), cudaMemcpyHostToDevice) );

}

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperMono::selectSECoeff(SimplexProblem *inP){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(inP->nVar, nBlocks, nThreads);

	selectSECoeffRevised(inP->nVar, nThreads, nBlocks,
						 d_seCoeff, d_xVal, d_xBounds, d_objFunc, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}
#endif

#if PIVOTING_METHOD == PIVOTING_OPTIMISED
/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperMono::selectSECoeffP1(SimplexProblem *inP){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(inP->nVar, nBlocks, nThreads);

	selectSECoeffRevised(inP->nVar, nThreads, nBlocks,
						 d_seCoeff, d_xVal, d_xBoundsP1, d_objFuncAux, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}
#endif

