//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * KernelWrapper.cpp
 *
 *  Created on: Oct 31, 2010
 *      Author: meyerx
 */

#include "KernelWrapperMulti.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "cutil_inline.h"
#include "cublas.h"

KernelWrapperMulti::KernelWrapperMulti() {
	cublasOn = false;

}

KernelWrapperMulti::KernelWrapperMulti(int inDev) {
	cublasOn = false;
	dev = inDev;
}


KernelWrapperMulti::~KernelWrapperMulti() {
}

/***********************************************/
/************ MISC FUNCTIONS *******************/
/***********************************************/

extern "C"
bool isPow2(unsigned int x)
{
    return ((x&(x-1))==0);
}

unsigned int nextPow2( unsigned int x ) {
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

void KernelWrapperMulti::getNumBlocksAndThreads( int n, int &blocks, int &threads)
{
	threads = (n < MAX_THREADS*2) ? nextPow2((n + 1)/ 2) : MAX_THREADS;
    blocks = MIN(MAX_BLOCKS,(n + (threads * 2 - 1)) / (threads * 2));
}

/***********************************************/
/************** Init Functions *****************/
/***********************************************/

/*!
 * Call the init functions
 */
void KernelWrapperMulti::initCuda(int device, LocalSProblem *inP){

	if(!cublasOn){
#if PINNED && MAPPED_RESULT
		cutilSafeCall(cudaSetDevice(device));
		cutilSafeCall(cudaSetDeviceFlags(cudaDeviceMapHost));
#else
		cutilSafeCall(cudaSetDevice(device));
#endif

		//printf("Running on device %d ! \n", device);
		if(cublasInit() != CUBLAS_STATUS_SUCCESS){
			printf("Cublas error at init.\n");
			throw 11;
		}
	}

	initResult(inP);
	initPivoting(inP);
	initSwap();
#if COL_CHOICE_METHOD == NESTED_STD
	initNested(inP);
#endif

	//Eqs
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_eqs, &pitch, inP->nRow*sizeof(*d_eqs), inP->nCol ));
	cutilSafeCallNoSync( cudaMemcpy2D(d_eqs, pitch, inP->eqs, inP->nRow*sizeof(*d_eqs), inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyHostToDevice) );

	//Obj
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFunc, inP->nCol*sizeof(*d_objFunc)));
	cutilSafeCallNoSync( cudaMemcpy(d_objFunc, inP->objFunc, inP->nCol*sizeof(*d_objFunc), cudaMemcpyHostToDevice) );

	//ObjAux
	cutilSafeCallNoSync( cudaMalloc((void**) &d_objFuncAux, inP->nCol*sizeof(*d_objFuncAux)));

	//Basis
	cutilSafeCallNoSync( cudaMalloc((void**) &d_basis, inP->nRow*sizeof(*d_basis)));
	cutilSafeCallNoSync( cudaMemcpy(d_basis, inP->basis, inP->nRow*sizeof(*d_basis), cudaMemcpyHostToDevice) );

	// x index (non-basic)
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xInd, inP->nCol*sizeof(*d_xInd)));
	cutilSafeCallNoSync( cudaMemcpy(d_xInd, inP->xInd, inP->nCol*sizeof(*d_xInd), cudaMemcpyHostToDevice) );

	//xVal, upper, lower
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xVal, inP->nCol*sizeof(*d_xVal)));
	cutilSafeCallNoSync( cudaMemcpy(d_xVal, inP->xVal, inP->nCol*sizeof(*d_xVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBounds, inP->nCol*sizeof(*d_xBounds)));
	cutilSafeCallNoSync( cudaMemcpy(d_xBounds, inP->xBounds, inP->nCol*sizeof(*d_xBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMalloc((void**) &d_xBoundsP1, inP->nCol*sizeof(*d_xBoundsP1)));


	//bVal, bLo, bUp
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bVal, inP->nRow*sizeof(*d_bVal)));
	cutilSafeCallNoSync( cudaMemcpy(d_bVal, inP->bVal, inP->nRow*sizeof(*d_bVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBounds, inP->nRow*sizeof(*d_bBounds)));
	cutilSafeCallNoSync( cudaMemcpy(d_bBounds, inP->bBounds, inP->nRow*sizeof(*d_bBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMalloc((void**) &d_bBoundsP1, inP->nRow*sizeof(*d_bBoundsP1)));

	if(inP->scaled){
		cutilSafeCallNoSync( cudaMalloc((void**) &d_xScaling, inP->nCol*sizeof(*d_xScaling)));
		cutilSafeCallNoSync( cudaMemcpy(d_xScaling, inP->xScaling, inP->nCol*sizeof(*d_xScaling), cudaMemcpyHostToDevice) );
	}

}

void KernelWrapperMulti::deInitCuda(bool deInitCublas){

	cutilSafeCallNoSync(cudaFree(d_eqs));
	cutilSafeCallNoSync(cudaFree(d_objFunc));
	cutilSafeCallNoSync(cudaFree(d_objFuncAux));
	cutilSafeCallNoSync(cudaFree(d_basis));
	cutilSafeCallNoSync(cudaFree(d_xInd));


	cutilSafeCallNoSync(cudaFree(d_xVal));
	cutilSafeCallNoSync(cudaFree(d_xBounds));
	cutilSafeCallNoSync(cudaFree(d_xBoundsP1));

	cutilSafeCallNoSync(cudaFree(d_bVal));
	cutilSafeCallNoSync(cudaFree(d_bBounds));
	cutilSafeCallNoSync(cudaFree(d_bBoundsP1));

	if(d_xScaling != NULL)
		cutilSafeCallNoSync(cudaFree(d_xScaling));

	deInitPivoting();
	deInitResult();
	deInitSwap();

#if COL_CHOICE_METHOD == NESTED_STD
	deInitNested();
#endif

	cudaThreadExit();

	cublasOn = !deInitCublas;
	if(deInitCublas)
		cublasShutdown();


}


void KernelWrapperMulti::initNested(LocalSProblem *inP){
	cutilSafeCallNoSync(cudaMalloc((void**) &d_mask, (inP->nCol-1)*sizeof(*d_mask)));
}

void KernelWrapperMulti::deInitNested(){
	cutilSafeCallNoSync(cudaFree(d_mask));
}


/*!
 * Define the number of blocks and threads for CUDA
 * Create the result array on CPU and Device
 */
void KernelWrapperMulti::initResult(LocalSProblem *inP){
	int nEl = MAX(inP->nRow, inP->nCol);
	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
#if MAPPED_RESULT && PINNED
	int flags = cudaHostAllocMapped;
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dRes, (void *)hRes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dERes, (void *)hERes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dIRes, (void *)hIRes, 0));
#elif PINNED
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#else
	hRes = (red_result_t*) malloc(MAX_BLOCKS*sizeof(red_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	hERes = (exp_result_t*) malloc(MAX_BLOCKS*sizeof(exp_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	hIRes = (int*) malloc(MAX_BLOCKS*sizeof(int));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#endif
}

void KernelWrapperMulti::deInitResult(){
#if MAPPED_RESULT || PINNED
	cutilSafeCall(cudaFreeHost(hRes));
	cutilSafeCall(cudaFreeHost(hERes));
	cutilSafeCall(cudaFreeHost(hIRes));
#else
	free(hRes);
	free(hERes);
	free(hIRes)
#endif
	cutilSafeCallNoSync(cudaFree(dRes));
	cutilSafeCallNoSync(cudaFree(dERes));
	cutilSafeCallNoSync(cudaFree(dIRes));

}


/*!
 * \brief Initializing CUBLAS
 */
bool KernelWrapperMulti::initPivoting(LocalSProblem *inP){

	cublasStatus stat;

	stat = cublasAlloc(inP->nRow, sizeof(*x), (void**)&x);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	stat = cublasAlloc(inP->nCol, sizeof(*y), (void**)&y);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}

	//d_zCol
	stat = cublasAlloc(inP->nRow, sizeof(*d_zCol), (void**)&d_zCol);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_zCol
	VAR_TYPE *tmpZ = new VAR_TYPE[inP->nRow];
	for(int i=0; i<inP->nRow; i++)
		tmpZ[i] = 0.0;
	cublasSetVector(inP->nRow, sizeof(VAR_TYPE), tmpZ, 1, d_zCol, 1);
	delete [] tmpZ;

	// d_one
	stat = cublasAlloc(1, sizeof(*d_one), (void**)&d_one);
	if (stat != CUBLAS_STATUS_SUCCESS) {
		printf ("device memory allocation failed");
		return false;
	}
	// init d_one
	VAR_TYPE tmpOne = 1.0;
	cublasSetVector(1, sizeof(VAR_TYPE), &tmpOne, 1, d_one, 1);



	return true;
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperMulti::deInitPivoting(){
	cublasFree (y);
	cublasFree (x);
	cublasFree (d_zCol);
}

/*!
 * \brief Initializing swap
 */
void KernelWrapperMulti::initSwap(){

	cutilSafeCallNoSync( cudaMalloc((void**) &swpInt, 1*sizeof(int)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt, 1*sizeof(float)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt2, 1*sizeof(float2)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpDbl, 1*sizeof(double)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpTyp, 1*sizeof(VAR_TYPE)) );
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperMulti::deInitSwap(){

	cutilSafeCallNoSync(cudaFree(swpInt));
	cutilSafeCallNoSync(cudaFree(swpFlt));
	cutilSafeCallNoSync(cudaFree(swpFlt2));
	cutilSafeCallNoSync(cudaFree(swpDbl));
	cutilSafeCallNoSync(cudaFree(swpTyp));

}



/***********************************************/
/************** Find Functions *****************/
/***********************************************/

// Standard simplex functions
// Normal mode
red_result_t KernelWrapperMulti::stdFindCol(LocalSProblem *inP){
	return argMaxPosW(inP->nCol-1, d_objFunc, 1);
}

// Steepest Edge
red_result_t KernelWrapperMulti::seFindCol(LocalSProblem *inP){
	return pSEValExpandW(inP->nRow, inP->nVar, d_objFunc, d_xBounds);
}

red_result_t KernelWrapperMulti::seFindColP1(LocalSProblem *inP){
	return pSEValExpandW(inP->nRow, inP->nVar, d_objFuncAux, d_xBoundsP1);
}

red_result_t KernelWrapperMulti::seFindCol2(LocalSProblem *inP){
	pSEValExpand3W(inP->nRow, inP->nVar, d_objFunc, 1, d_eqs, P2S(pitch), d_xVal, d_xBounds, y);
	return argMaxAbsW(inP->nVar, y, 1);
}

red_result_t KernelWrapperMulti::seFindCol2P1(LocalSProblem *inP){
	pSEValExpand3W(inP->nRow, inP->nVar, d_objFuncAux, 1, d_eqs, P2S(pitch), d_xVal, d_xBoundsP1, y);
	return argMaxAbsW(inP->nVar, y, 1);
}

red_result_t KernelWrapperMulti::seFindCol3(LocalSProblem *inP){
	red_result result = argMaxAbsNBW(inP->nVar, d_objFunc, d_xBounds);
	return pSEValExpandTW(inP->nRow, inP->nVar, d_objFunc, d_xBounds, MAX(SE_TH_COEFF*fabs(result.value), EPS1));
}

red_result_t KernelWrapperMulti::seFindCol3P1(LocalSProblem *inP){
	red_result result = argMaxAbsNBW(inP->nVar, d_objFuncAux, d_xBoundsP1);
	return pSEValExpandTW(inP->nRow, inP->nVar, d_objFuncAux, d_xBoundsP1, MAX(SE_TH_COEFF*fabs(result.value), EPS1));
}

red_result_t KernelWrapperMulti::nestedFindCol(LocalSProblem *inP){
	red_result_t res;
	if(firstNested){ // first time we have to create the nested mask
		res = argMaxNestedW(inP->nVar, d_objFunc, 1, d_mask, NESTED_INIT);
		firstNested = false;
	} else { // Then we have to apply it
		res = argMaxNestedW(inP->nVar, d_objFunc, 1, d_mask, NESTED_APPLY);
		if(res.index == -1) // if the nested return nothing we have to create the mask again
			res = argMaxNestedW(inP->nVar, d_objFunc, 1, d_mask, NESTED_INIT);
	}

	if(res.index != -1){ // if we have a winner then we must unset its mask
		int zero = 0;
		cutilSafeCallNoSync( cudaMemcpy(&d_mask[res.index], &zero, sizeof(*d_mask), cudaMemcpyHostToDevice) );
	}

	return res;
}

red_result_t KernelWrapperMulti::nestedFindColP1(LocalSProblem *inP){
	red_result_t res;
	if(firstNested){ // first time we have to create the nested mask
		res = argMaxNestedW(inP->nVar, d_objFuncAux, 1, d_mask, NESTED_INIT);
		firstNested = false;
	} else { // Then we have to apply it
		res = argMaxNestedW(inP->nVar, d_objFuncAux, 1, d_mask, NESTED_APPLY);
		if(res.index == -1) // if the nested return nothing we have to create the mask again
			res = argMaxNestedW(inP->nVar, d_objFuncAux, 1, d_mask, NESTED_INIT);
	}

	if(res.index != -1){ // if we have a winner then we must unset its mask
		int zero = 0;
		cutilSafeCallNoSync( cudaMemcpy(&d_mask[res.index], &zero, sizeof(*d_mask), cudaMemcpyHostToDevice) );
	}

	return res;
}

row_result_t KernelWrapperMulti::expandFindRow(int col, VAR_TYPE seVal, double delta, double tau, LocalSProblem *inP){

	red_result_t res1;
	exp_result_t res2;
	row_result_t res;
	VAR_TYPE xVal;
	float2 xBound;
	float lo, up;
	VAR_TYPE alpha1, alpha2;
	VAR_TYPE alphaMin;
	int objSign = (seVal > 0 ? 1 : -1);

	// Phase 1 : we expand the bound
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &d_xVal[col], 1 * sizeof(xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &d_xBounds[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );

	lo = xBound.x;
	up = xBound.y;

	res1 = expandP1W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBounds, delta, objSign);

	if(objSign == 1){
		if(up < FLT_MAX)
			alpha1 = (up + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha1 = xVal - (lo - delta);
		else
			alpha1 = MAX_TYPE;
	}

	alpha1 = MIN(alpha1, res1.value);

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		res.mode = M_UNBOUNDED;
		printf("We went there(unbounded)!");
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(up < FLT_MAX)
			alpha2 = up - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha2 = xVal - lo;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
	    alphaMin = tau;
	} else {
		//printf("exp2 : %d : %f : %d\n", inP->nRow, alpha1, objSign);
		res2 = expandP2W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBounds, alpha1, objSign);
		//printf("res2 : %d : %f : %f\n", res2.index, res2.alpha, res2.absP);
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1)
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);

	return res;
}



row_result_t KernelWrapperMulti::expandFindRowP1(int col, VAR_TYPE seVal, double delta, double tau, LocalSProblem *inP){

	row_result_t res;
	red_result_t resRed;
	red_result_t res1;
	exp_result_t res2;
	VAR_TYPE xVal;
	float2 xBound, xBoundP1;
	float lo, up, loP1, upP1;
	VAR_TYPE alpha1, alpha2, alphaI, teta;
	VAR_TYPE alphaMin;
	int objSign = (seVal > 0.0 ? 1 : -1);

	// Phase 1 : we expand the bound
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &d_xVal[col], 1 * sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &d_xBounds[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBoundP1, &d_xBoundsP1[col], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	res1 = expandP1W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBoundsP1, delta, objSign);

	 lo = xBound.x;
	 up = xBound.y;
	 loP1 = xBoundP1.x;
	 upP1 = xBoundP1.y;

	 //printf("Alpha : %f \n", res1.alpha);

	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha1 = (upP1 + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha1 = xVal - (loP1 - delta);
		else
			alpha1 = MAX_TYPE;
	}

	alpha1 = MIN(alpha1, res1.value);

	// Special step - trying to put a infeasible variable to it's feasible bound
	// Getting the max absolute pivot
	resRed = argMaxAbsW(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	// Tolerance on the pivot
	teta = fabs(resRed.value) * EXPAND_W;
	// Expand special Phase 1 step
	res2 = expandSpecW(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], teta, objSign);
	SET_E(res, res2);
	res.mode = M_INF_PIVOT;

	// Will proabaly not work
	if(up != upP1){
		//printf("inf var (up)\n");
		alphaI = xVal - up;
	} else if (lo != loP1) {
		//printf("inf var (lo)\n");
		alphaI = lo - xVal;
	} else {
		alphaI = 0;
	}

	/*if(alphaI != 0.0)
		printf("AlphaI : %e\n", alphaI);*/

	if(alphaI > res.alpha){
		res.index = -2;
		res.alpha = alphaI;
		res.absP = xVal;
		res.mode = M_INF_TO_BOUND;
	}

	if(res.alpha <= alpha1 && res.index != -1) {
		res.alpha *= (double)objSign;
		return res;
	}

	// if the pivoting step is limited by the bound of the variable
	// then we don't have to pivot

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		printf("We went there(unbounded)!");
		res.mode = M_UNBOUNDED;
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha2 = upP1 - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha2 = xVal - loP1;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
		alphaMin = tau;
	} else {
		res2 = expandP2W(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], d_bBoundsP1, alpha1, objSign);
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1){
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);
	}

	return res;

}


/***********************************************/
/***************** Wrappers ********************/
/***********************************************/

red_result_t KernelWrapperMulti::argMaxPosW(int nEl, VAR_TYPE* p1, int incP1){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxPos(nEl, nThreads, nBlocks, p1, incP1, dRes);
	//cudaThreadSynchronize();

	if (CPU_FINAL_REDUCTION) { // CPU does the final reduction
#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
	    for(int i=0; i<nBlocks; i++)
	    	SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	} else { // launching kernels recursively
		int s=nBlocks;
		while(s > CPU_FINAL_THRESHOLD) {
			int threads = 0, blocks = 0;
			getNumBlocksAndThreads(s, blocks, threads);
			argMaxPosNextSteps(s, threads, blocks, dRes, dRes);
			s = (s + (threads*2-1)) / (threads*2);
		}
		if (s > 1) {
#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, s * sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
			for(int i=0; i < s; i++)
				SET(gpu_result, MAX_R(gpu_result, hRes[i]));
		} else {
#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( &gpu_result, &dRes[0], sizeof(red_result_t), cudaMemcpyDeviceToHost) );
#endif
		}
	}
	if(F_EQUAL_Z(gpu_result.value)) gpu_result.index = -1;
	return gpu_result;
}

red_result_t KernelWrapperMulti::argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbs(nEl, nThreads, nBlocks, p1, incP1, dRes);
	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMulti::argMaxAbsNBW(int nEl, VAR_TYPE* obj, float2* bounds){
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbsNB(nEl, nThreads, nBlocks, obj, d_xVal, bounds, dRes);
	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMulti::argMaxNestedW(int nEl, VAR_TYPE* p1, int incP1, int* mask, int type){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	if(type == NESTED_INIT)
		argMaxNestedInit(nEl, nThreads, nBlocks, p1, incP1, mask, dRes);
	else if (type == NESTED_APPLY)
		argMaxNestedApply(nEl, nThreads, nBlocks, p1, incP1, mask, dRes);

		//cudaThreadSynchronize();
		#if !(PINNED_RESULT && PINNED)
			cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
		#endif
	    for(int i=0; i<nBlocks; i++)
	    	SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));

	return gpu_result;
}

red_result_t KernelWrapperMulti::expandP1W(int nEl, VAR_TYPE* pivots, float2 *bounds, double delta, int objSign){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = MAX_TYPE;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP1(nEl, nThreads, nBlocks, pivots, d_bVal, bounds, delta, objSign, dRes);

	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET(gpu_result, MIN_R(gpu_result, hRes[i]));
		//printf("[%d] Index : %d - Value : %e \n", i, hERes[i].index, hERes[i].alpha);
	}

	return gpu_result;

}

exp_result_t KernelWrapperMulti::expandP2W(int nEl, VAR_TYPE* pivots, float2* bounds, double alpha, int objSign){

	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.absP = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP2(nEl, nThreads, nBlocks, pivots, d_bVal, bounds, alpha, objSign, dERes);

	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET_E_P(gpu_result, MAX_E_P(gpu_result, hERes[i]));
		//printf("[%d] Index : %d - Value : %e \n", i, hERes[i].index, hERes[i].alpha);
	}

	return gpu_result;

}

exp_result_t KernelWrapperMulti::expandSpecW(int nEl, VAR_TYPE* pivots, double teta, int objSign){

	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.alpha = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandSpec(nEl, nThreads, nBlocks, pivots, d_bVal, d_bBounds, d_bBoundsP1, teta, objSign, dERes);

	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
		SET_E_A(gpu_result, MAX_E_A(gpu_result, hERes[i]));


	return gpu_result;

}

void KernelWrapperMulti::pSEValExpand3W( int m, int n, VAR_TYPE *obj, int incObj,
					 VAR_TYPE *eqs, int pitchEqs, VAR_TYPE *x,
					 float2 *xBounds, VAR_TYPE *result) {

	int threads = (m < MAX_THREADS*2) ? nextPow2((m + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(n, MAX_BLOCKS);
	pSEValExpand3(m, n, threads, blocks, obj, incObj, eqs, pitchEqs, x, xBounds, result);
}


red_result_t KernelWrapperMulti::pSEValExpandW( int m, int n, VAR_TYPE *obj, float2 *xBounds){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	int threads = (m < MAX_THREADS*2) ? nextPow2((m + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(n, MAX_BLOCKS);

	pSEValExpand(m, n, threads, blocks, obj, d_eqs, P2S(pitch), d_xVal, xBounds, dRes);
	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, blocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<blocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

red_result_t KernelWrapperMulti::pSEValExpandTW( int m, int n, VAR_TYPE *obj, float2 *xBounds, VAR_TYPE threshold){

	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	int threads = (m < MAX_THREADS*2) ? nextPow2((m + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(n, MAX_BLOCKS);

	pSEValExpandT(m, n, threads, blocks, threshold, obj, d_eqs, P2S(pitch), d_xVal, xBounds, dRes);
	//cudaThreadSynchronize();
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, blocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<blocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
		//printf("hRes[%d] value : %e \n", hRes[i].index, hRes[i].value);
	}

	return gpu_result;
}

// WS without slack
int KernelWrapperMulti::checkFeasXWSW(int nEl) {
	int sum = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	//printf("nEl : %d - nBlocks : %d - nThreads : %d \n", nEl, nBlocks, nThreads);
	checkFeasXWS(nEl, nThreads, nBlocks, d_xVal, d_xBounds, d_xBoundsP1, d_objFuncAux, dIRes);
	//cudaThreadSynchronize();

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMulti::checkFeasBWSW(int nEl) {
	int sum = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	checkFeasBWS(nEl, nThreads, nBlocks, d_bVal, d_bBounds, d_bBoundsP1, x, dIRes);
	//cudaThreadSynchronize();

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}



int KernelWrapperMulti::updateFeasBWSW(int nEl){
	int sum = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	updateFeasBWS(	nEl, nThreads, nBlocks, d_bVal, d_bBounds, d_bBoundsP1, x, dIRes);

	//cudaThreadSynchronize();

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}



int KernelWrapperMulti::updateFeasXWSW(int nEl){
	int sum = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	updateFeasXWS(nEl, nThreads, nBlocks, d_xVal,
				d_xBounds, d_xBoundsP1,
				d_objFuncAux, dIRes);

	//cudaThreadSynchronize();

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperMulti::resetXValW(int nEl){
	int sum = 0;

	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	resetXVal(nEl, nThreads, nBlocks, d_xVal, d_xBounds, dIRes);

	//cudaThreadSynchronize();

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

void KernelWrapperMulti::orderValsW(int nRow, int nCol){
	int nEl = nRow + nCol - 1;
	int threads = (nEl < MAX_THREADS) ? nextPow2((nEl + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(MAX_BLOCKS, ceil((float)nEl/threads));
	//printf("nEl : %d - threads : %d - blocks : %d\n", nEl, threads, blocks);
	orderVals(nEl, threads, blocks, nCol-1, d_xVal, d_xInd, d_bVal, d_basis, y, x);
}

void KernelWrapperMulti::vectorMulW(int nEl, VAR_TYPE *inX, VAR_TYPE *inY){
	getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	//printf("nEl : %d - threads : %d - blocks : %d\n", nEl, threads, blocks);
	vectorMul(nEl, nThreads, nBlocks, inX, inY);
}


/***********************************************/
/************** MATRIX PROCESSING  *************/
/***********************************************/

void KernelWrapperMulti::updatingBasisWS(int row, int col, VAR_TYPE alpha,  LocalSProblem *inP) {

	VAR_TYPE xVal;

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// Updating xVal and swaping with bVal
	cublasGetVector(1, sizeof(xVal), &d_xVal[col], 1, &xVal, 1);
	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &d_bVal[row], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cublasSetVector(1, sizeof(xVal), &xVal, 1, &d_bVal[row], 1);

	// Normal bounds
	cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBounds[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xBounds[col], &d_bBounds[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_bBounds[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

	// swap basis / non-basic index
	cutilSafeCallNoSync( cudaMemcpy( swpInt, &d_xInd[col], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xInd[col], &d_basis[row], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_basis[row], swpInt, sizeof(int), cudaMemcpyDeviceToDevice) );

	// Update obj (optionnal)
	//cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	//inP->objVal += objCoef*alpha;


	if(inP->mode == SimplexProblem::AUX){
		// Phase 1 bounds
		cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBoundsP1[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_xBoundsP1[col], &d_bBoundsP1[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_bBoundsP1[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

		// Useless imo
		//cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		//inP->objAuxVal += objCoef*alpha;
	}


}

void KernelWrapperMulti::nonBasicToBound(int col, VAR_TYPE alpha, VAR_TYPE xVal, LocalSProblem *inP) {

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// New X_j value : previous value + alpha
	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &xVal, 1 * sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
}

void KernelWrapperMulti::pivotingWS(int row, int col, LocalSProblem *inP){

	VAR_TYPE pivot, tmp, objCoef;
	VAR_TYPE zero = 0.0;

	cublasGetVector(1, sizeof(pivot), &d_eqs[IDX2C(row, col, P2S(pitch))], 1, &pivot, 1);
	//printf("Pivot : %e\n", pivot);

#if TYPE == USE_DOUBLE
	// Copy pivot Col into x
	cublasDcopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasDcopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasDcopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasDcopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == SimplexProblem::AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasDaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasDger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#elif TYPE == USE_FLOAT
	// Copy pivot Col into x
	cublasScopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	// set pivot Col value
	tmp = pivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);

	// Replacing entering variable by leaving variable in eqs
	cublasScopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasScopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	// Copy pivot Row into y
	cublasScopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);

	tmp = -1.0/pivot;

	// Obj coef
	cublasGetVector(1, sizeof(objCoef), &d_objFunc[col], 1, &objCoef, 1);
	// Replacing entering variable by leaving into objFunc
	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	// Processing new obj
	cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == inP->AUX){
		// Obj coef
		cublasGetVector(1, sizeof(objCoef), &d_objFuncAux[col], 1, &objCoef, 1);
		// Replacing entering variable by leaving into objFunc
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);
		// Processing new obj
		cublasSaxpy(inP->nCol, objCoef*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasSger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#endif
}

void KernelWrapperMulti::getDeviceBasis(LocalSProblem *inP){
	//printf("Get basis. \n");
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy( inP->basis, d_basis, inP->nRow * sizeof(*d_basis), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMulti::getDeviceObj(LocalSProblem *inP){
	//printf("Get obj. \n");
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy( inP->objFunc, d_objFunc, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMulti::getDeviceObjAux(LocalSProblem *inP){
	//printf("Get obj aux. \n");
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy( inP->objFuncAux, d_objFuncAux, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMulti::getDeviceEqs(LocalSProblem *inP){
	//printf("Get eqs. \n");
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy2D(inP->eqs, inP->nRow*sizeof(*d_eqs), d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
}

VAR_TYPE* KernelWrapperMulti::getColumn(int ind, LocalSProblem *inP){
	//printf("Get col. \n");
	VAR_TYPE* column = new VAR_TYPE[inP->nRow];
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy(column, &d_eqs[IDX2C(0, ind, P2S(pitch))], inP->nRow * sizeof(*x), cudaMemcpyDeviceToHost) );
	return column;
}

void KernelWrapperMulti::getDeviceData(LocalSProblem *inP){
	//printf("Get data. \n");
	//cudaThreadSynchronize();
	cutilSafeCallNoSync( cudaMemcpy2D(inP->eqs, inP->nRow*sizeof(*d_eqs), d_eqs, pitch, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->objFunc, d_objFunc, inP->nCol * sizeof(*d_objFunc), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->objFuncAux, d_objFuncAux, inP->nCol * sizeof(*d_objFuncAux), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xInd, d_xInd, inP->nCol * sizeof(*d_xInd), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->xVal, d_xVal, inP->nCol * sizeof(*d_xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBounds, d_xBounds, inP->nCol * sizeof(*d_xBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBoundsP1, d_xBoundsP1, inP->nCol * sizeof(*d_xBoundsP1), cudaMemcpyDeviceToHost) );

	if(inP->RHS){
		cutilSafeCallNoSync( cudaMemcpy( inP->bVal, d_bVal, inP->nRow * sizeof(*d_bVal), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( inP->bBounds, d_bBounds, inP->nRow * sizeof(*d_bBounds), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( inP->bBoundsP1, d_bBoundsP1, inP->nRow * sizeof(*d_bBoundsP1), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( inP->basis, d_basis, inP->nRow * sizeof(*d_basis), cudaMemcpyDeviceToHost) );
	}

}

void KernelWrapperMulti::resetBoundsWS(LocalSProblem *inP){

	int nTriv = resetXValW(inP->nVar);
	printf("Reset non basic bounds.\n");
	if(nTriv > 0){
		printf("Reset basic values.\n");
		#if TYPE == USE_DOUBLE
			cublasDgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
		#elif TYPE == USE_FLOAT
			cublasSgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
		#endif
	}

}

void KernelWrapperMulti::checkInfeasWS(LocalSProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = SimplexProblem::PRIMAL;

	infB = checkFeasBWSW(inP->nRow);
	inP->nInfB = infB;
	//printf("[%d] nVar : %d - infB : %d\n", dev, inP->nVar, inP->nInfB);
	infX = checkFeasXWSW(inP->nVar);
	inP->nInfX = infX;

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		VAR_TYPE zero = 0;
		if(inP->RHS)
			cutilSafeCallNoSync( cudaMemcpy( &d_objFuncAux[inP->nCol-1], &zero, 1*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		inP->mode = SimplexProblem::AUX;

		if(infB > 0){
			// x has the infeasibility coefficient setted in previously called checkFeasBW
			// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
			#if TYPE == USE_DOUBLE
				cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#elif TYPE == USE_FLOAT
				cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
			#endif
		}
	}
}

void KernelWrapperMulti::updateInfeasWS(LocalSProblem *inP) {

	int nInf2FeasB = updateFeasBWSW(inP->nRow);
	int nInf2FeasX = updateFeasXWSW(inP->nVar);

	// Checking the basis feasability : return the number of non feasible becoming feasible
	if(nInf2FeasB > 0){
		// x has the infeasibility coefficient setted in previously called checkInfBasis
		// objFuncAux = 1 * eqs^T * x + 1*objFuncAux
		#if TYPE == USE_DOUBLE
			cublasDgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
		#elif TYPE == USE_FLOAT
			cublasSgemv('T', inP->nRow, inP->nCol, 1.0, d_eqs, P2S(pitch), x, 1, 1.0, d_objFuncAux, 1);
		#endif

	}

	inP->nInfX = nInf2FeasX;
	inP->nInfB = nInf2FeasB;
	inP->nInf -= (nInf2FeasX + nInf2FeasB);
}

void KernelWrapperMulti::processOptimumWS(LocalSProblem *inP){

	#if TYPE == USE_DOUBLE
		inP->objVal = cublasDdot(inP->nCol, d_objFunc, 1, d_xVal, 1);
	#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(inP->nCol, d_objFunc, 1, d_xVal, 1);
	#endif

	if(inP->mode == SimplexProblem::AUX) {
		#if TYPE == USE_DOUBLE
			inP->objAuxVal = cublasDdot(inP->nCol, d_objFuncAux, 1, d_xVal, 1);
		#elif TYPE == USE_FLOAT
			inP->objAuxVal = cublasSdot(inP->nCol, d_objFuncAux, 1, d_xVal, 1);
		#endif

	}

}

void KernelWrapperMulti::orderValsWS(LocalSProblem *inP){
	// Order the vals as they were at beginning (x1..xn, bn+1...bm)
	// Ordered values are put in y (xVal => 1..nCol) and x (bVal => 1..nRow)
	orderValsW(inP->nRow, inP->nCol);
	cutilSafeCallNoSync( cudaMemcpy( d_xVal, y, inP->nVar*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( d_bVal, x, inP->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
}

/*
 * ATTENTION READ THIS :
 * This method wont works if debug mode is on since the value of the initial problem is updated
 */
void KernelWrapperMulti::reinitPWS(LocalSProblem *inP){
	// Order the val (so we begin with a nearly optimal solution)
	orderValsWS(inP);

	// Then we copy the initial problem (again) to start from fresh values
	// Eqs
	cutilSafeCallNoSync( cudaMemcpy2D(d_eqs, pitch, inP->eqs, inP->nRow*sizeof(*d_eqs), inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyHostToDevice) );

	// Obj
	cutilSafeCallNoSync( cudaMemcpy(d_objFunc, inP->objFunc, inP->nCol*sizeof(*d_objFunc), cudaMemcpyHostToDevice) );

	// x and b index
	cutilSafeCallNoSync( cudaMemcpy(d_xInd, inP->xInd, inP->nCol*sizeof(*d_xInd), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(d_basis, inP->basis, inP->nRow*sizeof(*d_basis), cudaMemcpyHostToDevice) );

	// Bounds
	cutilSafeCallNoSync( cudaMemcpy( d_xBounds, inP->xBounds, inP->nCol * sizeof(*d_xBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( d_bBounds, inP->bBounds, inP->nRow * sizeof(*d_bBounds), cudaMemcpyHostToDevice) );

}


/****************************************************/
/******************   MULTI GPU   *******************/
/****************************************************/
void KernelWrapperMulti::toBound1(int col, VAR_TYPE alpha, VAR_TYPE xVal, LocalSProblem *inP) {

	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &xVal, 1 * sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

}

void KernelWrapperMulti::toBound2(VAR_TYPE alpha, LocalSProblem *inP, multiGPU_t *mGPU) {

	cublasSetVector(inP->nRow, sizeof(VAR_TYPE), mGPU->exColumn, 1, x, 1);

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, x, 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, x, 1, d_bVal, 1);
#endif

}

void KernelWrapperMulti::planToBound(int col, LocalSProblem *inP, multiGPU_t *mGPU) {

	cublasGetVector(inP->nRow, sizeof(VAR_TYPE), &d_eqs[IDX2C(0, col, P2S(pitch))], 1, mGPU->exColumn, 1); // Column
}

void KernelWrapperMulti::planPivoting(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU) {

	//printf("row : %d - col : %d \n", row, col);

	// Multi GPU
	cublasGetVector(1, sizeof(VAR_TYPE), &d_eqs[IDX2C(row, col, P2S(pitch))], 1, &mGPU->exPivot, 1); //pivot
	//printf("Pivot : %e\n", mGPU->exPivot);

	cublasGetVector(inP->nRow, sizeof(VAR_TYPE), &d_eqs[IDX2C(0, col, P2S(pitch))], 1, mGPU->exColumn, 1); // Column
	cublasGetVector(1, sizeof(VAR_TYPE), &d_xVal[col], 1, &mGPU->xVal, 1); // xVal
	//printf("xVal : %e\n", mGPU->xVal);
	cublasGetVector(1, sizeof(int), &d_xInd[col], 1, &mGPU->xInd, 1); // xInd
	//printf("xInd : %e\n", mGPU->xInd);

	cublasGetVector(1, sizeof(VAR_TYPE), &d_xBounds[col], 1, &mGPU->xBound, 1); // xBound
	cublasGetVector(1, sizeof(VAR_TYPE), &d_objFunc[col], 1, &mGPU->exObjCoeff, 1); // obj func coeff
	//printf("Pivot : %e\n", mGPU->exObjCoeff);

	if(inP->mode == SimplexProblem::AUX){
		cublasGetVector(1, sizeof(VAR_TYPE), &d_xBoundsP1[col], 1, &mGPU->xBoundP1, 1); // xBoundP1
		cublasGetVector(1, sizeof(VAR_TYPE), &d_objFuncAux[col], 1, &mGPU->exObjAuxCoeff, 1); // obj func P1 coeff
	}
}

void KernelWrapperMulti::pivotingMGPU1(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU) {

	VAR_TYPE tmp;
	VAR_TYPE zero = 0.0;

#if TYPE == USE_DOUBLE
	cublasDcopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	tmp = mGPU->exPivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1); // set pivot column factor for pivot row (column[row] = pivot - 1.0)

	cublasDcopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasDcopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	cublasDcopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);
#elif TYPE == USE_FLOAT
	cublasScopy(inP->nRow, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, x, 1);
	tmp = pivot - 1.0f;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1); // set pivot column factor for pivot row (column[row] = pivot - 1.0)

	cublasScopy(inP->nRow, d_zCol, 1, &d_eqs[IDX2C(0, col, P2S(pitch))], 1);
	cublasScopy(1, d_one, 1, &d_eqs[IDX2C(row, col, P2S(pitch))], 1);

	cublasScopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);
#endif

	cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFunc[col], 1);
	if(inP->mode == SimplexProblem::AUX)
		cublasSetVector(1, sizeof(zero), &zero, 1, &d_objFuncAux[col], 1);

	pivotingMGPU(inP, mGPU);

}

void KernelWrapperMulti::pivotingMGPU2(int row, LocalSProblem *inP, multiGPU_t *mGPU){

	VAR_TYPE tmp;
	//mGPU->exColumn[row] = mGPU->exPivot - 1.0;

#if TYPE == USE_DOUBLE
	tmp = mGPU->exPivot - 1.0;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);
	// copy pivot Col (from mGPU) to y
	//cublasSetVector(inP->nRow, sizeof(VAR_TYPE), mGPU->exColumn, 1, x, 1);
	// Copy pivot Row into y
	cublasDcopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);
#elif TYPE == USE_FLOAT
	tmp = mGPU->exPivot - 1.0f;
	cublasSetVector(1, sizeof(tmp), &tmp, 1, &x[row], 1);
	// copy pivot Col (from mGPU) to y
	//cublasSetVector(inP->nRow, sizeof(VAR_TYPE), mGPU->exColumn, 1, x, 1);
	// Copy pivot Row into y
	cublasScopy(inP->nCol, &d_eqs[IDX2C(row, 0, P2S(pitch))], P2S(pitch), y, 1);
#endif

	pivotingMGPU(inP, mGPU);

}

void KernelWrapperMulti::pivotingMGPU(LocalSProblem *inP, multiGPU_t *mGPU){

	VAR_TYPE tmp;

#if TYPE == USE_DOUBLE
	tmp = -1.0/mGPU->exPivot;

	// Processing new obj
	cublasDaxpy(inP->nCol, mGPU->exObjCoeff*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == SimplexProblem::AUX){
		// Processing new obj
		cublasDaxpy(inP->nCol, mGPU->exObjAuxCoeff*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasDger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#elif TYPE == USE_FLOAT
	tmp = -1.0f/pivot;

	// Processing new obj
	cublasSaxpy(inP->nCol, mGPU->exObjCoeff*tmp , y, 1, d_objFunc, 1);
	if(inP->mode == inP->AUX){
		// Processing new obj
		cublasSaxpy(inP->nCol, mGPU->exObjAuxCoeff*tmp , y, 1, d_objFuncAux, 1);
	}
	cublasSger(inP->nRow, inP->nCol, tmp, x, 1, y, 1, d_eqs, P2S(pitch));
#endif

}

void KernelWrapperMulti::updatingBasisMGPU1(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU) {

	VAR_TYPE xVal = mGPU->xVal;
	VAR_TYPE alpha = mGPU->exRow.alpha;

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// Updating xVal and swaping with bVal
	xVal += alpha;
	cutilSafeCallNoSync( cudaMemcpy( &d_xVal[col], &d_bVal[row], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cublasSetVector(1, sizeof(xVal), &xVal, 1, &d_bVal[row], 1);

	// Normal bounds
	cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBounds[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xBounds[col], &d_bBounds[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_bBounds[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

	// swap basis / non-basic index
	cutilSafeCallNoSync( cudaMemcpy( swpInt, &d_xInd[col], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_xInd[col], &d_basis[row], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &d_basis[row], swpInt, sizeof(int), cudaMemcpyDeviceToDevice) );

	if(inP->mode == SimplexProblem::AUX){
		// Phase 1 bounds
		cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &d_xBoundsP1[col], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_xBoundsP1[col], &d_bBoundsP1[row], sizeof(float2), cudaMemcpyDeviceToDevice) );
		cutilSafeCallNoSync( cudaMemcpy( &d_bBoundsP1[row], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );
	}
}

void KernelWrapperMulti::updatingBasisMGPU2(int row, LocalSProblem *inP, multiGPU_t *mGPU) {

	VAR_TYPE xVal = mGPU->xVal;
	VAR_TYPE alpha = mGPU->exRow.alpha;

	// Change basic values
	cublasSetVector(inP->nRow, sizeof(VAR_TYPE), mGPU->exColumn, 1, x, 1);

#if TYPE == USE_DOUBLE
	cublasDaxpy(inP->nRow, -alpha, x, 1, d_bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -alpha, x, 1, d_bVal, 1);
#endif

	// Updating xVal and swaping with bVal
	xVal += alpha;
	cublasSetVector(1, sizeof(xVal), &xVal, 1, &d_bVal[row], 1);

	// Normal bounds
	cutilSafeCallNoSync( cudaMemcpy( &d_bBounds[row], &mGPU->xBound, sizeof(float2), cudaMemcpyHostToDevice) );

	// basis / non-basic index
	cutilSafeCallNoSync( cudaMemcpy( &d_basis[row], &mGPU->xInd, sizeof(int), cudaMemcpyHostToDevice) );

	if(inP->mode == SimplexProblem::AUX){
		// Phase 1 bounds
		cutilSafeCallNoSync( cudaMemcpy( &d_bBoundsP1[row], &mGPU->xBoundP1, sizeof(float2), cudaMemcpyHostToDevice) );
	}
}

int KernelWrapperMulti::resetXValMGPU(LocalSProblem *inP){

	return resetXValW(inP->nVar);

}

void KernelWrapperMulti::resetBValMGPU(int device, LocalSProblem *inP, multiGPU_t *mGPU){

	#if TYPE == USE_DOUBLE
		cublasDgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
	#elif TYPE == USE_FLOAT
		cublasSgemv('N', inP->nRow, inP->nCol, -1.0, d_eqs, P2S(pitch), d_xVal, 1, 0.0, d_bVal, 1);
	#endif

	cutilSafeCallNoSync( cudaMemcpy( &mGPU->exBVal[device*inP->nRow], d_bVal, inP->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
}

void KernelWrapperMulti::setBVal(LocalSProblem *inP){

	cutilSafeCallNoSync( cudaMemcpy(d_bVal, inP->bVal, inP->nRow*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

}
