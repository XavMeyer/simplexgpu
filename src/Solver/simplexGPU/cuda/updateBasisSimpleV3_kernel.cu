//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 * updateBasisV3_kernel.cu
 *
 *  Created on: Sep, 2010
 *      Author: meyerx
 */

#ifndef _UPDATEBASISSIMPLEV3_KERNEL_H_
#define _UPDATEBASISSIMPLEV3_KERNEL_H_

#include <stdio.h>
#include "../../Types.h"
#include "../../Config.h"

#ifdef __DEVICE_EMULATION__
#define EMUSYNC __syncthreads()
#else
#define EMUSYNC
#endif

#define UB_TILE_DIM				32
#define UB_BLOCKS_ROW			8

/*
	Updating the basis
*/
__global__ void
updateBasisSimpleV3K(VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTransp, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_x = blockIdx.x;//(blockIdx.x+blockIdx.y)%gridDim.x;
	unsigned int blockIdx_y = blockIdx.y;//blockIdx.x;
	
	__shared__ VAR_TYPE block[UB_TILE_DIM+2][UB_TILE_DIM+1];
	
    unsigned int xIndex = blockIdx_x * UB_TILE_DIM + threadIdx.x;
    unsigned int yIndex = blockIdx_y * UB_TILE_DIM + threadIdx.y;
    unsigned int xIndex2 = blockIdx_y * UB_TILE_DIM + threadIdx.x;
    unsigned int yIndex2 = blockIdx_x * UB_TILE_DIM + threadIdx.y;
    unsigned int indexIn = yIndex * pitch_B + xIndex;
    unsigned int indexOut = yIndex2 * pitch_B + xIndex2;
    unsigned int offset;
    
    VAR_TYPE Bij, Bij_2, Bij_3, Bij_4;
    // Compute the leaving row coeffs and store them into shared memory

	// Load the Bij value
	Bij = B[indexIn];
	Bij_2 = B[indexIn + UB_BLOCKS_ROW*pitch_B];

	if(threadIdx.y == 0){
		block[UB_TILE_DIM][threadIdx.x] = r[yIndex + threadIdx.x];
	} else if(threadIdx.y == 1) {
		block[UB_TILE_DIM+1][threadIdx.x] = alpha*d[xIndex];
	}
	__syncthreads();

	offset = threadIdx.y;

	// load Blj / dl
	Bij += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];

	// store into Bij if "is not leave row" else store Bij
	block[offset][threadIdx.x] = Bij;

	// Store into B[i,j]
	B[indexIn] = Bij;

	Bij_3 = B[indexIn + 2*UB_BLOCKS_ROW*pitch_B];
	Bij_4 = B[indexIn + 3*UB_BLOCKS_ROW*pitch_B];

	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij_2 += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	block[offset][threadIdx.x] = Bij_2;
	B[indexIn] = Bij_2;



	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij_3 += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	block[offset][threadIdx.x] = Bij_3;
	B[indexIn] = Bij_3;


	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij_4 += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	block[offset][threadIdx.x] = Bij_4;
	B[indexIn] = Bij_4;

	/*for(int i=3*UB_BLOCKS_ROW; i<UB_TILE_DIM; i+=UB_BLOCKS_ROW){
		// Load the Bij value
		Bij_4 = B[indexIn+i*pitch_B];
		// load Blj / dl
		Bij_4 += block[UB_TILE_DIM][threadIdx.y+i]*block[UB_TILE_DIM+1][threadIdx.x];
		
		// store into Bij if "is not leave row" else store Bij
		block[threadIdx.y+i][threadIdx.x] = Bij_4;

		// Store into B[i,j]
		B[indexIn+i*pitch_B] = Bij_4;
	}*/
	__syncthreads();
	
	for(int i=0; i<UB_TILE_DIM; i+=UB_BLOCKS_ROW){
		// Transpose B		
		BTransp[indexOut+i*pitch_B] = block[threadIdx.x][threadIdx.y+i];
	}
		
}

__global__ void
updateBasisSimpleV3_2K(VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTransp, unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{
	unsigned int blockIdx_x = blockIdx.x;//(blockIdx.x+blockIdx.y)%gridDim.x;
	unsigned int blockIdx_y = blockIdx.y;//blockIdx.x;

	__shared__ VAR_TYPE block[UB_TILE_DIM+2][UB_TILE_DIM+1];

    unsigned int xIndex = blockIdx_x * UB_TILE_DIM + threadIdx.x;
    unsigned int yIndex = blockIdx_y * UB_TILE_DIM + threadIdx.y;
    unsigned int xIndex2 = blockIdx_y * UB_TILE_DIM + threadIdx.x;
    unsigned int yIndex2 = blockIdx_x * UB_TILE_DIM + threadIdx.y;
    unsigned int indexIn = yIndex * pitch_B + xIndex;
    unsigned int indexOut = yIndex2 * pitch_B + xIndex2;
    unsigned int offset;

    VAR_TYPE Bij, Bij_2, Bij_3, Bij_4;
    // Compute the leaving row coeffs and store them into shared memory



	// Load the Bij value
	Bij = B[indexIn];
	Bij_2 = B[indexIn + UB_BLOCKS_ROW*pitch_B];
	Bij_3 = B[indexIn + 2*UB_BLOCKS_ROW*pitch_B];
	Bij_4 = B[indexIn + 3*UB_BLOCKS_ROW*pitch_B];

	block[threadIdx.y                  ][threadIdx.x] = Bij;
	block[threadIdx.y + UB_BLOCKS_ROW  ][threadIdx.x] = Bij_2;
	block[threadIdx.y + 2*UB_BLOCKS_ROW][threadIdx.x] = Bij_3;
	block[threadIdx.y + 3*UB_BLOCKS_ROW][threadIdx.x] = Bij_4;

	if(threadIdx.y == 0){
		block[UB_TILE_DIM][threadIdx.x] = r[yIndex + threadIdx.x];
	} else if(threadIdx.y == 1) {
		block[UB_TILE_DIM+1][threadIdx.x] = alpha*d[xIndex];
	}
	__syncthreads();

	offset = threadIdx.y;
	Bij = block[offset][threadIdx.x];
	Bij += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	B[indexIn] = Bij;
	block[offset][threadIdx.x] = Bij;

	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij = block[offset][threadIdx.x];
	Bij += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	B[indexIn] = Bij;
	block[offset][threadIdx.x] = Bij;

	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij = block[offset][threadIdx.x];
	Bij += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	B[indexIn] = Bij;
	block[offset][threadIdx.x] = Bij;

	offset += UB_BLOCKS_ROW;
	indexIn += UB_BLOCKS_ROW*pitch_B;
	Bij = block[offset][threadIdx.x];
	Bij += block[UB_TILE_DIM][offset]*block[UB_TILE_DIM+1][threadIdx.x];
	B[indexIn] = Bij;
	block[offset][threadIdx.x] = Bij;

	__syncthreads();

	for(int i=0; i<UB_TILE_DIM; i+=UB_BLOCKS_ROW){
		// Transpose B
		BTransp[indexOut+i*pitch_B] = block[threadIdx.x][threadIdx.y+i];
	}

}

////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
void 
updateBasisSimpleV3Kernel(	int blocks, 
							VAR_TYPE alpha, VAR_TYPE *B, VAR_TYPE *BTransp,
							unsigned int pitch_B, VAR_TYPE *d, VAR_TYPE *r)
{

    dim3 dimBlock(UB_TILE_DIM, UB_BLOCKS_ROW, 1);
    dim3 dimGrid(blocks, blocks, 1);
    //int smemSize = (UB_TILE_DIM+1)*(UB_TILE_DIM+2)*sizeof(VAR_TYPE);
    
    updateBasisSimpleV3K<<< dimGrid, dimBlock >>>(alpha, B, BTransp, pitch_B, d, r);
  
}

#endif // #ifndef _UPDATEBASISSIMPLEV3_KERNEL_H_
