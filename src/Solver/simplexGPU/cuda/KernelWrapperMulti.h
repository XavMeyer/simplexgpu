//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * KernelWrapper.h
 *
 *  Created on: Oct 31, 2010
 *      Author: meyerx
 */

#ifndef KERNELWRAPPERMULTI_H_
#define KERNELWRAPPERMULTI_H_

/*
#include "../../SimplexProblem.h"
#include "../../Config.h"
#include "../../Types.h"
#include "../LocalSProblem.h"
#include "kernelHeader.h"
#include <math.h>
*/
#include "KernelWrapper.h"
#include "../LocalSProblem.h"

/***********************************************/
/****************** DEFINES ********************/
/***********************************************/
class KernelWrapperMulti : public KernelWrapper {
public:
	KernelWrapperMulti();
	KernelWrapperMulti(int inDev);
	virtual ~KernelWrapperMulti();



void getNumBlocksAndThreads( int n, int &blocks, int &threads);

/***********************************************/
/******** WRAPER KERNELS PROTOTYPE *************/
/***********************************************/
// This kernel returns the index of the maximum positive value of a vector
red_result_t 	argMaxPosW(int nEl, VAR_TYPE* p1, int incP1); // Wrapper

// This kernel returns the index of the maximum absolute value of a vector
red_result_t 	argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1); // Wrapper

red_result_t 	argMaxAbsNBW(int nEl, VAR_TYPE* obj, float2* bounds); // Wrapper

void	pSEValExpand3W(	int m, int n,
						VAR_TYPE *obj, int incObj,
						VAR_TYPE *eqs, int pitchEqs,
						VAR_TYPE *x, float2 *xBounds,
						VAR_TYPE *result);

red_result_t pSEValExpandW( int m, int n, VAR_TYPE *obj, float2 *xBounds);

red_result_t pSEValExpandTW( int m, int n, VAR_TYPE *obj, float2 *xBounds, VAR_TYPE threshold);


// This kernel returns the index of the maximum positive value of a vector
red_result_t 	argMaxNestedW(int nEl, VAR_TYPE* p1, int incP1, int* mask, int type); // Wrapper

// These kernels returns pivot row using the EXPAND method
red_result_t expandP1W(int nEl, VAR_TYPE* pivots, float2* bounds, double delta, int objSign);

exp_result_t expandP2W(int nEl, VAR_TYPE* pivots, float2* bounds, double alpha, int objSign);

exp_result_t expandSpecW(int nEl, VAR_TYPE* pivots, double teta, int objSign);

// WS (without slack
int 	checkFeasXWSW(int nEl);

int 	checkFeasBWSW(int nEl);

int 	updateFeasXWSW(int nEl);

int 	updateFeasBWSW(int nEl);

int 	resetXValW(int nEl);

void orderValsW(int nEl, int nVar);

void vectorMulW(int nEl, VAR_TYPE *inX, VAR_TYPE *inY);



/***********************************************/
/*********** FUNCTION PROTOTYPE ****************/
/***********************************************/


// Init functions
void 	initCuda(int device, LocalSProblem *inP);
void 	deInitCuda(bool deInitCublas);

void 	initNested(LocalSProblem *inP);
void 	deInitNested();

void	initResult(LocalSProblem *inP);
void 	deInitResult();

bool 	initPivoting(LocalSProblem *inP);
void 	deInitPivoting();

void 	initSwap();
void 	deInitSwap();



// Standard simplex functions
// Normal mode
red_result_t	stdFindCol(LocalSProblem *inP);

// Steepest edge
red_result_t 	seFindCol(LocalSProblem *inP);
red_result_t	seFindColP1(LocalSProblem *inP);
red_result_t 	seFindCol2(LocalSProblem *inP);
red_result_t	seFindCol2P1(LocalSProblem *inP);
red_result_t 	seFindCol3(LocalSProblem *inP);
red_result_t	seFindCol3P1(LocalSProblem *inP);

// Nested std
red_result_t 	nestedFindCol(LocalSProblem *inP);
red_result_t 	nestedFindColP1(LocalSProblem *inP);

//Expand row choice
row_result_t 	expandFindRow(int col, VAR_TYPE seVal, double delta, double tao, LocalSProblem *inP);
row_result_t 	expandFindRowP1(int col, VAR_TYPE seVal, double delta, double tau, LocalSProblem *inP);

void 	nonBasicToBound(int col, VAR_TYPE alpha, VAR_TYPE xVal,  LocalSProblem *inP);

// useful function (at least for me...)
// return the optimum value (last element of obj function)
//VAR_TYPE 	getOptimum(LocalSProblem *inP);
VAR_TYPE* 	getColumn(int ind, LocalSProblem *inP);
void		getDeviceBasis(LocalSProblem *inP);
void 		getDeviceObj(LocalSProblem *inP);
void 		getDeviceObjAux(LocalSProblem *inP);
void 		getDeviceEqs(LocalSProblem *inP);
// Move device data to the LocalSProblem given at init
void 		getDeviceData(LocalSProblem *inP);


// Without slack version
void pivotingWS(int row, int col, LocalSProblem *inP);
void updatingBasisWS(int row, int col, VAR_TYPE alpha, LocalSProblem *inP);
void checkInfeasWS(LocalSProblem *inP);
void updateInfeasWS(LocalSProblem *inP);
void processOptimumWS(LocalSProblem *inP);
void resetBoundsWS(LocalSProblem *inP);
void orderValsWS(LocalSProblem *inP);
void reinitPWS(LocalSProblem *inP);

// multi GPU
void 	planToBound(int col, LocalSProblem *inP, multiGPU_t *mGPU);
void 	toBound1(int col, VAR_TYPE alpha, VAR_TYPE xVal, LocalSProblem *inP);
void	toBound2(VAR_TYPE alpha, LocalSProblem *inP, multiGPU_t *mGPU);
void 	planPivoting(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU);
void 	pivotingMGPU1(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU);
void 	pivotingMGPU2(int row, LocalSProblem *inP, multiGPU_t *mGPU);
void 	pivotingMGPU(LocalSProblem *inP, multiGPU_t *mGPU);
void 	updatingBasisMGPU1(int row, int col, LocalSProblem *inP, multiGPU_t *mGPU);
void 	updatingBasisMGPU2(int row, LocalSProblem *inP, multiGPU_t *mGPU);
int 	resetXValMGPU(LocalSProblem *inP);
void 	resetBValMGPU(int device, LocalSProblem *inP, multiGPU_t *mGPU);
void 	setBVal(LocalSProblem *inP);

private:

// Res variables
int 			nBlocks;
int 			nThreads;

int*			hIRes;
int*			dIRes;

red_result_t* 	hRes;
red_result_t* 	dRes;

exp_result_t* 	hERes ;
exp_result_t* 	dERes;

// Matrix variables
//int _nbRow, _nbCol;
VAR_TYPE* 		x;
VAR_TYPE* 		y;
VAR_TYPE*		d_one;
VAR_TYPE*		d_zCol;
int*			d_mask;

// Swap
int*			swpInt;
float*			swpFlt;
float2*			swpFlt2;
double*			swpDbl;
VAR_TYPE*		swpTyp;

// Global memory pointers
VAR_TYPE* 		d_eqs;
VAR_TYPE* 		d_objFunc;
VAR_TYPE*		d_objFuncAux;
int* 			d_basis;
int*			d_xInd;
VAR_TYPE*		d_xScaling;

VAR_TYPE*		d_xVal;
float2*			d_xBounds;
float2*			d_xBoundsP1;


VAR_TYPE* 		d_bVal;
float2*			d_bBounds;
float2*			d_bBoundsP1;

size_t			pitch;

//misc
bool			firstNested;
bool			cublasOn;

int dev;

};

#endif /* KERNELWRAPPER_H_ */
