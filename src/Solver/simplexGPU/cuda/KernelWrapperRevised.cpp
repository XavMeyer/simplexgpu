//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file KernelWrapperRevised.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "KernelWrapperRevised.h"

KernelWrapperRevised::KernelWrapperRevised(Logger *inL) {
	logger = inL;
	device = 0;
	init();
	initCuda();
}

KernelWrapperRevised::KernelWrapperRevised(Logger *inL, int inDevice) {
	logger = inL;
	device = inDevice;
	init();
	initCuda();
}

KernelWrapperRevised::KernelWrapperRevised(Logger *inL, bool noInit) {
	logger = inL;
	if(!noInit){
		device = 0;
		init();
		initCuda();
	}
}


KernelWrapperRevised::~KernelWrapperRevised() {
	deInitProblemStructure();
	deInitCuda();
}



void KernelWrapperRevised::init(){
	devexInit = NULL;
#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif
}

/*!
 * Call the init functions
 */
void KernelWrapperRevised::initCuda(){

	cutilSafeCall(cudaGetDeviceProperties(&devProp, device));
	cutilSafeCall(cudaSetDevice(device));

	if(cublasInit() != CUBLAS_STATUS_SUCCESS){
		logger->log(Logger::LOG_ERROR, "Cublas error at init.");
		throw 11;
	}

}

void KernelWrapperRevised::deInitCuda(){
	//cudaDeviceSynchronize();
	cublasShutdown();
	cudaThreadExit();
}


void KernelWrapperRevised::initProblemStructure(RevisedFormProblem *inP) {

	cudaError_t status;

	// Create the revised form problem that will be on GPU
	pGPU = new RevisedFormProblem(inP->nRow, inP->nVar, inP->nSlack, false);

	// Create the structure in GPU memory

	// Matrix
	// Ab
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->Ab, &pitch_Ab, pGPU->nRow*sizeof(*pGPU->Ab), pGPU->nBasic));
	// An
#if AN_STORAGE == AN_COL_WISE
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->An, &pitch_An, pGPU->nRow*sizeof(*pGPU->An), pGPU->nNonBasic));
#elif AN_STORAGE == AN_ROW_WISE
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->An, &pitch_An, pGPU->nNonBasic*sizeof(*pGPU->An), pGPU->nRow));
#endif
	// B
#if B_STORAGE == B_NORMAL
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->B, &pitch_B, pGPU->nBasic*sizeof(*pGPU->B), pGPU->nBasic));
#elif B_STORAGE == B_NORMAL_AND_TRANS

#if B_PADDING == B_PAD_16
	nBasicPad = 16*ceil((float)pGPU->nBasic/16);
#elif B_PADDING == B_PAD_32
	nBasicPad = 32*ceil((float)pGPU->nBasic/32);
#endif
	cutilSafeCallNoSync( cudaMallocPitch((void**) &pGPU->B, &pitch_B, nBasicPad*sizeof(*pGPU->B), nBasicPad));
#endif
	// Eqs TODO If there is not enough memory for eqs, use An and swap content on HDD

	status = cudaMallocPitch((void**) &d_eqs, &pitch_eqs, pGPU->nRow*sizeof(*d_eqs), pGPU->nNonBasic+1);

	if(status != cudaSuccess){
		cutilSafeCallNoSync(status);
		// Not working that well
		logger->log(logger->LOG_INFO, "Working on limited memory.");
		limitedMem = true;
	} else {
		limitedMem = false;
	}

	//Vectors
	// lhs
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->lhs, inP->nRow*sizeof(*pGPU->lhs)));

	// Basic variable bounds
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bBounds, inP->nBasic*sizeof(*pGPU->bBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1)));

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->nBounds, inP->nNonBasic*sizeof(*pGPU->nBounds)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->nBoundsP1, inP->nNonBasic*sizeof(*pGPU->nBoundsP1)));

	// Variable values
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->nVal, inP->nNonBasic*sizeof(*pGPU->nVal)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bVal, inP->nBasic*sizeof(*pGPU->bVal)));

	// Variable index
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->nInd, inP->nNonBasic*sizeof(*pGPU->nInd)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->bInd, inP->nBasic*sizeof(*pGPU->bInd)));

	// Objective function
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->Cb, inP->nBasic*sizeof(*pGPU->Cb)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->CbP1, inP->nBasic*sizeof(*pGPU->CbP1)));

	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->Cn, inP->nNonBasic*sizeof(*pGPU->Cn)));
	cutilSafeCallNoSync( cudaMalloc((void**) &pGPU->CnP1, inP->nNonBasic*sizeof(*pGPU->CnP1)));

	this->initMethodStructure();
	this->initResult();
	this->initSwap();
#if DBG_PRINT
	this->initDbg(pGPU);
#endif

}

void KernelWrapperRevised::deInitProblemStructure(){

	// Matrix
	cutilSafeCallNoSync(cudaFree(pGPU->Ab));
	pGPU->Ab = NULL;
	cutilSafeCallNoSync(cudaFree(pGPU->An));
	pGPU->An = NULL;
	cutilSafeCallNoSync(cudaFree(pGPU->B));
	pGPU->B = NULL;

	// Vectors
	cutilSafeCallNoSync(cudaFree(pGPU->lhs));

	cutilSafeCallNoSync(cudaFree(pGPU->bBounds));
	cutilSafeCallNoSync(cudaFree(pGPU->bBoundsP1));

	cutilSafeCallNoSync(cudaFree(pGPU->nBounds));
	cutilSafeCallNoSync(cudaFree(pGPU->nBoundsP1));

	cutilSafeCallNoSync(cudaFree(pGPU->bVal));
	cutilSafeCallNoSync(cudaFree(pGPU->nVal));

	cutilSafeCallNoSync(cudaFree(pGPU->bInd));
	cutilSafeCallNoSync(cudaFree(pGPU->nInd));

	cutilSafeCallNoSync(cudaFree(pGPU->Cb));
	cutilSafeCallNoSync(cudaFree(pGPU->CbP1));

	cutilSafeCallNoSync(cudaFree(pGPU->Cn));
	cutilSafeCallNoSync(cudaFree(pGPU->CnP1));


	this->deInitMethodStructure();
	this->deInitResult();
	this->deInitSwap();
#if DBG_PRINT
	this->deInitDbg();
#endif

	if(devexInit != NULL)
		delete [] devexInit;

	delete pGPU;

}

void KernelWrapperRevised::initMethodStructure(){

	// Tmp vectors
	//cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, pGPU->nRow*sizeof(*d_tmpCol)));
	//cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, pGPU->nBasic*sizeof(*d_tmpBasic)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpNonBasic, pGPU->nNonBasic*sizeof(*d_tmpNonBasic)));

	// Reduced cost vector
	cutilSafeCallNoSync( cudaMalloc((void**) &d_redCost, pGPU->nNonBasic*sizeof(*d_redCost)));

	// Steepest edge coefficient
	cutilSafeCallNoSync( cudaMalloc((void**) &d_seCoeff, pGPU->nNonBasic*sizeof(*d_seCoeff)));
	// Steepest edge vector alpha and beta
	cutilSafeCallNoSync( cudaMalloc((void**) &d_alpha, pGPU->nNonBasic*sizeof(*d_alpha)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_beta, pGPU->nNonBasic*sizeof(*d_beta)));

#if AN_STORAGE == AN_ROW_WISE
	h_transpAn = new VAR_TYPE[pGPU->nRow*pGPU->nNonBasic];
#endif

#if B_STORAGE == B_NORMAL_AND_TRANS
	// Entering variable column
	cutilSafeCallNoSync( cudaMalloc((void**) &d_enterCol, nBasicPad*sizeof(*d_enterCol)));
	// Tmp vectors
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, nBasicPad*sizeof(*d_tmpCol)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, nBasicPad*sizeof(*d_tmpBasic)));

	// BTrans
	cutilSafeCallNoSync( cudaMallocPitch((void**) &d_BTrans, &pitch_B, nBasicPad*sizeof(*d_BTrans), nBasicPad));
	#elif B_STORAGE == B_NORMAL
	// Entering variable column
	cutilSafeCallNoSync( cudaMalloc((void**) &d_enterCol, pGPU->nRow*sizeof(*d_enterCol)));
	// Tmp vectors
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpCol, pGPU->nRow*sizeof(*d_tmpCol)));
	cutilSafeCallNoSync( cudaMalloc((void**) &d_tmpBasic, pGPU->nBasic*sizeof(*d_tmpBasic)));
#endif

	// Vector 0
	VAR_TYPE zeros[pGPU->nBasic];
	// Init 0 vector and replace basis row l by 0 vector
	for(unsigned int i=0; i<pGPU->nBasic; i++){
		zeros[i] = 0.0;
	}
	cutilSafeCallNoSync(cudaMalloc((void **)&d_z, pGPU->nBasic*sizeof(VAR_TYPE)));
	cutilSafeCallNoSync(cudaMemcpy( d_z, zeros, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyHostToDevice));

}

void KernelWrapperRevised::deInitMethodStructure(){

	if(!limitedMem){
		cutilSafeCallNoSync(cudaFree(d_eqs));
	}

	cutilSafeCallNoSync(cudaFree(d_tmpCol));
	cutilSafeCallNoSync(cudaFree(d_tmpBasic));
	cutilSafeCallNoSync(cudaFree(d_tmpNonBasic));

	cutilSafeCallNoSync(cudaFree(d_redCost));

	cutilSafeCallNoSync(cudaFree(d_enterCol));

	cutilSafeCallNoSync(cudaFree(d_seCoeff));
	cutilSafeCallNoSync(cudaFree(d_alpha));
	cutilSafeCallNoSync(cudaFree(d_beta));

#if AN_STORAGE == AN_ROW_WISE
	delete [] h_transpAn;
#endif

#if B_STORAGE == B_NORMAL_AND_TRANS
	// BTrans
	cutilSafeCallNoSync(cudaFree(d_BTrans));
#endif
}

/*!
 * \brief Initializing swap
 */
void KernelWrapperRevised::initSwap(){

	cutilSafeCallNoSync( cudaMalloc((void**) &swpInt, 1*sizeof(int)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt, 1*sizeof(float)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpFlt2, 1*sizeof(float2)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpDbl, 1*sizeof(double)) );
	cutilSafeCallNoSync( cudaMalloc((void**) &swpTyp, 1*sizeof(VAR_TYPE)) );
}

/*!
 * \brief De-initializing CUBLAS
 */
void KernelWrapperRevised::deInitSwap(){

	cutilSafeCallNoSync(cudaFree(swpInt));
	cutilSafeCallNoSync(cudaFree(swpFlt));
	cutilSafeCallNoSync(cudaFree(swpFlt2));
	cutilSafeCallNoSync(cudaFree(swpDbl));
	cutilSafeCallNoSync(cudaFree(swpTyp));

}

/*!
 * Define the number of blocks and threads for CUDA
 * Create the result array on CPU and Device
 */
void KernelWrapperRevised::initResult(){
#if MAPPED_RESULT && PINNED
	int flags = cudaHostAllocMapped;
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dRes, (void *)hRes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dERes, (void *)hERes, 0));
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), flags));
	cutilSafeCall(cudaHostGetDevicePointer((void **)&dIRes, (void *)hIRes, 0));
#elif PINNED
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hRes, MAX_BLOCKS*sizeof(*hRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hERes, MAX_BLOCKS*sizeof(*hERes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	cutilSafeCallNoSync(cudaHostAlloc((void**)&hIRes, MAX_BLOCKS*sizeof(*hIRes), 0));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#else
	hRes = (red_result_t*) malloc(MAX_BLOCKS*sizeof(red_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dRes, MAX_BLOCKS*sizeof(red_result_t)) );
	hERes = (exp_result_t*) malloc(MAX_BLOCKS*sizeof(exp_result_t));
	cutilSafeCallNoSync( cudaMalloc((void**) &dERes, MAX_BLOCKS*sizeof(exp_result_t)) );
	hIRes = (int*) malloc(MAX_BLOCKS*sizeof(int));
	cutilSafeCallNoSync( cudaMalloc((void**) &dIRes, MAX_BLOCKS*sizeof(int)) );
#endif
}

void KernelWrapperRevised::deInitResult(){
#if MAPPED_RESULT || PINNED
	cutilSafeCall(cudaFreeHost(hRes));
	cutilSafeCall(cudaFreeHost(hERes));
	cutilSafeCall(cudaFreeHost(hIRes));
#else
	free(hRes);
	free(hERes);
	free(hIRes)
#endif
	cutilSafeCallNoSync(cudaFree(dRes));
	cutilSafeCallNoSync(cudaFree(dERes));
	cutilSafeCallNoSync(cudaFree(dIRes));
}




void KernelWrapperRevised::loadProblem(RevisedFormProblem *inP, bool isBasisID) {

	// Matrix
	// Ab
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->Ab, pitch_Ab, inP->Ab, inP->nRow*sizeof(*pGPU->Ab), inP->nRow*sizeof(*pGPU->Ab), inP->nBasic, cudaMemcpyHostToDevice) );
	// An
#if AN_STORAGE == AN_COL_WISE
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->An, pitch_An, inP->An, inP->nRow*sizeof(*pGPU->An), inP->nRow*sizeof(*pGPU->An), inP->nNonBasic, cudaMemcpyHostToDevice) );
#elif AN_STORAGE == AN_ROW_WISE
	for(unsigned int i=0; i<pGPU->nRow; i++){
		for(unsigned int j=0; j<pGPU->nNonBasic; j++){
			h_transpAn[j+i*pGPU->nNonBasic] = inP->An[i+j*pGPU->nRow];
		}
	}
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->An, pitch_An, h_transpAn, inP->nNonBasic*sizeof(*pGPU->An), inP->nNonBasic*sizeof(*pGPU->An), inP->nRow, cudaMemcpyHostToDevice) );
#endif
	// B
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->B, pitch_B, inP->B, inP->nRow*sizeof(*pGPU->B), inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyHostToDevice) );
#if B_STORAGE == B_NORMAL_AND_TRANS
	for(unsigned int i=0; i<pGPU->nBasic; i++){
		cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(0, i, P2S(pitch_B))], 1, &d_BTrans[IDX2C(i, 0, P2S(pitch_B))], P2S(pitch_B));
	}
#endif

	// Vectors
	// lhs
	cutilSafeCallNoSync( cudaMemcpy(pGPU->lhs, inP->lhs, inP->nRow*sizeof(*pGPU->lhs), cudaMemcpyHostToDevice) );
	// Basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBounds, inP->bBounds, inP->nBasic*sizeof(*pGPU->bBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBoundsP1, inP->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1), cudaMemcpyHostToDevice) );

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nBounds, inP->nBounds, inP->nNonBasic*sizeof(*pGPU->nBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nBoundsP1, inP->nBoundsP1, inP->nNonBasic*sizeof(*pGPU->nBoundsP1), cudaMemcpyHostToDevice) );

	// Variable values
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bVal, inP->bVal, inP->nBasic*sizeof(*pGPU->bVal), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nVal, inP->nVal, inP->nNonBasic*sizeof(*pGPU->nVal), cudaMemcpyHostToDevice) );

	// Variable index
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bInd, inP->bInd, inP->nBasic*sizeof(*pGPU->bInd), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nInd, inP->nInd, inP->nNonBasic*sizeof(*pGPU->nInd), cudaMemcpyHostToDevice) );

	// Objective function
	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cb, inP->Cb, inP->nBasic*sizeof(*pGPU->Cb), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CbP1, inP->CbP1, inP->nBasic*sizeof(*pGPU->CbP1), cudaMemcpyHostToDevice) );

	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cn, inP->Cn, inP->nNonBasic*sizeof(*pGPU->Cn), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CnP1, inP->CnP1, inP->nNonBasic*sizeof(*pGPU->CnP1), cudaMemcpyHostToDevice) );

	// If the basis is set the Identity matrix, we just have to copy the matrix An
	// and so Eqs are up to date, else they are not and they will be processed when needed
	eqsUpToDate = isBasisID;
	if(isBasisID && !limitedMem){
		cutilSafeCallNoSync( cudaMemcpy2D(d_eqs, pitch_eqs, inP->An, inP->nRow*sizeof(*pGPU->An), inP->nRow*sizeof(*pGPU->An), inP->nNonBasic, cudaMemcpyHostToDevice) );
		cutilSafeCallNoSync( cudaMemcpy(&d_eqs[IDX2C(0, pGPU->nNonBasic, P2S(pitch_eqs))], pGPU->lhs, inP->nRow*sizeof(*pGPU->lhs), cudaMemcpyDeviceToDevice) );
	}

}

/* Process using pGPU values
 *
 * y = Cb * B
 * RedCost = Cn - y * An
 *
 * Store the result in d_redCost
 */
void KernelWrapperRevised::processReducedCost(){

	#if TYPE == USE_DOUBLE
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
#endif
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->Cn, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
#if AN_STORAGE == AN_COL_WISE
	cublasDgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#elif AN_STORAGE == AN_ROW_WISE
	cublasDgemv('N', pGPU->nNonBasic, pGPU->nRow, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif
#elif TYPE == USE_FLOAT
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
	cublasSgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->Cn, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	cublasSgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif


}


/* Process using pGPU values
 *
 * y = Cb * B
 * RedCost = Cn - y * An
 *
 * Store the result in d_redCost
 */
void KernelWrapperRevised::processReducedCostP1(){

#if TYPE == USE_DOUBLE
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->CbP1, 1, 0.0, d_tmpBasic, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), pGPU->CbP1, 1, 0.0, d_tmpBasic, 1);
#endif
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->CnP1, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
#if AN_STORAGE == AN_COL_WISE
	cublasDgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#elif AN_STORAGE == AN_ROW_WISE
	cublasDgemv('N', pGPU->nNonBasic, pGPU->nRow, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif
#elif TYPE == USE_FLOAT
	// y = Cb * B ===> d_tmpBasic = B' * Cb + 0 * d_tmpBasic
	cublasSgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->Cb, 1, 0.0, d_tmpBasic, 1);
	// Copy Cn into redCost
	cutilSafeCallNoSync( cudaMemcpy(d_redCost, pGPU->Cn, pGPU->nNonBasic*sizeof(*d_redCost), cudaMemcpyDeviceToDevice) );
	// RedCost = Cn - y * An ===> RedCost = -1 * An' * d_tmpBasic + 1.0 * RedCost
	cublasSgemv('T', pGPU->nRow, pGPU->nNonBasic, -1.0, pGPU->An, P2S(pitch_An), d_tmpBasic, 1, 1.0, d_redCost, 1);
#endif

}

/* Process using pGPU values
 *
 * d_e = B * a_e
 *
 * Store the result in d_enterCol
 */
void KernelWrapperRevised::processColumn(red_result_t enteringVar){

	//VAR_TYPE Zq, ZqPrime;

	// d_e = B * a_e ===> d_enterCol = B * An(0,enteringVar) + 0.0 * d_enterCol
#if TYPE == USE_DOUBLE
#if AN_STORAGE == AN_COL_WISE
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), &pGPU->An[IDX2C(0, enteringVar.index, P2S(pitch_An))], 1, 0.0, d_enterCol, 1);
#elif AN_STORAGE == AN_ROW_WISE
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), &pGPU->An[IDX2C(enteringVar.index, 0, P2S(pitch_An))], P2S(pitch_An), 0.0, d_enterCol, 1);
#endif
#elif TYPE == USE_FLOAT
	cublasSgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), &pGPU->An[IDX2C(0, enteringVar.index, P2S(pitch_An))], 1, 0.0, d_enterCol, 1);
#endif

}


void KernelWrapperRevised::updateVariables(red_result_t enteringVar, row_result_t leavingVar){

	VAR_TYPE xVal;

	// Change basic values
#if TYPE == USE_DOUBLE
	cublasDaxpy(pGPU->nRow, -leavingVar.alpha, d_enterCol, 1, pGPU->bVal, 1);
#elif TYPE == USE_FLOAT
	cublasSaxpy(inP->nRow, -leavingVar.alpha, &d_eqs[IDX2C(0, col, P2S(pitch))], 1, d_bVal, 1);
#endif

	// Updating entering var. value
	// Read xVal TODO xVal is probably into leavingVar.absP (to check)
	cutilSafeCallNoSync(cudaMemcpy( &xVal, &pGPU->nVal[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	// Add alpha
	xVal += leavingVar.alpha;
	// Store xVal
	cutilSafeCallNoSync(cudaMemcpy(&pGPU->nVal[enteringVar.index], &xVal, sizeof(VAR_TYPE), cudaMemcpyHostToDevice));
}


void KernelWrapperRevised::updateBasis(row_result_t leavingVar){

	// since the basis has been updated, equations are no longer up to date
	eqsUpToDate = false;


/*
	//// B
	VAR_TYPE *BBis;
	size_t pitch_BBis;
	cutilSafeCallNoSync( cudaMallocPitch((void**) &BBis, &pitch_BBis, nBasicPad*sizeof(*pGPU->B), nBasicPad));
	cutilSafeCallNoSync( cudaMemcpy2D(BBis, pitch_B, pGPU->B, pitch_B, nBasicPad*sizeof(*pGPU->B), nBasicPad, cudaMemcpyDeviceToDevice) );
*/
#if B_STORAGE == B_NORMAL
	this->updateBasisW(leavingVar);
	//this->updateBasisCublas(leavingVar);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	//this->updateBasisW(leavingVar);
	this->updateBasisV2W(leavingVar, pGPU->B);
#endif

	/*VAR_TYPE *h_B1, *h_B2;
	h_B1 = new VAR_TYPE[pGPU->nBasic*pGPU->nBasic];
	h_B2 = new VAR_TYPE[pGPU->nBasic*pGPU->nBasic];


	cutilSafeCallNoSync( cudaMemcpy2D(h_B1, pGPU->nBasic*sizeof(*pGPU->B), pGPU->B, pitch_B, pGPU->nBasic*sizeof(*pGPU->B), pGPU->nBasic, cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy2D(h_B2, pGPU->nBasic*sizeof(*pGPU->B), BBis, pitch_BBis, pGPU->nBasic*sizeof(*pGPU->B), pGPU->nBasic, cudaMemcpyDeviceToHost) );

	cudaThreadSynchronize();

	for(unsigned int i=0; i<pGPU->nBasic;i++){
		for(unsigned int j=0; j<pGPU->nBasic;j++){
			if(h_B2[IDX2C(j,i,pGPU->nBasic)] != h_B1[IDX2C(j,i,pGPU->nBasic)]){
				cout << j << ", " << i << " : " << h_B1[IDX2C(j,i,pGPU->nBasic)] << " vs " << h_B2[IDX2C(j,i,pGPU->nBasic)] << endl;
				getchar();
			}
			if(h_B2[IDX2C(j,i,pGPU->nBasic)] != 0.0 && h_B2[IDX2C(j,i,pGPU->nBasic)] != 1.0){
				cout << j << ", " << i << " : " << h_B1[IDX2C(j,i,pGPU->nBasic)] << " vs " << h_B2[IDX2C(j,i,pGPU->nBasic)] << endl;
				getchar();
			}
			//cout << IDX2C(j, i, pGPU->nBasic) << " : " << h_B1[IDX2C(j,i, pGPU->nBasic)] << endl;
		}
	}
	cout << h_B2[IDX2C(leavingVar.index, leavingVar.index, pGPU->nBasic)] << endl;
	getchar();

	delete [] h_B1;
	delete [] h_B2;
	cudaFree(BBis);*/
}

void KernelWrapperRevised::swapVariables(red_result_t enteringVar, row_result_t leavingVar){

	int inV = enteringVar.index;
	int outV = leavingVar.index;

	// Updating xVal and swaping with bVal
	cutilSafeCallNoSync( cudaMemcpy( swpTyp, &pGPU->bVal[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bVal[outV], &pGPU->nVal[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->nVal[inV], swpTyp, sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	// Normal bounds
	cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &pGPU->bBounds[outV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bBounds[outV], &pGPU->nBounds[inV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->nBounds[inV], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

	// swap basis / non-basic index
	cutilSafeCallNoSync( cudaMemcpy( swpInt, &pGPU->bInd[outV], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bInd[outV], &pGPU->nInd[inV], sizeof(int), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->nInd[inV], swpInt, sizeof(int), cudaMemcpyDeviceToDevice) );

	// objective coefficient
	cutilSafeCallNoSync( cudaMemcpy( swpTyp, &pGPU->Cb[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->Cb[outV], &pGPU->Cn[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->Cn[inV], swpTyp, sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

	// Column swap (TODO check if correct)
#if AN_STORAGE == AN_COL_WISE
	cutilSafeCallNoSync( cudaMemcpy( d_tmpCol, &pGPU->Ab[IDX2C(0, outV, P2S(pitch_Ab))], pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->Ab[IDX2C(0, outV, P2S(pitch_Ab))], &pGPU->An[IDX2C(0, inV, P2S(pitch_An))], pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->An[IDX2C(0, inV, P2S(pitch_An))], d_tmpCol, pGPU->nRow*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
#elif AN_STORAGE == AN_ROW_WISE
	cublasDcopy(pGPU->nRow, &pGPU->Ab[IDX2C(0, outV, P2S(pitch_Ab))], 1, d_tmpCol, 1);
	cublasDcopy(pGPU->nRow, &pGPU->An[IDX2C(inV, 0, P2S(pitch_An))], P2S(pitch_An), &pGPU->Ab[IDX2C(0, outV, P2S(pitch_Ab))], 1);
	cublasDcopy(pGPU->nRow, d_tmpCol, 1, &pGPU->An[IDX2C(inV, 0, P2S(pitch_An))], P2S(pitch_An));
#endif

	//checkPivoting(enteringVar, leavingVar);

}

void KernelWrapperRevised::swapVariablesP1(red_result_t enteringVar, row_result_t leavingVar){

	int inV = enteringVar.index;
	int outV = leavingVar.index;

	// Same as phase 1 swap
	this->swapVariables(enteringVar, leavingVar);

	// Phase 1 bounds
	cutilSafeCallNoSync( cudaMemcpy( swpFlt2, &pGPU->bBoundsP1[outV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->bBoundsP1[outV], &pGPU->nBoundsP1[inV], sizeof(float2), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->nBoundsP1[inV], swpFlt2, sizeof(float2), cudaMemcpyDeviceToDevice) );

	// objective coefficient
	cutilSafeCallNoSync( cudaMemcpy( swpTyp, &pGPU->CbP1[outV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->CbP1[outV], &pGPU->CnP1[inV], sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
	cutilSafeCallNoSync( cudaMemcpy( &pGPU->CnP1[inV], swpTyp, sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
}

void KernelWrapperRevised::updateBasisW(row_result_t leavingVar){
	VAR_TYPE dl;

	// Nb of threads is pow2 nearest nBasic and lower or equals to MAX_THREADS
	int threads = (pGPU->nBasic < MAX_THREADS*2) ? nextPow2((pGPU->nBasic + 1)/ 2) : MAX_THREADS;
	// Nb of blocks is the smallest between nBasic and MAX_BLOCKS
	int blocks = pGPU->nBasic;//MIN(pGPU->nBasic, MAX_BLOCKS);

	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	updateBasisKernel(threads, blocks, pGPU->nBasic, leavingVar.index, dl, pGPU->B, P2S(pitch_B), d_enterCol);
	_SYNC_KERNEL
}

void KernelWrapperRevised::updateBasisCublas(row_result_t leavingVar){
	VAR_TYPE dl, dlBis, negOne;

	// Copy dl value
	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	// Change it for -1
	negOne = -1;
	cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
	// Copy basis row l
	cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
	// replace it by zero vector
	cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

	/* Process Bij' = -(1/dl)*di*rj + Bij
	 * With d_enterCol as d, d_tmpCol as r
	 * Row Blj is changed to 0 and dl to -1 such as when i=l :
	 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
	 */
	dlBis = -(1/dl);
	cublasDger(pGPU->nBasic, pGPU->nBasic, dlBis, d_enterCol, 1, d_tmpCol, 1, pGPU->B, P2S(pitch_B));
	//updateBasisSimpleKernel(threads, blocks, dlBis, pGPU->B, P2S(pitch_B), d_enterCol, d_tmpCol);

	// Change back enter column to its orinigal value (might be used later for other processing)
	cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
}

void KernelWrapperRevised::updateBasisV2W(row_result_t leavingVar, VAR_TYPE *bBis){
#if B_STORAGE == B_NORMAL_AND_TRANS

	VAR_TYPE dl;

	if(devProp.major >= 2){
		int blocks = ceil((float)pGPU->nBasic/32);

#ifdef SIMPLE
			VAR_TYPE dlBis, negOne;
			// Copy dl value
			cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
			// Change it for -1
			negOne = -1;
			cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
			// Copy basis row l
			cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
			// replace it by 0 row
			cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

			/* Process Bij' = -(1/dl)*di*rj + Bij
			 * With d_enterCol as d, d_tmpCol as r
			 * Row Blj is changed to 0 and dl to -1 such as when i=l :
			 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
			 */
			dlBis = -(1/dl);
			//cublasDger(pGPU->nBasic, pGPU->nBasic, dlBis, d_enterCol, 1, d_tmpCol, 1, pGPU->B, P2S(pitch_B));
			updateBasisSimpleV3Kernel(blocks, dlBis, pGPU->B, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);

			// Change back enter column to its orinigal value (might be used later for other processing)
			cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
#else
		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		//cublasDcopy(pGPU->nRow, &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))], 1, d_tmpCol, 1);
		cublasDcopy(pGPU->nRow, &bBis[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);

		updateBasisV3Kernel(blocks, pGPU->nBasic, leavingVar.index, dl, bBis, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);
		cudaThreadSynchronize();
#endif
	} else {

#ifdef SIMPLE

		int threads = 16;
		int blocks = ceil((float)pGPU->nBasic/32);

		VAR_TYPE dlBis, negOne;
		// Copy dl value
		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		// Change it for -1
		negOne = -1;
		cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &negOne, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );
		// Copy basis row l
		cublasDcopy(pGPU->nRow, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B), d_tmpCol, 1);
		// replace it by 0 row
		cublasDcopy(pGPU->nRow, d_z, 1, &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))], P2S(pitch_B));

		/* Process Bij' = -(1/dl)*di*rj + Bij
		 * With d_enterCol as d, d_tmpCol as r
		 * Row Blj is changed to 0 and dl to -1 such as when i=l :
		 * Blj' = -(1/dl)*-1*rj + 0 => Blj' = rj/dl
		 */
		dlBis = -(1/dl);
		updateBasisSTrKernel(threads, blocks, dlBis, pGPU->B, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);

		// Change back enter column to its orinigal value (might be used later for other processing)
		cutilSafeCallNoSync( cudaMemcpy(&d_enterCol[leavingVar.index], &dl, sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

#else
		int threads = 16;
		int blocks = ceil((float)pGPU->nBasic/threads);

		cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		cublasDcopy(pGPU->nRow, &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))], 1, d_tmpCol, 1);

		updateBasisV2Kernel(threads, blocks, pGPU->nBasic, leavingVar.index, dl, bBis, d_BTrans, P2S(pitch_B), d_enterCol, d_tmpCol);
#endif
	}

#endif
	_SYNC_KERNEL

}


row_result_t KernelWrapperRevised::expandFindRow(red_result_t enteringVar, double delta, double tau){
	red_result_t res1;
	exp_result_t res2;
	row_result_t res;
	VAR_TYPE xVal, objVal;
	float2 xBound;
	float lo, up;
	VAR_TYPE alpha1, alpha2;
	VAR_TYPE alphaMin;
	int objSign;// = (enteringVar.value > 0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT_MEM")
	cutilSafeCallNoSync( cudaMemcpy( &objVal, &d_redCost[enteringVar.index], 1 * sizeof(objVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &pGPU->nVal[enteringVar.index], 1 * sizeof(xVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &pGPU->nBounds[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT_MEM")

	objSign = (objVal > 0.0 ? 1 : -1);

	lo = xBound.x;
	up = xBound.y;

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(pGPU->nBasic, pGPU->bBounds, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	if(objSign == 1){
		if(up < FLT_MAX){
			alpha1 = (up + delta) - xVal;
		} else {
			alpha1 = MAX_TYPE;
		}
	} else {
		if(lo > -FLT_MAX) {
			alpha1 = xVal - (lo - delta);
		} else {
			alpha1 = MAX_TYPE;
		}
	}

	//cout << "Res1 value :  " << res1.value << " - alpha1 : " << alpha1 << endl;

	alpha1 = MIN(alpha1, res1.value);

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		res.mode = M_UNBOUNDED;
		printf("We went there(unbounded)!");
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(up < FLT_MAX)
			alpha2 = up - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(lo > -FLT_MAX)
			alpha2 = xVal - lo;
		else
			alpha2 = MAX_TYPE;
	}

	//cout << "alpha2 : " << alpha2 << " -  alpha1 : " << alpha1 << endl;

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
	    alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		res2 = expandP2W(pGPU->nBasic, pGPU->bBounds, alpha1, objSign);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1)
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);

	return res;
}



row_result_t KernelWrapperRevised::expandFindRowP1(red_result_t enteringVar, double delta, double tau){
	row_result_t res;
	red_result_t resRed;
	red_result_t res1;
	exp_result_t res2;
	VAR_TYPE xVal, objVal;
	float2 xBound, xBoundP1;
	float lo, up, loP1, upP1;
	VAR_TYPE alpha1, alpha2, alphaI, teta;
	VAR_TYPE alphaMin;
	int objSign; // = (enteringVar.value > 0.0 ? 1 : -1);

	// Phase 1 : we expand the bound
	_START_EVENT(prof, "INIT")
	cutilSafeCallNoSync( cudaMemcpy( &objVal, &d_redCost[enteringVar.index], 1 * sizeof(objVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xVal, &pGPU->nVal[enteringVar.index], 1 * sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBound, &pGPU->nBounds[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &xBoundP1, &pGPU->nBoundsP1[enteringVar.index], 1 * sizeof(float2), cudaMemcpyDeviceToHost) );
	_END_EVENT(prof, "INIT")

	objSign = (objVal > 0.0 ? 1 : -1);

	_START_EVENT(prof, "PHASE_1")
	res1 = expandP1W(pGPU->nBasic, pGPU->bBoundsP1, delta, objSign);
	_END_EVENT(prof, "PHASE_1")

	 lo = xBound.x;
	 up = xBound.y;
	 loP1 = xBoundP1.x;
	 upP1 = xBoundP1.y;

	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha1 = (upP1 + delta) - xVal;
		else
			alpha1 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha1 = xVal - (loP1 - delta);
		else
			alpha1 = MAX_TYPE;
	}
	alpha1 = MIN(alpha1, res1.value);

	// Special step - trying to put a infeasible variable to it's feasible bound
	// Getting the max absolute pivot
	_START_EVENT(prof, "REDUCTION_MAX")
	resRed = argMaxAbsW(pGPU->nRow, d_enterCol, 1);
	_END_EVENT(prof, "REDUCTION_MAX")
	// Tolerance on the pivot
	teta = fabs(resRed.value) * EXPAND_W;
	// Expand special Phase 1 step
	_START_EVENT(prof, "SPECIAL_P1_PHASE")
	res2 = expandSpecW(pGPU->nBasic, teta, objSign);
	_END_EVENT(prof, "SPECIAL_P1_PHASE")
	SET_E(res, res2);
	res.mode = M_INF_PIVOT;

	// Will proabaly not work
	if(up != upP1){
		alphaI = xVal - up;
	} else if (lo != loP1) {
		alphaI = lo - xVal;
	} else {
		alphaI = 0;
	}


	if(alphaI > res.alpha){
		res.index = -2;
		res.alpha = alphaI;
		res.absP = xVal;
		res.mode = M_INF_TO_BOUND;
	}

	if(res.alpha <= alpha1 && res.index != -1) {
		res.alpha *= (double)objSign;
		return res;
	}

	// if the pivoting step is limited by the bound of the variable
	// then we don't have to pivot

	// If there is no bound on this variable
	if(alpha1 == MAX_TYPE){
		printf("We went there(unbounded) [P1]!");
		res.mode = M_UNBOUNDED;
		res.index = -3;
		return res;
	}

	// Phase 2 : searching the biggest pivot within the expanded limiting alpha;
	if(objSign == 1){
		if(upP1 < FLT_MAX)
			alpha2 = upP1 - xVal;
		else
			alpha2 = MAX_TYPE;
	} else {
		if(loP1 > -FLT_MAX)
			alpha2 = xVal - loP1;
		else
			alpha2 = MAX_TYPE;
	}

	if(alpha2 <= alpha1) {
		res.index = -2;
		res.absP = xVal;
		res.alpha = alpha2;
		res.mode = M_BASIC_TO_BOUND;
		alphaMin = tau;
	} else {
		_START_EVENT(prof, "PHASE_2")
		res2 = expandP2W(pGPU->nBasic, pGPU->bBoundsP1, alpha1, objSign);
		_END_EVENT(prof, "PHASE_2")
		SET_E(res, res2);
		res.mode = M_PIVOT;
		alphaMin = tau/res.absP;
	}

	res.alpha = (double)objSign * MAX(res.alpha, alphaMin);

	if(res.index == -1){
		printf("p1 index : %d - p2 index : %d\n", res1.index, res2.index);
	}

	return res;

}

red_result_t KernelWrapperRevised::argMaxPosW(int nEl, VAR_TYPE* p1, int incP1){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxPos(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
	SET(gpu_result, MAX_R(gpu_result, hRes[i]));

	if(F_EQUAL_Z(gpu_result.value)) gpu_result.index = -1;

	return gpu_result;
}

red_result_t KernelWrapperRevised::argMaxAbsW(int nEl, VAR_TYPE* p1, int incP1){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	argMaxAbs(nEl, nThreads, nBlocks, p1, incP1, dRes);
	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_ABS_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


red_result_t KernelWrapperRevised::expandP1W(int nEl, float2 *bounds, double delta, int objSign){
	int nBlocks, nThreads;
	red_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.value = MAX_TYPE;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP1(nEl, nThreads, nBlocks, d_enterCol, pGPU->bVal, bounds, delta, objSign, dRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET(gpu_result, MIN_R(gpu_result, hRes[i]));
	}

	return gpu_result;

}

exp_result_t KernelWrapperRevised::expandP2W(int nEl, float2* bounds, double alpha, int objSign){
	int nBlocks, nThreads;
	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.absP = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandP2(nEl, nThreads, nBlocks, d_enterCol, pGPU->bVal, bounds, alpha, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++){
		SET_E_P(gpu_result, MAX_E_P(gpu_result, hERes[i]));
	}

	return gpu_result;

}

exp_result_t KernelWrapperRevised::expandSpecW(int nEl, double teta, int objSign){
	int nBlocks, nThreads;
	exp_result_t gpu_result;
	gpu_result.index = -1;
	gpu_result.alpha = 0;

	super::getNumBlocksAndThreads(nEl, nBlocks, nThreads);
	expandSpec(nEl, nThreads, nBlocks, d_enterCol,  pGPU->bVal, pGPU->bBounds, pGPU->bBoundsP1, teta, objSign, dERes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hERes, dERes, nBlocks*sizeof(exp_result_t), cudaMemcpyDeviceToHost) );
	#endif
	for(int i=0; i<nBlocks; i++)
		SET_E_A(gpu_result, MAX_E_A(gpu_result, hERes[i]));


	return gpu_result;

}


void KernelWrapperRevised::orderValsW(){
	int nEl = pGPU->nBasic+pGPU->nNonBasic;
	int threads = (nEl < MAX_THREADS) ? super::nextPow2((nEl + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(MAX_BLOCKS, ceil((float)nEl/threads));
	orderVals(nEl, threads, blocks, pGPU->nNonBasic, pGPU->nVal, pGPU->nInd, pGPU->bVal, pGPU->bInd, d_tmpNonBasic, d_tmpBasic);
	_SYNC_KERNEL

}



red_result KernelWrapperRevised::findCol(){

	//return argMaxAbsW(pGPU->nNonBasic, d_redCost, 1);
	return argMaxPosW(pGPU->nNonBasic, d_redCost, 1);
}

int KernelWrapperRevised::checkFeasNonBasicW() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);
	checkFeasXWS(pGPU->nNonBasic, nThreads, nBlocks, pGPU->nVal, pGPU->nBounds, pGPU->nBoundsP1, pGPU->CnP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperRevised::checkFeasBasicW() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nBasic, nBlocks, nThreads);
	checkFeasRevisedBWS(pGPU->nBasic, nThreads, nBlocks, pGPU->bVal, pGPU->bBounds, pGPU->bBoundsP1, pGPU->CbP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperRevised::checkFeasNonBasicV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);
	checkFeasXV2(pGPU->nNonBasic, nThreads, nBlocks, pGPU->nVal, pGPU->nBounds, pGPU->nBoundsP1, pGPU->CnP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperRevised::checkFeasBasicV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nBasic, nBlocks, nThreads);
	checkFeasRevisedBV2(pGPU->nBasic, nThreads, nBlocks, pGPU->bVal, pGPU->bBounds, pGPU->bBoundsP1, pGPU->CbP1, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}

int KernelWrapperRevised::updateFeasV2W() {
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nBasic, nBlocks, nThreads);
	updateFeasV2(pGPU->nBasic + pGPU->nNonBasic, nThreads, nBlocks, pGPU->nNonBasic, pGPU->nVal, pGPU->bVal, pGPU->nBounds, pGPU->bBounds, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}


int KernelWrapperRevised::resetXValW(){
	int sum = 0;
	int nBlocks, nThreads;

	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);
	resetXVal(pGPU->nNonBasic, nThreads, nBlocks, pGPU->nVal, pGPU->nBounds, dIRes);
	_SYNC_KERNEL

	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hIRes, dIRes, nBlocks*sizeof(int), cudaMemcpyDeviceToHost) );
	#endif

	for(int i=0; i<nBlocks; i++)
		sum += hIRes[i];

	return sum;
}



void KernelWrapperRevised::checkInfeasibility(SimplexProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = inP->PRIMAL;

	infB = checkFeasBasicW();
	infX = checkFeasNonBasicW();

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		inP->mode = inP->AUX;
	}
	_SYNC_KERNEL
}

void KernelWrapperRevised::updateInfeasibility(SimplexProblem *inP) {

	int infB = 0;
	int infX = 0;

	_START_EVENT(prof, "CHECK_BASIC")
	infB = checkFeasBasicW();
	_END_EVENT(prof, "CHECK_BASIC")
	_START_EVENT(prof, "CHECK_NON_BASIC")
	infX = checkFeasNonBasicW();
	_END_EVENT(prof, "CHECK_NON_BASIC")

	// Checking the basis feasability : return the number of non feasible becoming feasible
	if(inP->nInf != (infX + infB)){
		inP->nInf = infX + infB;

		_START_EVENT(prof, "UPDATE_OBJ_FUNC")

		if(inP->nInf > 0){
			// Y has the coefficient set in previously called checkFeasXW
			cutilSafeCallNoSync( cudaMemcpy( pGPU->CnP1, d_tmpNonBasic, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );
			cutilSafeCallNoSync( cudaMemcpy( pGPU->CbP1, d_tmpBasic, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice) );

			inP->mode = inP->AUX;

		} else {
			inP->mode = inP->AUX;
		}

		_END_EVENT(prof, "UPDATE_OBJ_FUNC")
	}
	_SYNC_KERNEL
}


void KernelWrapperRevised::checkInfeasibilityV2(SimplexProblem *inP) {
	int infB = 0;
	int infX = 0;
	inP->mode = inP->PRIMAL;

	infB = checkFeasBasicV2W();
	infX = checkFeasNonBasicV2W();
	cout << "Infeas basis : " << infB << " - infeas nBas" << infX << endl;

	inP->nInf = infX + infB;

	if(inP->nInf > 0){
		inP->mode = inP->AUX;
	}
	_SYNC_KERNEL
}

void KernelWrapperRevised::updateInfeasibilityV2(SimplexProblem *inP) {

	inP->nInf = updateFeasV2W();

	if(inP->nInf>0){
		inP->mode = inP->AUX;
	} else {
		inP->mode = inP->PRIMAL;
	}

	_SYNC_KERNEL
}

void KernelWrapperRevised::reinitProblem(RevisedFormProblem *inP){
	// Order the val (so we begin with a nearly optimal solution)

	orderValsW();

	// Matrix
	// Ab
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->Ab, pitch_Ab, inP->Ab, inP->nRow*sizeof(*pGPU->Ab), inP->nRow*sizeof(*pGPU->Ab), inP->nBasic, cudaMemcpyHostToDevice) );
	// An
	#if AN_STORAGE == AN_COL_WISE
		cutilSafeCallNoSync( cudaMemcpy2D(pGPU->An, pitch_An, inP->An, inP->nRow*sizeof(*pGPU->An), inP->nRow*sizeof(*pGPU->An), inP->nNonBasic, cudaMemcpyHostToDevice) );
	#elif AN_STORAGE == AN_ROW_WISE
		cutilSafeCallNoSync( cudaMemcpy2D(pGPU->An, pitch_An, h_transpAn, inP->nNonBasic*sizeof(*pGPU->An), inP->nNonBasic*sizeof(*pGPU->An), inP->nRow, cudaMemcpyHostToDevice) );
	#endif
	// B
	cutilSafeCallNoSync( cudaMemcpy2D(pGPU->B, pitch_B, inP->B, inP->nRow*sizeof(*pGPU->B), inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyHostToDevice) );

	// Vectors
	// Basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBounds, inP->bBounds, inP->nBasic*sizeof(*pGPU->bBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->bBoundsP1, inP->bBoundsP1, inP->nBasic*sizeof(*pGPU->bBoundsP1), cudaMemcpyHostToDevice) );

	// Non basic variable bounds
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nBounds, inP->nBounds, inP->nNonBasic*sizeof(*pGPU->nBounds), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->nBoundsP1, inP->nBoundsP1, inP->nNonBasic*sizeof(*pGPU->nBoundsP1), cudaMemcpyHostToDevice) );

	// Objective function
	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cb, inP->Cb, inP->nBasic*sizeof(*pGPU->Cb), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CbP1, inP->CbP1, inP->nBasic*sizeof(*pGPU->CbP1), cudaMemcpyHostToDevice) );

	cutilSafeCallNoSync( cudaMemcpy(pGPU->Cn, inP->Cn, inP->nNonBasic*sizeof(*pGPU->Cn), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(pGPU->CnP1, inP->CnP1, inP->nNonBasic*sizeof(*pGPU->CnP1), cudaMemcpyHostToDevice) );

}

void KernelWrapperRevised::changeBoundWK(IndexPivoting *inI, int index, float2 bound){
	int loc = inI->xLoc[index];
	if((unsigned int)loc >= pGPU->nNonBasic) {
		cutilSafeCallNoSync(cudaMemcpy(&pGPU->bBounds[loc-pGPU->nNonBasic], &bound, sizeof(float2), cudaMemcpyHostToDevice));
	} else {
		cutilSafeCallNoSync(cudaMemcpy(&pGPU->nBounds[loc], &bound, sizeof(float2), cudaMemcpyHostToDevice));
	}
	_SYNC_KERNEL
}

void KernelWrapperRevised::processOptimum(SimplexProblem *inP){

	#if TYPE == USE_DOUBLE
		inP->objVal = cublasDdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->Cb, 1);
		cudaThreadSynchronize();
		inP->objVal += cublasDdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->Cn, 1);
		cudaThreadSynchronize();
	#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->Cb, 1);
		cudaThreadSynchronize();
		inP->objVal += cublasSdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->Cn, 1);
		cudaThreadSynchronize();
	#endif

	if(inP->mode == inP->AUX) {
		#if TYPE == USE_DOUBLE
		inP->objVal = cublasDdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->CbP1, 1);
		cudaThreadSynchronize();
		inP->objVal += cublasDdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->CnP1, 1);
		cudaThreadSynchronize();
		#elif TYPE == USE_FLOAT
		inP->objVal = cublasSdot(pGPU->nBasic, pGPU->bVal, 1, pGPU->CbP1, 1);
		inP->objVal += cublasSdot(pGPU->nNonBasic, pGPU->nVal, 1, pGPU->CnP1, 1);
		#endif
	}
	_SYNC_KERNEL
}


void KernelWrapperRevised::processEqs(){

	if(!eqsUpToDate && !limitedMem){
		#if TYPE == USE_DOUBLE
			// Process An' = B^-1 * AN ==> into d_eqs
			#if AN_STORAGE == AN_COL_WISE
				cublasDgemm('N', 'N', pGPU->nBasic, pGPU->nNonBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->An, P2S(pitch_An), 0.0, d_eqs, P2S(pitch_eqs));
			#elif AN_STORAGE == AN_ROW_WISE
				cublasDgemm('N', 'T', pGPU->nBasic, pGPU->nNonBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->An, P2S(pitch_An), 0.0, d_eqs, P2S(pitch_eqs));
			#endif
			// Process lhs' = B^-1 * lhs ==> into d_eqs
			cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->lhs, 1, 0.0, &d_eqs[IDX2C(0, pGPU->nNonBasic, P2S(pitch_eqs))], 1);
		#elif TYPE == USE_FLOAT
			cublasSgemm('N', 'N', pGPU->nBasic, pGPU->nNonBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->An, P2S(pitch_An), 0.0, d_eqs, P2S(pitch_eqs));
			cublasSgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), pGPU->lhs, 1, 0.0, &d_eqs[IDX2C(0, pGPU->nNonBasic, pitch_eqs)], 1);
		#endif
		eqsUpToDate = true;
	}


	_SYNC_KERNEL

}

void KernelWrapperRevised::resetBounds(){

	// Reset Non basic values to their bounds after EXPAND
	// nTriv contains the number of non-trivial reset
	int nTriv = resetXValW();
	cout << "non trivial reset : " << nTriv << endl;

	//cout << "N trivial  : " << nTriv << endl;

	if(nTriv > 0 && !limitedMem){
		// Process the full equations (standard form problem)
		processEqs();
		// Copy last col of d_eqs (rhs of equations) into tmpBasic
		cutilSafeCallNoSync(cudaMemcpy(d_tmpBasic, &d_eqs[IDX2C(0, pGPU->nNonBasic, P2S(pitch_eqs))], pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice));
		// Process bVal afresh : bVal = tmpBasic - eqs*nVal
		#if TYPE == USE_DOUBLE
			cublasDgemv('N', pGPU->nRow, pGPU->nNonBasic, -1.0, d_eqs, P2S(pitch_eqs), pGPU->nVal, 1, 1.0, d_tmpBasic, 1);
		#elif TYPE == USE_FLOAT
			cublasSgemv('N', pGPU->nRow, pGPU->nNonBasic, -1.0, d_eqs, P2S(pitch_eqs), pGPU->nVal, 1, 1.0, d_tmpBasic, 1);
		#endif

		cutilSafeCallNoSync(cudaMemcpy(pGPU->bVal, d_tmpBasic, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToDevice));


	}
	_SYNC_KERNEL
}

void KernelWrapperRevised::getSolution(SimplexSolution *inSol){
	// Order the vals as they were at beginning (x1..xn, bn+1...bm)
	// Ordered values are put in y (xVal => 1..nCol) and x (bVal => 1..nRow)
	orderValsW();
	cutilSafeCallNoSync(cudaMemcpy(inSol->xVal, d_tmpNonBasic, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
	cutilSafeCallNoSync(cudaMemcpy(inSol->sVal, d_tmpBasic, pGPU->nBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost));
}

void KernelWrapperRevised::getDeviceData(SimplexProblem *inP){

	_SYNC_KERNEL
	if(!limitedMem){
		// Process the problem to its standard form
		processEqs();
		cutilSafeCallNoSync( cudaMemcpy2D(inP->eqs, inP->nRow*sizeof(*d_eqs), d_eqs, pitch_eqs, inP->nRow*sizeof(*d_eqs), inP->nCol, cudaMemcpyDeviceToHost) );
	}
	processReducedCost();
	cutilSafeCallNoSync( cudaMemcpy( inP->objFunc, d_redCost, inP->nVar * sizeof(*d_redCost), cudaMemcpyDeviceToHost) );
	this->processReducedCostP1();
	cutilSafeCallNoSync( cudaMemcpy( inP->objFuncAux, d_redCost, inP->nVar * sizeof(*d_redCost), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->basis, pGPU->bInd, inP->nRow * sizeof(*pGPU->bInd), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xInd, pGPU->nInd, inP->nVar * sizeof(*pGPU->nInd), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->xVal, pGPU->nVal, inP->nVar * sizeof(*pGPU->nVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBounds, pGPU->nBounds, inP->nVar * sizeof(*pGPU->nBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->xBoundsP1, pGPU->nBoundsP1, inP->nVar * sizeof(*pGPU->nBoundsP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->bVal, pGPU->bVal, inP->nRow * sizeof(*pGPU->bVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBounds, pGPU->bBounds, inP->nRow * sizeof(*pGPU->bBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBoundsP1, pGPU->bBoundsP1, inP->nRow * sizeof(*pGPU->bBoundsP1), cudaMemcpyDeviceToHost) );

}

// Get the problem
void KernelWrapperRevised::getDeviceData(RevisedFormProblem *inP){

	_SYNC_KERNEL

	cutilSafeCallNoSync( cudaMemcpy2D(inP->Ab, inP->nRow*sizeof(*pGPU->Ab), pGPU->Ab, pitch_Ab, inP->nRow*sizeof(*pGPU->Ab), inP->nBasic, cudaMemcpyDeviceToHost) );
	#if AN_STORAGE == AN_COL_WISE
		cutilSafeCallNoSync( cudaMemcpy2D(inP->An, inP->nRow*sizeof(*pGPU->An), pGPU->An, pitch_An, inP->nRow*sizeof(*pGPU->An), inP->nNonBasic, cudaMemcpyDeviceToHost) );
	#elif AN_STORAGE == AN_ROW_WISE
		for(unsigned int i=0; i<pGPU->nNonBasic; i++){
			cublasDcopy(pGPU->nRow, &pGPU->An[IDX2C(i, 0, P2S(pitch_An))], P2S(pitch_An), d_tmpCol, 1);
			cutilSafeCallNoSync( cudaMemcpy( &inP->An[IDX2C(0, i, inP->nRow)], d_tmpCol, inP->nRow * sizeof(*d_tmpCol), cudaMemcpyDeviceToHost) );
		}
	#endif
	cutilSafeCallNoSync( cudaMemcpy2D(inP->B, inP->nRow*sizeof(*pGPU->B), pGPU->B, pitch_B, inP->nRow*sizeof(*pGPU->B), inP->nBasic, cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->bInd, pGPU->bInd, inP->nBasic * sizeof(*pGPU->bInd), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->nInd, pGPU->nInd, inP->nNonBasic * sizeof(*pGPU->nInd), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->nVal, pGPU->nVal, inP->nNonBasic * sizeof(*pGPU->nVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->nBounds, pGPU->nBounds, inP->nNonBasic * sizeof(*pGPU->nBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->nBoundsP1, pGPU->nBoundsP1, inP->nNonBasic * sizeof(*pGPU->nBoundsP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->bVal, pGPU->bVal, inP->nBasic * sizeof(*pGPU->bVal), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBounds, pGPU->bBounds, inP->nBasic * sizeof(*pGPU->bBounds), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( inP->bBoundsP1, pGPU->bBoundsP1, inP->nBasic * sizeof(*pGPU->bBoundsP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy( inP->lhs, pGPU->lhs, inP->nRow * sizeof(*pGPU->lhs), cudaMemcpyDeviceToHost) );

	// Objective function
	cutilSafeCallNoSync( cudaMemcpy(inP->Cb, pGPU->Cb, inP->nBasic*sizeof(*pGPU->Cb), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->CbP1, pGPU->CbP1, inP->nBasic*sizeof(*pGPU->CbP1), cudaMemcpyDeviceToHost) );

	cutilSafeCallNoSync( cudaMemcpy(inP->Cn, pGPU->Cn, inP->nNonBasic*sizeof(*pGPU->Cn), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(inP->CnP1, pGPU->CnP1, inP->nNonBasic*sizeof(*pGPU->CnP1), cudaMemcpyDeviceToHost) );

}


/**
 * Process the steepest edge coefficient from scratch
 */
void KernelWrapperRevised::processSECoeff(){
	int threads = (pGPU->nRow < MAX_THREADS*2) ? super::nextPow2((pGPU->nRow + 1)/ 2) : MAX_THREADS;
	int blocks = MIN(pGPU->nNonBasic, MAX_BLOCKS);

	if(!limitedMem){

		// Process matrice [An Ab] afresh if required
		processEqs();
		// Process the n*n^(T) + 1 coefficient for each column (euclidian norm of each column)
		processSECoeffRevised(pGPU->nRow, pGPU->nNonBasic, threads, blocks, d_eqs, P2S(pitch_eqs), d_seCoeff);
		_SYNC_KERNEL
	} else if(eqsUpToDate){
		processSECoeffRevised(pGPU->nRow, pGPU->nNonBasic, threads, blocks, pGPU->An, P2S(pitch_An), d_seCoeff);
	}

}

void KernelWrapperRevised::processBeta(){
#if B_STORAGE == B_NORMAL
	cublasDgemv('T', pGPU->nBasic, pGPU->nBasic, 1.0, pGPU->B, P2S(pitch_B), d_enterCol, 1, 0.0, d_tmpCol, 1);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	cublasDgemv('N', pGPU->nBasic, pGPU->nBasic, 1.0, d_BTrans, P2S(pitch_B), d_enterCol, 1, 0.0, d_tmpCol, 1);
#endif

#if AN_STORAGE == AN_COL_WISE
	cublasDgemv('T', pGPU->nRow, pGPU->nNonBasic, 1.0, pGPU->An, P2S(pitch_An), d_tmpCol, 1, 0.0, d_beta, 1);
#elif AN_STORAGE == AN_ROW_WISE
	cublasDgemv('N', pGPU->nNonBasic, pGPU->nRow,  1.0, pGPU->An, P2S(pitch_An), d_tmpCol, 1, 0.0, d_beta, 1);
#endif
}

/**
 * Return the current pivot value from the d_enterCol vector
 */
VAR_TYPE KernelWrapperRevised::getPivotValue(row_result_t leavingVar){
	VAR_TYPE dl;
	//cout << leavingVar.index << endl;
	cutilSafeCallNoSync( cudaMemcpy( &dl, &d_enterCol[leavingVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	return dl;
}

/**
 * Process, afresh, the steepest edge coefficient of the entering variable
 */
VAR_TYPE KernelWrapperRevised::getEnteringSECoeff(){
	VAR_TYPE nrm = cublasDnrm2(pGPU->nRow, d_enterCol, 1);
	cudaThreadSynchronize();
	return (nrm*nrm + 1.0);
}

/**
 * Process, afresh, the reduced cost of the entering variable
 */
VAR_TYPE KernelWrapperRevised::getZq(red_result_t enteringVar){
	VAR_TYPE zq, cq;
	cutilSafeCallNoSync( cudaMemcpy( &cq, &pGPU->Cn[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	zq = -cublasDdot(pGPU->nRow, pGPU->Cb, 1, d_enterCol, 1);
	cudaThreadSynchronize();
	zq += cq;


	// TODO Check if needed
	//VAR_TYPE tmpq;
	//cutilSafeCallNoSync( cudaMemcpy( &tmpq, &d_redCost[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	return zq;
}

/**
 * Process, afresh, the reduced cost of the entering variable
 */
VAR_TYPE KernelWrapperRevised::getZqP1(red_result_t enteringVar){
	VAR_TYPE zq, cq;
	cutilSafeCallNoSync( cudaMemcpy( &cq, &pGPU->CnP1[enteringVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	zq = -cublasDdot(pGPU->nRow, pGPU->CbP1, 1, d_enterCol, 1);
	cudaThreadSynchronize();
	zq += cq;


	return zq;
}

/**
 * Process the pivot row alpha
 */
void KernelWrapperRevised::processAlpha(row_result_t leavingVar){

#if B_STORAGE == B_NORMAL
	VAR_TYPE *ptrB = &pGPU->B[IDX2C(leavingVar.index, 0, P2S(pitch_B))];
	int pitch = P2S(pitch_B);
#elif B_STORAGE == B_NORMAL_AND_TRANS
	VAR_TYPE *ptrB = &d_BTrans[IDX2C(0, leavingVar.index, P2S(pitch_B))];
	int pitch = 1;
#endif

#if AN_STORAGE == AN_COL_WISE
	cublasDgemv('T', pGPU->nRow, pGPU->nNonBasic, 1.0, pGPU->An, P2S(pitch_An), ptrB, pitch, 0.0, d_alpha, 1);
#elif AN_STORAGE == AN_ROW_WISE
	cublasDgemv('N', pGPU->nNonBasic, pGPU->nRow,  1.0, pGPU->An, P2S(pitch_An), ptrB, pitch, 0.0, d_alpha, 1);
#endif
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::updateSECoeff(red_result_t enteringVar, row_result_t leavingVar){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	seCoeffQ = getEnteringSECoeff();
	alphaQ = getPivotValue(leavingVar);
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);

	updateSECoeffRevised(pGPU->nNonBasic, nThreads, nBlocks,
			             enteringVar.index, seCoeffQ, newSeCoeffQ,
			             d_seCoeff, d_alpha, d_beta,
			             pGPU->nVal, pGPU->nBounds, d_redCost,
			             dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

red_result_t KernelWrapperRevised::updateSECoeffP1(red_result_t enteringVar, row_result_t leavingVar){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	seCoeffQ = getEnteringSECoeff();
	alphaQ = getPivotValue(leavingVar);
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);

	updateSECoeffRevised(pGPU->nNonBasic, nThreads, nBlocks,
			             enteringVar.index, seCoeffQ, newSeCoeffQ,
			             d_seCoeff, d_alpha, d_beta,
			             pGPU->nVal, pGPU->nBoundsP1, d_redCost,
			             dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;


	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperRevised::selectSECoeff(){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	/*/////////////////////////////////////////////////////////////////////////////////
	double h_seCoeff[pGPU->nNonBasic];
	double h_redCost[pGPU->nNonBasic];

	cutilSafeCallNoSync( cudaMemcpy( h_seCoeff, d_seCoeff, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_redCost, d_redCost, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );


	cout << "index\t seCoeff\t redCost" << endl;

	for(unsigned int i=0; i<pGPU->nNonBasic; i++){
		cout << i << "\t\t" << h_seCoeff[i] << "\t\t" << h_redCost[i] << endl;
	}

	getchar();
	/////////////////////////////////////////////////////////////////////////////////*/

	selectSECoeffRevised(pGPU->nNonBasic, nThreads, nBlocks,
						 d_seCoeff, pGPU->nVal, pGPU->nBounds, d_redCost, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Select the best entering column using updated se coefficient
 */
red_result_t KernelWrapperRevised::selectSECoeffP1(){

	red_result_t gpu_result;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	/*double h_seCoeff[pGPU->nNonBasic];
	double h_redCost[pGPU->nNonBasic];

	cutilSafeCallNoSync( cudaMemcpy( h_seCoeff, d_seCoeff, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( h_redCost, d_redCost, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );


	cout << "index\t seCoeff\t redCost" << endl;

	for(unsigned int i=0; i<pGPU->nNonBasic; i++){
		cout << i << "\t\t" << h_seCoeff[i] << "\t\t" << h_redCost[i] << endl;
	}

	getchar();*/

	selectSECoeffRevised(pGPU->nNonBasic, nThreads, nBlocks,
						 d_seCoeff, pGPU->nVal, pGPU->nBoundsP1, d_redCost, dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::updateInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	updateInVarRevised(pGPU->nNonBasic, nThreads, nBlocks,
			           enteringVar.index, seCoeffQ, newSeCoeffQ,
			           redCostQ, newRedCostQ,
			           d_seCoeff, d_alpha, d_beta,
			           pGPU->nVal, pGPU->nBounds, d_redCost,
			           dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::updateInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / (alphaQ * alphaQ);
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	//cout << "Nouvel valeur redCost Q[" <<  enteringVar.index <<"] = " << newRedCostQ << endl;
	//getchar();

	updateInVarRevised(pGPU->nNonBasic, nThreads, nBlocks,
			           enteringVar.index, seCoeffQ, newSeCoeffQ,
			           redCostQ, newRedCostQ,
			           d_seCoeff, d_alpha, d_beta,
			           pGPU->nVal, pGPU->nBoundsP1, d_redCost,
			           dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


void KernelWrapperRevised::updateRedCost(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	int nBlocks, nThreads;
	VAR_TYPE alphaQ, newZq;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	alphaQ = this->getPivotValue(leavingVar);
	newZq = -Zq/alphaQ;

	updateRCRevised(pGPU->nNonBasic, nThreads, nBlocks, enteringVar.index, Zq, newZq, d_alpha, d_redCost);

}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::approxInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ, pValPow2;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	// Pivot value
	alphaQ = getPivotValue(leavingVar);
	pValPow2 = alphaQ*alphaQ;
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / pValPow2;
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	approxInVarRevised(pGPU->nNonBasic, nThreads, nBlocks,
					   enteringVar.index,pValPow2, seCoeffQ, newSeCoeffQ,
					   redCostQ, newRedCostQ,
					   d_seCoeff, d_alpha,
					   pGPU->nVal, pGPU->nBoundsP1, d_redCost,
					   dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::approxInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ, pValPow2;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	// Pivot value
	alphaQ = getPivotValue(leavingVar);
	pValPow2 = alphaQ*alphaQ;
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / pValPow2;
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	approxInVarRevised(pGPU->nNonBasic, nThreads, nBlocks,
					   enteringVar.index, pValPow2, seCoeffQ, newSeCoeffQ,
					   redCostQ, newRedCostQ,
					   d_seCoeff, d_alpha,
					   pGPU->nVal, pGPU->nBounds, d_redCost,
					   dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


/**
 * Init all the devex coefficient to 1
 *
 */
void KernelWrapperRevised::initDevexCoeff(){

	if(devexInit == NULL){
		devexInit = new VAR_TYPE[pGPU->nNonBasic];
		for(unsigned int i=0; i<pGPU->nNonBasic; i++){
			devexInit[i] = 1.0;
		}
	}

	cutilSafeCallNoSync( cudaMemcpy( d_seCoeff, devexInit, pGPU->nNonBasic*sizeof(VAR_TYPE), cudaMemcpyHostToDevice) );

}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::devexInVarP1(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	// Pivot value
	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / fabs(alphaQ);
	newSeCoeffQ = 1 > newSeCoeffQ ? 1 : newSeCoeffQ;
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;


	//cout << "Nouvel valeur redCost Q[" <<  enteringVar.index <<"] = " << newRedCostQ << endl;
	//getchar();

	updateDVXInVarRevised(	pGPU->nNonBasic, nThreads, nBlocks,
							enteringVar.index, seCoeffQ, newSeCoeffQ,
							redCostQ, newRedCostQ,
							d_seCoeff, d_alpha,
							pGPU->nVal, pGPU->nBoundsP1, d_redCost,
							dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}

/**
 * Update the steepest edge weights and return the entering column
 * WARNING : Must be done after the pivoting has been done
 */
red_result_t KernelWrapperRevised::devexInVar(red_result_t enteringVar, row_result_t leavingVar, VAR_TYPE Zq){

	red_result_t gpu_result;
	VAR_TYPE seCoeffQ, alphaQ, newSeCoeffQ, redCostQ, newRedCostQ;
	int nBlocks, nThreads;
	super::getNumBlocksAndThreads(pGPU->nNonBasic, nBlocks, nThreads);

	// Pivot value
	alphaQ = getPivotValue(leavingVar);
	// Steepest edge coeff
	seCoeffQ = getEnteringSECoeff();
	newSeCoeffQ = seCoeffQ / fabs(alphaQ);
	newSeCoeffQ = 1 > newSeCoeffQ ? 1 : newSeCoeffQ;
	// Reduced cost
	redCostQ = Zq;
	newRedCostQ = -Zq/alphaQ;

	updateDVXInVarRevised(	pGPU->nNonBasic, nThreads, nBlocks,
						   enteringVar.index, seCoeffQ, newSeCoeffQ,
						   redCostQ, newRedCostQ,
						   d_seCoeff, d_alpha,
						   pGPU->nVal, pGPU->nBounds, d_redCost,
						   dRes);

	_SYNC_KERNEL
	#if !(PINNED_RESULT && PINNED)
		cutilSafeCallNoSync( cudaMemcpy( hRes, dRes, nBlocks*sizeof(red_result_t), cudaMemcpyDeviceToHost) );
	#endif

	gpu_result.index = -1;
	gpu_result.value = 0.0;

	for(int i=0; i<nBlocks; i++) {
		SET(gpu_result, MAX_R(gpu_result, hRes[i]));
	}

	return gpu_result;
}


void KernelWrapperRevised::checkInVar(red_result_t inVar){

	VAR_TYPE redCost, seCoeff;

	cout << "In var("<< inVar.index << ")" << flush;

	if(inVar.index >= 0){
		cutilSafeCallNoSync( cudaMemcpy( &redCost, &d_redCost[inVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
		cutilSafeCallNoSync( cudaMemcpy( &seCoeff, &d_seCoeff[inVar.index], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

		 cout << " ==> Red cost : " << redCost << " - Se coeff : " << seCoeff << endl;
	} else {
		cout << endl;
	}

}

void KernelWrapperRevised::checkPivoting(red_result_t inVar, row_result_t outVar){

	int idx;
	VAR_TYPE val, redCost, seCoeff;
	int outIdx = inVar.index;

	cutilSafeCallNoSync( cudaMemcpy( &idx, &pGPU->nInd[outIdx], sizeof(int), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &val, &pGPU->nVal[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &redCost, &d_redCost[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy( &seCoeff, &d_seCoeff[outIdx], sizeof(VAR_TYPE), cudaMemcpyDeviceToHost) );

	//cout << "in var x"<< idx << " ==> Red cost : " << redCost << " - Se coeff : " << seCoeff << endl;
	cout << "out x"<< idx << " ==> Rc : " << redCost << " - SE : " << seCoeff << "- Val : "<< val << endl;

}
