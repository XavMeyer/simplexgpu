//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemorySpaceGPU.h
 *
 *  Created on: Mar 1, 2012
 *      Author: meyerx
 */

#ifndef SPARSEMEMORYSPACEGPU_H_
#define SPARSEMEMORYSPACEGPU_H_

#include <cutil_inline.h>
#include "../../simplexCPU/utils/SparseMemorySpace.h"
#include "../../Types.h"

class SparseMemorySpaceGPU {
public:


	// Variables
	unsigned int currentSize;
	unsigned int maxSize;

	VAR_TYPE *data;
	int *colIdx, *rowIdx;

	SparseMemorySpaceGPU(SparseMemorySpace *sms);
	virtual ~SparseMemorySpaceGPU();

	bool createStucture(SparseMemorySpace *sms);
	bool copyToGPU(SparseMemorySpace *sms);

	unsigned int size();

};

#endif /* SPARSEMEMORYSPACEGPU_H_ */
