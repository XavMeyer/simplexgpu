//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemorySpaceGPU.cpp
 *
 *  Created on: Mar 1, 2012
 *      Author: meyerx
 */

#include "SparseMemorySpaceGPU.h"

SparseMemorySpaceGPU::SparseMemorySpaceGPU(SparseMemorySpace *sms){

	currentSize = sms->currentSize;
	maxSize = currentSize;

	this->createStucture(sms);
	this->copyToGPU(sms);

}

bool SparseMemorySpaceGPU::createStucture(SparseMemorySpace *sms){

	cutilSafeCallNoSync( cudaMalloc((void**) &data, currentSize*sizeof(*this->data)));
	cutilSafeCallNoSync( cudaMalloc((void**) &rowIdx, currentSize*sizeof(*this->rowIdx)));
	cutilSafeCallNoSync( cudaMalloc((void**) &colIdx, currentSize*sizeof(*this->colIdx)));

	return true;
}

bool SparseMemorySpaceGPU::copyToGPU(SparseMemorySpace *sms){

	cutilSafeCallNoSync( cudaMemcpy(this->data, sms->data, currentSize*sizeof(*this->data), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(this->rowIdx, sms->rowIdx, currentSize*sizeof(*this->rowIdx), cudaMemcpyHostToDevice) );
	cutilSafeCallNoSync( cudaMemcpy(this->colIdx, sms->colIdx, currentSize*sizeof(*this->colIdx), cudaMemcpyHostToDevice) );

	return true;
}



SparseMemorySpaceGPU::~SparseMemorySpaceGPU() {

	cudaFree(this->data);
	cudaFree(this->rowIdx);
	cudaFree(this->colIdx);

}

unsigned int SparseMemorySpaceGPU::size(){
	return currentSize;
}
