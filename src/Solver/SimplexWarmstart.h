//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file SimplexWarmstart.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 7, 2011
 *
 */

#ifndef SIMPLEXWARMSTART_H_
#define SIMPLEXWARMSTART_H_

#include "SimplexProblem.h"
#include "simplexGPU/IndexPivoting.h"

template <class Problem>
class SimplexWarmstart {
public:
	SimplexWarmstart(int inCounter, Problem *inP, IndexPivoting *inIP){
		enabled = true;
		counter = inCounter;
		p = inP;
		ip = inIP;
	}

	virtual ~SimplexWarmstart(){
		delete p;
		delete ip;
	}

	int getCounter(){
		return counter;
	}

	Problem * getProblem(){
		return p;
	}

	IndexPivoting * getIndexPivoting(){
		return ip;
	}

	void enableWarmstart(){
		enabled = true;
	}

	void disableWarmstart(){
		enabled = false;
	}

	bool isEnabled(){
		return enabled;
	}

	string toString(){
		ostringstream oss(ostringstream::out);
		oss << endl;
		oss << "<---------- Warmstart ------------>" << endl;
		if(enabled){
			oss << "Warmstart is enabled." << endl;
		} else {
			oss << "Warmstart is disabled." << endl;
		}
		oss << "Counter : " << counter << endl;
		oss << "<---------- End Warmstart ------------>" << endl;

		return oss.str();
	}

private:
	bool enabled;
	int	counter;
	Problem *p;
	IndexPivoting *ip;
};

#endif /* SIMPLEXWARMSTART_H_ */
