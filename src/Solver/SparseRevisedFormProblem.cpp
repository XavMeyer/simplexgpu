//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file RevisedFormProblem.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#include "SparseRevisedFormProblem.h"

SparseRevisedFormProblem::SparseRevisedFormProblem(SimplexProblem *inP) : Problem(inP->nRow, inP->nVar) {
	this->createFromSimplexProblem(inP);
}


SparseRevisedFormProblem::SparseRevisedFormProblem(unsigned int inNRow, unsigned int inNVar,
												   unsigned int inNSlack, unsigned int inNNoNull) :
												   Problem(inNRow, inNVar) {

	// Copy the size
	nCol = inNVar + inNSlack;
	nSlack = inNSlack;
	nNoNull = inNNoNull;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = nVar;


	this->initStructure();
	//this->initData();

}

SparseRevisedFormProblem::SparseRevisedFormProblem(unsigned int inNRow, unsigned int inNVar,
												   unsigned int inNSlack, unsigned int inNNoNull,
												   bool init) :
												   Problem(inNRow, inNVar, init) {

	// Copy the size
	nCol = inNVar + inNSlack;
	nSlack = inNSlack;
	nNoNull = inNNoNull;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = nVar;


	if(init){
		this->initStructure();
		//this->initData();
	} else {
		initPtr();
	}

}


SparseRevisedFormProblem::~SparseRevisedFormProblem() {

	if(sparseA != NULL){

		delete [] sparseA;
		delete [] B;

		delete [] rowIndA;
		delete [] colIndA;

		delete [] lhs;

		delete [] bBounds;
		delete [] bBoundsP1;

		delete [] bounds;
		delete [] boundsP1;

		delete [] bVal;
		delete [] val;

		delete [] bInd;

		delete [] Cb;
		delete [] CbP1;

		delete [] C;
		delete [] CP1;


	}
}


void SparseRevisedFormProblem::initPtr(){

	// Init matrix ptr to NULL
	sparseA		= NULL;
	B  			= NULL;

	rowIndA		= NULL;
	colIndA		= NULL;

	// Init vectors ptr to NULL
	lhs  		= NULL;

	bBounds   	= NULL;
	bBoundsP1  	= NULL;

	bounds 		= NULL;
	boundsP1	= NULL;

	bVal		= NULL;
	val			= NULL;

	bInd		= NULL;

	Cb = CbP1 	= NULL;
	C = CP1 	= NULL;

}

void SparseRevisedFormProblem::createFromSimplexProblem(SimplexProblem *inP){

	unsigned int tmpCnt = 0;

	// Copy the size
	nRow = inP->nRow;
	nVar = inP->nVar;
	nSlack = inP->nSlack;

	// Get basis and non-basic sizes
	nBasic = nRow;
	nNonBasic = nVar;

	nCol = nBasic + nNonBasic;

	nNoNull = nBasic;

	// Counting no null coefficient
	for(unsigned int i=0; i< inP->nRow*inP->nVar; i++){
		if(inP->eqs[i] != 0.0){
			nNoNull++;
		}
	}

	// Create the vector and array
	initStructure();

	// Init B matrix
	for(unsigned int i=0; i<nRow; i++){
		for(unsigned int j=0; j<nBasic; j++){
			if(i != j){
				B[IDX2C(i,j,nRow)] = 0;
			} else {
				B[IDX2C(i,j,nRow)] = 1;
			}
		}
	}

	// Init A matrix
	for(unsigned int j=0; j<nNonBasic; j++){
		for(unsigned int i=0; i<nRow; i++){
			if(inP->eqs[IDX2C(i,j,nRow)] != 0){
				sparseA[tmpCnt] = inP->eqs[IDX2C(i,j,nRow)];
				rowIndA[tmpCnt] = i;
				colIndA[tmpCnt] = j;
				tmpCnt++;
			}
		}
	}
	for(unsigned int i=0; i<nBasic; i++){
		sparseA[tmpCnt] = 1.0;
		rowIndA[tmpCnt] = i;
		colIndA[tmpCnt] = i+nNonBasic;
		tmpCnt++;
	}


	// copy the LHS
	memcpy(lhs, &(inP->eqs[nVar*nRow]), nRow*sizeof(VAR_TYPE));

	// copy the bounds
	// basic
	memcpy(bBounds, inP->bBounds, nBasic*sizeof(float2));
	memcpy(bBoundsP1, inP->bBoundsP1, nBasic*sizeof(float2));

	// All variables
	memcpy(bounds, inP->xBounds, nNonBasic*sizeof(float2));
	memcpy(&bounds[nNonBasic], inP->bBounds, nBasic*sizeof(float2));
	memcpy(boundsP1, inP->xBoundsP1, nNonBasic*sizeof(float2));
	memcpy(&boundsP1[nNonBasic], inP->bBoundsP1, nBasic*sizeof(float2));

	// Copy the values
	memcpy(bVal, inP->bVal, nBasic*sizeof(VAR_TYPE));
	memcpy(val, inP->xVal, nNonBasic*sizeof(VAR_TYPE));
	memcpy(&val[nNonBasic], inP->bVal, nBasic*sizeof(VAR_TYPE));

	// Replace basis slack variabe value into val by NAN
	for(unsigned int i=nNonBasic; i<nNonBasic+nBasic; i++){
			val[i] = NAN;
	}

	// Copy the index
	for(unsigned int i=0; i<nBasic; i++) {
		bInd[i] = (unsigned int)inP->basis[i];
	}

	// Objective function
	for(unsigned int i=0; i<nBasic; i++){
		Cb[i] = 0;
		CbP1[i] = 0;
	}
	memcpy(C, inP->objFunc, nNonBasic*sizeof(VAR_TYPE));
	memcpy(&C[nNonBasic], Cb, nBasic*sizeof(VAR_TYPE));
	memcpy(CP1, inP->objFuncAux, nNonBasic*sizeof(VAR_TYPE));
	memcpy(&CP1[nNonBasic], CbP1, nBasic*sizeof(VAR_TYPE));

}

void SparseRevisedFormProblem::initStructure(){

	// Create matrix
	sparseA  	= new VAR_TYPE[nNoNull];
	B		 	= new VAR_TYPE[nBasic*nBasic];

	// Create vectors

	rowIndA 	= new int[nNoNull];
	colIndA 	= new int[nNoNull];

	lhs			= new VAR_TYPE[nRow];

	bBounds 	= new float2[nBasic];
	bBoundsP1 	= new float2[nBasic];

	bounds 		= new float2[nCol];
	boundsP1	= new float2[nCol];

	bVal		= new VAR_TYPE[nBasic];
	val			= new VAR_TYPE[nCol];

	bInd		= new unsigned int[nBasic];

	Cb 			= new VAR_TYPE[nBasic];
	CbP1 		= new VAR_TYPE[nBasic];
	C 			= new VAR_TYPE[nCol];
	CP1 		= new VAR_TYPE[nCol];

	initData();

}

SparseRevisedFormProblem * SparseRevisedFormProblem::clone(){
	SparseRevisedFormProblem* clone = new SparseRevisedFormProblem(nRow, nVar, nSlack, nNoNull);

	memcpy(clone->sparseA, sparseA, nNoNull*sizeof(VAR_TYPE));
	memcpy(clone->B, B, nRow*nBasic*sizeof(VAR_TYPE));

	memcpy(clone->rowIndA, rowIndA, nNoNull*sizeof(int));
	memcpy(clone->colIndA, colIndA, nNoNull*sizeof(int));

	memcpy(clone->lhs, lhs, nRow*sizeof(VAR_TYPE));

	memcpy(clone->bBounds, bBounds, nBasic*sizeof(float2));
	memcpy(clone->bBoundsP1, bBoundsP1, nBasic*sizeof(float2));

	memcpy(clone->bounds, bounds, nCol*sizeof(float2));
	memcpy(clone->boundsP1, boundsP1, nCol*sizeof(float2));

	memcpy(clone->bVal, bVal, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->val, val, nCol*sizeof(VAR_TYPE));

	memcpy(clone->bInd, bInd, nBasic*sizeof(int));

	memcpy(clone->Cb, Cb, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->CbP1, CbP1, nBasic*sizeof(VAR_TYPE));
	memcpy(clone->C, C, nCol*sizeof(VAR_TYPE));
	memcpy(clone->CP1, CP1, nCol*sizeof(VAR_TYPE));

	return clone;
}


void SparseRevisedFormProblem::initData(){

	for(unsigned int i=0; i<nCol; i++){
		C[i] 				= 0.0;
		CP1[i] 				= 0.0;
		boundsType[i] 		= LOWER_BOUND;
		val[i] 				= 0.0;
		bounds[i]			= make_float2(0.0, FLT_MAX);
		boundsP1[i] 		= bounds[i];
	}

	for(unsigned int i=0; i<nRow; i++){
		Cb[i] 				= 0.0;
		CbP1[i]				= 0.0;
		bVal[i] 			= 0.0;
		lhs[i]				= 0.0;
		eqsType[i]			= 'L';
		bounds[i+nVar]		= make_float2(0.0, FLT_MAX);
		boundsP1[i+nVar]	= bounds[i+nVar];
		bBounds[i]			= bounds[i+nVar];
		bBoundsP1[i]		= bounds[i+nVar];
	}

	for(unsigned int i=0; i<nBasic; i++){
		for(unsigned int j=0; j<nBasic; j++){
			if (i!=j){
				B[IDX2C(i,j,nBasic)] = 0.0;
			} else {
				B[IDX2C(i,j,nBasic)] = 1.0;
			}
		}
	}


}


void SparseRevisedFormProblem::print(){

	printf("********************************************\n");
	printf("REVISED FORM PROBLEM\n");
	printf("Nb var : %d || Nb row : %d || Nb coeff no null : %d\n", nVar, nRow, nNoNull);


	printf(" A - RHS\n");
	printEquations();
	printf("\n");
	printf(" B \n");
	printBasis();

}


void SparseRevisedFormProblem::printEquations(){

	int tmp = -1;
	int tmpCnt = 0;

	for(unsigned int i=0; i<nNoNull; i++){

		if(tmp != colIndA[tmpCnt]){
			printf("\n Col %d \n", colIndA[tmpCnt]);
			tmp = colIndA[tmpCnt];
		}

		printf("%d:%4.3f  \t", rowIndA[tmpCnt] ,sparseA[tmpCnt]);

		tmpCnt++;
	}

	printf("\n\n");

	printf("RHS \t");
	for(unsigned int i=0; i<nRow; i++) {
		printf("%d:%4.3f \t", i, lhs[i]);
	}

	printf("\n\n");

	for(unsigned int i=0; i<nCol; i++){
		if(i<nVar)
			printf("[x%d]  \t\t", i);
		else
			printf("[b%d]  \t\t", i);
	}

	printf("Obj func \n");

	for(unsigned int j=0; j<nCol; j++){
		printf("[%d]%4.3f\t", j, C[j]);
	}
	printf("\n");

	printf("Aux obj func \n");

	for(unsigned int j=0; j<nNonBasic+nBasic; j++){
		printf("[%d]%4.3f\t", j, CP1[j]);
	}
	printf("\n");

	printf("Vals \n");

	for(unsigned int j=0; j<nCol; j++) {
		if(bounds[j].x == -FLT_MAX)
			printf("[%d]  -inf   ", j);
		else
			printf("[%d]%4.3f", j, bounds[j].x);

		if(bounds[j].x != boundsP1[j].x){
			if(boundsP1[j].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)",boundsP1[j].x);
		}
		printf("\t");
	}
	printf("\n");

	for(unsigned int j=0; j<nCol; j++) {
		printf("[%d]%4.3f\t", j, val[j]);
	}
	printf("\n");

	for(unsigned int j=0; j<nCol; j++) {
		if(bounds[j].y == FLT_MAX)
			printf("[%d]   inf  ", j);
		else
			printf("[%d]%4.3f", j, bounds[j].y);

		if(bounds[j].y != boundsP1[j].y){
			if(boundsP1[j].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)",boundsP1[j].y);
		}
		printf("\t");
	}
	printf("\n");

}



void SparseRevisedFormProblem::printBasis(){

	for(unsigned int i=0; i<nBasic; i++){

		// Variable index
		if(bInd[i] < nVar){
			printf("[x%d] \t", bInd[i]);
		} else {
			printf("[b%d] \t", bInd[i]);
		}

		// Row values
		for(unsigned int j=0; j<nBasic; j++){
			printf("[%d][%d]%4.3f\t", i, j, B[IDX2C(i,j,nRow)]);
		}

		// Lower bound
		if(bounds[i].x == -FLT_MAX)
			printf("-inf ");
		else
			printf("%4.3f ", bounds[i].x);

		if(bounds[i].x != boundsP1[i].x){
			if(boundsP1[i].x == -FLT_MAX)
				printf("(-inf)");
			else
				printf("(%4.3f)",boundsP1[i].x);
		}
		printf("\t");

		// Value
		printf("%4.3f\t", val[i]);

		// Upper bound
		if(bounds[i].y == FLT_MAX)
			printf("inf ");
		else
			printf("%4.3f ", bounds[i].y);

		if(bounds[i].y != boundsP1[i].y){
			if(boundsP1[i].y == FLT_MAX)
				printf("(inf)");
			else
				printf("(%4.3f)", boundsP1[i].y);
		}
		printf("\n");
	}

}

