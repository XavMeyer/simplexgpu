//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * types.h
 *
 *  Created on: May 16, 2011
 *      Author: meyerx
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <float.h>
#include "Config.h"
#include <pthread.h>


#include <cutil.h>
#include <cutil_math.h>
//#include <helper_cuda.h>

// Don't touch that, except you want to add another type.
#if TYPE == USE_FLOAT
	#define VAR_TYPE float
	#define MAX_TYPE FLT_MAX
	#define MIN_TYPE FLT_MIN
	#define EPS_TYPE FLT_EPSILON
#elif TYPE == USE_DOUBLE
	#define VAR_TYPE double
	#define MAX_TYPE DBL_MAX
	#define MIN_TYPE DBL_MIN
	#define EPS_TYPE DBL_EPSILON
#endif


typedef enum { SUCCESS,
			   UNBOUNDED,
			   NO_SOLUTION,
			   SKIPPED_PHASE2,
			   UNBOUNDED_AUX,
			   INFEASIBLE,
			   MAX_ITER_REACHED,
			   GOTO_PHASE1,
			   ERROR}
solver_res_t;


typedef struct red_result {
	int 	index;
	VAR_TYPE 	value;
} red_result_t;

typedef struct exp_result {
	int				index;
	VAR_TYPE 		absP;
	VAR_TYPE		alpha;
} exp_result_t;

typedef struct row_result{
	int				index;
	VAR_TYPE 		absP;
	VAR_TYPE		alpha;
	char			mode;
} row_result_t;

/*
typedef red_result red_result_t;
typedef exp_result exp_result_t;
typedef row_result row_result_t;
*/

typedef struct {
	int 				nbGPU;
	red_result_t 		*exSE; 			// red_result_t[nbThreads]
	row_result_t 		exRow;			// row_result_t
	VAR_TYPE 			*exColumn; 		// VAR_TYPE[p->nCol]
	VAR_TYPE			*exBVal;
	VAR_TYPE			*exLocOpti;		// VAR_TYPE[nbThreads]
	VAR_TYPE			*exLocOptiAux;
	int					*exInfX;		// int[nbThreads]
	int					*exInfB;
	int					*exNTriv;
	VAR_TYPE			exPivot;
	VAR_TYPE 			exObjCoeff;
	VAR_TYPE 			exObjAuxCoeff;
	VAR_TYPE 			xVal;
	VAR_TYPE			xInd;
	float2				xBound;
	float2				xBoundP1;
	pthread_barrier_t	barrier;

} multiGPU_t;


/* MAX / MIN etc.. for red_result_t */
#define MAX_R(x,y) 			((x.value > y.value) ? x : y)
#define MAX_ABS_R(x,y)		(fabs(x.value) > fabs(y.value) ? x : y)
//#define MAXPRIO_R(x,y)		((x.value > y.value) ? x : (ESS_EQUAL(x.value,y.value) ? PRIO_INDEX_R(x,y) : y))
#define MIN_R(x,y) 			((x.value < y.value) ? x : y)
#define MINPRIO_R(x,y) 		((x.value < y.value) ? x : y)
//#define MINPRIO_R(x,y)		((x.value < y.value) ? x : (ESS_EQUAL(x.value,y.value) ? PRIO_INDEX_R(x,y) : y))
#define PRIO_INDEX_R(x,y)	((x.index < y.index) ? x : y)
#define SET(x,y) 			{x.index = y.index; x.value = y.value;}

#define SET_E(x,y)				SET_E_P(x,y)
#define MAX_E_P(x,y)			((x.absP > y.absP) ? x : y)
#define MAX_E_A(x,y)			((x.alpha > y.alpha) ? x : y)
#define MIN_E_A(x,y)			((x.alpha < y.alpha) ? x : y)
#define SET_E_P(x,y)			{x.index = y.index; x.alpha = y.alpha; x.absP = y.absP;}
#define SET_E_A(x,y)			{x.index = y.index; x.absP = y.absP; x.alpha = y.alpha;}
#define SET_MAX(x,y)			{if(y.absP > x.absP) SET_E(x, y);}
#define SET_R(x,y)				{x.index = y.index; x.alpha = y.alpha; x.absP = y.absP;; x.mode = y.mode;}

#define M_BASIC_TO_BOUND		0
#define M_PIVOT					1
#define M_INF_TO_BOUND			2
#define M_INF_PIVOT				3
#define M_INFEASIBLE			4
#define M_UNBOUNDED				5


#endif /* TYPES_H_ */
