//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BasisUpdaterCPU.cpp
 *
 *  Created on: Mar 5, 2012
 *      Author: meyerx
 */

#include "BasisUpdaterCPU.h"

BasisUpdaterCPU::BasisUpdaterCPU(unsigned int inNTask, SparseRevisedProblem *p){

	nTask = inNTask;
	m = p->nBasic;

	// Create initial B
	B = new SparseMemorySpace(p->sparseB);
	/*memcpy(B->data, p->sparseB, B->size()*sizeof(*B->data));
	memcpy(B->rowIdx, p->rowIndB, B->size()*sizeof(*B->rowIdx));
	memcpy(B->colIdx, p->colIndB, B->size()*sizeof(*B->colIdx));*/

	mm = new MemoryManager(nTask, B->size());

	if(nTask > 1){
		pmu = new ParallelMatrixUpdater(nTask, mm);
		mu = NULL;
	} else {
		mu = new MatrixUpdater(mm);
		pmu = NULL;
	}


}

BasisUpdaterCPU::~BasisUpdaterCPU() {

	delete B;
	delete mm;

	if(nTask > 1){
		delete pmu;
	} else {
		delete mu;
	}
}


SparseMemorySpace * BasisUpdaterCPU::monoUpdate(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d){

	SparseMemorySpace *r;
	unsigned int *rowPtr;

	r = B->getRowCopy(idxRow);
	rowPtr = B->getRowPtr(m);
	B->eraseRow(m, idxRow, rowPtr);

	mu->setDataInput(m, m, alpha, d, r, rowPtr, B);

	mu->updateMatrix();

	delete r;
	delete B;
	delete [] rowPtr;

	B = mm->merge();
	mm->reset();

	return B;
}

SparseMemorySpace * BasisUpdaterCPU::multiUpdate(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d){

	SparseMemorySpace *r;
	unsigned int *rowPtr;

	r = B->getRowCopy(idxRow);
	rowPtr = B->getRowPtr(m);
	B->eraseRow(m, idxRow, rowPtr);

	pmu->setDataInput(m, m, alpha, d, r, rowPtr, B);

	pmu->updateMatrix();

	delete r;
	delete B;

	B = mm->merge();
	mm->reset();

	delete pmu;
	delete mm;
	delete [] rowPtr;

	return B;
}




SparseMemorySpace * BasisUpdaterCPU::updateBasis(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d){

	if(nTask > 1){
		return this->multiUpdate(alpha, idxRow, d);
	} else {
		return this->monoUpdate(alpha, idxRow, d);
	}

}





