//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemorySpace.cpp
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#include "SparseMemorySpace.h"

SparseMemorySpace::SparseMemorySpace(unsigned int requiredSize) {

	currentSize = 0;
	maxSize = 4*ceil((float)requiredSize/4);

	this->createStructure();
}


SparseMemorySpace::SparseMemorySpace(SparseMemorySpace *sms){
	currentSize = 0;
	maxSize = sms->maxSize;

	this->createStructure();
	this->append(sms);
}
/*
SparseMemorySpace::SparseMemorySpace(SparseMemorySpaceGPU *smsGPU){
	currentSize = smsGPU->size();
	maxSize = smsGPU->maxSize;

	this->createStructure();
	this->copyFromGPU(smsGPU);

}*/

SparseMemorySpace::~SparseMemorySpace() {

	delete [] data;
	delete [] colIdx;
	delete [] rowIdx;

}


void SparseMemorySpace::createStructure(){
	data = new VAR_TYPE[maxSize];
	colIdx = new unsigned int[maxSize];
	rowIdx = new unsigned int[maxSize];
}


/*
void SparseMemorySpace::copyFromGPU(SparseMemorySpaceGPU *smsGPU){
	cutilSafeCallNoSync( cudaMemcpy(this->data, smsGPU->data, this->size()*sizeof(*this->data), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(this->rowIdx, smsGPU->rowIdx, this->size()*sizeof(*this->rowIdx), cudaMemcpyDeviceToHost) );
	cutilSafeCallNoSync( cudaMemcpy(this->colIdx, smsGPU->colIdx, this->size()*sizeof(*this->colIdx), cudaMemcpyDeviceToHost) );
}*/


bool SparseMemorySpace::insertData(VAR_TYPE inData, unsigned int inColIdx, unsigned int inRowIdx){

	if(currentSize < maxSize){

		data[currentSize] = inData;
		colIdx[currentSize] = inColIdx;
		rowIdx[currentSize] = inRowIdx;

		currentSize++;

		return true;
	} else {
		return false;
	}
}

bool SparseMemorySpace::isEmpty(){
	return currentSize == 0;
}


bool SparseMemorySpace::isFull(){
	return currentSize >= maxSize;
}

void SparseMemorySpace::clear(){
	currentSize = 0;
}

unsigned int SparseMemorySpace::size(){
	return currentSize;
}

/*
void SparseMemorySpace::copyFromAt(SparseMemorySpace *sms, unsigned int offset){
	memcpy(&this->data[offset], sms->data, sms->size()*sizeof(VAR_TYPE));
	memcpy(&this->colIdx[offset], sms->colIdx, sms->size()*sizeof(unsigned int));
	memcpy(&this->rowIdx[offset], sms->rowIdx, sms->size()*sizeof(unsigned int));
}
*/

bool SparseMemorySpace::append(SparseMemorySpace *sms){

	// Check if there is enough space to append sms
	if((sms->size() + this->currentSize) > this->maxSize){
		return false;
	}

	memcpy(&data[currentSize], sms->data, sms->size()*sizeof(VAR_TYPE));
	memcpy(&colIdx[currentSize], sms->colIdx, sms->size()*sizeof(unsigned int));
	memcpy(&rowIdx[currentSize], sms->rowIdx, sms->size()*sizeof(unsigned int));

	currentSize += sms->size();

	return true;
}

SparseMemorySpace * SparseMemorySpace::getRowCopy(unsigned int inRowIdx){

	bool startFound, endFound;
	unsigned int start, size, pos;

	pos = 0;
	startFound = false;
	endFound = false;

	// Search for the first element of row "inRowIdx"
	while(pos < currentSize && !startFound){
		if(rowIdx[pos] == inRowIdx){
			startFound = true;
			start = pos;
		}
		pos++;
	}

	// Search for the size of row "inRowIdx"
	while(pos < currentSize && !endFound){
		if(rowIdx[pos] != inRowIdx){
			endFound = true;
			size = pos - start;
		}
		pos++;
	}

	// If we have found the start, but not the end then
	if(startFound && !endFound){
		// size equals matrix size - start
		size = currentSize - start;
	}

	SparseMemorySpace * sms = new SparseMemorySpace(size);
	memcpy(sms->data, &data[start], size * sizeof(VAR_TYPE));
	memcpy(sms->colIdx, &colIdx[start], size * sizeof(unsigned int));
	memcpy(sms->rowIdx, &rowIdx[start], size * sizeof(unsigned int));
	sms->currentSize = size;

	return sms;

}

unsigned int * SparseMemorySpace::getRowPtr(unsigned int m){

	unsigned int pos = 0;
	unsigned int *rowPtr = new unsigned int[m+1];

	rowPtr[0] = 0;

	for(unsigned int i=0; i<this->size(); i++){

		while(this->rowIdx[i] > pos){
			pos++;
			rowPtr[pos] = i;

		}
	}
	rowPtr[m] = this->size();

	return rowPtr;
}

// Should be implemented for a SparseMatrix class and not the genric SparseMemorySpace
void SparseMemorySpace::eraseRow(unsigned int m, unsigned int inRowIdx, unsigned int *inOutRowPtr){

	unsigned int start, end, offset, nbToMove;

	start = inOutRowPtr[inRowIdx];
	end = inOutRowPtr[inRowIdx+1];
	offset = end - start;
	nbToMove = size()-end;

	// change size
	currentSize -= offset;

	// Copy data
	for(unsigned int i=0; i<nbToMove; i++){
		data[i+start] = data[i+end];
		rowIdx[i+start] = rowIdx[i+end];
		colIdx[i+start] = colIdx[i+end];
	}

	// change inOutRowPtr
	// Change the ptr to deleted row
	if(inRowIdx > 0){
		inOutRowPtr[inRowIdx+1] = inOutRowPtr[inRowIdx];
	} else {
		inOutRowPtr[inRowIdx+1] = 0;
	}

	// Shift all the following pointer of the offset
	for(unsigned int i=inRowIdx+2; i<m+1; i++){
		inOutRowPtr[i] -= offset;
	}

}



bool SparseMemorySpace::compare(SparseMemorySpace *sms) {

	if(this->size() != sms->size())
		return false;

	for(unsigned int i=0; i< this->size(); i++){
		if(this->data[i] != sms->data[i])
			return false;

		if(this->colIdx[i] != sms->colIdx[i])
			return false;

		if(this->rowIdx[i] != sms->rowIdx[i])
			return false;
	}

	return true;
}

void SparseMemorySpace::print(){

	printf("Sparse Matrix\n");
	this->printData();
}

void SparseMemorySpace::print(string name){

	printf("Sparse Matrix : %s\n", name.c_str());
	this->printData();
}


void SparseMemorySpace::printData(){
	printf("Size : %d\n", currentSize);
	printf("ColIdx \t\t rowIdx \t\t Value \n");

	for(unsigned int i=0; i<this->size(); i++){
		printf("%d \t\t %d \t\t %f \n", colIdx[i], rowIdx[i], data[i]);
	}
}

