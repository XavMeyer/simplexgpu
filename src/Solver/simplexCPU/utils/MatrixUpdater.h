//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * matrixUpdater.h
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#ifndef MATRIXUPDATER_H_
#define MATRIXUPDATER_H_

#include "SparseMemorySpace.h"
#include "MemoryManager.h"

class MatrixUpdater {
public:
	MatrixUpdater(MemoryManager *inMemMgr);
	MatrixUpdater(int inTaskId, MemoryManager *inMemMgr);

	void setDataInput(	unsigned int m, unsigned int n, VAR_TYPE inAlpha,
						VAR_TYPE *inD, SparseMemorySpace *inR,
						unsigned int *inRowPtrA, SparseMemorySpace *inA);

	void setDataInput(	VAR_TYPE inAlpha,
						unsigned int inNRowToProcess, unsigned int inOffsetD, VAR_TYPE *inD,
						SparseMemorySpace *inR,
						unsigned int *inRowPtrA, SparseMemorySpace *inA);

	virtual ~MatrixUpdater();

	void updateMatrix();


	int taskId;

private :

	MemoryManager *memMgr;

	VAR_TYPE alpha;

	// Col d related
	unsigned int nRowToProcess;
	VAR_TYPE *d;
	unsigned int offsetD;

	// row r related
	SparseMemorySpace *r;

	// Matrix A related
	unsigned int *rowPtrA;
	SparseMemorySpace *A;


	void init();

	void updateMatrixV1();
	void updateMatrixV2();

};

#endif /* MATRIXUPDATER_H_ */
