//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemorySpace.h
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#ifndef SPARSEMEMORYSPACE_H_
#define SPARSEMEMORYSPACE_H_

#include "../../Types.h"

#include <cutil_inline.h>

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>
#include <string>
using namespace std;


class SparseMemorySpace {
public:

	// Variables
	unsigned int currentSize;
	unsigned int maxSize;

	VAR_TYPE *data;
	unsigned int *colIdx, *rowIdx;

	// Constructors
	SparseMemorySpace(unsigned int requiredSize);
	SparseMemorySpace(SparseMemorySpace *sms);
	//SparseMemorySpace(SparseMemorySpaceGPU *smsGPU);
	virtual ~SparseMemorySpace();

	// Functions
	// Safe data insert
	bool insertData(VAR_TYPE inData, unsigned int inColIdx, unsigned int inRowIdx);

	// Get size
	unsigned int size();

	// Check if full or empty
	bool isEmpty();
	bool isFull();

	// Clear the content
	void clear();

	// Copy from another SparseMemorySpace at the given offset
	bool append(SparseMemorySpace *sms);
	//void copyFromAt(SparseMemorySpace *sms, unsigned int offset);

	// Copy the given row into a SparseMemorySpace
	SparseMemorySpace * getRowCopy(unsigned int inRowIdx);

	// Return the rowptr (CSR format)
	unsigned int * getRowPtr(unsigned int m);

	void eraseRow(unsigned int m, unsigned int inRowIdx, unsigned int *inOutrowPtr);

	// Compare 2 SparseMemorySpace. Return true if the content is the same, 0 else
	bool compare(SparseMemorySpace *sms);


	void print();
	void print(string name);
	void printData();

private :

	void createStructure();
	//void copyFromGPU(SparseMemorySpaceGPU *smsGPU);



};

#endif /* SPARSEMEMORYSPACE_H_ */
