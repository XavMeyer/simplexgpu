//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParallelMatrixUpdater.h
 *
 *  Created on: Feb 29, 2012
 *      Author: meyerx
 */

#ifndef PARALLELMATRIXUPDATER_H_
#define PARALLELMATRIXUPDATER_H_

#include "SparseMemorySpace.h"
#include "MemoryManager.h"
#include "MatrixUpdater.h"

#include <math.h>
#include <pthread.h>

class ParallelMatrixUpdater {
public:
	ParallelMatrixUpdater(int inNTask, MemoryManager *iNmemMgr);
	virtual ~ParallelMatrixUpdater();

	void setDataInput(	unsigned int m, unsigned int n, VAR_TYPE inAlpha,
						VAR_TYPE *inD, SparseMemorySpace *inR,
						unsigned int *inRowPtrA, SparseMemorySpace *inA);


	void updateMatrix();

private :

	int nTask;
	unsigned int m, n;
	VAR_TYPE alpha;
	VAR_TYPE *d;

	SparseMemorySpace *r;

	unsigned int *rowPtrA;
	SparseMemorySpace *A;
	MemoryManager *memMgr;

	typedef MatrixUpdater* MatrixUpdater_ptr;
	MatrixUpdater_ptr *muPtrs;

	//pthread_t *threads;

	void init();

	static void* launchUpdate(void* mu);

};

#endif /* PARALLELMATRIXUPDATER_H_ */
