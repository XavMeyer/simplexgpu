//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BasisUpdaterCPU.h
 *
 *  Created on: Mar 5, 2012
 *      Author: meyerx
 */

#ifndef BASISUPDATERCPU_H_
#define BASISUPDATERCPU_H_

#include "MemoryManager.h"
#include "MatrixUpdater.h"
#include "ParallelMatrixUpdater.h"
#include "SparseMemorySpace.h"

#include "../../SparseRevisedProblem.h"

class BasisUpdaterCPU {
public:
	BasisUpdaterCPU(unsigned int inNTask, SparseRevisedProblem *p);
	virtual ~BasisUpdaterCPU();

	SparseMemorySpace * updateBasis(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d);

private:

	unsigned int nTask;
	unsigned int m;

	MemoryManager *mm;
	MatrixUpdater *mu;
	ParallelMatrixUpdater *pmu;
	SparseMemorySpace *B;

	SparseMemorySpace * monoUpdate(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d);
	SparseMemorySpace * multiUpdate(VAR_TYPE alpha, unsigned int idxRow, VAR_TYPE *d);


};

#endif /* BASISUPDATERCPU_H_ */
