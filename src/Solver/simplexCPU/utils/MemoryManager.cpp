//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MemoryManager.cpp
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#include "MemoryManager.h"

MemoryManager::MemoryManager(unsigned int inNTasks, unsigned int expectedSize) {

	unsigned int sizePerTask = ceil((float)expectedSize / inNTasks);
	unsigned int nbBank;

	nTask = inNTasks;

	// Init tasks pools
	for(unsigned int i=0; i<nTask; i++){
		tasksPools.push_back(new taskPool_t());
	}

	// init the memory pool with :
	if(sizePerTask > MAX_MEMORY_SPACE_SIZE) {
		bankSize = MAX_MEMORY_SPACE_SIZE;
		nbBank = nTask*ceil((float)sizePerTask/MAX_MEMORY_SPACE_SIZE);
	} else {
		bankSize = sizePerTask;
		nbBank = nTask;
	}

	// nbBank of size bankSize
	for(unsigned int i=0; i < nbBank; i++){
		pool.push_back(new SparseMemorySpace(bankSize));
	}

	pthread_mutex_init(&mutex, NULL);
}

MemoryManager::~MemoryManager() {

	SparseMemorySpace *ptrSms;
	taskPool_t *ptrTP;

	this->reset();

	while(!pool.empty()){
		ptrSms = pool.back();
		pool.pop_back();
		delete ptrSms;
	}

	while(!tasksPools.empty()){
		ptrTP = tasksPools.back();
		tasksPools.pop_back();
		delete ptrTP;
	}

	pthread_mutex_destroy(&mutex);

}

SparseMemorySpace* MemoryManager::getEmptySpace(){
	SparseMemorySpace *ptr;

	if(nTask > 1){
		pthread_mutex_lock (&mutex);
	}

	// check if there is still free space in the pool
	if(pool.empty()){ // no
		ptr = new SparseMemorySpace(bankSize);
	} else { // yes
		ptr = pool.back();
		pool.pop_back();
	}

	if(nTask > 1){
		pthread_mutex_unlock (&mutex);
	}

	return ptr;
}


void MemoryManager::insertData(int taskId, VAR_TYPE inData, unsigned int inColIdx, unsigned int inRowIdx){


	// check if there is still space into the last bank
	if(tasksPools[taskId]->empty()){
		tasksPools[taskId]->push_back(this->getEmptySpace());
	} else if(tasksPools[taskId]->back()->isFull()){
		tasksPools[taskId]->push_back(this->getEmptySpace());
	}

	tasksPools[taskId]->back()->insertData(inData, inColIdx, inRowIdx);


}

void MemoryManager::reset(){

	SparseMemorySpace *ptr;
	tasksPools_iter_t it1;

	// remove each memoryBank from tasks pool and push them back into the main pool
	for(it1 = tasksPools.begin(); it1 < tasksPools.end(); it1++){
		while(!(*it1)->empty()){
			ptr = (*it1)->back();
			(*it1)->pop_back();
			ptr->clear();
			pool.push_back(ptr);
		}
	}
}

unsigned int MemoryManager::getNbSparseData(){

	tasksPools_iter_t it1;
	taskPool_iter_t it2;
	unsigned int nbSparseData = 0;

	// remove each memoryBank from tasks pool and push them back into the main pool
	for(it1 = tasksPools.begin(); it1 < tasksPools.end(); it1++){
		for(it2 = (*it1)->begin(); it2 < (*it1)->end(); it2++){
			nbSparseData += (*it2)->size();
		}
	}

	return nbSparseData;
}

SparseMemorySpace* MemoryManager::merge(){

	unsigned int nbSparseData = getNbSparseData();
	tasksPools_iter_t it1;
	taskPool_iter_t it2;

	SparseMemorySpace * sms = new SparseMemorySpace(nbSparseData);

	for(it1 = tasksPools.begin(); it1 < tasksPools.end(); it1++){
		for(it2 = (*it1)->begin(); it2 < (*it1)->end(); it2++){
			sms->append((*it2));
		}
	}

	assert(sms->currentSize == nbSparseData);

	return sms;
}


