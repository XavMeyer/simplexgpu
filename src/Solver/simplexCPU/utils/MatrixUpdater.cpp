//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * matrixUpdater.cpp
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#include "MatrixUpdater.h"

MatrixUpdater::MatrixUpdater(MemoryManager *inMemMgr) {

	taskId = 0;
	memMgr = inMemMgr;
	init();
}

MatrixUpdater::MatrixUpdater(int inTaskId, MemoryManager *inMemMgr) {

	taskId = inTaskId;
	memMgr = inMemMgr;
	init();
}

void MatrixUpdater::init(){

	alpha = 0.0;
	nRowToProcess = 0;
	offsetD = 0;
	d = NULL;
	r = NULL;
	A = NULL;
	rowPtrA = NULL;
}

MatrixUpdater::~MatrixUpdater() {

	r = NULL;
	A = NULL;
	memMgr = NULL;
	rowPtrA = NULL;

	//pthread_exit(NULL);

}

void MatrixUpdater::setDataInput(	unsigned int m, unsigned int n, VAR_TYPE inAlpha,
									VAR_TYPE *inD, SparseMemorySpace *inR,
									unsigned int *inRowPtrA, SparseMemorySpace *inA){

	alpha = inAlpha;
	nRowToProcess = n;
	offsetD = 0;
	d = inD;
	r = inR;
	rowPtrA = inRowPtrA;
	A = inA;

}


void MatrixUpdater::setDataInput(	VAR_TYPE inAlpha,
									unsigned int inNRowToProcess, unsigned int inOffsetD, VAR_TYPE *inD,
									SparseMemorySpace *inR,
									unsigned int *inRowPtrA, SparseMemorySpace *inA){

	alpha = inAlpha;
	nRowToProcess = inNRowToProcess;
	offsetD = inOffsetD;
	d = inD;
	r = inR;
	rowPtrA = inRowPtrA;
	A = inA;

}


void MatrixUpdater::updateMatrix(){

	//updateMatrixV1();
	updateMatrixV2();

}


void MatrixUpdater::updateMatrixV1(){

	SparseMemorySpace locR(r);

	unsigned int posD = offsetD;
	unsigned int posA = rowPtrA[posD];
	unsigned int posR = 0;
	VAR_TYPE tmp, tmpD;

	for(posD=offsetD; posD<(offsetD+nRowToProcess); posD++){
		// We copy row from matrix A if d[posD] == 0
		if(fabs(d[posD]) < 1e-20){ // TODO maybe add a tolerance, like 10^-12 ?
			while(posA < A->size() && A->rowIdx[posA] == posD){
				memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
				posA++;
			}
		// Else we have to check the "collisions" or "additions" with / from d*r
		} else {
			// Reinit row counter
			posR = 0;

			// Pre-process d[posD] * Alpha
			tmpD = d[posD] * alpha;

			// While there remains data in matrix A && we are still in row posD
			// && we are still there remains data in posR
			while((posA < A->size() && A->rowIdx[posA] == posD) && posR < locR.size()){
				if(locR.colIdx[posR] == A->colIdx[posA]) { // Same x,y process r*d + A
					tmp = A->data[posA] + tmpD * locR.data[posR];
					memMgr->insertData(taskId, tmp, A->colIdx[posA], A->rowIdx[posA]);
					posR++; posA++;
				} else if(locR.colIdx[posR] <  A->colIdx[posA]) { // r is before A, process r*d
					tmp = tmpD * locR.data[posR];
					memMgr->insertData(taskId, tmp, locR.colIdx[posR], posD);
					posR++;
				} else { // A is before r, copy A
					memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
					posA++;
				}
			}

			// We now have to check if there remains data for this row in r or A
			// If there remains data in A at row posD
			while(posA < A->size() && A->rowIdx[posA] == posD){
				memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
				posA++;
			}
			// If there remains data in r
			while(posR < locR.size()) {
				tmp = tmpD * locR.data[posR];
				memMgr->insertData(taskId, tmp, locR.colIdx[posR], posD);
				posR++;
			}
		}
	}
}


void MatrixUpdater::updateMatrixV2(){

	SparseMemorySpace locR(r);

	unsigned int posD = offsetD;
	unsigned int posA = rowPtrA[posD];
	unsigned int posR = 0;
	VAR_TYPE tmp, tmpD;

	/*cout << "Variable pivot : " << alpha << endl;

	cout << "Col (D)" << endl;
	for(unsigned int j=0; j<nRowToProcess; j++){
		if(fabs(d[j]) > 1e-20){
			cout << j << "\t\t" << d[j] << endl;
		}
	}

	cout << endl;

	locR.print("R");
	//A->print("A");*/

	for(posD=offsetD; posD<(offsetD+nRowToProcess); posD++){

		//cout << posD << " : " << rowPtrA[posD+1] << endl;

		// We copy row from matrix A if d[posD] == 0
		if(d[posD] == 0){ // TODO maybe add a tolerance, like 10^-12 ?
			while(posA < rowPtrA[posD+1]){
				memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
				posA++;
			}
		// Else we have to check the "collisions" or "additions" with / from d*r
		} else {
			// Reinit row counter
			posR = 0;
			// Process the end of row A in matrix A

			// Pre-process d[posD] * Alpha
			tmpD = d[posD] * alpha;

			// While there remains data in matrix A && we are still in row posD
			// && we are still there remains data in posR
			while(posA < rowPtrA[posD+1] && posR < locR.size()){
				if(locR.colIdx[posR] == A->colIdx[posA]) { // Same x,y process r*d + A
					tmp = A->data[posA] + tmpD * locR.data[posR];
					memMgr->insertData(taskId, tmp, A->colIdx[posA], A->rowIdx[posA]);
					posR++; posA++;
				} else if(locR.colIdx[posR] <  A->colIdx[posA]) { // r is before A, process r*d
					tmp = tmpD * locR.data[posR];
					memMgr->insertData(taskId, tmp, locR.colIdx[posR], posD);
					posR++;
				} else { // A is before r, copy A
					memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
					posA++;
				}
			}

			// We now have to check if there remains data for this row in r or A
			// If there remains data in A at row posD
			while(posA < rowPtrA[posD+1]){
				memMgr->insertData(taskId, A->data[posA], A->colIdx[posA], A->rowIdx[posA]);
				posA++;
			}
			// If there remains data in r
			while(posR < locR.size()) {
				tmp = tmpD * locR.data[posR];
				memMgr->insertData(taskId, tmp, locR.colIdx[posR], posD);
				posR++;
			}
		}
	}
}
