//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MemoryManager.h
 *
 *  Created on: Feb 28, 2012
 *      Author: meyerx
 */

#ifndef MEMORYMANAGER_H_
#define MEMORYMANAGER_H_

#include "SparseMemorySpace.h"
#include <math.h>
#include <vector>
#include <assert.h>

#define MAX_MEMORY_SPACE_SIZE			256

using namespace std;

class MemoryManager {
public:
	MemoryManager(unsigned int inNTasks, unsigned int expectedSize);
	virtual ~MemoryManager();

	void insertData(int taskId, VAR_TYPE inData, unsigned int inColIdx, unsigned int inRowIdx);

	void reset();

	unsigned int getNbSparseData();

	SparseMemorySpace* merge();

private:

	unsigned int nTask;
	unsigned int bankSize;

	// This pool contains free memory bank
	typedef vector<SparseMemorySpace*> pool_t;
	typedef pool_t::iterator pool_iter_t;
	pool_t pool;

	// Each task has a pool in which its banks are stored
	typedef pool_t taskPool_t;
	typedef pool_iter_t taskPool_iter_t;
	typedef vector<taskPool_t*> tasksPools_t;
	typedef tasksPools_t::iterator tasksPools_iter_t;
	tasksPools_t tasksPools;

	pthread_mutex_t mutex;

	SparseMemorySpace* getEmptySpace();


};

#endif /* MEMORYMANAGER_H_ */
