//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParallelMatrixUpdater.cpp
 *
 *  Created on: Feb 29, 2012
 *      Author: meyerx
 */

#include "ParallelMatrixUpdater.h"

ParallelMatrixUpdater::ParallelMatrixUpdater(int inNTask, MemoryManager *inMemMgr) {
	nTask = inNTask;
	memMgr = inMemMgr;

	// init the array of Matrix Updater pointers
	muPtrs = new MatrixUpdater_ptr[nTask];
	for(int tid=0; tid<nTask;tid++){
		muPtrs[tid] = new MatrixUpdater(tid, memMgr);
	}

	// Create the array of threads
	//threads = new pthread_t[nTask];

	init();

}

ParallelMatrixUpdater::~ParallelMatrixUpdater() {

	for(int tid=0; tid<nTask;tid++){
		delete muPtrs[tid];
	}

	delete [] muPtrs;

	//delete [] threads;

}


void ParallelMatrixUpdater::init(){

	this->A = NULL;
	this->r = NULL;
	this->d = NULL;

}

/*void ParallelMatrixUpdater::setDataInput(	unsigned int m, unsigned int n, VAR_TYPE inAlpha,
											VAR_TYPE *inD, SparseMemorySpace *inR, SparseMemorySpace *inA){

	this->m = m;
 	this->A = inA;
	this->n = n;

	this->alpha = inAlpha;

	this->d = inD;
	this->r = inR;



}*/

void ParallelMatrixUpdater::setDataInput(	unsigned int m, unsigned int n, VAR_TYPE inAlpha,
										VAR_TYPE *inD, SparseMemorySpace *inR,
										unsigned int *inRowPtrA, SparseMemorySpace *inA) {

	this->m = m;
	this->rowPtrA = inRowPtrA;
 	this->A = inA;
	this->n = n;

	this->alpha = inAlpha;

	this->d = inD;
	this->r = inR;

}

void* ParallelMatrixUpdater::launchUpdate(void* mu){

	MatrixUpdater *localMu = (MatrixUpdater*) mu;
	localMu->updateMatrix();
	localMu = NULL;
	pthread_exit(NULL);
}


void ParallelMatrixUpdater::updateMatrix() {


	void *status;
	unsigned int offsetD = 0;
	unsigned int nRowToProcess = ceil((float)m/nTask);
	unsigned int nRowForLast = m - nRowToProcess*(nTask-1);
	pthread_t threads[nTask];


	// Init MatrixUpdaters data
	for(int tid=0; tid<nTask; tid++){

		if(tid < (nTask-1)){
			//cout << "Task " << tid << " - nRowToProcess : " << nRowToProcess << endl;
			muPtrs[tid]->setDataInput(alpha, nRowToProcess, offsetD, d,
									  r, rowPtrA, A);
		} else {
			muPtrs[tid]->setDataInput(alpha, nRowForLast, offsetD, d,
									  r, rowPtrA, A);
		}

		offsetD += nRowToProcess;
	}

	// Launch the update
	for(int tid=0; tid<nTask; tid++){
		pthread_create(&threads[tid], NULL, &ParallelMatrixUpdater::launchUpdate, (void*)muPtrs[tid]);
	}

	// Wait for all MatrixUpdater to have finished
	for(int tid=0; tid<nTask; tid++){
		 pthread_join(threads[tid], &status);
	}



}
