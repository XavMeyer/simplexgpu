//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file SparseRevisedFormProblem.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 13, 2011
 *
 */

#ifndef SPARSEREVISEDFORMPROBLEM_H_
#define SPARSEREVISEDFORMPROBLEM_H_

#include <string.h>

#include "Problem.h"
#include "SimplexProblem.h"

class SparseRevisedFormProblem : public Problem{
public:

	SparseRevisedFormProblem(SimplexProblem *inP);
	SparseRevisedFormProblem(unsigned int inNRow, unsigned int inNVar,
							 unsigned int inNSlack, unsigned int inNNoNull);
	SparseRevisedFormProblem(unsigned int inNRow, unsigned int inNVar,
							 unsigned int inNSlack, unsigned int inNNoNull,
							 bool init);
	virtual ~SparseRevisedFormProblem();

	SparseRevisedFormProblem * clone();

	void print();
	void printEquations();
	void printBasis();


	/*
	 * Problem size
	 */
	unsigned int nBasic, nNonBasic, nCol;

	/*
	 * Matrix containing the equations
	 */
	VAR_TYPE *sparseA, *B;

	/*
	 * Indice of sparse matrix A
	 */
	int *rowIndA;
	int *colIndA;

	/*
	 * Vector containing the LHS (b)
	 */
	VAR_TYPE *lhs;

	/*
	 * Vector containing the objective function
	 */
	VAR_TYPE *C, *Cb, *CP1, *CbP1;

	/*
	 * Index of basic and non basic variables
	 */
	unsigned int *bInd;

	/*
	 * Variable values
	 */
	VAR_TYPE *bVal, *val;

	/*
	 * Bound for each variables U/L
	 * xBounds.x ==> lower bound
	 * xBounds.y ==> upper bound
	 */
	float2* bounds;
	float2* boundsP1;

	float2*	bBounds;
	float2*	bBoundsP1;


	/*
	 * Two phase simplex important value
	 */
	unsigned int nInf;

	/*
	 * Objective function value
	 */
	VAR_TYPE objVal, objAuxVal;

	/*
	 * Names of the equations and variables
	 */
	typedef struct idxRow {
		int name;
		int var;
	} idxRow_t;

	typedef map <string, int> MapTypeCol_t;
	typedef map <string, idxRow> MapTypeRow_t;
	typedef map <string, idxRow>::iterator ItTypeRow_t;
	vector<string> rows, columns;
	MapTypeCol_t colsMap;
	MapTypeRow_t rowsMap;



private:
	void initPtr();
	void initStructure();
	void createFromSimplexProblem(SimplexProblem *inP);
	void initData();

};

#endif /* SPARSEREVISEDFORMPROBLEM_H_ */
