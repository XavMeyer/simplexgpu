//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Trace.cpp
 *
 *  Created on: May 17, 2011
 *      Author: meyerx
 */

#include "Logger.h"

const string Logger::printable_llevel[] = {"Normal",
										  "Critical",
										  "Error",
										  "Warning",
										  "Info",
										  "Debug"};

Logger::Logger() {
	set_verbose_level(DISABLE);
}

Logger::Logger(verbose_level_t v_level) {
	set_verbose_level(v_level);

}



Logger::~Logger() {
}


void Logger::set_verbose_level(verbose_level_t v_level){
	verbose_level = v_level;
}

bool Logger::isActive(){
	return verbose_level != DISABLE;
}

void Logger::log(log_level_t level, string msg){
	switch(verbose_level){
		case ALL:
			switch(level){
				case LOG_NOTHING : output(level, msg); break;
				case LOG_CRITICAL : output(level, msg); break;
				case LOG_ERROR : output(level, msg); break;
				case LOG_WARNING : output(level, msg); break;
				case LOG_INFO : output(level, msg); break;
				case LOG_DEBUG : output(level, msg); break;
				default : break;
			}
			break;
		case MINIMAL:
			switch(level){
				case LOG_NOTHING : break;
				case LOG_CRITICAL : output(level, msg); break;
				case LOG_ERROR : output(level, msg); break;
				case LOG_WARNING : break;
				case LOG_INFO : break;
				case LOG_DEBUG : break;
				default : break;
			}
			break;
		case LOW:
			switch(level){
				case LOG_NOTHING : break;
				case LOG_CRITICAL : output(level, msg); break;
				case LOG_ERROR : output(level, msg); break;
				case LOG_WARNING : break;
				case LOG_INFO : output(level, msg); break;
				case LOG_DEBUG : break;
				default : break;
			}
			break;
		default:
			break;
	}
}

void Logger::output(log_level_t level, string msg){
	cout << "[" << printable_llevel[level] << "]" << msg << endl;
}
