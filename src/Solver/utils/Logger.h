//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Trace.h
 *
 *  Created on: May 17, 2011
 *      Author: meyerx
 */

#ifndef TRACE_H_
#define TRACE_H_

#include <string.h>
#include <iostream>

using namespace std;

class Logger {
public:

	typedef enum {
	    LOG_NOTHING = 0,
	    LOG_CRITICAL = 1,
	    LOG_ERROR = 2,
	    LOG_WARNING = 3,
	    LOG_INFO = 4,
	    LOG_DEBUG = 5
	} log_level_t;

	typedef enum {
		DISABLE,
		DEBUG,
		MINIMAL,
		LOW,
		ALL
		// etc..
	} verbose_level_t;

	// Constructor
	Logger();
	Logger(verbose_level_t v_level);

	// Destructor
	virtual ~Logger();

	// Setters
	void set_verbose_level(verbose_level_t v_level);

	// Checkers
	bool isActive();

	// Logging function
	void log(log_level_t level, string msg);

private:

	verbose_level_t verbose_level;
    static const string printable_llevel[];

	void output(log_level_t level, string msg);
};

#endif /* TRACE_H_ */
