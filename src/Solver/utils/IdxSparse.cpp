//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * idxSparse.cpp
 *
 *  Created on: Feb 21, 2012
 *      Author: meyerx
 */

#include "IdxSparse.h"

IdxSparse::IdxSparse() {
	x = -1;
	y = -1;
	pos = -1;

}


IdxSparse::IdxSparse(int inX, int inY, int inPos) {
	x = inX;
	y = inY;
	pos = inPos;

}

IdxSparse::~IdxSparse() {
}


bool IdxSparse::operator < (const IdxSparse &rhs) const {
	if(y < rhs.y){
		return true;
	} else if (rhs.y < y) {
		return false;
	} else {
		if(x < rhs.x) {
			return true;
		} else {
			return false;
		}
	}
}

void IdxSparse::set(int inX, int inY, int inPos){
	x = inX;
	y = inY;
	pos = inPos;
}

