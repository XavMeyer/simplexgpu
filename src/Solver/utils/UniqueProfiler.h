//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * UniqueProfiler.h
 *
 *  Created on: Nov 30, 2011
 *      Author: meyerx
 */

#include "Singleton.h"
#include "../Config.h"

#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#ifndef UNIQUEPROFILER_H_
#define UNIQUEPROFILER_H_

#define NB_TIMER		40

#if PROFILING
#define _START_EVENT(profiler, event) 	profiler->startEvent(event);
#define _END_EVENT(profiler, event)		profiler->endEvent(event);
#else
#define _START_EVENT(profiler, event) 	void();
#define _END_EVENT(profiler, event)		void();
#endif



class UniqueProfiler : public Singleton<UniqueProfiler>{

	friend class Singleton<UniqueProfiler>;

private:
	// Constructor destructor
	UniqueProfiler();
	virtual ~UniqueProfiler();

public:

	void startEvent(string event);
	void endEvent(string event);


private:

	typedef struct {
		timeval startTime;
		timeval endTime;
		string event;
		bool running;
	} timer_t;

	unsigned short int level;
	timer_t timers[NB_TIMER];

	ofstream myFile;

	double duration();

};

#endif /* UNIQUEPROFILER_H_ */
