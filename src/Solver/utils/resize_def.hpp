//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*!
 * This type define the way a 2d array is stored.
 * It's either by row (ROW_WISE) or by column (COLUMN_WISE).
 */
typedef enum {ROW_WISE, COLUMN_WISE} array_storage_t;

/*!
 * This function resize an array (1d) and copy its old content
 * \param[in, out]	ptrArray	The array to resize
 * \param[in]  		oldSize		The current size of the array
 * \param[in]  		newSize		The new size of the array after resize
 */
template <typename T>
inline void resize_1D(T **ptrArray, unsigned int oldSize, unsigned int newSize){
	T *tmp = new T[newSize];
	memcpy(tmp, *ptrArray, oldSize*sizeof(T));
	delete [] *ptrArray;
	*ptrArray = tmp;
}


/*!
 * This function resize an array (2d) and copy its old content
 * \param[in]	 	ptrArray	The array (2d) to resize
 * \param[in]  		nRowO		The old number of array's row
 * \param[in]  		nColO		The old number of array's column
 * \param[in]  		nRowN		The new number of array's row
 * \param[in]  		nColN		The new number of array's column
 * \param[in]		arrayType	The type of storage used for this 2d array
 * \return						Return the pointer on the new array*
 */
template <typename T>
inline void resize_2D(T **ptrArray,
		              unsigned int nRowO, unsigned int nColO,
					  unsigned int nRowN, unsigned int nColN,
					  array_storage_t arrayType){
	// Create new array
	T *array = *ptrArray;
	T *tmp = new T[nRowN*nColN];
	// Copy the data in function of the storage type
	if(arrayType == ROW_WISE){
		// Copy row by row
		for(unsigned int i=0; i<nRowO; i++)
			memcpy(&tmp[nColN*i], &array[nColO*i], nColO*sizeof(T));
	} else {
		// Copy column by column
		for(unsigned int i=0; i<nColO; i++)
			memcpy(&tmp[nRowN*i], &array[nRowO*i], nRowO*sizeof(T));
	}
	// Delete old array and swap pointers
	delete [] array;
	*ptrArray = tmp;
}
