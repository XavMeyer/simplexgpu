//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *  Created on: Sep 11, 2010
 *      Author: meyerx
 */

#ifndef CUSTOMPROFLING_H_
#define CUSTOMPROFLING_H_

#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class CustomProfiling {
public:

	CustomProfiling();
	virtual ~CustomProfiling();

	void startTime();
	void endTime();
	double duration();

	void startTime(int iClock);
	void endTime(int iClock);
	double duration(int iClock);
	void writeDuration(int iClock, string message);

private:

	timeval start[10];
	timeval end[10];

	ofstream myFile;
};

#endif /* CUSTOMPROFLING_H_ */
