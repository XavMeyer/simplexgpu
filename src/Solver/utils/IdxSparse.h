//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * idxSparse.h
 *
 *  Created on: Feb 21, 2012
 *      Author: meyerx
 */

#ifndef IDXSPARSE_H_
#define IDXSPARSE_H_

class IdxSparse {
public:
	IdxSparse();
	IdxSparse(int inX, int inY, int inPos);
	virtual ~IdxSparse();

	bool operator < (const IdxSparse &rhs) const;

	void set(int inX, int inY, int inPos);

	int x;
	int y;
	int pos;
};

#endif /* IDXSPARSE_H_ */
