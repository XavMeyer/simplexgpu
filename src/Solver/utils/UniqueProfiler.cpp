//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file UniqueProfiler.cpp
 * \brief
 * \author Xavier Meyer
 * \date Nov 30, 2011
 *
 */

#include "UniqueProfiler.h"

UniqueProfiler::UniqueProfiler() : myFile("custProf.txt", ios::out) {

	level = 0;
	for(int i=0; i<NB_TIMER; i++)
		timers[i].running = false;

}

UniqueProfiler::~UniqueProfiler(){
	myFile.close();
}

/*
void UniqueProfiler::startTime(){
	 startTimer(0);
}

void UniqueProfiler::endTime(){
	endTimer(0);
}

double UniqueProfiler::duration(){
	return duration(0);
}
*/


void UniqueProfiler::startEvent(string event){

	//cout << "[" << level << "] " <<  event << endl;

	// if this current timer is already used, start a new one
	if(timers[level].running)
		level++;

	// Save the event name
	timers[level].event = event;

	// Set timer to running state
	timers[level].running = true;

	// Get the start time
	gettimeofday(&timers[level].startTime, NULL);
}

void UniqueProfiler::endEvent(string event){

	// Only capture event if the current timer is running
	if(timers[level].running){

		if(timers[level].event.compare(event) != 0){
			cout << "Profiler error (not same event)!" << endl;
			cout << scientific <<  duration() << " ";
			for(int i=0; i<level; i++) {
				cout << timers[i].event.c_str() << ">";
			}
			cout << timers[level].event.c_str() << endl;
		}

		// Capture end time
		gettimeofday(&timers[level].endTime, NULL);

		// Write to file
		myFile << scientific <<  duration() << " ";
		for(int i=0; i<level; i++) {
			myFile << timers[i].event.c_str() << ">";
		}
		myFile << timers[level].event.c_str() << endl;

		timers[level].running = false;
		if(level > 0)
			level--;
	} else {
		cout << "Profiler error (no running event)!" << endl;
		cout << scientific <<  duration() << " ";
		for(int i=0; i<level; i++) {
			cout << timers[i].event.c_str() << ">";
		}
		cout << timers[level].event.c_str() << endl;
	}


}


double UniqueProfiler::duration(){
	long s = timers[level].endTime.tv_sec - timers[level].startTime.tv_sec;
	long us = timers[level].endTime.tv_usec - timers[level].startTime.tv_usec;
	return ((double)s + us*1e-6);
}




