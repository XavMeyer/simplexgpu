//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SimplexSolution.h
 *
 *  Created on: May 23, 2011
 *      Author: meyerx
 */

#ifndef SIMPLEXSOLUTION_H_
#define SIMPLEXSOLUTION_H_

#define MAX_LINE 		1//10
#define MAX_RES			5000//30

#include "Types.h"
#include "Config.h"
#include <string.h>
#include <iostream>
#include <sstream>
using namespace std;


class SimplexSolution {
public:
	SimplexSolution(int inNVar, int inNSlack);
	virtual ~SimplexSolution();

	void copy(SimplexSolution * source);

	string toString();

	SimplexSolution * clone();

	bool isIntegral(int index);

	bool isIntegral();

	int	nVar;
	int nSlack;
	double* xVal;
	double* sVal;
	double objVal;

};

#endif /* SIMPLEXSOLUTION_H_ */
