//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file TwoPhaseSimplex.h
 * \brief
 * \author Xavier Meyer
 * \date Dec 9, 2011
 *
 */

#ifndef TWOPHASESIMPLEX_H_
#define TWOPHASESIMPLEX_H_

#define PRINT_DEBUG 	0

#include "Solver.h"
#include "Method.h"

template<class SimplexMethod>
class TwoPhaseSimplex : public Solver {
public:

	//typedef GenericSolver<SimplexMethod> super;

	/*!
	 * \brief Constructor with the device number
	 */
	TwoPhaseSimplex(Logger *inL, int device);


	/*!
	 * \brief Destructor
	 */
	virtual ~TwoPhaseSimplex();

	void setLogger(Logger *inL) {logger = inL;};

	/*!
	 * \brief Solve the given equation matrix.
	 */
	solver_res_t solve();

	/*!
	 * \brief Solve the given equation matrix.
	 * \param debug : Mode debug on/off (print the stages)
	 */
	solver_res_t solve(unsigned int inMaxIter);

	void setProblem(SimplexProblem *inP);
	void setDenseProblem(SimplexProblem *inP);
	void setSparseProblem(SparseRevisedFormProblem *inP);
	void reloadProblem();
	void getProblem(SimplexProblem *outP);

	void getSolution(SimplexSolution *outSol);

	void changeBound(int index, float2 bound);

	int getCounter();

	void saveWarmstart();
	void loadWarmstart();
	void enableWarmstart();
	void disableWarmstart();

	SimplexProblemStats * getStats();

private:

	// Variables
	IndexPivoting *ip;
	Problem *p;
	Method *sm;
	SimplexProblemStats *pStats;
	Logger *logger;

#if PRINT_DEBUG
	SimplexProblem *currentP;
#endif

#if PROFILING
	UniqueProfiler *prof;
#endif
	VAR_TYPE	sig;

	bool isEnableWs;

	//Expand
	unsigned int iterK; 		// Number of iteration since last expand reset
	unsigned int iterCnt; 		// Number of iteration for this relaxation
	unsigned int iterLimit;		// Maximum number of iteration for this relaxation
	unsigned int counter;		// Number of iteration since last problem full reset

	// Methods
	// Initialization methods
	void 			initVariables();

	// Solver methods
	solver_res_t		SolveSimplex();
	solver_res_t		SolveAuxiliary();

	// Expand
	void 			resetExpand();


};

#endif /* TWOPHASESIMPLEX_H_ */
