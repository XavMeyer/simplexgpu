//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * \file TwoPhaseSimplex.cpp
 * \brief
 * \author Xavier Meyer
 * \date Dec 9, 2011
 *
 */

#include "TwoPhaseSimplex.h"

template<class SimplexMethod>
TwoPhaseSimplex<SimplexMethod>::TwoPhaseSimplex(Logger *inL, int device) {
	initVariables();
	setLogger(inL);

	sm = new SimplexMethod(inL, device);
}

template<class SimplexMethod>
TwoPhaseSimplex<SimplexMethod>::~TwoPhaseSimplex() {
	logger = NULL;
	p = NULL;

	delete sm;

	if(ip != NULL)
		delete ip;

	if(pStats != NULL)
		delete pStats;

#if PRINT_DEBUG
	if(currentP != NULL)
		delete currentP;
#endif

}

/*!
 * \brief This function initialize the private variables
 *
 */
template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::initVariables() {

	counter = 0;

	p = NULL;
	ip = NULL;
	isEnableWs = false;
	pStats = NULL;

#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif

#if PRINT_DEBUG
	currentP = NULL;
#endif
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::setProblem(SimplexProblem *inP){
	this->setDenseProblem(inP);
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::setDenseProblem(SimplexProblem *inP){

	sm->setDenseProblem(inP);

	if(p == NULL) { // no problem in memory
		p = inP;
	} else {
		p = inP;
		delete ip;
	}

#if PRINT_DEBUG
	currentP = p->clone();
#endif

	ip = new IndexPivoting(p->nVar, p->nRow);

	if(p->optiType == SimplexProblem::MAX)
		sig = 1.0;
	else
		sig = -1.0;

	if(pStats == NULL)
		pStats = new SimplexProblemStats();
	else
		pStats->init();

}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::setSparseProblem(SparseRevisedFormProblem *inP){

	sm->setSparseProblem(inP);

	if(p == NULL) { // no problem in memory
		p = inP;
	} else {
		p = inP;
		delete ip;
	}

#if PRINT_DEBUG
	currentP = p->clone();
#endif

	ip = new IndexPivoting(p->nVar, p->nRow);

	if(p->optiType == SimplexProblem::MAX)
		sig = 1.0;
	else
		sig = -1.0;

	if(pStats == NULL)
		pStats = new SimplexProblemStats();
	else
		pStats->init();

}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::getProblem(SimplexProblem *outP){
	// Copy GPU data in CPU problem
	sm->getProblem(outP);
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::getSolution(SimplexSolution *outSol){
	sm->getSolution(outSol);
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::changeBound(int index, float2 bound){
	sm->changeBound(index, bound, ip);
}

template<class SimplexMethod>
int TwoPhaseSimplex<SimplexMethod>::getCounter(){
	return iterCnt;
}

template<class SimplexMethod>
SimplexProblemStats * TwoPhaseSimplex<SimplexMethod>::getStats(){
	return pStats;
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::reloadProblem() {
	sm->reinitProblem();
	ip->initVal();
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::saveWarmstart(){
	sm->saveWarmstart(counter, ip);
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::loadWarmstart(){
	sm->loadWarmstart(counter, ip);
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::enableWarmstart(){
	isEnableWs = true;
}

template<class SimplexMethod>
void TwoPhaseSimplex<SimplexMethod>::disableWarmstart(){
	isEnableWs = false;
}



/*!
 * This function launch the simplex algorithm
 */
template<class SimplexMethod>
solver_res_t TwoPhaseSimplex<SimplexMethod>::solve(unsigned int inMaxIter) {
	int r = 0;
	unsigned int lastCnt;
	solver_res_t result = SUCCESS;

	// Init expand reset counter
	iterK = 0;
	iterCnt = 0;
	iterLimit = inMaxIter;

	// Check if the problem is infeasible
	_START_EVENT(prof,"INITIAL_INF_CHECK")
	sm->checkInfeasabilities();
	_END_EVENT(prof,"INITIAL_INF_CHECK")

#if PRINT_DEBUG
	cout << "INITIAL PROBLEM" << endl;
	sm->getProblem(currentP);
	currentP->print();
	getchar();
#endif

	while(r < EXPAND_T_RESET){

		lastCnt = counter;

		if(p->mode == p->AUX){
			_START_EVENT(prof,"SOLVE_AUXILIARY")
			result = SolveAuxiliary();
			_END_EVENT(prof,"SOLVE_AUXILIARY")
		}

		if(result == SUCCESS){
			_START_EVENT(prof, "SOLVE_SIMPLEX")
			result = SolveSimplex();
			_END_EVENT(prof, "SOLVE_SIMPLEX")
		}

		if(lastCnt == counter)
			break;

		if(result != GOTO_PHASE1){
			_START_EVENT(prof,"RESET")
			iterK = 0;
			sm->resetExpand();
			// Is it time for a reinit'
			if(counter > NB_ITER_REINIT){
				_START_EVENT(prof, "PROBLEM_RESET")
				pStats->nbReset++;
				// If there is a warmstart, using it... else going back to initial problem
				if(isEnableWs){
					loadWarmstart();
					//cout << "Starting from warmStart" << endl;
				} else {
					counter = 0;
					sm->reinitProblem();
					ip->initVal();
				}
				logger->log(Logger::LOG_DEBUG, "Pb reinit!");
				_END_EVENT(prof, "PROBLEM_RESET")
			}

			r++;
			_END_EVENT(prof,"RESET")
		}
	}

	sm->processOptimum();

	pStats->addRelaxation(iterCnt, result == SUCCESS);

	if(logger->isActive()){
		if(result == SUCCESS){
			ostringstream oss(ostringstream::out);
			oss << "Success - result : " << fixed << sig*p->objVal << " iteration : " << iterCnt << " !";
			logger->log(Logger::LOG_NOTHING, oss.str());
		} else if(result == UNBOUNDED_AUX){
			logger->log(Logger::LOG_NOTHING, "Aux infeasible : Unbounded problem.");
		} else if(result == UNBOUNDED){
			logger->log(Logger::LOG_NOTHING, "Problem is not bounded!");
		} else if(result == NO_SOLUTION){
			logger->log(Logger::LOG_NOTHING, "Dual infeasible : No initial dictionary found!");
		} else if(result == INFEASIBLE){
			logger->log(Logger::LOG_NOTHING, "Problem is infeasible!");
		} else if(result == MAX_ITER_REACHED){
			ostringstream oss(ostringstream::out);
			oss << "Max iteration " << MAX_ITER << " reached without founding an optimum." << endl;
			logger->log(Logger::LOG_NOTHING, oss.str());
		}
	}

#if PRINT_DEBUG
	cout << "FINAL PROBLEM" << endl;
	sm->getProblem(currentP);
	currentP->print();
	getchar();
#endif

	return result;
}

template<class SimplexMethod>
solver_res_t TwoPhaseSimplex<SimplexMethod>::solve(){
	return solve(UINT_MAX);
}


/*!
 * This function solve the simplex problem
 * from a feasible dictionary.
 */
template<class SimplexMethod>
solver_res_t TwoPhaseSimplex<SimplexMethod>::SolveSimplex() {

	solver_res_t res;
	red_result_t pColResult;
	row_result_t expandRowRes;

	if(logger->isActive())
		logger->log(Logger::LOG_INFO, "Phase 2 : std Simplex.");

	// While max_iter not reached
	while((counter < MAX_ITER) && (iterCnt < iterLimit)) {

		// Get the pivot column
		_START_EVENT(prof,"FIND_ENTERING")
		pColResult = sm->findEnteringVariable();
		_END_EVENT(prof,"FIND_ENTERING")

		// If there is a potential column
		if(pColResult.index >= 0) {

			// Get the pivot row
			_START_EVENT(prof,"FIND_LEAVING")
			expandRowRes = sm->findLeavingVariable(pColResult);
			_END_EVENT(prof,"FIND_LEAVING")

			// Do the pivoting step / update the basis
			_START_EVENT(prof,"PIVOTING")
			res = sm->pivotingStep(pColResult, expandRowRes, ip);
			_END_EVENT(prof, "PIVOTING")

#if PRINT_DEBUG
			sm->getProblem(currentP);
			currentP->print();
			getchar();
#endif

			if(res != SUCCESS) return res;

			// optimum state and counter
			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){
				if(logger->isActive()){
					sm->processOptimum();
					ostringstream oss(ostringstream::out);
					oss << "Iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
					logger->log(Logger::LOG_INFO, oss.str());
				}
			}
			counter++;
			iterCnt++;

			// expand state, do we need a reset ?
			if(iterK == EXPAND_K) {
				iterK = 0;
				sm->resetExpand();
				if(p->nInf > 0) {
					return GOTO_PHASE1;
				}
			} else {
				iterK++;
			}

		} else {
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "(Sucess) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
				logger->log(Logger::LOG_INFO, oss.str());
			}
			return SUCCESS;
		}
	}

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "(Max iter) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
		logger->log(Logger::LOG_INFO, oss.str());
	}

	return MAX_ITER_REACHED;
}


/*!
 * \brief This function solve the Auxiliary problem
 */
template<class SimplexMethod>
solver_res_t TwoPhaseSimplex<SimplexMethod>::SolveAuxiliary() {
	solver_res_t res;
	red_result_t pColResult;
	row_result_t expandRowRes;

	if(logger->isActive())
		logger->log(Logger::LOG_INFO, "Phase 1 : Auxiliary problem.");

	while((counter < MAX_ITER) && (iterCnt < iterLimit)){ // while MAX_ITER not reached

		// Get the pivot column
		_START_EVENT(prof, "FIND_ENTERING")
		pColResult = sm->findEnteringVariableP1();
		_END_EVENT(prof, "FIND_ENTERING")

		if(pColResult.index >= 0) {
			// Get the pivot row
			_START_EVENT(prof,"FIND_LEAVING")
			expandRowRes = sm->findLeavingVariableP1(pColResult);
			_END_EVENT(prof,"FIND_LEAVING")

			// Doe the pivoting step / update the basis
			_START_EVENT(prof,"PIVOTING")
			res = sm->pivotingStepP1(pColResult, expandRowRes, ip);
			_END_EVENT(prof, "PIVOTING")
			if(res != SUCCESS) return res;

#if PRINT_DEBUG
			sm->getProblem(currentP);
			currentP->print();
			getchar();
#endif

			_START_EVENT(prof,"UPDATE_INF")
			if(iterK == EXPAND_K) {
				iterK = 0;
				sm->resetExpand();
			} else {
				iterK++;
				sm->updateInfeasabilities();
			}
			_END_EVENT(prof, "UPDATE_INF")

			if(PROFILER_DELAY != 0 && counter % PROFILER_DELAY == 0){
				if(logger->isActive()){
					sm->processOptimum();
					ostringstream oss(ostringstream::out);
					oss << "Iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << "(Inf. var : " << p->nInf << ").";
					logger->log(Logger::LOG_INFO, oss.str());
				}
			}
			counter++;
			iterCnt++;

			if(p->nInf == 0){
				return SUCCESS;
			}

		} else {
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
				logger->log(Logger::LOG_INFO, oss.str());
			}
			if(p->nInf > 0) { // we have not found the optiumum : 0
				return NO_SOLUTION;
			} else { // we have found the optimum : 0 and so we create the new starting dictionary
				return SUCCESS;
			}
		}
	}

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "(Max iter) Last iteration : " << counter << "- Optimum : " << scientific << sig*p->objVal << ".";
		logger->log(Logger::LOG_INFO, oss.str());
	}


	return MAX_ITER_REACHED;

}

