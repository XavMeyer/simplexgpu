//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Method.h
 *
 *  Created on: Dec 9, 2011
 *      Author: meyerx
 */

#ifndef METHOD_H_
#define METHOD_H_

#include "../Types.h"
#include "../SimplexWarmstart.h"
#include "../SimplexSolution.h"
#include "../SimplexProblem.h"
#include "../SimplexProblemStats.h"
#include "../SparseRevisedFormProblem.h"
#include "../SparseRevisedProblem.h"
#include "../Config.h"
#include "../utils/Logger.h"
#include "../utils/UniqueProfiler.h"
#include "SolverConfig.h"
#include "../Error.h"

class Method {
public:

	virtual ~Method(){}

	/*
	 * Phase 1 methods
	 *
	 */
	// Check for infeasabilities and update the phase 1 objective function
	virtual void checkInfeasabilities() = 0;

	// Update the phase 1 objective function
	virtual void updateInfeasabilities() = 0;

	// Find the entering variable
	virtual red_result_t findEnteringVariableP1() = 0;

	// Find the leaving variable
	virtual row_result_t findLeavingVariableP1(red_result_t enteringVar) = 0;

	// Do the pivoting steps / upgrade the basis
	virtual solver_res_t pivotingStepP1(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip) = 0;

	/*
	 * Phase 2 methods
	 *
	 */
	// Find the entering variable
	virtual red_result_t findEnteringVariable() = 0;

	// Find the leaving variable
	virtual row_result_t findLeavingVariable(red_result_t enteringVar) = 0;

	// Do the pivoting steps / upgrade the basis
	virtual solver_res_t pivotingStep(red_result_t enteringVar, row_result_t leavingVar, IndexPivoting *ip) = 0;

	/*
	 * Problem related methods
	 *
	 */

	// Set the starting problem
	// Warning : must be called prior to a loadProblem or reinitProblem call
	virtual void setDenseProblem(SimplexProblem *inP) = 0;
	virtual void setSparseProblem(SparseRevisedFormProblem *inP) = 0;
	virtual void setSparseProblem(SparseRevisedProblem *inP) = 0;

	// The problem is reinit'd but the current value of the solution is kept
	// Warning :  setProblem must have been called prior to this
	virtual void reinitProblem() = 0;

	// Return the current state of the problem in the object pointed by outP
	virtual void getProblem(SimplexProblem *outP) = 0;

	// Return the current solution of the problem
	virtual void getSolution(SimplexSolution *outSol) = 0;

	// Change a bound of the problem
	virtual void changeBound(int index, float2 bound, IndexPivoting *ip) = 0;

	// Process the value of the optimum function
	virtual void processOptimum() = 0;

	// Reset the perturbation of the expand method
	virtual void resetExpand() = 0;

	// Warmstart management
	virtual void saveWarmstart(unsigned int &counter, IndexPivoting *ip) = 0;
	virtual void loadWarmstart(unsigned int &counter, IndexPivoting *ip) = 0;

};

#endif /* METHOD_H_ */
