//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Solver.h
 *
 *  Created on: May 18, 2011
 *      Author: meyerx
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include "../Types.h"
#include "../SimplexWarmstart.h"
#include "../SimplexSolution.h"
#include "../SimplexProblem.h"
#include "../SimplexProblemStats.h"
#include "../Config.h"
#include "../utils/Logger.h"
#include "../utils/UniqueProfiler.h"

class Solver {
public:

	virtual ~Solver(){};

	/*typedef enum { SUCCESS,
				   UNBOUNDED,
				   NO_SOLUTION,
				   SKIPPED_PHASE2,
				   UNBOUNDED_AUX,
				   INFEASIBLE,
				   MAX_ITER_REACHED,
				   GOTO_PHASE1,
				   ERROR}
	result_t;*/

	// Logger related functions
	void setLogger(Logger *inL) {logger = inL;};


	// Problem related functions
	/**
	 * set the given problem in memory
	 */
	virtual void setProblem(SimplexProblem *inP) = 0;

	/**
	 * reload the simplex tableau from the given problem
	 * The problem size must be the same
	 */
	virtual void reloadProblem() = 0;

	/**
	 * Return a pointer to the current problem
	 */
	virtual void getProblem(SimplexProblem *inP) = 0;


	/*
	 * Return the current solution
	 */
	virtual void getSolution(SimplexSolution *inSol) = 0;

	/*
	 * Change a bound (into the GPU memory)
	 */
	virtual void changeBound(int index, float2 bound) = 0;

	/*
	 * Warmstart related function
	 *
	 */
	virtual void saveWarmstart() = 0;
	virtual void loadWarmstart() = 0;
	virtual void enableWarmstart() = 0;
	virtual void disableWarmstart() = 0;


	// Main functions
	virtual solver_res_t solve() = 0;
	virtual solver_res_t solve(unsigned int inMaxIter) = 0;

	virtual int getCounter() = 0;
	virtual SimplexProblemStats * getStats() = 0;
	//virtual void reinitProblem() = 0;

protected:

	Logger *logger;

#if PROFILING
	UniqueProfiler *prof;
#endif

};

#endif /* SOLVER_H_ */
