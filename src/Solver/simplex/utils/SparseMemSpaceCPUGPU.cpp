//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemSpaceCPUGPU.cpp
 *
 *  Created on: Mar 1, 2012
 *      Author: meyerx
 */

#include "SparseMemSpaceCPUGPU.h"

SparseMemSpaceCPUGPU::SparseMemSpaceCPUGPU(SparseMemorySpace *inSms) {

	smsCPU = inSms;
	smsGPU = new SparseMemorySpaceGPU(inSms);

}

SparseMemSpaceCPUGPU::~SparseMemSpaceCPUGPU() {
	smsCPU = NULL;
	smsGPU = NULL;
}
