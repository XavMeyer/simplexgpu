//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SparseMemSpaceCPUGPU.h
 *
 *  Created on: Mar 1, 2012
 *      Author: meyerx
 */

#ifndef SPARSEMEMSPACECPUGPU_H_
#define SPARSEMEMSPACECPUGPU_H_


#include "../../simplexCPU/utils/SparseMemorySpace.h"
#include "../../simplexGPU/utils/SparseMemorySpaceGPU.h"

class SparseMemSpaceCPUGPU {
public:

	SparseMemSpaceCPUGPU(SparseMemorySpace *inSms);
	virtual ~SparseMemSpaceCPUGPU();


	SparseMemorySpace *smsCPU;
	SparseMemorySpaceGPU *smsGPU;

};

#endif /* SPARSEMEMSPACECPUGPU_H_ */
