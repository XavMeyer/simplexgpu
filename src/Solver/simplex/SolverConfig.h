//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SolverConfig.h
 *
 *  Created on: Feb 13, 2012
 *      Author: meyerx
 */

#ifndef SOLVERCONFIG_H_
#define SOLVERCONFIG_H_

// Number of update on P1 objective before refresh
#define MAX_UPDATE_INFEASABILITY			1000
// Number of update on reduced cost before refresh
#define MAX_UPDATE_RED_COST					1000
// Number of update on SE coefficient before refresh
#define MAX_UPDATE_SE_COEFF 				10000

// Type of update and check on infeasabilities
// IN_CHECK_NEW is faster but a little bit less accurate
#define INF_CHECK_NEW						1
#define INF_CHECK_OLD						2
#define INF_CHECK_TYPE						INF_CHECK_NEW




/********************* STANDARD SIMPLEX ********************/

#define PIVOTING_NORMAL						1
#define PIVOTING_OPTIMISED					2
#define PIVOTING_METHOD						PIVOTING_NORMAL



/*************** PARALLEL CPU & GPU SIMPLEX ****************/
#define N_CPU								1



/******************** DEPRECATED ***********************/
// Print the problem data for debug purpose
#define PRINT_DEBUG 						0

#endif /* SOLVERCONFIG_H_ */
