//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Config.h
 *
 *  Created on: May 16, 2011
 *      Author: meyerx
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define USE_FLOAT 0
#define USE_DOUBLE 1

#define TYPE USE_DOUBLE// <== SET THE TYPE YOU WANT TO USE

/* ARRAY INDEXING : COLUMN MAJOR => DON'T TOUCH */
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

/* EPSILONS */
#define EPS1				1e-6 // zero tolerance
#define EPS2				1e-9 // row choice zero tolerance
#define EPS3				1e-5 // not used
#define EPS4				1e-5 // Max type comparison
#define EPS_NESTED			1e-3 // nested zero tolerence
#define F_EQUAL_Z(x)		(fabs(x) <= EPS1)

#define TOLERANCE			0.00
#define ESS_EQUAL(x,y)		(fabs(x-y) <= ((fabs(x) > fabs(y) ? fabs(y) : fabs(x))*TOLERANCE))

/* COL CHOICE ALGORITHM */
#define STD_METHOD			0
#define STEEPEST_EDGE 		1
#define NESTED_STD			2

#define COL_CHOICE_METHOD	STEEPEST_EDGE

/* MEMORY TYPE (mainly for reduction results)*/
#define PINNED				1
#define MAPPED_RESULT 		0

/* MAX ITERATION */
#define MAX_ITER			1000000

/* NUMBER OF ITERATION BEFORE REINIT */
#define NB_ITER_REINIT	    		50000

/* CYCLING DETECTION */
#define NB_DEGENERACY  20000

/* NB OF ITERATIONS BETWEEN EACH OBJ PRINTOUT */
#define PROFILER_DELAY 		100

/* CUSTOM PROFILING */
#define PROFILING 			0

/* SCALING */
#define ROW_SCALING			0 // Scales rows (max of row normalized)
#define COL_SCALING			0 // Scales cols (max of col normalized and bounds then changed accordingly)

/* EXPAND */
#define EXPAND_EPSILON		1e-16			 			// precision (1e-16)
#define EXPAND_DELTA_F		1e-6						// master feasibility tolerance (1e-6)
#define EXPAND_K			1e4 					  	// nIter Before reset (1e4)
#define EXPAND_DELTA_0		0.5*EXPAND_DELTA_F			// init feasibility tolerance
#define EXPAND_DELTA_K		0.99*EXPAND_DELTA_F			// maximum feasibility tolerance
// increase amount of EXPAND_DELTA_K per iteration
#define EXPAND_TAU			0.49*1e-10
// Non trivial move to bound
#define EXPAND_NON_TRIV		1e-11
// Nb of terminaison reset
#define EXPAND_T_RESET		2
#define EXPAND_W			0.001

/* deprecated */
#define CLEAN				0
#define CLEAN_DELAY			1

#define SE_TH_COEFF			0.05

#endif /* CONFIG_H_ */
