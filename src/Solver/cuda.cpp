//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * cuda.cpp
 *
 *  Created on: Oct 1, 2010
 *      Author: meyerx
 */

#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include "MPSReaderOpti.h"
#include "MPSReaderSparse.h"
#include "SimplexProblem.h"
#include "SparseRevisedFormProblem.h"
#include "Config.h"
#include "Types.h"
#include "simplexGPU/MultiGPU.h"
#include "simplexGPU/SolverMono.h"
#include "simplexGPU/cuda/KernelWrapperMono.h"
#include "utils/Logger.h"
#include "utils/CustomProfiling.h"
#include "simplex/Solver.h"
#include "simplex/Method.h"
#include "simplex/TwoPhaseSimplex.hpp"
#include "simplexGPU/StandardMethodGPU.h"
#include "simplexGPU/StandardMethodSpecP1GPU.h"
#include "simplexGPU/RevisedMethodGPU.h"
#include "simplexGPU/RevisedSEMethodGPU.h"
#include "simplexGPU/RevisedSEOptiMethodGPU.h"
#include "simplexGPU/RevisedASEOptiMethodGPU.h"
#include "simplexGPU/RevisedDVXOptiMethodGPU.h"
#include "simplexGPU/RevisedSparseAMethodGPU.h"
#include "simplexGPU/RevisedSparseMethodMGPU.h"



using namespace std;

#define MAXIMIZE 	"-max"
#define MINIMIZE	"-min"



void printUsage(){
	printf("usage    - ./SimplexCUDA filename.mps -max|-min [--device=x] [--ngpu=y] [-d]\n");
	printf("filename - must be an mps file (with mps extension)\n");
	printf("type     - either -max or -min \n");
	printf("device   - --device=x where x stands for the device identifier (0 if not specified)\n");
	printf("ngpu     - --ngpu=y where y stands for the number of GPU used\n");
	printf("-d       - debug, only for small problem - will print all the matrix\n");
	printf("-sol     - solution, print the solution in a file\n");
}

template<class SimplexMethod>
void testImpl(SimplexProblem* p, Logger *logger, int device, ofstream &fStream){
	int nMes = 3;
	double time[nMes], sumT = 0.0, meanT, sigma=0.0;
	CustomProfiling cp;
	TwoPhaseSimplex<SimplexMethod> s = TwoPhaseSimplex<SimplexMethod>(logger, device);
	for(int i=0; i<nMes; i++){
		s.setDenseProblem(p);
		cp.startTime();
		s.solve();
		cp.endTime();
		time[i] = cp.duration();
		sumT += time[i];
	}
	meanT = sumT/(double)nMes;
	cout << "Time : " << fixed << meanT << " seconds." <<endl;
	fStream << fixed << meanT << " ";

	for(int i=0; i<nMes; i++){
		sigma += pow(time[i]-meanT, 2.0);
	}
	sigma = sqrt(sigma/(double)nMes);
	cout << "Std dev : " << sigma << endl;
	fStream << fixed << sigma << " ";

	fStream << fixed << p->objVal << " ";
}

void testProb(SimplexProblem *p, Logger *logger, ofstream &fStream){
	uint device = 2;
	cout << "Problem : " << p->name << endl;
	cout << "Standard version " << endl;
	testImpl<StandardMethodSpecP1GPU>(p, logger, device, fStream);
	cout << "Revised SE version " << endl;
	testImpl<RevisedSEMethodGPU>(p, logger, device, fStream);
	cout << "Revised SE Opti version " << endl;
	testImpl<RevisedSEOptiMethodGPU>(p, logger, device, fStream);
	cout << "Revised sparse SE Opti version " << endl;
	testImpl<RevisedSparseAMethodGPU>(p, logger, device, fStream);
}

void test(){


	uint nProb = 9; //56;
	string path = "/home/meyerx/simplex/problems/netlib/";
	string probs[] = {/*"25fv47.mps",
					  "80bau3b.mps",
					  "adlittle.mps",
					  "afiro.mps",
					  "agg2.mps",
					  "agg3.mps",
					  "agg.mps",
					  "bandm.mps",
					  "beaconfd.mps",
					  "blend.mps",
					  "bnl1.mps",
					  "bnl2.mps",
					  "boeing1.mps",
					  "boeing2.mps",
					  "bore3d.mps",
					  "brandy.mps",
					  "capri.mps",
					  "cycle.mps",
					  "czprob.mps",
					  "d2q06c.mps",
					  "d6cube.mps",
					  "degen2.mps",
					  "degen3.mps",*/
					  //"dfl001.mps",
					  /*"e226.mps",
					  "etamacro.mps",
					  "fffff800.mps",
					  "finnis.mps",
					  "fit1d.mps",
					  "fit2d.mps",
					  //"fit2p.mps",
					  "forplan.mps",
					  "ganges.mps",
					  "gfrd-pnc.mps",
					  //"greenbea.mps",
					  //"greenbeb.mps",
					  "grow15.mps",
					  "grow22.mps",
					  "grow7.mps",
					  "israel.mps",
					  "kb2.mps",
					  "lotfi.mps",
					  "maros.mps",
					  "maros-r7.mps",
					  "nesm.mps",
					  "perold.mps",
					  "pilot4.mps",*/
					  "pilot87.mps",
					  "pilot.mps",
					  "pilotnov.mps",
					  "recipe.mps",
					  "sc105.mps",
					  "sc205.mps",
					  "sc50a.mps",
					  "sc50b.mps",
					  "scagr25.mps"};

	Logger *logger = new Logger(Logger::ALL);
	ofstream fStream("results.txt");

	for(uint i=0; i<nProb; i++){
		fStream << probs[i] << " ";
		string probName = path + probs[i];
		MPSReaderOpti *m = new MPSReaderOpti(probName.c_str());
		SimplexProblem* p = m->readFile();
		p->changeToMinimize();
		delete m;
		testProb(p, logger, fStream);
		delete p;
		fStream << endl;
	}
	fStream.close();
}

int main(int argc, const char* argv[]) {

	bool debug = false;
	bool maximize = true;
	string solName = "";
	int device = 0;
	int nGPU = 1;

	if(argc < 3 || argc > 8){
		printUsage();
		return -1;
	} else {
		for(int i=3; i<argc; i++){
			if(strncmp("-d", argv[i], 2) == 0) {
				debug = true;
			} else if(strncmp("--device=", argv[i], 9) == 0) {
				device = atoi(&argv[i][9]);
				if(device > 3){
					printf("Device id goes from 0 to 3 \n.");
					return -1;
				}
			} else if(strncmp("--sol=", argv[i], 6) == 0) {
				solName = string(&argv[i][6]);
			} else if(strncmp("--ngpu=", argv[i], 7) == 0) {
				nGPU = atoi(&argv[i][7]);
				if(nGPU < 1 || nGPU > 4){
					printf("Number of GPU goes from 1 to 4 \n.");
					return -1;
				}
			} else {
				printUsage();
				return -1;
			}
		}
	}

	//test();
	//return 0 ;

	/*printf("*******************************************\n");
	printf("************* SIMPLEX SOLVER **************\n");*/

	try{

		/*MPSReaderSparse *reader = new MPSReaderSparse(argv[1]);
		SparseRevisedFormProblem *p = reader->readFile();

		//p->print();

		double test = 0;
		test = NAN;
		test = test *3.0;

		if (test == NAN)
			cout << "I am NAN" << endl;
		else
			cout << "I am " << test  << endl;

		if (test > 0)
			cout << "I am greater than 0" << endl;
		else
			cout << "I am not greater than 0"   << endl;

		if (test < 0)
			cout << "I am less than 0" << endl;
		else
			cout << "I am not less than 0"  << endl;



		delete reader;
		delete p;*/

		CustomProfiling cp;

		Logger *logger = new Logger(Logger::ALL);

		cp.startTime();
		MPSReaderOpti *m = new MPSReaderOpti(argv[1]);

		SimplexProblem* p = m->readFile();
		cp.endTime();
		cout << "File read in " << fixed << cp.duration() << " seconds" << endl;



		delete m;
		//if(debug)
		//	p->Print();


		if(strcmp(argv[2], MINIMIZE) == 0) {
			maximize = false;
		} else if(!strcmp(argv[2], MAXIMIZE) == 0){
			// Default to Maximize but print usage anyway
			printUsage();
			return -1;
		}


		if(nGPU > 1){
			//MultiGPU s = MultiGPU(nGPU, p);
			//s.solve(maximize, debug);
		} else {
			if(!maximize)
				p->changeToMinimize();

			/*cout << "Standard version " << endl;
			testImpl<StandardMethodSpecP1GPU>(p, logger, 0);
			//testImpl<RevisedMethodGPU>(p, logger, device);
			cout << "Revised SE version " << endl;
			testImpl<RevisedSEMethodGPU>(p, logger, 1);
			cout << "Revised SE Opti version " << endl;
			testImpl<RevisedSEOptiMethodGPU>(p, logger, 2);
			cout << "Revised sparse SE Opti version " << endl;
			testImpl<RevisedSparseAMethodGPU>(p, logger, 3);*/
			//cout << "[beta]Revised sparse SE Opti version " << endl;
			//testImpl<RevisedSparseMethodMGPU>(p, logger, device);

			//SolverMono s = SolverMono(logger, device);
			//TwoPhaseSimplex<StandardMethodGPU> s = TwoPhaseSimplex<StandardMethodGPU>(logger, device);
			//TwoPhaseSimplex<StandardMethodSpecP1GPU> s = TwoPhaseSimplex<StandardMethodSpecP1GPU>(logger, device);
			//TwoPhaseSimplex<RevisedMethodGPU> s = TwoPhaseSimplex<RevisedMethodGPU>(logger, device);
			//TwoPhaseSimplex<RevisedSEMethodGPU> s = TwoPhaseSimplex<RevisedSEMethodGPU>(logger, device);
			//TwoPhaseSimplex<RevisedSEOptiMethodGPU> s = TwoPhaseSimplex<RevisedSEOptiMethodGPU>(logger, device);
			//TwoPhaseSimplex<RevisedASEOptiMethodGPU> s = TwoPhaseSimplex<RevisedASEOptiMethodGPU>(logger, device);
			//TwoPhaseSimplex<RevisedDVXOptiMethodGPU> s = TwoPhaseSimplex<RevisedDVXOptiMethodGPU>(logger, device);
			TwoPhaseSimplex<RevisedSparseAMethodGPU> s = TwoPhaseSimplex<RevisedSparseAMethodGPU>(logger, device);
			//TwoPhaseSimplex<RevisedSparseMethodMGPU> s = TwoPhaseSimplex<RevisedSparseMethodMGPU>(logger, device);
			cp.startTime(0);
			//for(int i=0; i<4; i++){
			//cout << "HELLOOOOO" << endl;
			cp.startTime(1);
			s.setDenseProblem(p);
			cp.endTime(1);
			s.solve();
			//}
			cp.endTime(0);
			cout << "SetPb time : " << fixed << cp.duration(1) << " seconds." <<endl;
			cout << "Total time : " << fixed << cp.duration(0) << " seconds." <<endl;


			//if(solName.size() > 0)
			//	p->saveSolution(solName, true);

			//kw->deInitCuda(true);

			delete logger;

		}

		if(solName.size() > 0)
			p->saveSolution(solName, true);
		delete p;
	} catch (int e){
		printf("An exception occurred (%d).\n", e);
	}


	return 0;

}
