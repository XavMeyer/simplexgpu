//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * *SimplexCuda* is a LP solver using the simplex method on GPU.
 * Copyright (C) <2010>  <Xavier Meyer>
 *
 *
 * MPSReader2Phase.h
 *
 *  Created on: Sep 23, 2010
 *      Author: meyerx
 *
 *       edited by Kunzli Pierre
 *      jan 10 2011
 */

#ifndef MPSREADEROPTI_H_
#define MPSREADEROPTI_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "SimplexProblem.h"
#include "Types.h"
#include "Config.h"
using namespace std;

#define MPS_NAME 			"NAME"
#define MPS_ROWS 			"ROWS"
#define MPS_COLUMNS 		"COLUMNS"
#define MPS_RHS 			"RHS"
#define MPS_BOUNDS 			"BOUNDS"
#define MPS_RANGE 			"RANGES"
#define MPS_END 			"ENDATA"
#define MPS_UPPER			"UP"
#define MPS_LOWER 			"LO"
#define MPS_FIXED			"FX"
#define MPS_FREE			"FR"
#define MPS_LOWER_INT		"LI"
#define MPS_UPPER_INT		"UI"
#define MPS_BIN				"BV"

#define MPS_MARKER			"'MARKER'"
#define MPS_MARKER_INTORG	"'INTORG'"
#define MPS_MARKER_INTEND	"'INTEND'"


#define MPS_ROW_LOWER	'L'
#define MPS_ROW_GREATER	'G'
#define MPS_ROW_EQUAL	'E'
#define MPS_ROW_COST	'N'

#ifndef MAX
#define MAX(a,b)		(a > b ? a : b)
#endif

#define LINE_SIZE 		80
#define WORD_SIZE		20
#define	NB_WORDS		10

class MPSReaderOpti {
public:

	enum error_t {NO_ERROR, NAME_ERROR, NROW_NCOL_ERROR, ROW_ERROR, COLUMNS_ERROR, RHS_ERROR, BOUNDS_ERROR, RANGES_ERROR, NO_FILE, NO_BOUND, NO_RANGE, NO_RB};
	//enum error_t error;


	MPSReaderOpti(string inFilePath);
	virtual ~MPSReaderOpti();


	SimplexProblem*	readFile(void);


private:
	string filePath;

	int nEquNoNull;
	int nObjNeg;
	int nEquality;
	int nVar;
	int nEqs;

	bool markerInt;

	VAR_TYPE *maxRow;
//	VAR_TYPE *maxCol;

	char *line;
	char **words;
	int lLine, nWords;
	int *lWords;

	bool			splitLine(FILE *file);
	void			debugSL();

	error_t 		cntPhase();
	error_t 		readPhase(SimplexProblem *p);
	void 			checkError(error_t err);

	error_t 		readName(FILE *mpsFile, SimplexProblem *p);
	error_t         readRows(FILE *mpsFile, SimplexProblem *p);
	error_t			readColumns(FILE *mpsFile, SimplexProblem *p);
	error_t 		readRHS(FILE *mpsFile, SimplexProblem *p);
	error_t			readRanges(FILE *mpsFile, SimplexProblem *p);
	error_t 		readBounds(FILE *mpsFile, SimplexProblem *p);

	// No artificial / slack var methods
	SimplexProblem* initStructures();
	void 			createMatrix(SimplexProblem *p);
	void 			SSS(SimplexProblem *p);
	void 			initXVals(SimplexProblem *p);
	void 			initXVals2(SimplexProblem *p);

};

#endif /* MPSREADEROPTI_H_ */
