//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBoundV2.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef BRANCHANDBOUNDV2_H_
#define BRANCHANDBOUNDV2_H_

#include "NodeSelection/NodeSelectionStrategy.h"
#include "Branching/BranchingStrategy.h"

#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/simplex/Solver.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/utils/Logger.h"

#define INFO_FREQUENCY		5000

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
class BranchAndBoundV2 {
public:
	BranchAndBoundV2();
	BranchAndBoundV2(Logger *inL);
	BranchAndBoundV2(int device);
	BranchAndBoundV2(Logger *inL, int device);

	virtual ~BranchAndBoundV2();

	SimplexSolution * BranchAndBoundV2(SimplexProblem *inP);

	unsigned int getNSolvedNode();
	unsigned int getNCreatedNode();
	unsigned int getNInfNode();

private:

	unsigned int nCreatedNode, nSolvedNode, nInfNode;

	// Node selection strategy
	NodeSelectionStrategy *nss;
	// Branching strategy
	BranchingStrategy *bs;

	// Simplex solver
	SolverImpl *solver;

	// Logger for debug, info, etc. messages
	Logger *logger;
	Logger *solverLogger;

	// Initial problem
	SimplexProblem *problem;
	// Current problem
	SimplexProblem *cProb;

	// Integer variable indices
	IntegerVariables *intVar;
	// Pseudo-costs / Reliability
	Reliability *rel;

	// current solution
	SimplexSolution *cSol;
	// best solution
	SimplexSolution *bSol;
	// initial solution (first relaxation)
	SimplexSolution *iSol;

	//! Init variables and classes.
	void init(int device);

	//! set the problem to solve
	void setProblem(SimplexProblem *inP);

	//! Init the branch and bound algorithm.
	void initBranchAndBoundV2(SimplexProblem *inP);

	//! First relaxation, solve the initial (non integral) problem.
	void initialRelaxation();

	//! Solve a problem and update pseudocost if solving is successful.
	solver_res_t solve();

};

#endif /* BRANCHANDBOUNDV2_H_ */
