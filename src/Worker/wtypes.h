//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * types.h
 *
 *  Created on: Jul 5, 2011
 *      Author: meyerx
 */

#ifndef WTYPES_H_
#define WTYPES_H_

#include <pthread.h>
#include <sched.h>
#include <semaphore.h>

	typedef enum {SEQUENTIAL, PARALLEL} execution_mode_t;

	typedef enum {LEAF, PRUNE, BRANCH} node_type_t;

	typedef enum {WAITING, WORKING, TERMINATING} worker_state_t;
	typedef enum {WORK, TERMINATE} worker_signal_t;

	typedef struct {
		sem_t sem;
		sem_t bSolMutex;
		pthread_barrier_t barrier;
	} manager_context_t;

#endif /* WTYPES_H_ */
