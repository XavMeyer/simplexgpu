//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef MANAGERCNB_H_
#define MANAGERCNB_H_

#include "WorkerCNB_def.hpp"

#include "NodeSelection/ParNodeSelectionStrategy.h"
#include "NodeSelection/ParBFSStrategy_def.hpp"
#include "Branching/ParBranchingStrategy.h"
#include "Branching/ParSmallestIndexFirst.h"
#include "Branching/ParStrongBranching.h"
#include "Branching/ParReliabilityBranching.h"
#include "CutGenerator/cg_includes.h"

#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/simplex/Solver.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/utils/Logger.h"

#include "wtypes.h"

#include <sched.h>
#include <pthread.h>

#include "cpgpu.cfg"

#define INFO_FREQUENCY		5000

template<class CutGen, class SolverImpl>
class ManagerCNB {
public:
	ManagerCNB();
	ManagerCNB(Logger *inL);
	ManagerCNB(int inNWorker);
	ManagerCNB(Logger *inL, int inNWorker);

	virtual ~ManagerCNB();

	SimplexSolution * cutAndBranch(SimplexProblem *inP);

	unsigned int getNSolvedNode();
	unsigned int getNCreatedNode();
	unsigned int getNInfNode();

private:

	int nWorker;
	bool bSolFound;
	unsigned int nCreatedNode, nSolvedNode, nInfNode;

	manager_context_t mngContext;

	Solver *solver;
	Logger *solverLog;

	// Node selection strategy
	ParNodeSelectionStrategy *nss;
	// Branching strategy
	ParBranchingStrategy *bs;
	// Cut generator
	CutGenerator *cg;

	// Logger for debug, info, etc. messages
	Logger *logger;

	// Initial problem
	SimplexProblem *problem;
	// Current problem
	SimplexProblem *cProb;

	// Integer variable indices
	IntegerVariables *intVar;
	// Pseudo-costs / Reliability
	ParReliability *rel;

	// current solution
	SimplexSolution *cSol;
	// best solution
	SimplexSolution *bSol;
	// initial solution (first relaxation)
	SimplexSolution *iSol;

	//! Init variables and classes.
	void init(int inNWorker);

	//! set the problem to solve
	void setProblem(SimplexProblem *inP);

	//! Init the branch and bound algorithm.
	void initManagerCNB(SimplexProblem *inP);

	//! First relaxation, solve the initial (non integral) problem.
	node_type_t initialRelaxation();

	//! do the cut part
	void addCuts();

	//! launch the n solvers
	void launchSolvers();

};

#endif /* MANAGERCNB_H_ */
