//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.cpp
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#include "ManagerCNB.h"

template<class CutGen, class SolverImpl>
ManagerCNB<CutGen, SolverImpl>::ManagerCNB() {
	nWorker = 2;
	logger = new Logger(Logger::DISABLE);
}

template<class CutGen, class SolverImpl>
ManagerCNB<CutGen, SolverImpl>::ManagerCNB(Logger *inL) : logger(inL){
	init(2);
}

template<class CutGen, class SolverImpl>
ManagerCNB<CutGen, SolverImpl>::ManagerCNB(int inNWorker) {
	init(inNWorker);
	logger = new Logger(Logger::DISABLE);
}

template<class CutGen, class SolverImpl>
ManagerCNB<CutGen, SolverImpl>::ManagerCNB(Logger *inL, int inNWorker) : logger(inL){

	init(inNWorker);
}

template<class CutGen, class SolverImpl>
ManagerCNB<CutGen, SolverImpl>::~ManagerCNB() {

	if(cSol != NULL)
		delete cSol;
	if(bSol != NULL)
		delete bSol;
	if(iSol != NULL)
		delete iSol;

	if(cProb != NULL)
		delete cProb;

	if(intVar != NULL)
		delete intVar;

	if(bs != NULL)
		delete bs;
	if(nss != NULL)
		delete nss;
	if(cg != NULL)
		delete cg;

	if(rel != NULL)
		delete rel;

	logger 		= NULL;
	problem 	= NULL;
}

template<class CutGen, class SolverImpl>
void ManagerCNB<CutGen, SolverImpl>::init(int inNWorker){

	nWorker = inNWorker;
	bSolFound = false;
	nCreatedNode = nSolvedNode = nInfNode =0;

	// Init context
	sem_init(&mngContext.sem, 0, 1);
	sem_init(&mngContext.bSolMutex, 1, 1);
	pthread_barrier_init(&mngContext.barrier, NULL, nWorker+1);

	problem 		= NULL;
	cProb 			= NULL;

	cSol			= NULL;
	bSol 			= NULL;
	iSol			= NULL;

	intVar 			= NULL;

	bs 				= new ParReliabilityBranching(logger);
	nss 			= new ParBFSStrategy<ESTIMATE, true>(nWorker, logger);
	cg				= new CutGen(logger);

}


template<class CutGen, class SolverImpl>
void ManagerCNB<CutGen, SolverImpl>::setProblem(SimplexProblem *inP){
	problem = inP;
	cProb = problem->clone();
}


template<class CutGen, class SolverImpl>
void ManagerCNB<CutGen, SolverImpl>::initManagerCNB(SimplexProblem *inP){
	// set the initial problem to the worker
	setProblem(inP);
	// Init the current solution
	iSol = new SimplexSolution(problem->nVar, problem->nRow);

	// Create the integer variable index
	intVar = new IntegerVariables(cProb);
	// Create the pseudo-costs / reliability object
	rel = new ParReliability(intVar->nIntVar);

	if(logger->isActive()){
		logger->log(Logger::LOG_DEBUG, "Problem transmitted to solver.");
	}
}


template<class CutGen, class SolverImpl>
node_type_t ManagerCNB<CutGen, SolverImpl>::initialRelaxation(){
	int intVarPos;
	node_type_t nodeType = PRUNE;

	// Initial relaxation is done in initialization in order to memorize the
	// initial solution (and avoid a unneeded check in the main loop)

	// Create solver
	solverLog = new Logger(Logger::DISABLE);
	solver = new SolverImpl(solverLog, GPUS[0]);
	// set the initial problem to the solver
	solver->setProblem(cProb);

	// Solve the initial problem
	// If the solver found a solution, we are trying to create new node
	if(solver->solve() == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(iSol);

		// Init the cut generator, create the cuts and add them to the "current problem"
		addCuts();

		// Create cSol
		cSol = new SimplexSolution(cProb->nVar, cProb->nRow);
		cSol->objVal = iSol->objVal;
		memcpy(cSol->xVal, iSol->xVal, iSol->nVar*sizeof(VAR_TYPE));

		bSol = cSol->clone();

		// Init the branching strategy and node selection strategy
		bs->initBS(intVar, rel, problem, cSol);
		nss->initNSS(intVar, rel, problem, iSol);

		// set the root node value
		nss->setCurrentNodeObjValue(0, cSol->objVal);

		// choose next variable for branching
		nodeType = bs->selectNextVariable(intVarPos, cSol, cProb, solver);

		if(nodeType == LEAF) { // Integer solution
			bSol->copy(cSol); // best sol already found
			if(logger->isActive()){
				logger->log(Logger::LOG_DEBUG, "New best!");
				logger->log(Logger::LOG_DEBUG, bSol->toString());
			}
		} else if(nodeType == BRANCH) { // Has children probably
			nCreatedNode += nss->createNodes(0, intVarPos, cSol, cProb); // trying to create children
		} // else do nothing, we can't prune the root node!
	}

	// Solver is now useless
	delete solver;
	delete solverLog;

	return nodeType;
}

template<class CutGen, class SolverImpl>
void ManagerCNB<CutGen, SolverImpl>::addCuts() {

	// Clone the current problem
	SimplexProblem *relaxedProb = cProb->clone();
	SimplexProblem *cutProb = problem->clone();
	// Get the relaxed problem
	solver->getProblem(relaxedProb);
	// init the cut generator (use the relaxed simplex tableau)
	cg->init(problem, iSol, relaxedProb, intVar);
	// generate the cuts
	cg->generateCuts();
	// modify the current problem by adding the generated cuts
	cg->modifyProblem(cutProb);
	// Modify the current solver problem, with the new one
	solver->setProblem(cutProb);

	// Replace cProb by cutProb
	delete cProb;
	cProb = cutProb;

	delete relaxedProb;
}

template<class CutGen, class SolverImpl>
void ManagerCNB<CutGen, SolverImpl>::launchSolvers(){

	pthread_t thread; // manager pthread
	WorkerCNB<SolverImpl> workers[nWorker]; // workers
	cpu_set_t cpuset[nWorker+1]; // Cpu set

	//Setting self affinity (new) todo check if working
	thread = pthread_self();
	CPU_ZERO(&cpuset[nWorker]);
	CPU_SET(CPUS[nWorker],&cpuset[nWorker]);
	pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset[nWorker]);

	for(int i=0; i<nWorker; i++){
		workers[i].setDevice(i);
		workers[i].setProblem(cProb);
		workers[i].setProblemData(iSol, &bSolFound, bSol);
		workers[i].setBNBTools(intVar, nss, bs, rel);
		workers[i].setMngContext(&mngContext);
		workers[i].start();

		CPU_ZERO(&cpuset[i]);
		CPU_SET(CPUS[i],&cpuset[i]);
		pthread_setaffinity_np(workers[i].getPThread(), sizeof(cpu_set_t), &cpuset[i]);
	}
	logger->log(Logger::LOG_DEBUG, "Manager init finished!");
	// synchronise before starting
	pthread_barrier_wait(&mngContext.barrier);
	logger->log(Logger::LOG_DEBUG, "Manager started!");

	sem_wait(&mngContext.sem);
	while(!nss->isTerminated()){
		logger->log(Logger::LOG_DEBUG, "Manager got woke up!");
		// We have been awakened, this mean that a node has been solved
		nSolvedNode++;

		if(nSolvedNode % INFO_FREQUENCY == 0){
			ostringstream oss(ostringstream::out);
			oss << nSolvedNode << " solved nodes.";
			if(bSolFound)
				oss << " Best sol : " << fixed << bSol->objVal;
			else
				oss << " No best sol currently.";

			logger->log(Logger::LOG_INFO, oss.str());
		}
		// waiting until a worker has solved a node
		sem_wait(&mngContext.sem);
	}
	logger->log(Logger::LOG_DEBUG, "Manager terminated!");

	pthread_barrier_wait(&mngContext.barrier);

	for(int i=0; i<nWorker; i++){
		pthread_join (workers[i].getPThread(), NULL);
	}

}

template<class CutGen, class SolverImpl>
SimplexSolution * ManagerCNB<CutGen, SolverImpl>::cutAndBranch(SimplexProblem *inP){

	// init the problem, class, etc.
	initManagerCNB(inP);

	// Do the first relaxation and create first nodes
	if(initialRelaxation() == BRANCH) {
		// Launch solvers
		launchSolvers();
	}

	if(bSol != NULL){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished." << endl;
			oss << "Solution : " << bSol->objVal << endl;
			oss << bSol->toString() << endl;
			oss << "Total node created : " << nCreatedNode << endl;
			oss << "Total node solved : " << nSolvedNode << endl;
			oss << "Total node pruned : " << (nCreatedNode - nSolvedNode);
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	} else {
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished : no solution found." << endl;
			oss << "Total node created : " << nCreatedNode << endl;
			oss << "Total node solved : " << nSolvedNode << endl;
			oss << "Total node pruned : " << (nCreatedNode - nSolvedNode);
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	}
}

template<class CutGen, class SolverImpl>
unsigned int ManagerCNB<CutGen, SolverImpl>::getNSolvedNode(){
	return nSolvedNode;
}

template<class CutGen, class SolverImpl>
unsigned int ManagerCNB<CutGen, SolverImpl>::getNCreatedNode(){
	return nCreatedNode;
}

template<class CutGen, class SolverImpl>
unsigned int ManagerCNB<CutGen, SolverImpl>::getNInfNode(){
	return nInfNode;
}
