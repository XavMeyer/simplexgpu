//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Tree.h
 *
 *  Created on: Jun 16, 2011
 *      Author: meyerx
 */

#ifndef TREE_H_
#define TREE_H_

#include "../Node/NodePtr.h"

template <node_comparator_t NodeComp>
class Tree {
public:

	Node<NodeComp>* root;

	Tree(){
		nbNode = 1;
		root = new Node<NodeComp>();
	};

	virtual ~Tree(){
		deleteSubtree(root);
	};

	Node<NodeComp> * createChildLo(Node<NodeComp> *parent){
		nbNode++;
		parent->child_lo = new Node<NodeComp>(parent);
		return parent->child_lo;
	};

	Node<NodeComp> * createChildUp(Node<NodeComp> *parent){
		nbNode++;
		parent->child_up = new Node<NodeComp>(parent);
		return parent->child_up;
	};

	bool deleteLeaf(Node<NodeComp> *leaf){
		if((leaf->child_lo != NULL) || (leaf->child_up != NULL) || leaf == root){
			return false;
		}
		nbNode--;

		if(leaf->parent != NULL){
			leaf->parent->nChild--;
			if(leaf->isLowChild()){
				leaf->parent->child_lo = NULL;
			} else {
				leaf->parent->child_up = NULL;
			}
		}

		delete leaf;

		return true;
	};

	void deleteSubtree(Node<NodeComp> *parent){
		Node<NodeComp> *tmp = parent;
		Node<NodeComp> *leaf;

		do {
			if(tmp->child_lo != NULL){
				tmp = tmp->child_lo;
			} else if(tmp->child_up != NULL) {
				tmp = tmp->child_up;
			} else {
				if(tmp->parent != NULL){
					leaf = tmp;
					tmp = tmp->parent;
					this->deleteLeaf(leaf);
				}
			}
		} while(tmp != parent || parent->child_lo != NULL || parent->child_up != NULL);

		delete tmp;
	};

	void cleanTree(Node<NodeComp> *startNode){
		Node<NodeComp> *parent;

		if(startNode != NULL && startNode->nChild == 0){
			parent = startNode->parent;
			this->deleteLeaf(startNode);
			this->cleanTree(parent);
		}
	};

	bool deleteLeafAndCleanTree(Node<NodeComp> *leaf){
		Node<NodeComp> *parent = leaf->parent;
		if(!this->deleteLeaf(leaf)) return false;

		this->cleanTree(parent);

		return true;
	};

	unsigned int getNbNode(){
		return nbNode;
	}

private:
	unsigned int nbNode;
};

#endif /* TREE_H_ */
