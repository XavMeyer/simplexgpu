//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SortedNodes.h
 *
 *  Created on: Jun 16, 2011
 *      Author: meyerx
 */

#ifndef SORTEDNODESV2_H_
#define SORTEDNODESV2_H_

#include "../Node/NodePtr.h"
#include "Tree.h"
#include "../../Solver/SimplexSolution.h"
#include <vector>

template <node_comparator_t NodeComp>
class SortedNodesV2 {
public:

	SortedNodesV2(bool inIsCrescentOrder){
		isCrescentOrder = inIsCrescentOrder;
		//sorted = true;
	}

	virtual ~SortedNodesV2(){

		while(!nodes.empty()){
			//delete nodes.back().ptr;
			//nodes.pop_back();
			delete (*nodes.begin()).ptr;
			nodes.erase(nodes.begin());
		}
	}

	void push(Node<NodeComp> *element){
		//sorted = false;
		//nodes.push_back(NodePtr<NodeComp>(element));
		nodes.insert(NodePtr<NodeComp>(element));
	}

	/*void sort(){
		sorted = true;
		if(isCrescentOrder){
			nodes.sort(less<NodePtr<NodeComp> >());
		} else {
			nodes.sort(greater<NodePtr<NodeComp> >());
		}
	}*/

	Node<NodeComp> * popBest(){
		Node<NodeComp> *best;

		// check if empty
		if(nodes.empty()) return NULL;

		/*if(!sorted){
			this->sort();
		}

		// Get the ptr to the best node
		best = nodes.front().ptr;
		// Pop the reference to the best node
		nodes.pop_front();
		*/

		if(isCrescentOrder){
			best = (*nodes.begin()).ptr;
			nodes.erase(nodes.begin());
		} else {
			best = (*nodes.rbegin()).ptr;
			nodes.erase(--nodes.end());
		}

		return best;
	}

	Node<NodeComp> * best(){
		// check if empty
		if(nodes.empty()) return NULL;

		/*if(!sorted){
			this->sort();
		}

		return nodes.front().ptr;*/
		if(isCrescentOrder)
			return (*nodes.begin()).ptr;
		else
			return (*nodes.rbegin()).ptr;
	}

	Node<NodeComp> * popWorst(){
		Node<NodeComp> *worst;

		// check if empty
		if(nodes.empty()) return NULL;

		/*if(!sorted){
			this->sort();
		}

		// Get the ptr to the best node
		worst = nodes.back().ptr;
		// Pop the reference to the best node
		nodes.pop_back();*/
		if(isCrescentOrder){
			worst = (*nodes.rbegin()).ptr;
			nodes.erase(--nodes.end());
		} else {
			worst = (*nodes.begin()).ptr;
			nodes.erase(nodes.begin());
		}

		return worst;
	}

	Node<NodeComp> * worst(){
		// check if empty
		if(nodes.empty()) return NULL;

		/*if(!sorted){
			this->sort();
		}
		// Get the ptr to the best node
		return nodes.back().ptr;*/
		if(isCrescentOrder)
			return (*nodes.rbegin()).ptr;
		else
			return (*nodes.begin()).ptr;

	}

	Node<NodeComp> * end(){
		// check if empty
		if(nodes.empty()) return NULL;

		return (*--nodes.end()).ptr;
	}

	bool isEmpty(){
		return nodes.empty();
	}

	int getNbNodes(){
		return nodes.size();
	}

	void initIterator(){
		iter = nodes.begin();
	}

	Node<NodeComp> * getCurrentNode(){
		return (*iter).ptr;
	}

	void iterNextNode(){
		iter++;
	}

	void deleteCurrentNode(){
		typename nodes_ms_t::iterator tmp;
		tmp = iter;
		this->iterNextNode();
		nodes.erase(tmp);
		//sorted = false;
	}

private:

	typedef multiset<NodePtr<NodeComp> > nodes_ms_t;
	typename nodes_ms_t::iterator iter;

	//bool sorted;
	bool isCrescentOrder;
	nodes_ms_t nodes;

};

#endif /* SORTEDNODESV2_H_ */
