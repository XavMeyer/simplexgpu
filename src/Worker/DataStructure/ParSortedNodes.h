//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SortedNodes.h
 *
 *  Created on: Jun 16, 2011
 *      Author: meyerx
 */

#ifndef PARSORTEDNODES
#define PARSORTEDNODES

#include "SortedNodes.h"
#include <semaphore.h>

template <node_comparator_t NodeComp>
class ParSortedNodes : public SortedNodes<NodeComp> {
public:

	typedef SortedNodes<NodeComp> super;

	ParSortedNodes(bool inIsCrescentOrder) : SortedNodes<NodeComp>(inIsCrescentOrder){
		sem_init(&mutex, 1, 1);
	}

	virtual ~ParSortedNodes(){
	}

	void push(Node<NodeComp> *element){
		sem_wait(&mutex);
		super::push(element);
		sem_post(&mutex);
	}

	void sort(){
		sem_wait(&mutex);
		super::sort();
		sem_post(&mutex);
	}

	Node<NodeComp> * popBest(){
		Node<NodeComp> *node;
		sem_wait(&mutex);
		node = super::popBest();
		sem_post(&mutex);

		return node;
	}

	Node<NodeComp> * best(){

		Node<NodeComp> *node;
		sem_wait(&mutex);
		node = super::best();
		sem_post(&mutex);

		return node;
	}

	Node<NodeComp> * popWorst(){
		Node<NodeComp> *node;
		sem_wait(&mutex);
		node = super::popWorst();
		sem_post(&mutex);

		return node;

	}

	Node<NodeComp> * worst(){
		Node<NodeComp> *node;
		sem_wait(&mutex);
		node = super::worst();
		sem_post(&mutex);

		return node;
	}

	Node<NodeComp> * end(){
		Node<NodeComp> *node;
		sem_wait(&mutex);
		node = super::end();
		sem_post(&mutex);

		return node;
	}

	bool isEmpty(){
		bool isEmpty;
		sem_wait(&mutex);
		isEmpty = super::isEmpty();
		sem_post(&mutex);
		return isEmpty;
	}

private:

	sem_t mutex;

};

#endif /* PARSORTEDNODES */
