//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SortedNodes.h
 *
 *  Created on: Jun 16, 2011
 *      Author: meyerx
 */

#ifndef SORTEDNODES_H_
#define SORTEDNODES_H_

#include "../Node/NodePtr.h"
#include "Tree.h"
#include "../../Solver/SimplexSolution.h"
#include <vector>

template <node_comparator_t NodeComp>
class SortedNodes {
public:

	SortedNodes(bool inIsCrescentOrder){
		isCrescentOrder = inIsCrescentOrder;
		sorted = true;
	}

	virtual ~SortedNodes(){
		while(!nodes.empty()){
			delete nodes.back().ptr;
			nodes.pop_back();
		}
	}

	void push(Node<NodeComp> *element){
		sorted = false;
		nodes.push_back(NodePtr<NodeComp>(element));
	}

	void sort(){
		sorted = true;
		if(isCrescentOrder){
			nodes.sort(less<NodePtr<NodeComp> >());
		} else {
			nodes.sort(greater<NodePtr<NodeComp> >());
		}
	}

	Node<NodeComp> * popBest(){
		Node<NodeComp> *best;

		// check if empty
		if(nodes.empty()) return NULL;

		if(!sorted){
			this->sort();
		}

		// Get the ptr to the best node
		best = nodes.front().ptr;
		// Pop the reference to the best node
		nodes.pop_front();

		return best;
	}

	Node<NodeComp> * best(){
		// check if empty
		if(nodes.empty()) return NULL;

		if(!sorted){
			this->sort();
		}

		return nodes.front().ptr;
	}

	Node<NodeComp> * popWorst(){
		Node<NodeComp> *worst;

		// check if empty
		if(nodes.empty()) return NULL;

		if(!sorted){
			this->sort();
		}

		// Get the ptr to the best node
		worst = nodes.back().ptr;
		// Pop the reference to the best node
		nodes.pop_back();

		return worst;
	}

	Node<NodeComp> * worst(){
		// check if empty
		if(nodes.empty()) return NULL;

		if(!sorted){
			this->sort();
		}
		// Get the ptr to the best node
		return nodes.back().ptr;
	}

	Node<NodeComp> * end(){
		// check if empty
		if(nodes.empty()) return NULL;

		// Get the ptr to the best node
		return nodes.back().ptr;
	}

	bool isEmpty(){
		return nodes.empty();
	}

	int getNbNodes(){
		return nodes.size();
	}

	void initIterator(){
		iter = nodes.begin();
	}

	Node<NodeComp> * getCurrentNode(){
		return (*iter).ptr;
	}

	void iterNextNode(){
		iter++;
	}

	void deleteCurrentNode(){
		typename nodes_list_t::iterator tmp;
		tmp = iter;
		this->iterNextNode();
		nodes.erase(tmp);
		sorted = false;
	}

private:

	typedef list<NodePtr<NodeComp> > nodes_list_t;
	typename nodes_list_t::iterator iter;

	bool sorted;
	bool isCrescentOrder;
	nodes_list_t nodes;

};

#endif /* SORTEDNODES_H_ */
