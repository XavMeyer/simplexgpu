//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef WORKERCNB_H_
#define WORKERCNB_H_

#include "NodeSelection/ParNodeSelectionStrategy.h"
#include "Branching/ParBranchingStrategy.h"
#include "CutGenerator/cg_includes.h"

#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/simplex/Solver.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/utils/Logger.h"

#include "wtypes.h"

#include "cpgpu.cfg"


#define INFO_FREQUENCY		5000

template<class SolverImpl>
class WorkerCNB {
public:
	WorkerCNB();
	WorkerCNB(int device);
	WorkerCNB(Logger *inL, int device);

	virtual ~WorkerCNB();

	void start();

	void cutAndBranch();

	void setDevice(int inDevice);
	void setProblem(SimplexProblem *inP);
	void setProblemData(SimplexSolution *inISol, bool *inBSolFound, SimplexSolution *inBSol);
	void setBNBTools(IntegerVariables *inIntVar, ParNodeSelectionStrategy *inNSS, ParBranchingStrategy *inBS, ParReliability *inRel);
	void setMngContext(manager_context_t *inMngContext);

	pthread_t getPThread();

private:


	int workerID;

	pthread_t mThread;
	manager_context_t *mngContext;

	// has a best solution already found
	bool *bSolFound;

	// Node selection strategy
	ParNodeSelectionStrategy *nss;
	// Branching strategy
	ParBranchingStrategy *bs;

	// Simplex solver
	Solver *solver;

	// Logger for debug, info, etc. messages
	Logger *logger;
	Logger *solverLogger;

	// Initial problem
	SimplexProblem *problem;
	// Current problem
	SimplexProblem *cProb;

	// Integer variable indices
	IntegerVariables *intVar;
	// Pseudo-costs / Reliability
	ParReliability *rel;

	// current solution
	SimplexSolution *cSol;
	// best solution
	SimplexSolution *bSol;
	SimplexSolution *localBSol;
	// initial solution (first relaxation)
	SimplexSolution *iSol;
	SimplexSolution *_rSol;


	//! Init variables and classes.
	void init();

	//! Init the branch and bound algorithm.
	void initWorkerCNB();

	//! Solve a problem and update pseudocost if solving is successful.
	solver_res_t solve();

	static void * run(void * arg);

	void updateLocalBSol();

	void updateGlobalBSol();

	void DBG_init();
	void DBG_check(solver_res_t res);

};

#endif /* WORKERCNB_H_ */
