//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DefaultCutGen.cpp
 *
 *  Created on: Aug 10, 2011
 *      Author: meyerx
 */

#include "DefaultCutGen.h"

DefaultCutGen::DefaultCutGen(Logger *inLog) : super(inLog) {
}

DefaultCutGen::~DefaultCutGen() {
	cProb = NULL;
	iv = NULL;
}

void DefaultCutGen::generateCuts(){
	int nCuts;
	CMIRCutGen cmir(&cp, iProb, logger);
	GomoryCutGen gomory(&cp, iProb, logger);

	// Generate CMIR cuts
	nCuts = cmir.generateCuts(sol, iv, cProb);
	if(logger != NULL && logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << nCuts << " CMIR cuts generated.";
		logger->log(Logger::LOG_INFO, oss.str());
	}

	// Generate gomory cuts
	nCuts = gomory.generateCuts(sol, iv, cProb);
	if(logger != NULL && logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << nCuts << " gomory cuts generated.";
		logger->log(Logger::LOG_INFO, oss.str());
	}

	// Total nb of cuts generated
	if(logger != NULL && logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << cp.getNbCuts() << " cuts generated in total.";
		logger->log(Logger::LOG_INFO, oss.str());
	}
}
