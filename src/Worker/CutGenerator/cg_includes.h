//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * cg_includes.h
 *
 *  Created on: Jul 27, 2011
 *      Author: meyerx
 */

#ifndef CG_INCLUDES_H_
#define CG_INCLUDES_H_

#include "CutManagement/CutPool.h"
#include "CutManagement/CutSelector.h"

#include "CutMethods/CMIRCutGen.h"
#include "CutMethods/GomoryCutGen.h"

#include "CutGenerator.h"
#include "DefaultCutGen.h"

#endif /* CG_INCLUDES_H_ */
