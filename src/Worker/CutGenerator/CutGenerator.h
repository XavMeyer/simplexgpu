//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutGenerator.h
 *
 *  Created on: Aug 10, 2011
 *      Author: meyerx
 */

#ifndef CUTGENERATOR_H_
#define CUTGENERATOR_H_

#define MAX_CUT_TO_GENERATE			2000

#include "cg_includes.h"

#include "../../Solver/utils/Logger.h"
#include "../../Solver/SimplexProblem.h"
#include "../../Solver/SimplexSolution.h"

class CutGenerator {

public:
	CutGenerator(Logger *inLogger) : logger(inLogger), cp(MAX_CUT_TO_GENERATE)
	{
	}

	virtual ~CutGenerator() {
		logger = NULL;
		iProb = NULL;
		sol = NULL;
	}

	/*!
	 * This function init the cut generator
	 * \param[in]	inIProb 	Initial problem
	 * \param[in]	inSol		Solution after relaxation
	 * \param[in]	inCProb 	Problem after relaxation
	 * \param[in]	inIV		Integer variables index
	 */
	void init(SimplexProblem *inIProb, SimplexSolution *inSol,
			  SimplexProblem *inCProb, IntegerVariables *inIV) {
		iProb = inIProb;
		sol =  inSol;
		cProb = inCProb;
		iv = inIV;
	}

	/*!
	 * This function must be implemented.
	 * It should instanced the cuts methods and call their generate cuts method.
	 */
	virtual void generateCuts() = 0;

	/*!
	 * This function select the best cuts and add them to a given problem.
	 * \param[int,out]	prob	The problem we want to add the cut to.
	 */
	void modifyProblem(SimplexProblem *prob){
		CutSelector *cs;
		vector<Inequality*> *cuts;

		// If cuts have been generated
		if(cp.getNbCuts() < 1)
			return;

		// Create the cut selector and get the selected cuts
		cs = new CutSelector(&cp);
		cuts = cs->selectCuts(iProb, sol);
		if(logger != NULL && logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << cuts->size() << " cuts selected.";
			logger->log(Logger::LOG_INFO, oss.str());
		}

		// Add the cut to the problem given in entry
		if(!cuts->empty()){
			prob->addRows(cuts);
		}

		cuts = NULL;
		delete cs;
	}

protected:

	// Logger for the cut generator
	Logger *logger;
	// Contains all the cut generated
	CutPool cp;

	// Problem data
	SimplexProblem *iProb, *cProb;
	SimplexSolution *sol;
	IntegerVariables *iv;


};


#endif /* CUTGENERATOR_H_ */
