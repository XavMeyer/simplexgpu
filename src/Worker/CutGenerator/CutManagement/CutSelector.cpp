//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutSelector.cpp
 *
 *  Created on: Jul 25, 2011
 *      Author: meyerx
 */

#include "CutSelector.h"

CutSelector::CutSelector(CutPool *inCp) {

	vector<Inequality*>::iterator it;
	vector<Inequality*> *cutPoolPtr = inCp->getCuts();

	// Copy the ptr of each cut into the cutPoolCp
	for(it = cutPoolPtr->begin(); it != cutPoolPtr->end(); it++){
		cutPoolCp.push_back(CutScore(*it));
	}

	finalCuts = NULL;
}

CutSelector::~CutSelector() {

	if(finalCuts != NULL)
		delete finalCuts;
}

void CutSelector::initScores(SimplexProblem *iProb, SimplexSolution *sol){

	list<CutScore>::iterator it;
	double objNorm = cblas_dnrm2(iProb->nVar, iProb->objFunc, 1);

	// process Efficacy and objective function parallelism
	// Orthogonality is set to 1 by default, since it
	// varies in function of selected cuts
	for(it = cutPoolCp.begin(); it != cutPoolCp.end(); it++){
		(*it).processEfficacy(sol);
		(*it).processObjParallelism(iProb->objFunc, objNorm);
	}

	// remove element with e_r < MINEFF (lower than EPS2 efficacity)
	cutPoolCp.remove_if(isLTMinEfficacity());
}

void CutSelector::updateScore(CutScore *selectedCut){
	list<CutScore>::iterator it, tmp;

	// return if there are no cuts in cutPoolCp
	if(cutPoolCp.empty()) return;

	// Update orthogonality of each cut in function of "selectedCut"
	for(it = cutPoolCp.begin(); it != cutPoolCp.end(); it++){
		// Update orthogonality
		(*it).updateOrthogonality(selectedCut);
	}

	// remove element with o_r < MINORTHO
	cutPoolCp.remove_if(isLTMinOrtho());
}

vector<Inequality*> * CutSelector::selectCuts(SimplexProblem *iProb, SimplexSolution *sol){
	vector<CutScore>::iterator it;
	finalCuts = new vector<Inequality*>();

	// init. the scores of each cut (from lesser to greater)
	initScores(iProb, sol);

	// While we have not reached the nb max of cuts and we have still cuts to add do:
	while(selectedCut.size() < MAXSEPACUTS && !cutPoolCp.empty()){
		// Sort cuts by score (required to be done each time since o_r is updated)
		cutPoolCp.sort();
		// Take the best (at CutPoolCp since lesser<cutScore> is applied)
		// and remove it from cutPoolCp
		selectedCut.push_back(cutPoolCp.back());
		cutPoolCp.pop_back();
		//cout << "Cut efficacity : " << selectedCut.back().getEfficacity() << endl;

		// Update orthogonality for each row and thus, score
		updateScore(&selectedCut.back());
	}

	// Return a ptr of a vector of cut (removing score info)

	for(it = selectedCut.begin(); it != selectedCut.end(); it++)
		finalCuts->push_back((*it).getCutPtr());

	return finalCuts;

}
