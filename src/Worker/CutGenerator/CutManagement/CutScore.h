//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutScore.h
 *
 *  Created on: Jul 25, 2011
 *      Author: meyerx
 */

#ifndef CUTSCORE_H_
#define CUTSCORE_H_

#include "../../../Solver/Inequality.h"
#include "../../../Solver/SimplexSolution.h"

#include "cblas.h"
#include <assert.h>

#define WEIGHT_EFFICACITY				1.0
#define WEIGHT_OBJ_PARALLELISM			0.1
#define WEIGHT_ORTHOGONALITY			1.0

// With MINORTHO we accept plane that have at least a pi/3 (60 deg) with the others
#define MINORTHO						0.5

// Only cuts having an efficacity greater than EPS2 are accepted
#define MINEFF							EPS2

class CutScore {
public:

	CutScore(Inequality *inCutPtr);
	virtual ~CutScore();

	void processEfficacy(SimplexSolution *sol);
	void processObjParallelism(VAR_TYPE *objCoeff, double objNorm);
	void updateOrthogonality(CutScore *selected);

	double getCutNorm();
	double getOrthogonality();
	Inequality* getCutPtr();
	double getEfficacity();

	double getScore() const;
	bool operator < (const CutScore &rhs) const;
	bool operator > (const CutScore &rhs) const;

	bool isLesserThanMinOrtho() const;
	bool isLesserThanMinEfficacity() const;

private:

	// Pointer on cut
	Inequality* cutPtr;
	// Efficacity, parallelism, orthogonality
	double e, p, o;
	double cutNorm;

	void processCutNorm();
};

#endif /* CUTSCORE_H_ */
