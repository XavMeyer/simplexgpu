//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutPool.h
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#ifndef CUTPOOL_H_
#define CUTPOOL_H_

#include "../../../Solver/Inequality.h"
#include "../../../Solver/utils/Logger.h"
#include "../../../Solver/SimplexProblem.h"
#include "../../../Solver/SimplexSolution.h"

#include <vector>
using namespace std;

class CutPool {
public:
	CutPool(unsigned int inMaxCuts);
	virtual ~CutPool();

	bool addCut(Inequality *inCut);
	int getNbCuts();

	bool DBG_checkCuts(SimplexSolution *sol);

	vector<Inequality*> * getCuts();

	string toString();

private:

	unsigned int maxCuts;
	vector<Inequality*> cuts;

};

#endif /* CUTPOOL_H_ */
