//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutSelector.h
 *
 *  Created on: Jul 25, 2011
 *      Author: meyerx
 */

#ifndef CUTSELECTOR_H_
#define CUTSELECTOR_H_

#define MAXSEPACUTS		2000

#include "CutPool.h"
#include "CutScore.h"

#include "cblas.h"

#include <algorithm>
#include <vector>
#include <list>
using namespace std;

class CutSelector {
public:
	CutSelector(CutPool *inCp);
	virtual ~CutSelector();

	vector<Inequality*> * selectCuts(SimplexProblem *iProb, SimplexSolution *sol);

private:

	list<CutScore> cutPoolCp;
	vector<CutScore> selectedCut;
	CutPool *cp;
	vector<Inequality*> *finalCuts;

	void initScores(SimplexProblem *iProb, SimplexSolution *sol);
	void updateScore(CutScore *selectedCut);

	class isLTMinOrtho {
		public:
		bool operator() (const CutScore& cs) {return cs.isLesserThanMinOrtho(); }
	};

	class isLTMinEfficacity {
	public:
		bool operator() (const CutScore& cs) {return cs.isLesserThanMinEfficacity();}
	};

};

#endif /* CUTSELECTOR_H_ */
