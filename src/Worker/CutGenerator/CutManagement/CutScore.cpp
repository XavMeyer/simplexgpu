//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutScore.cpp
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#include "CutScore.h"

CutScore::CutScore(Inequality *inCutPtr) : cutPtr(inCutPtr){
	e = p = 0.0;
	o = 1.0;
	processCutNorm();
}

CutScore::~CutScore(){
	cutPtr = NULL;
}

double CutScore::getScore() const {
	double score;
	score = WEIGHT_EFFICACITY*e + WEIGHT_OBJ_PARALLELISM*p + WEIGHT_ORTHOGONALITY*o;
	return score;
}

bool CutScore::operator < (const CutScore &rhs) const
{
	return getScore() < rhs.getScore();
}

bool CutScore::operator > (const CutScore &rhs) const
{
	return getScore() > rhs.getScore();
}

bool  CutScore::isLesserThanMinOrtho() const
{
	return o < MINORTHO;
}


bool CutScore::isLesserThanMinEfficacity() const {
	return e < MINEFF;
}

void CutScore::processCutNorm(){
	// Norm
	cutNorm = cblas_dnrm2(cutPtr->nLHS, cutPtr->coeffLHS, 1);
	assert(cutNorm != 0.0);
}

void CutScore::processEfficacy(SimplexSolution *sol){

	// e = trans(d_r)*x
	e = cblas_ddot(sol->nVar, sol->xVal, 1, cutPtr->coeffLHS, 1);
	//cout << "LHS value : " << e << " - RHS value : " << cutPtr->rhs << " - cut norm : " << cutNorm << endl;

	// e = trans(d_r)*x - rhs (how much this row is violated?)
	e -= cutPtr->rhs;

	// e = (trans(d_r)*x - rhs)/||d_r||
	e /= cutNorm;

}

void CutScore::processObjParallelism(VAR_TYPE *objCoeff, double objNorm){

	// p = trans(d_r)*c
	p = cblas_ddot(cutPtr->nLHS, cutPtr->coeffLHS, 1, objCoeff, 1);

	// p = |trans(d_r)*c|
	p = fabs(p);

	// p = (trans(d_r)*c)/(||d_r|| * || c ||)
	p /= (cutNorm * objNorm);
}

void CutScore::updateOrthogonality(CutScore *selectedCut){
	double tmp;

	// tmp = trans(d*_r)*d_r
	tmp = cblas_ddot(cutPtr->nLHS, cutPtr->coeffLHS, 1, selectedCut->cutPtr->coeffLHS, 1);

	// tmp = 1-|trans(d*_r)*d_r|/(|| d*_r || * || d_r ||)
	tmp = 1.0-(fabs(tmp)/(cutNorm*selectedCut->cutNorm));

	// min(o_r, 1-|trans(d*_r)*d_r|/(|| d*_r || * || d_r ||))
	o = o < tmp ? o : tmp;
}

double CutScore::getCutNorm(){
	return cutNorm;
}

double CutScore::getOrthogonality(){
	return o;
}

Inequality* CutScore::getCutPtr(){
	return cutPtr;
}

double CutScore::getEfficacity(){
	return e;
}
