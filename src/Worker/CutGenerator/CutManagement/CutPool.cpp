//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CutPool.cpp
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#include "CutPool.h"

CutPool::CutPool(unsigned int inMaxCuts) {

	maxCuts = inMaxCuts;

}

CutPool::~CutPool() {
	Inequality *temp;

	while(!cuts.empty()){
		temp = cuts.back();
		cuts.pop_back();
		delete temp;
	}
}

bool CutPool::addCut(Inequality *inCut){

	if(cuts.size() >= maxCuts)
		return false;

	cuts.push_back(inCut);

	return true;
}

bool CutPool::DBG_checkCuts(SimplexSolution *sol){
	vector<Inequality*>::iterator it;

	// Process the lhs with only the decision variable
	for(it = cuts.begin(); it != cuts.end(); it++){
		if(!(*it)->DBG_check(sol)) {
			cuts.erase(it);
			return false;
		}
	}

	return true;
}

vector<Inequality*> * CutPool::getCuts(){
	return &cuts;
}

int CutPool::getNbCuts() {
	return cuts.size();
}

string CutPool::toString(){
	vector<Inequality*>::iterator it;
	ostringstream oss(ostringstream::out);

	oss << "Generated cuts : " << endl;

	if(cuts.empty()){
		oss << "No cuts!" << endl;
		return oss.str();
	}

	for(unsigned int i=0; i<cuts.front()->nLHS; i++){
		oss << "x[" << i << "]\t";
	}
	oss << "[RHS]" << endl;


	for(it = cuts.begin(); it != cuts.end(); it++){
		oss << (*it)->toString() << endl;
	}

	return oss.str();
}
