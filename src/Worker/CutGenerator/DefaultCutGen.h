//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DefaultCutGen.h
 *
 *  Created on: Aug 10, 2011
 *      Author: meyerx
 */

#ifndef DEFAULTCUTGEN_H_
#define DEFAULTCUTGEN_H_

#include "CutGenerator.h"

#include "../Misc/IntegerVariables.h"

class DefaultCutGen : public CutGenerator {
public:
	DefaultCutGen(Logger *inLog);
	virtual ~DefaultCutGen();

	void init(SimplexProblem *inIProb, SimplexSolution *inSol,
			  SimplexProblem *inCProb, IntegerVariables *inIV);

	void generateCuts();

private:
	// Define parent class
	typedef CutGenerator super;

};

#endif /* DEFAULTCUTGEN_H_ */
