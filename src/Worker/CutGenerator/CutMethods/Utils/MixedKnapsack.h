//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MixedKnapsack.h
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#ifndef MIXEDKNAPSACK_H_
#define MIXEDKNAPSACK_H_

#include "cblas.h"
#include "../../../../Solver/SimplexProblem.h"

#include <string.h>
#include <iostream>
#include <sstream>

class MixedKnapsack {
public:

	unsigned int n, m;

	VAR_TYPE alpha0;

	VAR_TYPE *xStar;
	VAR_TYPE *alpha;
	float *bound;
	unsigned int *xInd;

	VAR_TYPE *yStar;
	VAR_TYPE *gamma;
	unsigned int *yInd;
	VAR_TYPE sStar;

	MixedKnapsack(const MixedKnapsack &toCopy){
		copy(toCopy);
	}

	MixedKnapsack(unsigned int inN, unsigned int inM){

		n = inN;
		m = inM;

		xStar = new VAR_TYPE[n];
		alpha = new VAR_TYPE[n];
		bound = new float[n];
		xInd = new unsigned int[n];

		yStar = new VAR_TYPE[m];
		gamma = new VAR_TYPE[m];
		yInd = new unsigned int[m];

		initVals();
	}

	virtual ~MixedKnapsack(){
		delete [] xStar;
		delete [] alpha;
		delete [] bound;
		delete [] xInd;

		delete [] yStar;
		delete [] gamma;
		delete [] yInd;
	}

	void initVals(){
		alpha0 = 0;
		sStar = 0;

		for(unsigned int i=0; i<n; i++){
			xStar[i] = 0;
			alpha[i] = 0;
			bound[i] = 0;
		}

		for(unsigned int i=0; i<m; i++){
			yStar[i] = 0;
			gamma[i] = 0;
		}
	}

	VAR_TYPE processS(VAR_TYPE* yVal){
		VAR_TYPE outS = 0.0;

		outS = cblas_ddot(m, gamma, 1, yVal, 1);
		/*for(unsigned int i=0; i<m; i++){
			if(gamma[i] < 0)
				outS -= gamma[i]*yVal[i];
		}*/

		return outS;
	}

	void processSStar(){
		sStar = processS(yStar);
	}

	void copy(const MixedKnapsack &toCopy){
		n = toCopy.n;
		m = toCopy.m;

		alpha0 = toCopy.alpha0;

		sStar = toCopy.sStar;

		for(unsigned int i=0; i<n; i++){
			xStar[i] = toCopy.xStar[i];
			alpha[i] = toCopy.alpha[i];
			xInd[i] = toCopy.xInd[i];
			bound[i] = toCopy.bound[i];
		}

		for(unsigned int i=0; i<m; i++){
			yStar[i] = toCopy.yStar[i];
			yInd[i] = toCopy.yInd[i];
			gamma[i] = toCopy.gamma[i];
		}
	}

	bool isViolated(){
		VAR_TYPE rhs, lhs;
		rhs = 0;

		rhs = cblas_ddot(n, alpha, 1, xStar, 1);
		/*for(unsigned int i=0; i<n; i++)
			rhs += alpha[i] * xStar[i];*/

		lhs = alpha0 + sStar;

		return rhs > lhs;
	}

	string toString(){
		ostringstream oss(ostringstream::out);
		oss << "Xmk : " << endl;
		for(unsigned int i=0; i<n; i++)
			oss << "x" << i << "(x" << xInd[i] << ")\t";
		for(unsigned int i=0; i<m; i++)
					oss << "y" << i << "(x" << yInd[i] << ")\t";
		oss << "RHS" << endl;

		for(unsigned int i=0; i<n; i++)
			oss << alpha[i] << "\t";
		for(unsigned int i=0; i<m; i++)
			oss << gamma[i] << "\t";
		oss << alpha0 << endl;

		for(unsigned int i=0; i<n; i++)
			oss << xStar[i] << "\t";
		for(unsigned int i=0; i<m; i++)
			oss << yStar[i] << "\t";

		return oss.str();
	}

};

#endif /* MIXEDKNAPSACK_H_ */
