//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * RowScore.h
 *
 *  Created on: Aug 1, 2011
 *      Author: meyerx
 */

#ifndef ROWSCORE_H_
#define ROWSCORE_H_

class RowScore {
public:
	RowScore() {
		aggCount = 0;
		dualScore = density = tightness = 0.0;
	}
	virtual ~RowScore(){}

	double getScore() const {
		return pow(0.9, aggCount)*(dualScore+density+tightness);
	}

	bool operator < (const RowScore &rhs) const
	{
		return getScore() < rhs.getScore();
	}

	bool operator > (const RowScore &rhs) const
	{
		return getScore() > rhs.getScore();
	}

	double dualScore, density, tightness;
	int aggCount;
};

#endif /* ROWSCORE_H_ */
