//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * RowAggregator.cpp
 *
 *  Created on: Aug 1, 2011
 *      Author: meyerx
 */

#include "RowAggregator.h"

RowAggregator::RowAggregator(SimplexProblem *inIProb, SimplexSolution *inSol, SimplexProblem *inCProb){

	//curP = inCurP; // todo fix if not using init pb
	iProb = inIProb;
	cProb = inCProb;
	sol = inSol;

	nTotalAgg = 0;

	initAggregator();

}

RowAggregator::~RowAggregator() {

	iProb = NULL;

	if(rowScores != NULL)
		delete [] rowScores;

}

void RowAggregator::initAggregator(){

	locateContinuousVar();
	initRowScores();

}

void RowAggregator::locateContinuousVar(){
	for(int i=0; i<iProb->nVar; i++){
		// If it is not an integer bound, variable is continuous
		if(!iProb->isBoundInteger(i)){
			contVar.push_back(ContVarInd(i, i, false));
			setContVarDistance(&contVar.back());
		}
	}
	for(int i=iProb->nVar; i<iProb->nSlack+iProb->nVar; i++){
		// If it is not an integer bound, variable is continuous
		if(!iProb->isBoundInteger(i)){
			contVar.push_back(ContVarInd(i, i-iProb->nVar, true));
			setContVarDistance(&contVar.back());
		}
	}
}

void RowAggregator::setContVarDistance(ContVarInd *cvi){
	float lowerD, upperD;
	 VAR_TYPE val;
	 float2 bound;

	// Get variable bound and value
	if(cvi->basic){
		val = sol->sVal[cvi->pos];
		bound = iProb->bBounds[cvi->pos];
	} else {
		val = sol->xVal[cvi->pos];
		bound = iProb->xBounds[cvi->pos];
	}

	lowerD = val - bound.x; // process lower distance
	upperD = bound.y - val; // process upper distance

	// Store the minimum of both
	cvi->dist = lowerD < upperD ? lowerD : upperD;
}

void RowAggregator::initRowScores(){

	int ind;
	double objCoeffNRM = cblas_dnrm2(iProb->nVar, iProb->objFunc, 1);

	generateDualSol();

	rowScores = new RowScore[iProb->nRow];

	for(int i=0; i<iProb->nRow; i++){
		// Process the dual sol score
		rowScores[i].dualScore = dualSol->xVal[i]/objCoeffNRM;
		// Take the max between the dual sol score and 0.0001
		rowScores[i].dualScore = rowScores[i].dualScore > 0.0001 ? rowScores[i].dualScore : 0.0001;
		// Take into account the coeff
		rowScores[i].dualScore *= DUAL_SCORE_COEFF;
	}

	ind = 0; // Index in the eqs array
	for(int j=0; j<iProb->nVar; j++){
		for(int i=0; i<iProb->nRow; i++){ // check coeff c_ij (row i, col j)
			if(fabs(iProb->eqs[ind]) > EPS1){ // update density of row i and norm (used for tightness)
				rowScores[i].density += 1.0;
				rowScores[i].tightness += pow(iProb->eqs[ind], 2.0);
			}
			ind++;
		}
	}

	for(int i=0; i<iProb->nRow; i++){
		// Build the norm
		rowScores[i].tightness = sqrt(rowScores[i].tightness);
		// Get the max of norm and 0.1
		rowScores[i].tightness = rowScores[i].tightness > 0.1 ? rowScores[i].tightness : 0.1;
		// 1-y_si/max(||r||, 0.1)
		rowScores[i].tightness = 1-(sol->sVal[i]/rowScores[i].tightness);
		// Take into account the coeff
		rowScores[i].tightness *= TIGHTNESS_COEFF;

		//process density (1-dens_i)
		rowScores[i].density = 1-(rowScores[i].density/iProb->nVar);
		rowScores[i].density *= DENSITY_COEFF;
	}
}

void RowAggregator::generateDualSol() {
	 dualSol = new SimplexSolution(cProb->nSlack, cProb->nVar);

	for(int i=0; i<cProb->nSlack; i++){
		if(cProb->basis[i] < cProb->nVar)
			dualSol->sVal[cProb->basis[i]] = 0.0;
		else
			dualSol->xVal[cProb->basis[i]-cProb->nVar] = 0.0;
	}

	for(int i=0; i<cProb->nVar; i++){
		if(cProb->xInd[i] < cProb->nVar)
			dualSol->sVal[cProb->xInd[i]] = -cProb->objFunc[i];
		else
			dualSol->xVal[cProb->xInd[i]-cProb->nVar] = -cProb->objFunc[i];
	}

	dualSol->objVal = cblas_ddot(dualSol->nVar, dualSol->xVal, 1, &iProb->eqs[IDX2C(0, cProb->nCol-1, cProb->nRow)], 1);
}


void RowAggregator::setFirstRow(int ind) {
	// Init aggregated row set
	currentlyAggRow.clear();
	currentlyAggRow.insert(ind);
	minCoeff = MAX_TYPE;
	maxCoeff = 0.0;
}

bool RowAggregator::aggregateRow(Inequality *aggRow) {
	int iCV;
	int bestRow = -1;
	double bestAggScore = -DBL_MAX;
	VAR_TYPE pCoeff;
	priority_queue<ContVarDist, vector<ContVarDist>, greater<ContVarDist> > pq;

	// Create the set of potential variable that can be used to aggregate another row
	for(unsigned int i=0; i<contVar.size(); i++){
		/*if((contVar[i].dist > EPS1) && !contVar[i].basic){
			cout << "["<< i << "] Distance not null and not basic." << endl;
			if(fabs(aggRow->coeffLHS[contVar[i].index]) > EPS2)
				cout << "["<< contVar[i].index << "] Distance and coeff not null." << endl;
		}*/
		// If distance is not null, coeff_i not null and there is a row that can be aggregated..
		if((contVar[i].dist > EPS1) && (fabs(aggRow->coeffLHS[contVar[i].index]) > EPS2) && existAggRow(&contVar[i])){
			// Insert the variable index and distance into a priority queue sorted from the greater dist to the lesser one
			pq.push(ContVarDist(i, contVar[i].dist));
			//cout << "Variable " << i << " can be aggregated." << endl;
		}
	}

	// Go through all the variable in the priority queue
	while(!pq.empty()){
		iCV = pq.top().ind;
		// For each row
		for(int i=0; i<iProb->nRow; i++){
			// Check if the row is not already in the aggregated row
			if(isRowAggregated(i))
				continue;

			// If variable is basic in current row
			if((contVar[iCV].basic && (i == contVar[iCV].pos)) ||
			// or variable is non basic but has a coefficient non null
			   (fabs(iProb->eqs[IDX2C(i, contVar[iCV].pos, iProb->nRow)]) > EPS2)){

				// Check if the the pivot coeff ratio isn't too big todo check that
				if(contVar[iCV].basic)
					pCoeff = fabs(aggRow->coeffLHS[contVar[iCV].index]);
				else
					pCoeff = fabs(aggRow->coeffLHS[contVar[iCV].index]/iProb->eqs[IDX2C(i, contVar[iCV].pos, iProb->nRow)]);

				if((maxCoeff/pCoeff > PIVOT_COEFF_TOLERANCE) || (pCoeff/minCoeff) > PIVOT_COEFF_TOLERANCE)
					continue;

				// Then if the distance is row score is the best encountered
				if(rowScores[i].getScore() > bestAggScore){
					// Memorize row index and score
					bestRow = i;
					bestAggScore = rowScores[i].getScore();
				}
			}
		}

		// Have we found a row ?
		if(bestRow >= 0) {
			// aggregate the row
			doAggregation(bestRow, &contVar[iCV], aggRow);
			// increment row aggregation counter
			rowScores[bestRow].aggCount++;
			// add the row index into the current aggregated row set
			currentlyAggRow.insert(bestRow);
			// Aggregation counter
			nTotalAgg++;
			// Aggregation done correctly, we return true
			return true;
		}
		pq.pop();
	}

	// No aggregation done, we return false
	return false;
}



bool RowAggregator::existAggRow(ContVarInd *cvi){

	// if the continuous variable is basic
	if(cvi->basic){
		//cout << "Basic! : " << cvi->pos << endl;
		// and the row is already aggregated
		if(isRowAggregated(cvi->pos))
			return false; // Then it variable can't be used for aggregation
		else // else (row not aggregated)
			return true; // Variable can be used for aggregation
	}


	// Variable is not basic, we have to check each rows
	for(int i=0; i<iProb->nRow; i++){
		// Check if the row is not already in the aggregated row
		if(isRowAggregated(i))
			continue; // Row is aggregated, it is then skipped

		// Row is not aggregated then we check its coefficient for variable cvi
		if(fabs(iProb->eqs[IDX2C(i, cvi->pos, iProb->nRow)]) > EPS2){
			return true;
		}
	}

	// No coefficient found
	return false;
}

void RowAggregator::doAggregation(int iRow, ContVarInd *cvi, Inequality *aggRow){
	VAR_TYPE coeff;

	// Get the pivot coeff
	if(cvi->basic){
		coeff = -aggRow->coeffLHS[cvi->index];
	} else {
		coeff = -(aggRow->coeffLHS[cvi->index]/iProb->eqs[IDX2C(iRow, cvi->pos, iProb->nRow)]);
	}

	// Todo check if working
	minCoeff = minCoeff < fabs(coeff) ? minCoeff : fabs(coeff);
	maxCoeff = maxCoeff > fabs(coeff) ? maxCoeff : fabs(coeff);

	// Copy current x_j coefficient a_ij
	for(int j=0; j<iProb->nVar; j++){
		aggRow->coeffLHS[j] += coeff*iProb->eqs[IDX2C(iRow,j,iProb->nRow)];
	}

	// add basis variable coefficient
	aggRow->coeffLHS[iProb->basis[iRow]] += coeff*1.0;
	// add RHS value
	aggRow->rhs = iProb->eqs[IDX2C(iRow, iProb->nCol-1, iProb->nRow)];
}

bool RowAggregator::isRowAggregated(int iRow){
	return currentlyAggRow.find(iRow) != currentlyAggRow.end();
}


unsigned int RowAggregator::getNTotalAgg(){
	return nTotalAgg;
}
