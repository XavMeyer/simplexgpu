//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * RowAggregator.h
 *
 *  Created on: Aug 1, 2011
 *      Author: meyerx
 */

#ifndef ROWAGGREGATOR_H_
#define ROWAGGREGATOR_H_

#include "../../../../Solver/SimplexProblem.h"
#include "../../../../Solver/SimplexSolution.h"
#include "RowScore.h"

#include <set>
#include <vector>
#include <queue>
using namespace std;

#define DUAL_SCORE_COEFF 			1.0
#define DENSITY_COEFF 				0.0001
#define TIGHTNESS_COEFF 			0.001

#define PIVOT_COEFF_TOLERANCE		10000.0

class RowAggregator {
public:
	RowAggregator(SimplexProblem *inIProb, SimplexSolution *inSol, SimplexProblem *cProb);
	virtual ~RowAggregator();

	void setFirstRow(int ind);
	bool aggregateRow(Inequality *aggRow);

	unsigned int getNTotalAgg();

private:

	class ContVarInd {
	public:
		int index, pos;
		bool basic;
		float dist;
		ContVarInd(int inIndex, int inPos, bool inBasic) : index(inIndex), pos(inPos), basic(inBasic) {
			dist = 0.0;
		}
		virtual ~ContVarInd(){}
	};

	class ContVarDist {
	public:
		int ind;
		float dist;
		ContVarDist(int inCVI, float inDist) : ind(inCVI), dist(inDist) {}
		virtual ~ContVarDist(){}
		bool operator > (const ContVarDist &rhs) const
		{
			return dist > rhs.dist;
		}
	};

	unsigned int nTotalAgg;
	VAR_TYPE maxCoeff, minCoeff;
	set<int> currentlyAggRow;
	vector<ContVarInd> contVar;
	RowScore *rowScores;
	SimplexProblem *iProb, *cProb;
	SimplexSolution *sol;
	SimplexSolution *dualSol;

	void initAggregator();
	void locateContinuousVar();
	void initRowScores();
	void generateDualSol();
	void setContVarDistance(ContVarInd *cvi);
	bool existAggRow(ContVarInd *cvi);
	void doAggregation(int iRow, ContVarInd *cvi, Inequality *aggRow);
	bool isRowAggregated(int iRow);
};

#endif /* ROWAGGREGATOR_H_ */
