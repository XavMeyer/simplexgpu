//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * GomoryCutGen.cpp
 *
 *  Created on: Aug 5, 2011
 *      Author: meyerx
 */

#include "GomoryCutGen.h"

GomoryCutGen::GomoryCutGen(CutPool *inCp, SimplexProblem *inIProb) {
	cp = inCp;
	iProb = inIProb;
	nGenCut = 0;
	logger = NULL;
}

GomoryCutGen::GomoryCutGen(CutPool *inCp, SimplexProblem *inIProb, Logger *inLog) {
	cp = inCp;
	iProb = inIProb;
	nGenCut = 0;
	logger = inLog;
}

GomoryCutGen::~GomoryCutGen() {
	cp = NULL;
	iProb = NULL;
}

int GomoryCutGen::generateCuts(SimplexSolution *sol, IntegerVariables *intVar, SimplexProblem *cProb) {
	int iAgg;
	bool rowAggregated;
	int nVariables = iProb->nVar+iProb->nSlack; // Slack + decision var
	Inequality extendedRow(nVariables);
	VAR_TYPE val[nVariables];
	float2 bound[nVariables];
	Inequality extendedCut(nVariables);
	Inequality *finalCut = NULL;
	RowAggregator rowAgg(iProb, sol, cProb);

	// Copy the variable values and bound
	copyValnBound(val, bound, sol);

	for(unsigned int i=0; i<iProb->nRow; i++) {

		// Check if the basic variable of this row is an integer variable with a non integral result
		if(!iProb->isBoundInteger(cProb->basis[i]) || sol->isIntegral(cProb->basis[i]))
			continue;

		//if(logger != NULL) logger->log(Logger::LOG_DEBUG, "***************************");

		// Copy cut into currentCut
		copyRow(i, &extendedRow, cProb);
		//if(logger != NULL) logger->log(Logger::LOG_DEBUG, extendedRow.toString());
		// Init the row aggregator for this row
		rowAggregated = true;
		rowAgg.setFirstRow(i);

		iAgg = 0;
		while((iAgg < (AGGREGATION_MAX+1)) && rowAggregated){
			// Complement variable at their upper bound
			boundSubstitution(&extendedRow, val, bound, intVar, &extendedCut);

			// Create gomory MIR cut and add it if working
			if(createGomoryMIRCut(i, &extendedCut, intVar, cProb) && isViolated(&extendedCut, sol)){
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, extendedCut.toString());
				// Reverse the substitution made previously on the bound
				reverseBoundSubstitution(&extendedCut, val, bound, intVar);
				// Create the cut in the form of the initial problem
				finalCut = new Inequality(iProb->nVar);
				slacksSubstitution(&extendedCut, finalCut);
				// Add the cut to the cut pool
				if(!cp->addCut(finalCut))
					return nGenCut; // Max nb of cut reached
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, finalCut->toString());
				nGenCut++;
				break; // leaving the while loop, we have found a cut
			}
			iAgg++; // increment aggregation counter
			// If we can aggregated more row
			if(iAgg < (AGGREGATION_MAX+1)){
				rowAggregated = rowAgg.aggregateRow(&extendedRow);
				/*if(rowAggregated) {
					if(logger != NULL) logger->log(Logger::LOG_DEBUG, "Row aggregated.");
				} else {
					if(logger != NULL) logger->log(Logger::LOG_DEBUG, "No possible aggregation.");
				}*/
			}
		}
	}

	//cout << "Gomory agg total : " << rowAgg.getNTotalAgg() << endl;

	return nGenCut;
}

/**
 * set the value of the currentVal vector (decision and slack var value)
 *
 */
void GomoryCutGen::copyValnBound(VAR_TYPE *val, float2 *bound, SimplexSolution *sol){
	//double integralPart;

	// Copy basic variables value
	for(unsigned int i=0; i<iProb->nVar; i++){
		val[i] = sol->xVal[i];
		bound[i] = iProb->xBounds[i];
	}

	// Copy non-basic variables value
	for(unsigned int j=iProb->nVar; j<iProb->nVar+iProb->nSlack; j++){
		val[j] = sol->sVal[j-iProb->nVar];
		bound[j] = iProb->bBounds[j-iProb->nVar];
	}
}

/**
 * Transform the selected row into a "standard row" with n+m variable and a RHS
 *
 */
void GomoryCutGen::copyRow(int row, Inequality *eRow, SimplexProblem *cProb){

	// init all to 0
	for(unsigned int i=0; i<eRow->nLHS; i++){
		eRow->coeffLHS[i] = 0.0;
	}

	// Copy current x_j coefficient a_ij
	for(unsigned int j=0; j<cProb->nVar; j++){
		eRow->coeffLHS[cProb->xInd[j]] = cProb->eqs[IDX2C(row,j,cProb->nRow)];
	}

	// set basis variable coefficient
	eRow->coeffLHS[cProb->basis[row]] = 1.0;
	// set RHS value
	//printf("Row : %i - nVar : %i - nCol : %i - IDX : %i\n", row, cProb->nVar, cProb->nCol, IDX2C(row, cProb->nVar, cProb->nCol));
	eRow->rhs = cProb->eqs[IDX2C(row, cProb->nCol-1, cProb->nRow)];
}

/**
 * Transform the "standard row into a mixed knapsack equation
 *
 */
void GomoryCutGen::boundSubstitution(Inequality *eRow, VAR_TYPE *val, float2 *bound,
									 IntegerVariables *intVar, Inequality *eCut) {
	unsigned int iIntVar = 0;

	eCut->rhs = eRow->rhs; // Last element of cRow is RHS

	for(int j=0; j<(int)eCut->nLHS; j++){ // Not nVariables+1 because we don't care of RHS here
		if((iIntVar < intVar->nIntVar) && (j == intVar->intVarIndex[iIntVar])) { // We are processing an integer variable
			eCut->coeffLHS[j] = eRow->coeffLHS[j];
			iIntVar++;
		} else { // We are processing a real variable
			if((fabs(bound[j].y - val[j]) < EPS1) && (bound[j].y != MAX_TYPE)) { // Variable j is at upper bound
				// Complementing
				eCut->coeffLHS[j] = -eRow->coeffLHS[j];
				eCut->rhs -= eRow->coeffLHS[j]*bound[j].y;
			} else {
				// Otherwise we just copy the value
				eCut->coeffLHS[j] = eRow->coeffLHS[j];
			}
		}
	}
}

bool GomoryCutGen::createGomoryMIRCut(int iRow, Inequality *eCut, IntegerVariables *intVar, SimplexProblem *cProb){

	unsigned int iIntVar = 0;
	VAR_TYPE denom, floorCoeff, fRhs, fCoeff;

	// Process floor of a_0 and also f_0
	fRhs = eCut->rhs - floor(eCut->rhs);
	eCut->rhs = floor(eCut->rhs);
	if(fRhs > LIMIT_A0_FRACTIONAL_PART)
		return false;
	// Pre process denominator
	denom = 1.0 - fRhs;

	for(int i=0; i<(int)eCut->nLHS; i++){
		if((iIntVar < intVar->nIntVar) && (i == intVar->intVarIndex[iIntVar])) { // We are processing an integer variable
			if(cProb->basis[iRow] == i) { // basis variable
				assert(fabs(eCut->coeffLHS[i] - 1.0) <= EPS1);
				eCut->coeffLHS[i] = 1.0;
			} else {
				// process floor of a_j
				floorCoeff = floor(eCut->coeffLHS[i]);
				// process f_j
				fCoeff = eCut->coeffLHS[i] - floorCoeff;
				// Process new value
				eCut->coeffLHS[i] = (fCoeff-fRhs) / denom;
				// Takes max of this previous result and 0
				eCut->coeffLHS[i] =  eCut->coeffLHS[i] > EPS2 ? eCut->coeffLHS[i] : 0.0;
				// Add coefficient a_j floor value
				eCut->coeffLHS[i] += floorCoeff;

			}
			iIntVar++;
		} else { // We are processing a real variable
			// Divide coefficient a_j by 1 - f_0
			//cout << "coeff a_" << i << " : " << eCut->coeffLHS[i] << endl;
			eCut->coeffLHS[i] /= denom;
			// Takes min of this previous result and 0
			eCut->coeffLHS[i] = eCut->coeffLHS[i] < -EPS2 ? eCut->coeffLHS[i] : 0.0;
		}
	}

	return true;
}

void GomoryCutGen::reverseBoundSubstitution(Inequality *eCut, VAR_TYPE *val, float2 *bound, IntegerVariables *intVar){

	unsigned int iIntVar = 0;

	for(int i=0; i<(int)eCut->nLHS; i++){ // Not nVariables+1 because we don't care of RHS here
		if((iIntVar < intVar->nIntVar) && (i == intVar->intVarIndex[iIntVar])) { // We are processing an integer variable
			iIntVar++;
		} else { // We are processing a real variable
			if((fabs(bound[i].y - val[i]) < EPS1) && (bound[i].y != MAX_TYPE)) { // We are closer to lower boundd
				eCut->coeffLHS[i] = -eCut->coeffLHS[i];
				eCut->rhs += eCut->coeffLHS[i]*bound[i].y;
			}
		}
	}
}

void GomoryCutGen::slackSubstitution(int row, VAR_TYPE sCoeff, Inequality *fCut){
	cblas_daxpy(fCut->nLHS, -sCoeff, &iProb->eqs[IDX2C(row, 0, iProb->nRow)], iProb->nRow, fCut->coeffLHS, 1);

	fCut->rhs -= sCoeff*iProb->eqs[IDX2C(row, iProb->nVar, iProb->nRow)];

}

void GomoryCutGen::slacksSubstitution(Inequality *eCut, Inequality *fCut){

	// same size for row and final cut
	assert((unsigned int)fCut->nLHS == iProb->nVar);

	// Copy RHS and decision variables coefficient
	fCut->rhs = eCut->rhs;
	memcpy(fCut->coeffLHS, eCut->coeffLHS, fCut->nLHS*sizeof(VAR_TYPE));
	/*for(unsigned int i=0; i<fCut->nLHS; i++)
		fCut->coeffLHS[i] = eCut->coeffLHS[i];*/

	// For each slack variable
	for(unsigned int i=iProb->nVar; i<(iProb->nVar+iProb->nSlack); i++){
		// If coefficient non-null
		if(fabs(eCut->coeffLHS[i]) > EPS1){
			// Substitute the slack variable, using the row from initial problem
			slackSubstitution((i-iProb->nVar), eCut->coeffLHS[i], fCut);
		}
	}
}

bool GomoryCutGen::isViolated(Inequality *eCut, SimplexSolution *sol) {
	VAR_TYPE lhs;
	lhs = cblas_ddot(sol->nVar, sol->xVal, 1, eCut->coeffLHS, 1);
	lhs += cblas_ddot(sol->nSlack, sol->sVal, 1, &eCut->coeffLHS[sol->nVar], 1);

	return lhs > eCut->rhs;
}
