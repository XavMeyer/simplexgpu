//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * GomoryCutGen.h
 *
 *  Created on: Aug 5, 2011
 *      Author: meyerx
 */

#ifndef GOMORYCUTGEN_H_
#define GOMORYCUTGEN_H_

#define AGGREGATION_MAX						6
#define LIMIT_A0_FRACTIONAL_PART			0.95

#include "../../Misc/IntegerVariables.h"


#include "../../../Solver/utils/Logger.h"
#include "../../../Solver/SimplexProblem.h"
#include "../../../Solver/SimplexSolution.h"

#include "../CutManagement/CutPool.h"
#include "Utils/RowAggregator.h"

#include "cblas.h"

#include <assert.h>
#include <set>
#include <queue>
using namespace std;

class GomoryCutGen {
public:
	GomoryCutGen(CutPool *inCp, SimplexProblem *inIProb);
	GomoryCutGen(CutPool *inCp, SimplexProblem *inIProb, Logger *inLog);
	virtual ~GomoryCutGen();

	int generateCuts(SimplexSolution *sol, IntegerVariables *intVar, SimplexProblem *cProb);

private:
	Logger *logger;

	int nGenCut;
	CutPool *cp;
	SimplexProblem *iProb;

	void copyValnBound(VAR_TYPE *val, float2 *bound, SimplexSolution *sol);
	void copyRow(int row, Inequality *eRow, SimplexProblem *cProb);
	void boundSubstitution(Inequality *eRow, VAR_TYPE *val, float2 *bound,
						   IntegerVariables *intVar, Inequality *eCut);
	bool createGomoryMIRCut(int iRow, Inequality *eCut, IntegerVariables *intVar, SimplexProblem *cProb);
	void reverseBoundSubstitution(Inequality *eCut, VAR_TYPE *val, float2 *bound, IntegerVariables *intVar);
	void slackSubstitution(int row, VAR_TYPE sCoeff, Inequality *fCut);
	void slacksSubstitution(Inequality *eCut, Inequality *fCut);
	bool isViolated(Inequality *eCut, SimplexSolution *sol);
};

#endif /* GOMORYCUTGEN_H_ */
