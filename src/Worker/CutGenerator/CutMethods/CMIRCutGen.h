//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CMIRCutGen.h
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#ifndef CMIRCUTGEN_H_
#define CMIRCUTGEN_H_

#define LIMIT_BETA_FRACTIONAL_PART 			0.95
#define AGGREGATION_MAX						6

#include "../../Misc/IntegerVariables.h"


#include "../../../Solver/utils/Logger.h"
#include "../../../Solver/SimplexProblem.h"
#include "../../../Solver/SimplexSolution.h"

#include "../CutManagement/CutPool.h"
#include "Utils/MixedKnapsack.h"
#include "Utils/RowAggregator.h"

#include "cblas.h"

#include <assert.h>
#include <set>
#include <queue>
using namespace std;

class CMIRCutGen {
public:
	CMIRCutGen(CutPool *inCp, SimplexProblem *inIProb);
	CMIRCutGen(CutPool *inCp, SimplexProblem *inIProb, Logger *inLog);
	virtual ~CMIRCutGen();

	int generateCuts(SimplexSolution *sol, IntegerVariables *intVar, SimplexProblem *cProb);

private:

	Logger *logger;

	int nGenCut;
	CutPool *cp;
	SimplexProblem *iProb;

	class ElementPQ {
	public:

		unsigned int index;
		float val;

		ElementPQ(unsigned int inIndex, float inVal){
			index = inIndex;
			val = inVal;
		}

		virtual ~ElementPQ(){}

		bool operator > (const ElementPQ &rhs) const {
			return val > rhs.val;
		}

		bool operator < (const ElementPQ &rhs) const {
			return val < rhs.val;
		}
	};

	void copyValnBound(VAR_TYPE *val, float2 *bound, SimplexSolution *sol);
	void copyRow(int row, Inequality *eRow);
	bool transformToXmk(Inequality *eRow, VAR_TYPE *val, float2 *bound,
						IntegerVariables *intVar, MixedKnapsack *Xmk);
	void initSets(priority_queue<ElementPQ> *pq, set<unsigned int> *U, set<unsigned int> *T, MixedKnapsack *Xmk);
	void initDeltas(set<VAR_TYPE> *deltas, MixedKnapsack *Xmk);
	bool searchCMIRCut(MixedKnapsack *Xmk, MixedKnapsack *XmkCut);
	bool isAcceptableBeta(double beta);
	VAR_TYPE processBeta(VAR_TYPE delta, set<unsigned int> *U, MixedKnapsack *Xmk);
	VAR_TYPE processFfyOfx(VAR_TYPE delta, VAR_TYPE x);
	VAR_TYPE processNu(VAR_TYPE beta, VAR_TYPE delta, set<unsigned int> *T, set<unsigned int> *U, MixedKnapsack *Xmk);
	void createCMIRCut(VAR_TYPE beta, VAR_TYPE delta,
			           set<unsigned int> *T, set<unsigned int> *U,
			           MixedKnapsack *Xmk, MixedKnapsack *XmkCut);
	void transformFromXmkCut(MixedKnapsack *XmkCut, int nVariables, Inequality *eCut);
	void reverseBoundSubstitution(Inequality *eCut, VAR_TYPE *val, float2 *bound, MixedKnapsack *XmkCut);
	void slackSubstitution(int row, VAR_TYPE sCoeff, Inequality *fCut);
	void slacksSubstitution(Inequality *eCut, Inequality *fCut);

};

#endif /* CMIRCUTGEN_H_ */
