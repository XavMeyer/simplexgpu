//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * CMIRCutGen.cpp
 *
 *  Created on: Jul 18, 2011
 *      Author: meyerx
 */

#include "CMIRCutGen.h"

CMIRCutGen::CMIRCutGen(CutPool *inCp, SimplexProblem *inIProb) {
	cp = inCp;
	iProb = inIProb;
	nGenCut = 0;
	logger = NULL;
}

CMIRCutGen::CMIRCutGen(CutPool *inCp, SimplexProblem *inIProb, Logger *inLog) {
	cp = inCp;
	iProb = inIProb;
	nGenCut = 0;
	logger = inLog;
}

CMIRCutGen::~CMIRCutGen() {
	cp = NULL;
	iProb = NULL;
}

int CMIRCutGen::generateCuts(SimplexSolution *sol, IntegerVariables *intVar, SimplexProblem *cProb) {
	int iAgg;
	bool rowAggregated;
	int nVariables = iProb->nVar+iProb->nSlack; // Slack + decision var
	MixedKnapsack Xmk(intVar->nIntVar, nVariables-intVar->nIntVar);
	MixedKnapsack XmkCut(intVar->nIntVar, nVariables-intVar->nIntVar);
	Inequality extendedRow(nVariables);
	VAR_TYPE val[nVariables];
	float2 bound[nVariables];
	Inequality extendedCut(nVariables);
	Inequality *finalCut = NULL;
	RowAggregator rowAgg(iProb, sol, cProb);

	// Copy the variable values and bound
	copyValnBound(val, bound, sol);

	for(unsigned int i=0; i<iProb->nRow; i++) {
		//cout << "Row : " << i << endl;
		//if(logger != NULL) logger->log(Logger::LOG_DEBUG, "***************************");
		// Copy cut into currentCut
		copyRow(i, &extendedRow);
		// Init the row aggregator for this row
		rowAggregated = true;
		rowAgg.setFirstRow(i);
		//if(logger != NULL) logger->log(Logger::LOG_DEBUG, extendedRow.toString());

		iAgg = 0;
		while((iAgg < (AGGREGATION_MAX+1)) && rowAggregated){
			// Put the row into canonical
			transformToXmk(&extendedRow, val, bound, intVar, &Xmk);
			//if(logger != NULL) logger->log(Logger::LOG_DEBUG, Xmk.toString());

			// CreateMirCut and add it if working
			if(searchCMIRCut(&Xmk, &XmkCut) && XmkCut.isViolated()){
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, XmkCut.toString());
				// Transform cut from the mixed Knapsack cut to our problem variables
				transformFromXmkCut(&XmkCut, nVariables, &extendedCut);
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, extendedCut.toString());
				// Reverse the substitution made previously on the bound
				reverseBoundSubstitution(&extendedCut, val, bound, &XmkCut);
				// Create the cut in the form of the initial problem
				finalCut = new Inequality(iProb->nVar);
				slacksSubstitution(&extendedCut, finalCut);
				// Add the cut to the cut pool
				if(!cp->addCut(finalCut))
					return nGenCut; // Max nb of cut reached
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, finalCut->toString());
				//if(logger != NULL) logger->log(Logger::LOG_DEBUG, "Cut added.");
				nGenCut++;
				break; // leaving the while loop, we have found a cut
			}
			iAgg++; // increment aggregation counter
			// If we can aggregated more row
			if(iAgg < (AGGREGATION_MAX+1)){
				rowAggregated = rowAgg.aggregateRow(&extendedRow);
				/*if(rowAggregated) {
					if(logger != NULL) logger->log(Logger::LOG_DEBUG, "Row aggregated.");
				} else {
					if(logger != NULL) logger->log(Logger::LOG_DEBUG, "No possible aggregation.");
				}*/
			}
		}
	}

	//cout << "Cmir agg total : " << rowAgg.getNTotalAgg() << endl;

	return nGenCut;
}

/**
 * set the value of the currentVal vector (decision and slack var value)
 *
 */
void CMIRCutGen::copyValnBound(VAR_TYPE *val, float2 *bound, SimplexSolution *sol){
	//double integralPart;

	// Copy basic variables value
	for(unsigned int i=0; i<iProb->nVar; i++){
		val[i] = sol->xVal[i];
		bound[i] = iProb->xBounds[i];
	}

	// Copy non-basic variables value
	for(unsigned int j=iProb->nVar; j<iProb->nVar+iProb->nSlack; j++){
		val[j] = sol->sVal[j-iProb->nVar];
		bound[j] = iProb->bBounds[j-iProb->nVar];
	}
}

/**
 * Transform the selected row into a "standard row" with n+m variable and a RHS
 *
 */
void CMIRCutGen::copyRow(int row, Inequality *eRow){

	// Set slack to 0
	for(unsigned int i=iProb->nVar; i<iProb->nVar+iProb->nSlack; i++){
		eRow->coeffLHS[i] = 0.0;
	}

	// Copy current x_j coefficient a_ij (can't use memcpy because matrice is store column-wise)
	for(unsigned int i=0; i<iProb->nVar; i++)
		eRow->coeffLHS[i] = iProb->eqs[IDX2C(row, i, iProb->nRow)];

	// set slack variable (basis)coefficient
	eRow->coeffLHS[row + iProb->nVar] = 1.0;
	// set RHS value
	eRow->rhs = iProb->eqs[IDX2C(row, iProb->nCol-1, iProb->nRow)];
}


/**
 * Transform the "standard row into a mixed knapsack equation
 *
 */
bool CMIRCutGen::transformToXmk(Inequality *eRow, VAR_TYPE *val, float2 *bound,
								IntegerVariables *intVar, MixedKnapsack *Xmk) {
	bool existingS = false;
	unsigned int iIntVar = 0;
	unsigned int iRealVar = 0;
	//double integralPart;

	Xmk->alpha0 = eRow->rhs; // Last element of cRow is RHS

	for(int j=0; j<(int)eRow->nLHS; j++){ // Not nVariables+1 because we don't care of RHS here
		if((iIntVar < intVar->nIntVar) && (j == intVar->intVarIndex[iIntVar])) { // We are processing an integer variable
			Xmk->alpha[iIntVar] = eRow->coeffLHS[j];
			Xmk->xStar[iIntVar] = val[j];
			//Xmk->xStar[iIntVar] = modf(val[j], &integralPart);
			//cout << "[x" << j << "] : " << Xmk->xStar[iIntVar] << endl;
			Xmk->bound[iIntVar] = bound[j].y; // upper bound
			Xmk->xInd[iIntVar] = j;
			iIntVar++;
		} else { // We are processing a real variable
			if((val[j] - bound[j].x) <= (bound[j].y - val[j]) || (bound[j].y == MAX_TYPE)) { // We are closer to lower bound
				Xmk->gamma[iRealVar] = eRow->coeffLHS[j];
				Xmk->yStar[iRealVar] = val[j] - bound[j].x;
				Xmk->alpha0 -= eRow->coeffLHS[j]*bound[j].x;
			} else { // We are closer to upper bound
				Xmk->gamma[iRealVar] = -eRow->coeffLHS[j];
				Xmk->yStar[iRealVar] = bound[j].y - val[j];
				Xmk->alpha0 -= eRow->coeffLHS[j]*bound[j].y;
			}

			// gamma_j is the min of 0 and gamma_j (gamma_j<-min(0, gamma_j))
			if(Xmk->gamma[iRealVar] > 0.0) {
				Xmk->gamma[iRealVar] = 0.0;
			} else {
			//	cout << "x" << j << " : y" << iRealVar << " : " << Xmk->gamma[iRealVar] << endl;
				Xmk->gamma[iRealVar] *= -1.0;
				existingS = true;
			}

			Xmk->yInd[iRealVar] = j;
			iRealVar++;
		}
	}

	Xmk->processSStar();
	return existingS;
}

void CMIRCutGen::transformFromXmkCut(MixedKnapsack *XmkCut, int nVariables, Inequality *eCut){

	// copying LHS alpha0 (omega0)
	eCut->rhs = XmkCut->alpha0;

	// copying alpha_j (omega_j)
	for(unsigned int i=0; i<XmkCut->n;i++)
		eCut->coeffLHS[XmkCut->xInd[i]] = XmkCut->alpha[i];

	// moving gamma_j (omega*gamma_j) from LHS to RHS
	for(unsigned int i=0; i<XmkCut->m;i++){
		eCut->coeffLHS[XmkCut->yInd[i]] = -XmkCut->gamma[i];
	}


}

void CMIRCutGen::reverseBoundSubstitution(Inequality *eCut, VAR_TYPE *val, float2 *bound, MixedKnapsack *XmkCut){

	unsigned int j;

	for(unsigned int i=0; i<XmkCut->m; i++){
		j = XmkCut->yInd[i];
		if((val[j] - bound[j].x) <= (bound[j].y - val[j]) || (bound[j].y == MAX_TYPE)) { // We were closer to lower bound
			eCut->rhs += eCut->coeffLHS[j]*bound[j].x; // Change RHS
			//Xmk->gamma[iRealVar] = cRow[j]; <--- to XMK transformation (nothing to change)
			//Xmk->alpha0 -= cRow[j]*bound[j].x; <--- to XMK transformation
		} else { // We were closer to upper bound
			eCut->coeffLHS[j] = -eCut->coeffLHS[j]; // Change sign
			eCut->rhs += eCut->coeffLHS[j]*bound[j].y; // Change RHS (todo control this, but should be right)
			//Xmk->gamma[iRealVar] = -cRow[j]; <--- to XMK transformation
			//Xmk->alpha0 -= cRow[j]*bound[j].y; <--- to XMK transformation
		}
	}
}

void CMIRCutGen::slackSubstitution(int row, VAR_TYPE sCoeff, Inequality *fCut){
	cblas_daxpy(fCut->nLHS, -sCoeff, &iProb->eqs[IDX2C(row, 0, iProb->nRow)], iProb->nRow, fCut->coeffLHS, 1);
	/*for(unsigned int j=0; j<fCut->nLHS; j++){
		fCut->coeffLHS[j] -= sCoeff*iProb->eqs[IDX2C(row, j, iProb->nCol)];
	}*/

	fCut->rhs -= sCoeff*iProb->eqs[IDX2C(row, iProb->nVar, iProb->nRow)];

}

void CMIRCutGen::slacksSubstitution(Inequality *eCut, Inequality *fCut){

	// same size for row and final cut
	assert((unsigned int)fCut->nLHS == iProb->nVar);

	// Copy RHS and decision variables coefficient
	fCut->rhs = eCut->rhs;
	memcpy(fCut->coeffLHS, eCut->coeffLHS, fCut->nLHS*sizeof(VAR_TYPE));
	/*for(unsigned int i=0; i<fCut->nLHS; i++)
		fCut->coeffLHS[i] = eCut->coeffLHS[i];*/

	// For each slack variable
	for(unsigned int i=iProb->nVar; i<(iProb->nVar+iProb->nSlack); i++){
		// If coefficient non-null
		if(fabs(eCut->coeffLHS[i]) > EPS1){
			// Substitute the slack variable, using the row from initial problem
			slackSubstitution((i-iProb->nVar), eCut->coeffLHS[i], fCut);
		}
	}
}


void CMIRCutGen::initSets(priority_queue<ElementPQ> *pq, set<unsigned int> *U, set<unsigned int> *T, MixedKnapsack *Xmk){

	float val;
	//ElementPQ const *ePQ;

	for(unsigned int i=0; i<Xmk->n; i++){
		val = Xmk->xStar[i]-(Xmk->bound[i]/2.0);
		if(val >= 0){
			U->insert(i);
		} else {
			T->insert(i);
			//ePQ = new ElementPQ(i, val);
			pq->push(ElementPQ(i, val));
		}
	}
}

void CMIRCutGen::initDeltas(set<VAR_TYPE> *deltas, MixedKnapsack *Xmk){

	VAR_TYPE absAlpha;
	VAR_TYPE maxAbsAlpha = 0.0;

	for(unsigned int i=0; i<Xmk->n; i++){
		absAlpha = fabs(Xmk->alpha[i]);
		if((absAlpha > EPS1) && (Xmk->xStar[i] > 0) && (Xmk->xStar[i] < Xmk->bound[i])){
			deltas->insert(absAlpha);
		}
		if(absAlpha > maxAbsAlpha)
			maxAbsAlpha = absAlpha;
	}
	deltas->insert(maxAbsAlpha+1.0);
}

VAR_TYPE CMIRCutGen::processBeta(VAR_TYPE delta, set<unsigned int> *U, MixedKnapsack *Xmk){
	VAR_TYPE beta;

	// set beta to alpha_0
	beta = Xmk->alpha0;

	// Substract each alpha_j (in U) multiplied by bound_j to beta
	for(set<unsigned int>::iterator itU = U->begin(); itU != U->end(); itU++)
		beta -= Xmk->alpha[*itU]*Xmk->bound[*itU];

	// Then divide by delta
	return (beta/delta);
}

VAR_TYPE CMIRCutGen::processFfyOfx(VAR_TYPE y, VAR_TYPE x){
	VAR_TYPE num, denom, fy;

	fy = y - floor(y); 					// fBeta

	num = (x - floor(x)) - fy; 			// (fx - fBeta)
	num = 0 > num ? 0 : num; 			// max(0, (fx - fBeta))

	denom = 1.0 - fy; 					// (1-fBeta)

	return (floor(x) + num/denom);

}

VAR_TYPE CMIRCutGen::processNu(VAR_TYPE beta, VAR_TYPE delta, set<unsigned int> *T, set<unsigned int> *U, MixedKnapsack *Xmk){
	VAR_TYPE nu = 0.0; // v init to 0

	// For each j in T
	for(set<unsigned int>::iterator itT = T->begin(); itT != T->end(); itT++){
		// v + FfBeta(alpha_j/delta)*xStar_j
		nu += processFfyOfx(beta, (Xmk->alpha[*itT]/delta)) * Xmk->xStar[*itT];
	}

	// For each j in U
	for(set<unsigned int>::iterator itU = U->begin(); itU != U->end(); itU++){
		// v + FfBeta(-alpha_j/delta)*(bound_j-xStar_j)
		nu += processFfyOfx(beta, (-Xmk->alpha[*itU]/delta)) * (Xmk->bound[*itU]-Xmk->xStar[*itU]);
	}
	nu -= floor(beta);
	nu -= Xmk->sStar/(delta*(ceil(beta) - beta));

	return nu;
}

void CMIRCutGen::createCMIRCut(VAR_TYPE beta, VAR_TYPE delta,
		                       set<unsigned int> *T, set<unsigned int> *U,
		                       MixedKnapsack *Xmk, MixedKnapsack *XmkCut){
	VAR_TYPE temp, omega;

	XmkCut->copy(*Xmk);

	XmkCut->alpha0 = floor(beta);

	// For each j in T
	for(set<unsigned int>::iterator itT = T->begin(); itT != T->end(); itT++){
		// Process the coefficient w_j for x_j
		XmkCut->alpha[*itT] = processFfyOfx(beta, (Xmk->alpha[*itT]/delta));
	}

	// For each j in U
	for(set<unsigned int>::iterator itU = U->begin(); itU != U->end(); itU++){
		// Process the coefficient w_j for x_j and update w0
		temp = processFfyOfx(beta, (-Xmk->alpha[*itU]/delta));
		XmkCut->alpha[*itU] = -temp;
		XmkCut->alpha0 -= temp*XmkCut->bound[*itU];
	}

	// Update s (gamma) and sStar with w
	omega = 1.0/(delta*(ceil(beta) - beta));
	XmkCut->sStar *= omega;
	for(unsigned int i=0; i<XmkCut->m; i++){
		XmkCut->gamma[i] *= omega;
	}

}

bool CMIRCutGen::isAcceptableBeta(double beta){
	// check that beta is not to "small" or integral
	return ((beta - floor(beta)) <= LIMIT_BETA_FRACTIONAL_PART) && (beta - floor(beta) > EPS1);
}

bool CMIRCutGen::searchCMIRCut(MixedKnapsack *Xmk, MixedKnapsack *XmkCut){

	priority_queue<ElementPQ> pq;
	set<unsigned int> U, T, UBest, TBest;
	set<VAR_TYPE>::iterator itD;
	set<VAR_TYPE> deltas;
	bool deltaFound, first;
	VAR_TYPE beta, betaBest, nu, nuBest, deltaBest, deltaTemp;

	initSets(&pq, &U, &T, Xmk);
	initDeltas(&deltas, Xmk);

	first = true;
	deltaFound = false;
	nuBest = -MAX_TYPE;
	betaBest = deltaBest = 0;

	// Searching for the best possible delta, if existing
	while(!pq.empty() && !deltaFound){

		// Complement an additional variable (updating set)
		if(!first){
			if(Xmk->xStar[pq.top().index] > 0){
				U.insert(pq.top().index);
				T.erase(pq.top().index);
				pq.pop(); // removing top of priority queue (set swap of variable xStar_l)
			} else {
				pq.pop(); // removing top of priority queue (means that xStar_l <= 0)
				continue;
			}
		} else {
			first = false;
		}

		// For each delta
		for(itD = deltas.begin(); itD != deltas.end(); itD++){
			beta = processBeta(*itD, &U, Xmk); // Process beta
			if(isAcceptableBeta(beta)){ // Check if not integer
				deltaFound = true;
				nu = processNu(beta, *itD, &T, &U, Xmk); // Process v
				if(nu > nuBest){ // If best v, keep v and delta
					nuBest = nu;
					deltaBest = *itD;
					betaBest = beta;
				}
			}
		}
	}

	// No delta found
	if(!deltaFound) return false;

	// Trying to improve the violation
	deltaTemp = deltaBest;
	for(int den=2; den <= 8; den*=2){
		beta = processBeta(deltaTemp/den, &U, Xmk); // Process beta with delta/div
		nu = processNu(beta, deltaTemp/den, &T, &U, Xmk);
		if(nu > nuBest && isAcceptableBeta(beta)){ // If best v, keep v and delta
			nuBest = nu;
			deltaBest = deltaTemp/den;
			betaBest = beta;
		}
	}

	/* Can we improve the violation by complementing more variables
	 * At this point U contains complemented variable, T non-complemented variables.
	 * pq contains the remaining untested non-complemented variables, the tested ones
	 * that add their xStar <= 0 have already been poped out of pq.
	 *
	 * The point is then to empty pq of all variable candidate to complementing and
	 * keep the best sets (U and T).
	 */
	copy(T.begin(), T.end(), inserter(TBest, TBest.begin()));
	copy(U.begin(), U.end(), inserter(UBest, UBest.begin()));

	while(!pq.empty()){
		// Complement an additional variable (updating set)
		if(Xmk->xStar[pq.top().index] > 0){
			U.insert(pq.top().index);
			T.erase(pq.top().index);
			pq.pop(); // removing top of priority queue (set swap of variable xStar_l)
		} else {
			pq.pop(); // removing top of priority queue (means that xStar_l <= 0)
			continue;
		}

		beta = processBeta(deltaBest, &U, Xmk); // Process beta with delta/div
		nu = processNu(beta, deltaBest, &T, &U, Xmk);
		if(nu > nuBest && isAcceptableBeta(beta)){ // If best v, keep v and delta
			TBest.clear();
			UBest.clear();
			copy(T.begin(), T.end(), inserter(TBest, TBest.begin()));
			copy(U.begin(), U.end(), inserter(UBest, UBest.begin()));
			nuBest = nu;
			betaBest = beta;
		}
	}

	// todo Working but not clean enough, should be tested previously -- FIXED by isAcceptableBeta... not yet tested
	//if((betaBest - floor(betaBest)) > 0.95) return false;

	createCMIRCut(betaBest, deltaBest, &TBest, &UBest, Xmk, XmkCut);

	return true;
}

