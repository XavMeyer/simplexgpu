//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Reliability.h
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#ifndef RELIABILITY_H_
#define RELIABILITY_H_

#include "Pseudocosts.h"
#include <list>
#include <iostream>

using namespace std;

class Reliability: public Pseudocosts {
public:
	Reliability(int inNVar);
	virtual ~Reliability();

	void setEtaRel(int inEtaRel);
	void setK(int inK);

	void sortLessReliableFirst();
	int getNextLessReliable();

private:

	int K, cntK; // Number of unreliable value to compute
	int etaRel; // Number after which a score is reputed reliable

	bool empty;
	list<SumCount> sortedCount;
	list<SumCount>::iterator itLessReliable;

};

#endif /* RELIABILITY_H_ */
