//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Reliability.cpp
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#include "ParReliability.h"

ParReliability::ParReliability(int inNVar) : ParPseudocosts(inNVar) {

	K = DEFAULT_K;
	etaRel = DEFAULT_ETA;

	for(int i=0; i<this->nVar; i++){
		costs[i].index = i;
		sortedCount.push_back(this->costs[i]);
	}
}

ParReliability::~ParReliability() {

}

ReliabilityList * ParReliability::getSortedReliability(){
	ReliabilityList * relList;
	sem_wait(&mutexUp);
	sem_wait(&mutexDown);

	relList = new ReliabilityList(&sortedCount, K, etaRel);

	sem_post(&mutexDown);
	sem_post(&mutexUp);

	return relList;
}

void ParReliability::setEtaRel(int inEtaRel){
	etaRel = inEtaRel;
}

void ParReliability::setK(int inK){
	K = inK;
}
