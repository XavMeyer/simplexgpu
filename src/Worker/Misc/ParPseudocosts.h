//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParPseudocosts.h
 *
 *  Created on: Aug 24, 2011
 *      Author: meyerx
 */

#ifndef PARPSEUDOCOSTS_H_
#define PARPSEUDOCOSTS_H_

#include "Pseudocosts.h"
#include "semaphore.h"

class ParPseudocosts: public Pseudocosts {
public:

	ParPseudocosts(int inNVar) : Pseudocosts(inNVar) {
		sem_init(&mutexUp, 1, 1);
		sem_init(&mutexDown, 1, 1);

	}
	virtual ~ParPseudocosts() {
		//sem_destroy(&mutexUp);
		//sem_destroy(&mutexDown);
	}

	void addGainUp(int index, float gain){
		sem_wait(&mutexUp);
		super::addGainUp(index, gain);
		sem_post(&mutexUp);
	}

	void addGainDown(int index, float gain){
		sem_wait(&mutexDown);
		super::addGainDown(index, gain);
		sem_post(&mutexDown);
	}

	float getPcostUp(int index){
		float pcostUp;
		sem_wait(&mutexUp);
		pcostUp = super::getPcostUp(index);
		sem_post(&mutexUp);
		return pcostUp;
	}

	float getPcostDown(int index){
		float pcostDown;
		sem_wait(&mutexDown);
		pcostDown = super::getPcostDown(index);
		sem_post(&mutexDown);
		return pcostDown;
	}

protected:

	typedef Pseudocosts super;

	sem_t mutexUp, mutexDown;



};

#endif /* PARPSEUDOCOSTS_H_ */
