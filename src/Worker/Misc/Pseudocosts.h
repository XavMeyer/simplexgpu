//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Pseudocosts.h
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#ifndef PSEUDOCOSTS_H_
#define PSEUDOCOSTS_H_

#include "SumCount.h"

#define DEFAULT_K		100
#define DEFAULT_ETA		8

class Pseudocosts {
public:
	Pseudocosts(int inNVar);
	virtual ~Pseudocosts();

	void addGainUp(int index, float gain);
	void addGainDown(int index, float gain);

	float getPcostUp(int index);
	float getPcostDown(int index);

protected:

	void init();

	int nVar;

	SumCount *costs;

	SumCount mean;

};

#endif /* PSEUDOCOSTS_H_ */
