//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Reliability.cpp
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#include "Reliability.h"

Reliability::Reliability(int inNVar) : Pseudocosts(inNVar) {

	K = DEFAULT_K;
	etaRel = DEFAULT_ETA;

	for(int i=0; i<this->nVar; i++){
		costs[i].index = i;
		sortedCount.push_back(this->costs[i]);
	}
}

Reliability::~Reliability() {

}

void Reliability::sortLessReliableFirst(){
	sortedCount.sort();
	itLessReliable = sortedCount.begin();
	cntK = 0;
	empty = false;
}

int Reliability::getNextLessReliable(){

	// Return -1 when min(nVar,K) variable have been sent
	if(cntK == nVar || cntK == K) return -1;

	// Get next variable count / sum
	SumCount * res = &(*itLessReliable);

	// Return -1 if the min(countUp, countDown) exceed etaRel
	if(res->upCount >= etaRel || res->downCount >= etaRel) return -1;

	itLessReliable++;
	cntK++;

	return res->index;
}

void Reliability::setEtaRel(int inEtaRel){
	etaRel = inEtaRel;
}

void Reliability::setK(int inK){
	K = inK;
}
