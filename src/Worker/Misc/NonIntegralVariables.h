//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * IntegerVariables.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef INTEGRALVARIABLES_H_
#define INTEGRALVARIABLES_H_

#include "IntegerVariables.h"
#include "../../Solver/SimplexSolution.h"

class NonIntegralVariables {
public:

	unsigned int nIntVar;
	int *intVarIndex;

	NonIntegralVariables(IntegerVariables *inIV, SimplexSolution *inSol){
		createIntegerArray(inIV, inSol);
	}
	virtual ~NonIntegralVariables(){
		delete [] intVarIndex;
	}

	void createIntegerArray(IntegerVariables *inIV, SimplexSolution *inSol){
		int count;

		// Count number of integral variable
		count = 0;
		for(unsigned int i=0; i<inIV->nIntVar; i++)
			if(!inSol->isIntegral(inIV->getVarIndex(i)))
				count++;

		// Create integer variable index array
		nIntVar = count;
		intVarIndex = new int[count];

		// Memorize integral variable index
		count = 0;
		for(unsigned int i=0; i<inIV->nIntVar; i++){
			if(!inSol->isIntegral(inIV->getVarIndex(i))){
					intVarIndex[count++] =  i;
			}
		}
	}

	int getVarIndex(int integerIndex){
		return intVarIndex[integerIndex];
	}

};

#endif /* INTEGRALVARIABLES_H_ */
