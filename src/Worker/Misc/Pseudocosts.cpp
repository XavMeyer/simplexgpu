//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Pseudocosts.cpp
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#include "Pseudocosts.h"

Pseudocosts::Pseudocosts(int inNVar) {

	nVar = inNVar;
	costs = new SumCount[nVar];

	init();
}

Pseudocosts::~Pseudocosts() {

	if(costs != NULL)
		delete [] costs;
}

void Pseudocosts::init(){

	mean.downSum = 1;
	mean.upSum = 1;


}

void Pseudocosts::addGainUp(int index, float gain){

	//printf("[pcost] index : %d - gain %f - upSum %f - upCount %d \n", index, gain, costs[index].upSum, costs[index].upCount);

	// Update the upward mean value (substract pre increase pseudocost)
	if(costs[index].upCount > 0){
		mean.upSum -= (costs[index].upSum/costs[index].upCount);
		mean.upCount--;
	}

	// Update variable upward pseudocost
	costs[index].upSum += gain;
	costs[index].upCount++;

	// Update the upward mean value (add post increase pseudocost)
	if(mean.upCount > 0)
		mean.upSum += (costs[index].upSum/costs[index].upCount);
	else
		mean.upSum = (costs[index].upSum/costs[index].upCount);

	mean.upCount++;

}

void Pseudocosts::addGainDown(int index, float gain){

	//printf("[pcost] index : %d - gain %f - downSum %f - downCount %d \n", index, gain, costs[index].downSum, costs[index].downCount);

	// Update the downward mean value (substract pre increase pseudocost)
	if(costs[index].downCount > 0){
		mean.downSum -= (costs[index].downSum/costs[index].downCount);
		mean.downCount--;
	}

	// update variable downward pseudocost
	costs[index].downSum += gain;
	costs[index].downCount++;

	// Update the downward mean value (add post increase pseudocost)
	if(mean.downCount > 0)
		mean.downSum += (costs[index].downSum/costs[index].downCount);
	else
		mean.downSum = (costs[index].downSum/costs[index].downCount);

	mean.downCount++;


}

float Pseudocosts::getPcostUp(int index){
	if(costs[index].upCount > 0) // Variable pseudocost
		return costs[index].upSum/costs[index].upCount;
	 else
		if(mean.upCount > 0) // mean pseudocost
			return mean.upSum/mean.upCount;
		else // Uninitialized mean
			return mean.upSum; // Should be 1
}

float Pseudocosts::getPcostDown(int index){
	if(costs[index].downCount > 0) // Variable pseudocost
		return costs[index].downSum/costs[index].downCount;
	 else
		if(mean.downCount > 0) // mean pseudocost
			return mean.downSum/mean.downCount;
		else // Uninitialized mean
			return mean.downSum; // Should be 1
}

