//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SumCount.h
 *
 *  Created on: Jun 7, 2011
 *      Author: meyerx
 */

#ifndef SUMCOUNT_H_
#define SUMCOUNT_H_

#include <stdio.h>

class SumCount {
public:

	int index;

	float upSum;
	int upCount;

	float downSum;
	int downCount;

	SumCount() {

		index = 0;

		upSum = 0;
		upCount = 0;

		downSum = 0;
		downCount = 0;
	}

	virtual ~SumCount() {}

	bool operator > (const SumCount &rhs) const
	{
		int minLhs, minRhs;
		minLhs = upCount < downCount ? upCount : downCount;
		minRhs = rhs.upCount < rhs.downCount ? rhs.upCount : rhs.downCount;

		return minLhs > minRhs;
	}

	bool operator < (const SumCount &rhs) const
	{
		int minLhs, minRhs;
		minLhs = upCount < downCount ? upCount : downCount;
		minRhs = rhs.upCount < rhs.downCount ? rhs.upCount : rhs.downCount;

		return minLhs < minRhs;
	}

};

#endif /* SUMCOUNT_H_ */
