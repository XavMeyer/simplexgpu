//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ReliabilityList.h
 *
 *  Created on: Aug 24, 2011
 *      Author: meyerx
 */

#ifndef RELIABILITYLIST_H_
#define RELIABILITYLIST_H_

#include <vector>
#include <list>
#include <iostream>

using namespace std;

class ReliabilityList {
public:
	ReliabilityList(vector<SumCount> *inCounts, int inK, int inEtaRel) : localList(inCounts->begin(), inCounts->end()){
		countK = 0;
		K = inK;
		etaRel = inEtaRel;
		localList.sort();
		itLessReliable = localList.begin();
	}

	virtual ~ReliabilityList()
	{}

	int getNextLessReliable(){
		// Return -1 when min(nVar,K) variable have been sent
		if(countK == localList.size() || countK == K) return -1;

		// Get next variable count / sum
		SumCount * res = &(*itLessReliable);

		// Return -1 if the min(countUp, countDown) exceed etaRel
		if(res->upCount > (int)etaRel || res->downCount > (int)etaRel) return -1;

		itLessReliable++;
		countK++;
		return res->index;
	}

private:

	unsigned int countK, K, etaRel;
	list<SumCount> localList;
	list<SumCount>::iterator itLessReliable;

};

#endif /* RELIABILITYLIST_H_ */
