//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * IntegerVariables.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef INTEGERVARIABLES_H_
#define INTEGERVARIABLES_H_

#include "../../Solver/SimplexProblem.h"

class IntegerVariables {
public:

	unsigned int nIntVar;
	int *intVarIndex;

	IntegerVariables(SimplexProblem *inProb){
		createIntegerArray(inProb);
	}
	virtual ~IntegerVariables(){
		delete [] intVarIndex;
	}

	void createIntegerArray(SimplexProblem *inProb){
		int count;

		// Count number of integer variable
		count = 0;
		for(unsigned int i=0; i<inProb->nVar; i++)
			if(inProb->isBoundInteger(i))
				count++;

		// Create integer variable index array
		nIntVar = count;
		intVarIndex = new int[count];

		// Memorize integral variable index
		count = 0;
		for(unsigned int i=0; i<inProb->nVar; i++){
			if(inProb->isBoundInteger(i)){
					intVarIndex[count++] =  i;
			}
		}
	}

	int getVarIndex(int integerIndex){
		return intVarIndex[integerIndex];
	}

};

#endif /* INTEGERVARIABLES_H_ */
