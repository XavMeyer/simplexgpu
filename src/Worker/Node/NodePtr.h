//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * NodePtr.h
 *
 *  Created on: Jun 15, 2011
 *      Author: meyerx
 */

#ifndef NODEPTR_H_
#define NODEPTR_H_

#include "Node.h"

template <node_comparator_t NodeComp>
class NodePtr {
public:
	Node<NodeComp> *ptr;

	NodePtr(Node<NodeComp> *inNode) {ptr = inNode;}
	virtual ~NodePtr() {ptr = NULL;}

	inline bool operator < (const NodePtr<NodeComp> &rhs) const
	{
		return (*(this->ptr)) < (*(rhs.ptr));
	}

	inline bool operator > (const NodePtr<NodeComp> &rhs) const
	{
		return (*(this->ptr)) > (*(rhs.ptr));
	}

};

#endif /* NODEPTR_H_ */
