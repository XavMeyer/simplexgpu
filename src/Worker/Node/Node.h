//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Node.h
 *
 *  Created on: Jun 6, 2011
 *      Author: meyerx
 */

#ifndef NODE_H_
#define NODE_H_

#include "../../Solver/Config.h"
#include <stdlib.h>
#include <cutil_math.h>

typedef enum {BEST, ESTIMATE} node_comparator_t;

template <node_comparator_t NodeComp>
class Node {
public:

	// Constructor / Destructor
	Node(){init();}

	Node(Node *inParent){init(inParent);}

	virtual ~Node(){
		parent = NULL;
		child_lo = NULL;
		child_up = NULL;
	};
	void init(){
		nChild = -1;
		child_lo = NULL;
		child_up = NULL;
		parent = NULL;
		level = 0;
	}

	void init(Node *inParent){
		nChild = -1;
		child_lo = NULL;
		child_up = NULL;
		parent = inParent;
		level = parent->level+1;
		objValue = parent->objValue;
		inParent->nChild++;
	}

	bool isUpChild(){
		if(parent == NULL) return false;
		if(parent->child_up == this)
			return true;
		else
			return false;
	}

	bool isLowChild(){
		if(parent == NULL) return false;
		if(parent->child_lo == this)
			return true;
		else
			return false;
	}

	bool operator > (const Node<NodeComp> &rhs);
	bool operator < (const Node<NodeComp> &rhs);

	double getScore();

	// Tree structure
	Node *parent;
	short int nChild; // -1 means not yet bound, 0 means no child, etc.
	Node *child_lo;
	Node *child_up;

	// Datas
	unsigned int level;
	int modifiedBound;
	float fVal;
	float2 oldBound;
	float2 newBound;
	double objValue;
	double estimateValue;

};

#endif /* NODE_H_ */
