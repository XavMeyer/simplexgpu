//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * NodeComparator.cpp
 *
 *  Created on: Jun 20, 2011
 *      Author: meyerx
 */

#include "Node.h"

/******************** BEST NODES ********************/
template <>
bool Node<BEST> ::operator > (const Node<BEST> &rhs){
	if(fabs(this->objValue - rhs.objValue) < EPS1){
		if(fabs(this->estimateValue - rhs.estimateValue) < EPS1) {
			// If same Estimate and obj value, sort in function of index
			return this->modifiedBound > rhs.modifiedBound;
		} else {
			// If same obj value sort in function of estimate value
			return this->estimateValue > rhs.estimateValue;
		}
	} else {
		// If obj value are different, sort in function of obj value
		return this->objValue > rhs.objValue;
	}
}

template <>
bool Node<BEST> ::operator < (const Node<BEST> &rhs){
	if(fabs(this->objValue - rhs.objValue) < EPS1){
		if(fabs(this->estimateValue - rhs.estimateValue) < EPS1) {
			// If same Estimate and obj value, sort in function of index
			return this->modifiedBound < rhs.modifiedBound;
		} else {
			// If same obj value sort in function of estimate value
			return this->estimateValue < rhs.estimateValue;
		}
	} else {
		// If obj value are different, sort in function of obj value
		return this->objValue < rhs.objValue;
	}
}

template <>
double Node<BEST> ::getScore(){
	return this->objValue;
}
/****************************************************/


/****************** ESTIMATE NODES ******************/
template <>
bool Node<ESTIMATE> ::operator > (const Node<ESTIMATE> &rhs){
	if(fabs(this->estimateValue - rhs.estimateValue) < EPS1) {
		// If same Estimate and obj value, sort in function of index
		return this->modifiedBound > rhs.modifiedBound;
	} else {
		// If same obj value sort in function of estimate value
		return this->estimateValue > rhs.estimateValue;
	}
}

template <>
bool Node<ESTIMATE> ::operator < (const Node<ESTIMATE> &rhs){
	if(fabs(this->estimateValue - rhs.estimateValue) < EPS1) {
		// If same Estimate and obj value, sort in function of index
		return this->modifiedBound < rhs.modifiedBound;
	} else {
		// If same obj value sort in function of estimate value
		return this->estimateValue < rhs.estimateValue;
	}
}

template <>
double Node<ESTIMATE> ::getScore(){
	return this->estimateValue;
}
/****************************************************/
