//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.cpp
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#include "WorkerCNB.h"


template<class SolverImpl>
WorkerCNB<SolverImpl>::WorkerCNB() {
	logger = new Logger(Logger::DISABLE);
}

template<class SolverImpl>
WorkerCNB<SolverImpl>::WorkerCNB(int device) {
	workerID = device;
	logger = new Logger(Logger::DISABLE);
}

template<class SolverImpl>
WorkerCNB<SolverImpl>::WorkerCNB(Logger *inL, int device) : logger(inL){
	workerID = device;
}

template<class SolverImpl>
WorkerCNB<SolverImpl>::~WorkerCNB() {

	if(solverLogger != NULL)
		delete solverLogger;

	if(cProb != NULL)
		delete cProb;

	if(localBSol != NULL)
		delete localBSol;

	if(cSol != NULL)
		delete cSol;

}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::init(){

	// Solver is by default in silent mode
	solverLogger 	= new Logger(Logger::DISABLE);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "] init solver with device : " << workerID;
		logger->log(Logger::LOG_DEBUG, oss.str());
	}
	solver = new SolverImpl(solverLogger, GPUS[workerID]);

	//localBSol = NULL;
	//cSol = NULL;
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::setDevice(int inDevice){
	workerID = inDevice;
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::setProblemData(SimplexSolution *inISol, bool *inBSolFound, SimplexSolution *inBSol){

	iSol = inISol;
	bSolFound = inBSolFound;
	bSol = inBSol;

	if(*bSolFound)
		localBSol = bSol->clone();
	else
		localBSol = NULL;

	cSol = new SimplexSolution(bSol->nVar, bSol->nSlack);

}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::setBNBTools(IntegerVariables *inIntVar, ParNodeSelectionStrategy *inNSS, ParBranchingStrategy *inBS, ParReliability *inRel){
	intVar = inIntVar;
	nss = inNSS;
	bs = inBS;
	rel = inRel;
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::setMngContext(manager_context_t *inMngContext){
	mngContext = inMngContext;
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::setProblem(SimplexProblem *inP){
	problem = inP;
	cProb = problem->clone();
}


template<class SolverImpl>
void WorkerCNB<SolverImpl>::initWorkerCNB(){

	// DBG_init(); // Todo remove

	// set the initial problem to the solver
	solver->setProblem(cProb);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "]Problem transmitted to solver.";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::start(){
	pthread_create(&mThread, NULL, WorkerCNB<SolverImpl>::run, this);
}

template<class SolverImpl>
void * WorkerCNB<SolverImpl>::run(void * arg){

	WorkerCNB<SolverImpl> *w = (WorkerCNB<SolverImpl>*)arg;
	w->cutAndBranch();

	return NULL;
}


template<class SolverImpl>
solver_res_t WorkerCNB<SolverImpl>::solve(){

	float gain;
	solver_res_t res = solver->solve();

	// DBG_check(res); // todo remove

	if(res == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(cSol);
		// set the current node obj value
		nss->setCurrentNodeObjValue(workerID, cSol->objVal);

		gain = cSol->objVal - nss->getParentObjValue(workerID);
		if(problem->isMax())
			gain *= -1.0;

		nss->updatePCost(workerID, gain);
	}

	// default "solving"
	return res;
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::updateLocalBSol() {

	// this part must be safely executed
	sem_wait(&mngContext->bSolMutex);

	if(*bSolFound){
		// There is not yet a local solution
		if(localBSol == NULL){
			localBSol = bSol->clone();
		// There is a global best solution who is better than our local
		} else if((problem->isMax() && (bSol->objVal > localBSol->objVal)) ||
				  (problem->isMin() && (bSol->objVal < localBSol->objVal))) {
			// we copy the global into the local
			localBSol->copy(bSol);
		}
	}

	sem_post(&mngContext->bSolMutex);
}

template<class SolverImpl>
void WorkerCNB<SolverImpl>::updateGlobalBSol() {

	// this part must be safely executed
	sem_wait(&mngContext->bSolMutex);
	// First solution found
	if(!(*bSolFound)){
		// Setting the bSolFound bool to true and copying the localBSol into the global bSol
		*bSolFound = true;
		bSol->copy(localBSol);
		nss->clearUselessNodes(bSol);
				// local best solution is better than global best solution
	} else if ((problem->isMax() && (localBSol->objVal > bSol->objVal)) ||
			   (problem->isMin() && (localBSol->objVal < bSol->objVal))) {
		//copying the localBSol into the global bSol
		bSol->copy(localBSol);
		nss->clearUselessNodes(bSol);
	}
	sem_post(&mngContext->bSolMutex);
}


template<class SolverImpl>
void WorkerCNB<SolverImpl>::cutAndBranch(){

	int intVarPos, nCreatedNode;
	node_type_t nodeType;
	solver_res_t result;

	// init the solver
	init();

	// init the problem, class, etc.
	initWorkerCNB();

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "] rdy!";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}
	pthread_barrier_wait(&mngContext->barrier);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "] start!";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	// Select the next node
	 while(nss->selectNextNode(workerID, localBSol, cProb, solver)){

		// Solve the modified problem
		result = this->solve();
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "W[" << workerID << "] Solving done";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}

		// Select a node
		// If the solver found a solution, we are trying to create new node
		if(result == SUCCESS){

			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "W[" << workerID << "] Solving success";
				logger->log(Logger::LOG_DEBUG, oss.str());
			}

			// Is this a potential better branch ?
			updateLocalBSol(); // Updating the localBSol before selecting the next variable

			if(localBSol == NULL || // No existing solution
			   ((problem->isMax() && (cSol->objVal > localBSol->objVal)) ||  // Potentially better
			    (problem->isMin() && (cSol->objVal < localBSol->objVal)))){  // Potentially better

				nodeType = bs->selectNextVariable(intVarPos, cSol, cProb, solver);

				if(nodeType == BRANCH){ // Has probably children
					if(logger->isActive()){
						ostringstream oss(ostringstream::out);
						oss << "W[" << workerID << "] Selected variable is : " << intVarPos;
						logger->log(Logger::LOG_DEBUG, oss.str());
					}
					nCreatedNode += nss->createNodes(workerID, intVarPos, cSol, cProb);
				} else if(nodeType == LEAF){ // Integral solution, better than best
					if(localBSol == NULL) { // first solution
						localBSol = cSol->clone();
						if(logger->isActive()){
							ostringstream oss(ostringstream::out);
							oss << "W[" << workerID << "] New best!" << endl;
							oss << localBSol->toString();
							logger->log(Logger::LOG_DEBUG, oss.str());
						}
					} else {
						localBSol->copy(cSol); // New best solution
						if(logger->isActive()){
							ostringstream oss(ostringstream::out);
							oss << "W[" << workerID << "] New best!" << endl;
							oss << localBSol->toString();
							logger->log(Logger::LOG_DEBUG, oss.str());
						}
					}
					// Local best solution has been updated
					// we have to check if this must be reported to the global best solution
					updateGlobalBSol();
				} // else we prune this node
			}
		} else {
			ostringstream oss(ostringstream::out);
			oss << "W[" << workerID << "] Solving unseccessful";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
		sem_post(&mngContext->sem); // wake up manager
		updateLocalBSol(); // Updating the local best solution before asking for a new node
	}
	sem_post(&mngContext->sem); // wake up manager
	cout << "Worker [" << workerID << "] ultimate wake up!" << endl;

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "] finished!";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	pthread_barrier_wait(&mngContext->barrier);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "W[" << workerID << "] out!";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	if(solver != NULL)
		delete solver;
}

template<class SolverImpl>
pthread_t WorkerCNB<SolverImpl>::getPThread(){
	return mThread;
}

template <class SolverImpl>
void WorkerCNB<SolverImpl> ::DBG_init(){
	/**** DBG ****/
		_rSol = new SimplexSolution(cProb->nVar, cProb->nSlack);
		for(int i=0; i<_rSol->nVar; i++){
			_rSol->xVal[i] = 0;
		}
		_rSol->xVal[2] = 1.0;
		_rSol->xVal[4] = 1.0;
		_rSol->xVal[5] = 1.0;
		_rSol->xVal[6] = 1.0;
		_rSol->xVal[9] = 1.0;
		_rSol->xVal[10] = 1.0;
		_rSol->xVal[11] = 1.0;
		_rSol->xVal[16] = 1.0;
		_rSol->xVal[17] = 1.0;
		_rSol->xVal[18] = 1.0;
		_rSol->xVal[19] = 1.0;
		_rSol->xVal[24] = 1.0;
		_rSol->xVal[26] = 1.0;
		_rSol->xVal[29] = 1.0;
		_rSol->xVal[30] = 1.0;
		/*****END DBG*****/

}

template <class SolverImpl>
void WorkerCNB<SolverImpl> ::DBG_check(solver_res_t res){

	bool feas = true;
	for(int i=0; i<_rSol->nVar; i++){
		if((_rSol->xVal[i] < cProb->xBounds[i].x) || (_rSol->xVal[i] > cProb->xBounds[i].y)){
			feas = false;
		}
	}

	if((res != SUCCESS) && feas){
		if(res == UNBOUNDED_AUX){
			logger->log(Logger::LOG_CRITICAL, "Aux infeasible : Unbounded problem.");
		} else if(res == UNBOUNDED){
			logger->log(Logger::LOG_CRITICAL, "Problem is not bounded!");
		} else if(res == NO_SOLUTION){
			logger->log(Logger::LOG_CRITICAL, "Dual infeasible : No initial dictionary found!");
		} else if(res == INFEASIBLE){
			logger->log(Logger::LOG_CRITICAL, "Problem is infeasible!");
		} else if(res == MAX_ITER_REACHED){
			logger->log(Logger::LOG_CRITICAL, "Max iteration reached without founding an optimum.");
		}
	}

}
