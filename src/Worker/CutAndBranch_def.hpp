//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.cpp
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#include "CutAndBranch.h"

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::CutAndBranch() {
	init(0);
	logger = new Logger(Logger::DISABLE);
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::CutAndBranch(Logger *inL) : logger(inL){
	init(0);
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::CutAndBranch(int device) {
	init(device);
	logger = new Logger(Logger::DISABLE);
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::CutAndBranch(Logger *inL, int device) : logger(inL){
	init(device);
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::~CutAndBranch() {

	if(solver != NULL)
		delete solver;
	if(solverLogger != NULL)
		delete solverLogger;

	if(cSol != NULL)
		delete cSol;
	if(bSol != NULL)
		delete bSol;
	if(iSol != NULL)
		delete iSol;

	if(cProb != NULL)
		delete cProb;

	if(intVar != NULL)
		delete intVar;

	if(bs != NULL)
		delete bs;
	if(nss != NULL)
		delete nss;
	if(cg != NULL)
		delete cg;

	if(rel != NULL)
		delete rel;

	logger 		= NULL;
	problem 	= NULL;
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
void CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::init(int device){

	nCreatedNode = nSolvedNode = nInfNode =0;

	// Solver is by default in silent mode
	solverLogger 	= new Logger(Logger::ALL);

#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif

	problem 		= NULL;
	cProb 			= NULL;

	cSol			= NULL;
	bSol 			= NULL;
	iSol			= NULL;

	intVar 			= NULL;

	_START_EVENT(prof, "EXTERNAL_CLASS_INIT")

	solver 			= new SolverImpl(solverLogger, device);
	bs 				= new BranchStrat(logger);
	nss 			= new NodeSelStrat(logger);
	cg				= new CutGen(logger);

	_END_EVENT(prof, "EXTERNAL_CLASS_INIT")

}


template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
void CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::setProblem(SimplexProblem *inP){
	problem = inP;
	cProb = problem->clone();
}


template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
void CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::initCutAndBranch(SimplexProblem *inP){

	_START_EVENT(prof,"SIMPLEX_PROBLEM")

	// set the initial problem to the worker
	setProblem(inP);
	// Init the current solution
	iSol = new SimplexSolution(problem->nVar, problem->nRow);

	// set the initial problem to the solver
	solver->setProblem(cProb);
	// Create the integer variable index
	intVar = new IntegerVariables(cProb);
	// Create the pseudo-costs / reliability object
	rel = new Reliability(intVar->nIntVar);

	if(logger->isActive()){
		logger->log(Logger::LOG_DEBUG, "Problem transmitted to solver.");
	}

	_END_EVENT(prof,"SIMPLEX_PROBLEM")
}


template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
void CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::initialRelaxation(){
	int intVarPos;
	node_type_t nodeType;
	solver_res_t res;

	_START_EVENT(prof,"INITIAL_RELAXATION")

	// Initial relaxation is done in initialization in order to memorize the
	// initial solution (and avoid a unneeded check in the main loop)

	// Solve the initial problem
	// If the solver found a solution, we are trying to create new node
	res = solver->solve();

	_END_EVENT(prof,"INITIAL_RELAXATION")

	if(res == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(iSol);

		if(!iSol->isIntegral()){
			// Init the cut generator, create the cuts and add them to the "current problem"
			addCuts();

			// Create cSol
			cSol = new SimplexSolution(cProb->nVar, cProb->nRow);
			cSol->objVal = iSol->objVal;
			memcpy(cSol->xVal, iSol->xVal, iSol->nVar*sizeof(VAR_TYPE));

		} else {
			cSol = iSol->clone();
		}

		// Init the branching strategy and node selection strategy
		bs->initBS(intVar, rel, cProb);
		nss->initNSS(intVar, rel, cProb, iSol);

		// set the root node value
		nss->setCurrentNodeObjValue(cSol->objVal);

		// choose next variable for branching
		nodeType = bs->selectNextVariable(intVarPos, cSol, solver);

		if(nodeType == LEAF) { // Integer solution
			bSol = iSol->clone(); // best sol already found
			if(logger->isActive()){
				logger->log(Logger::LOG_DEBUG, "New best!");
				logger->log(Logger::LOG_DEBUG, bSol->toString());
			}
		} else if(nodeType == BRANCH) { // Has children probably
			nCreatedNode += nss->createNodes(intVarPos, cSol); // trying to create children
		} // else do nothing, we can't prune the root node!
	}
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
void CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::addCuts() {

	_START_EVENT(prof, "CUT_GENERATION")

	// Clone the current problem
	SimplexProblem *relaxedProb = cProb->clone();
	SimplexProblem *cutProb = problem->clone();
	// Get the relaxed problem
	solver->getProblem(relaxedProb);
	// init the cut generator (use the relaxed simplex tableau)
	cg->init(problem, iSol, relaxedProb, intVar);
	// generate the cuts
	cg->generateCuts();
	// modify the current problem by adding the generated cuts
	cg->modifyProblem(cutProb);
	// Modify the current solver problem, with the new one
	solver->setProblem(cutProb);

	// Replace cProb by cutProb
	delete cProb;
	cProb = cutProb;

	delete relaxedProb;

	_END_EVENT(prof, "CUT_GENERATION")
}


template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
solver_res_t CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::solve(){

	float gain;
	solver_res_t res = solver->solve();

	cProb->nbRelaxation++;
	cProb->meanIter += (solver->getCounter() - cProb->meanIter)/cProb->nbRelaxation;

	if(res == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(cSol);
		// set the current node obj value
		nss->setCurrentNodeObjValue(cSol->objVal);

		gain = cSol->objVal - nss->getParentObjValue();
		if(problem->isMax())
			gain *= -1.0;

		nss->updatePCost(gain);
	}

	// default "solving"
	return res;
}


template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
SimplexSolution * CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::cutAndBranch(SimplexProblem *inP){

	int intVarPos;
	unsigned int nNextPrint = INFO_FREQUENCY;
	node_type_t nodeType;
	solver_res_t result;

	_START_EVENT(prof, "INIT")

	// init the problem, class, etc.
	initCutAndBranch(inP);

	// Do the first relaxation and create first nodes
	initialRelaxation();

	_END_EVENT(prof, "INIT")

	// TODO CHECK IF WORKING
	solver->saveWarmstart();
	solver->enableWarmstart();

	// Select the next node
	while(nss->selectNextNode(bSol, solver)){

		// Solve the modified problem

		_START_EVENT(prof, "PROBLEM_RELAXATION")

		result = this->solve();
		nSolvedNode++;

		_END_EVENT(prof, "PROBLEM_RELAXATION")

		// Select a node
		// If the solver found a solution, we are trying to create new node
		if(result == SUCCESS){

			// Is this a potential better branch ?
			if((bSol == NULL) || // No existing solution
			   ((problem->isMax() && (cSol->objVal > bSol->objVal)) ||  // Potentially better
			    (problem->isMin() && (cSol->objVal < bSol->objVal)))){  // Potentially better

				nodeType = bs->selectNextVariable(intVarPos, cSol, solver);

				if(nodeType == BRANCH){ // Has probably children
					if(logger->isActive()){
						ostringstream oss(ostringstream::out);
						oss << "Selected variable is : " << intVarPos;
						logger->log(Logger::LOG_DEBUG, oss.str());
					}
					nCreatedNode += nss->createNodes(intVarPos, cSol);
				} if(nodeType == LEAF){ // Integral solution, better than best
					if(bSol == NULL) { // first solution
						bSol = cSol->clone();
						if(logger->isActive()){
							logger->log(Logger::LOG_DEBUG, "New best!");
							logger->log(Logger::LOG_DEBUG, bSol->toString());
						}
					} else {
						bSol->copy(cSol); // New best solution
						if(logger->isActive()){
							logger->log(Logger::LOG_DEBUG, "New best!");
							logger->log(Logger::LOG_DEBUG, bSol->toString());
						}
					}
					nss->clearUselessNodes(bSol);
				} // else we prune this node
			}
		} else {
			logger->log(Logger::LOG_DEBUG, "Solving not successful.");
			nInfNode++;
		}

		if(logger->isActive() && (nSolvedNode > nNextPrint)){
			nNextPrint+=INFO_FREQUENCY;
			ostringstream oss(ostringstream::out);
			oss << "After " << nSolvedNode << " nodes, ";
			if(bSol == NULL)
				oss << "no integer sol found, ";
			else
				oss << "best sol : " << fixed << bSol->objVal << ", ";

			oss << (nCreatedNode-nSolvedNode) << " remaining node.";
			logger->log(Logger::LOG_INFO, oss.str());
		}
	}
	if(bSol != NULL){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished." << endl;
			oss << "Solution : " << bSol->objVal << endl;
			oss << bSol->toString() << endl;
			oss << "<------------- Statistics ------------->" << endl;
			oss << "*Branch and bound nodes : " << endl;
			oss << " Total node created : " << nCreatedNode << endl;
			oss << " Total node solved : " << nSolvedNode << endl;
			oss << " Total node pruned : " << (nCreatedNode - nSolvedNode);
			oss << solver->getStats()->toString().c_str() << endl;
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	} else {
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished : no solution found." << endl;
			oss << "<------------- Statistics ------------->" << endl;
			oss << "*Branch and bound nodes : " << endl;
			oss << " Total node created : " << nCreatedNode << endl;
			oss << " Total node solved : " << nSolvedNode << endl;
			oss << " Total node pruned : " << (nCreatedNode - nSolvedNode);
			oss << solver->getStats()->toString().c_str() << endl;
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	}
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
unsigned int CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::getNSolvedNode(){
	return nSolvedNode;
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
unsigned int CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::getNCreatedNode(){
	return nCreatedNode;
}

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
unsigned int CutAndBranch<BranchStrat, NodeSelStrat, CutGen, SolverImpl>::getNInfNode(){
	return nInfNode;
}
