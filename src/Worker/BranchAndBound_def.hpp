//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBound.cpp
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#include "BranchAndBound.h"

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::BranchAndBound() {
	init(0);
	logger = new Logger(Logger::DISABLE);
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::BranchAndBound(Logger *inL) : logger(inL){
	init(0);
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::BranchAndBound(int device) {
	init(device);
	logger = new Logger(Logger::DISABLE);
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::BranchAndBound(Logger *inL, int device) : logger(inL){
	init(device);
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::~BranchAndBound() {

	if(solver != NULL)
		delete solver;
	if(solverLogger != NULL)
		delete solverLogger;

	if(cSol != NULL)
		delete cSol;
	if(bSol != NULL)
		delete bSol;
	if(iSol != NULL)
		delete iSol;

	if(cProb != NULL)
		delete cProb;

	if(intVar != NULL)
		delete intVar;

	if(bs != NULL)
		delete bs;
	if(nss != NULL)
		delete nss;

	if(rel != NULL)
		delete rel;

	logger 		= NULL;
	problem 	= NULL;
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
void BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::init(int device){

	nCreatedNode = nSolvedNode = nInfNode =0;

	// Solver is by default in silent mode
	solverLogger 	= new Logger(Logger::DISABLE);

	problem 		= NULL;
	cProb 			= NULL;

	cSol			= NULL;
	bSol 			= NULL;
	iSol			= NULL;

	intVar 			= NULL;

	solver 			= new SolverImpl(solverLogger, device);
	bs 				= new BranchStrat(logger);
	nss 			= new NodeSelStrat(logger);

}


template<class BranchStrat, class NodeSelStrat, class SolverImpl>
void BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::setProblem(SimplexProblem *inP){
	problem = inP;
	cProb = problem->clone();
}


template<class BranchStrat, class NodeSelStrat, class SolverImpl>
void BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::initBranchAndBound(SimplexProblem *inP){
	// set the initial problem to the worker
	setProblem(inP);
	// Init the current solution
	cSol = new SimplexSolution(problem->nVar, problem->nRow);

	// set the initial problem to the solver
	solver->setProblem(cProb);
	// Create the integer variable index
	intVar = new IntegerVariables(cProb);
	// Create the pseudo-costs / reliability object
	rel = new Reliability(intVar->nIntVar);

	if(logger->isActive()){
		logger->log(Logger::LOG_DEBUG, "Problem transmitted to solver.");
	}
}


template<class BranchStrat, class NodeSelStrat, class SolverImpl>
void BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::initialRelaxation(){
	int intVarPos;
	node_type_t nodeType;

	// Initial relaxation is done in initialization in order to memorize the
	// initial solution (and avoid a unneeded check in the main loop)

	// Solve the initial problem
	// If the solver found a solution, we are trying to create new node
	if(solver->solve() == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(cSol);
		iSol = cSol->clone(); // memorizing the initial solution

		// Init the branching strategy and node selection strategy
		bs->initBS(intVar, rel, cProb);
		nss->initNSS(intVar, rel, cProb, iSol);

		// set the root node value
		nss->setCurrentNodeObjValue(cSol->objVal);

		// choose next variable for branching
		nodeType = bs->selectNextVariable(intVarPos, cSol, solver);

		if(nodeType == LEAF) { // Integer solution
			bSol = cSol->clone(); // best sol already found
			if(logger->isActive()){
				logger->log(Logger::LOG_DEBUG, "New best!");
				logger->log(Logger::LOG_DEBUG, bSol->toString());
			}
		} else if(nodeType == BRANCH) { // Has children probably
			nCreatedNode += nss->createNodes(intVarPos, cSol); // trying to create children
		} // else do nothing, we can't prune the root node!
	}
}


template<class BranchStrat, class NodeSelStrat, class SolverImpl>
solver_res_t BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::solve(){

	float gain;
	solver_res_t res = solver->solve();

	cProb->nbRelaxation++;
	cProb->meanIter += (solver->getCounter() - cProb->meanIter)/cProb->nbRelaxation;

	if(res == SUCCESS){
		// Retrieve the current solution
		solver->getSolution(cSol);
		// set the current node obj value
		nss->setCurrentNodeObjValue(cSol->objVal);

		gain = cSol->objVal - nss->getParentObjValue();
		if(problem->isMax())
			gain *= -1.0;

		nss->updatePCost(gain);
	}

	// default "solving"
	return res;
}


template<class BranchStrat, class NodeSelStrat, class SolverImpl>
SimplexSolution * BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::branchAndBound(SimplexProblem *inP){

	int intVarPos;
	unsigned int nNextPrint = INFO_FREQUENCY;
	node_type_t nodeType;
	solver_res_t result;

	// init the problem, class, etc.
	initBranchAndBound(inP);

	// Do the first relaxation and create first nodes
	initialRelaxation();

	// Select the next node
	 while(nss->selectNextNode(bSol, solver)){

		// Solve the modified problem
		result = this->solve();
		nSolvedNode++;

		// Select a node
		// If the solver found a solution, we are trying to create new node
		if(result == SUCCESS){

			// Is this a potential better branch ?
			if((bSol == NULL) || // No existing solution
			   ((problem->isMax() && (cSol->objVal > bSol->objVal)) ||  // Potentially better
			    (problem->isMin() && (cSol->objVal < bSol->objVal)))){  // Potentially better

				nodeType = bs->selectNextVariable(intVarPos, cSol, solver);

				if(nodeType == BRANCH){ // Has probably children
					if(logger->isActive()){
						ostringstream oss(ostringstream::out);
						oss << "Selected variable is : " << intVarPos;
						logger->log(Logger::LOG_DEBUG, oss.str());
					}
					nCreatedNode += nss->createNodes(intVarPos, cSol);
				} if(nodeType == LEAF){ // Integral solution, better than best
					if(bSol == NULL) { // first solution
						bSol = cSol->clone();
						if(logger->isActive()){
							logger->log(Logger::LOG_DEBUG, "New best!");
							logger->log(Logger::LOG_DEBUG, bSol->toString());
						}
					} else {
						bSol->copy(cSol); // New best solution
						if(logger->isActive()){
							logger->log(Logger::LOG_DEBUG, "New best!");
							logger->log(Logger::LOG_DEBUG, bSol->toString());
						}
					}
					nss->clearUselessNodes(bSol);
				} // else we prune this node
			}
		} else {
			nInfNode++;
			logger->log(Logger::LOG_DEBUG, "Solving not successful.");
		}

		if(logger->isActive() && (nSolvedNode > nNextPrint)){
			nNextPrint+=INFO_FREQUENCY;
			ostringstream oss(ostringstream::out);
			oss << "After " << nSolvedNode << " nodes, ";
			if(bSol == NULL)
				oss << "no integer sol found, ";
			else
				oss << "best sol : " << fixed << bSol->objVal << ", ";

			oss << (nCreatedNode-nSolvedNode) << " remaining node.";
			logger->log(Logger::LOG_INFO, oss.str());
		}
	}
	if(bSol != NULL){
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished." << endl;
			oss << "Solution : " << bSol->objVal << endl;
			oss << bSol->toString() << endl;
			oss << "Total node created : " << nCreatedNode << endl;
			oss << "Total node solved : " << nSolvedNode << endl;
			oss << "Total node pruned : " << (nCreatedNode - nSolvedNode);
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	} else {
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Branch and bound finished : no solution found." << endl;
			oss << "Total node created : " << nCreatedNode << endl;
			oss << "Total node solved : " << nSolvedNode << endl;
			oss << "Total node pruned : " << (nCreatedNode - nSolvedNode);
			logger->log(Logger::LOG_INFO, oss.str());
		}
		return bSol;
	}
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
unsigned int BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::getNSolvedNode(){
	return nSolvedNode;
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
unsigned int BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::getNCreatedNode(){
	return nCreatedNode;
}

template<class BranchStrat, class NodeSelStrat, class SolverImpl>
unsigned int BranchAndBound<BranchStrat, NodeSelStrat, SolverImpl>::getNInfNode(){
	return nInfNode;
}

