//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BFSStragegy_def.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef BFSSTRATEGY_DEF
#define BFSSTRATEGY_DEF

#include "BFSStrategy.h"

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
BFSStrategy<NodeComp, PLUNGE_MODE>::BFSStrategy(Logger *inL) :
												NodeSelectionStrategy(inL){
	maxLevel = 0;
	plungMin = plungDone = plungMax = 0;

	initXBounds = NULL;

	focusN = NULL;
	currentN = NULL;

	sortedNodes = NULL;
	tree = NULL;

	//pool = NULL;

	plunging = false;
	//execMode = SEQUENTIAL;
	//nWaitingWorker = 0;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
BFSStrategy<NodeComp, PLUNGE_MODE>::~BFSStrategy() {
	currentN = NULL;
	focusN = NULL;
	if(sortedNodes != NULL)
		delete sortedNodes;
	if(tree != NULL)
		delete tree;
	if(initXBounds != NULL)
		delete [] initXBounds;
	//if(pool != NULL)
		//delete pool;
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::processEstimates(unsigned int intVarPos, SimplexSolution *cSol,
		                                                double &estimateDown, double &estimateUp) {
	int index = 0;
	double upCost, downCost, f;
	double estimate = 0;

	// for each fractional "integer" defined variable (but the branching var)
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		if(!cSol->isIntegral(index) && intVarPos != i){
			// Process the lowest estimate decrease
			f = cSol->xVal[index] - floor(cSol->xVal[index]);
			upCost = (1.0-f)*rel->getPcostUp(i);
			downCost = f*rel->getPcostDown(i);
			estimate += upCost < downCost ? upCost : downCost;
		}
	}

	// Special case for branching variable
	index = intVar->getVarIndex(intVarPos);
	// in case we are going up
	estimateUp = (ceil(cSol->xVal[index]) - cSol->xVal[index]) * rel->getPcostUp(intVarPos);
	estimateUp += estimate;
	// in case we are going down
	estimateDown = (cSol->xVal[index] - floor(cSol->xVal[index])) * rel->getPcostDown(intVarPos);
	estimateDown += estimate;

	// If max changing the sign
	if(cProb->isMax()){
		estimateUp *= -1.0;
		estimateDown *= -1.0;
	}

	// Adding the current value
	estimateUp += cSol->objVal;
	estimateDown += cSol->objVal;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::insertNode(SpecNode *node){
	if(PLUNGE_MODE){
		if(!plunging)
			sortedNodes->push(node);
		else
			childrenNodes.push_back(NodePtr<NodeComp>(node));
	} else {
		sortedNodes->push(node);
	}
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
short int BFSStrategy<NodeComp, PLUNGE_MODE> ::createNodes(int intVarPos, SimplexSolution *cSol) {
	int index = intVar->getVarIndex(intVarPos);
	int lo, up;
	short newNode = 0;
	double estimateDown, estimateUp;

	// Get the estimate "integral solution" obj value
	processEstimates(intVarPos, cSol, estimateDown, estimateUp);

	// Check lower bound, new node ?
	lo = floor(cSol->xVal[index]);
	if(lo >= cProb->xBounds[index].x){
		tree->createChildLo(currentN);
		currentN->child_lo->estimateValue = estimateDown;
		currentN->child_lo->modifiedBound = intVarPos;
		currentN->child_lo->fVal = cSol->xVal[index] - lo;
		currentN->child_lo->oldBound = cProb->xBounds[index];
		currentN->child_lo->newBound = make_float2(cProb->xBounds[index].x,lo);

		newNode++;
		insertNode(currentN->child_lo);
		//if((execMode == PARALLEL) && !plunging) // If we are not plunging and we in a parallel bfs
		//	sem_post(pool); // Add one work to do on the pool

		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Lower node created ( x" << index << " - bound(" << currentN->child_lo->newBound.x;
			oss << ", " << currentN->child_lo->newBound.y << ") - level " << currentN->child_lo->level;
			oss << " obj : " << currentN->child_lo->objValue << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	}

	// Check upper bound, new node ?
	up = ceil(cSol->xVal[index]);
	if(up <= cProb->xBounds[index].y){
		tree->createChildUp(currentN);
		currentN->child_up->estimateValue = estimateUp;
		currentN->child_up->modifiedBound = intVarPos;
		currentN->child_up->fVal = up - cSol->xVal[index];
		currentN->child_up->oldBound = cProb->xBounds[index];
		currentN->child_up->newBound = make_float2(up,cProb->xBounds[index].y);

		newNode++;
		insertNode(currentN->child_up);
		//if((execMode == PARALLEL) && !plunging) // If we are not plunging and we in a parallel bfs
		//	sem_post(pool); // Add one work to do on the pool

		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Upper node created ( x" << index << " - bound(" << currentN->child_up->newBound.x;
			oss << ", " << currentN->child_up->newBound.y << ") - level " << currentN->child_up->level;
			oss << " obj : " << currentN->child_up->objValue << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	}

	if(PLUNGE_MODE){
		if(newNode == 2 && plunging){
			if(cmpNode(index, cSol) < 0){ // swaping
				NodePtr<NodeComp> tmp1 = childrenNodes.back();
				childrenNodes.pop_back();
				NodePtr<NodeComp> tmp2 = childrenNodes.back();
				childrenNodes.pop_back();
				childrenNodes.push_back(tmp1);
				childrenNodes.push_back(tmp2);
			}
		}
	}

	return newNode;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::clearUselessNodes(SimplexSolution *bSol) {

	if(sortedNodes->isEmpty()) return;

	if(bSol != NULL){
		if(NodeComp == BEST){
			//sortedNodes->sort();
			if(cProb->isMin()){
				while(!sortedNodes->isEmpty() && sortedNodes->worst()->objValue >= bSol->objVal){
					tree->deleteLeafAndCleanTree(sortedNodes->worst());
					sortedNodes->popWorst();
				}
			} else {
				while(!sortedNodes->isEmpty() && sortedNodes->worst()->objValue <= bSol->objVal){
					tree->deleteLeafAndCleanTree(sortedNodes->worst());
					sortedNodes->popWorst();
				}
			}
		} else {
			sortedNodes->initIterator(); // Init at worst node
			if(cProb->isMin()){
				while(sortedNodes->getCurrentNode() != sortedNodes->end()){
					if(sortedNodes->getCurrentNode()->objValue >= bSol->objVal){
						tree->deleteLeafAndCleanTree(sortedNodes->getCurrentNode());
						sortedNodes->deleteCurrentNode();
					} else {
						sortedNodes->iterNextNode();
					}
				}
			} else {
				while(sortedNodes->getCurrentNode() != sortedNodes->end()){
					if(sortedNodes->getCurrentNode()->objValue <= bSol->objVal){
						tree->deleteLeafAndCleanTree(sortedNodes->getCurrentNode());
						sortedNodes->deleteCurrentNode();
						//this->nCreatedNode--;
					} else {
						sortedNodes->iterNextNode();
					}
				}
			}
		}
	}
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE> ::selectBestNode(SimplexSolution *bSol, Solver *solver) {
	int index = 0;
	bool modified[intVar->nIntVar];
	float2 newBounds[intVar->nIntVar];
	// Clean if necessary
	//clearUselessNodes(bSol);
	if(sortedNodes->isEmpty()) return false;

	// Get best
	focusN = currentN = sortedNodes->popBest();

	// init
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		newBounds[i] = initXBounds[intVar->getVarIndex(i)];
		modified[i] = false;
	}

	// Create new bounds
	while(currentN->parent != NULL){
		if(!modified[currentN->modifiedBound]){
			newBounds[currentN->modifiedBound] = currentN->newBound;
			modified[currentN->modifiedBound] = true;
		}
		currentN = currentN->parent;
	}

	// Change bound, only for the different one
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		if((newBounds[i].x != cProb->xBounds[index].x) ||
		   (newBounds[i].y != cProb->xBounds[index].y)){
			solver->changeBound(index, newBounds[i]);
		}
	}

	// set current node as focuses node
	currentN = focusN;

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		index = intVar->getVarIndex(currentN->modifiedBound);
		oss << "Best node selected : ( x" << index << " - bound(" << currentN->newBound.x;
		oss << ", " << currentN->newBound.y << ") - level " << currentN->level;
		oss << " obj : " << currentN->objValue << ").";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	// Update plunging configuration
	if(maxLevel < currentN->level){
		maxLevel = currentN->level;
		plungMin = round(RATIO_MIN_PLUNGING*maxLevel);
		plungMax = round(RATIO_MAX_PLUNGING*maxLevel);
	}

	if(PLUNGE_MODE && plungMax > 0){
		plunging = true;
		plungDone = 0;
	}

	return true;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE> ::isGapAcceptable(SimplexSolution *bSol) {
	double best;

	if(cProb->isMin()){
		if(bSol != NULL){
			best = bSol->objVal;
		} else {
			best = DBL_MAX;
		}

		return ((currentN->objValue - iSol->objVal)/(best - iSol->objVal)) < GAP_THRESHOLD;
	} else {
		if(bSol != NULL) {
			best = bSol->objVal;
		} else {
			best = -DBL_MAX;
		}

		return ((iSol->objVal - currentN->objValue)/(iSol->objVal - best)) < GAP_THRESHOLD;
	}
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE> ::selectNextPlungingNode(SimplexSolution *bSol, Solver *solver) {
	int index;

	// No more children
	if(childrenNodes.empty()) return false;

	// End of PLUNGE_MODE
	if(plungDone >= plungMax || !isGapAcceptable(bSol)){
		// Copying non-visited node into bestNode list
		while(!childrenNodes.empty()){
			sortedNodes->push(childrenNodes.back().ptr);
			//if(execMode == PARALLEL)
			//	sem_post(pool); // Add one work to do on the pool
			childrenNodes.pop_back();
		}
		return false;
	}

	// Backtracking if needed
	while(currentN->level >= childrenNodes.back().ptr->level){
		index = intVar->getVarIndex(currentN->modifiedBound);
		solver->changeBound(index, currentN->oldBound);
		if(currentN->isLowChild()){
			currentN = currentN->parent;
			tree->deleteLeaf(currentN->child_lo);
		} else {
			currentN = currentN->parent;
			tree->deleteLeaf(currentN->child_up);
		}
	}

	currentN = childrenNodes.back().ptr;
	childrenNodes.pop_back();
	index = intVar->getVarIndex(currentN->modifiedBound);
	solver->changeBound(index, currentN->newBound);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		index = intVar->getVarIndex(currentN->modifiedBound);
		oss << "Plunging node selected : ( x" << index << " - bound(" << currentN->newBound.x;
		oss << ", " << currentN->newBound.y << ") - level " << currentN->level;
		oss << " obj : " << currentN->objValue << ").";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	plungDone++;

	return true;
}



template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE> ::selectNextNode(SimplexSolution *bSol, Solver *solver) {

	bool res;

	_START_EVENT(prof, "NODE_SELECTION")

	if(PLUNGE_MODE){
		if(!plunging){ // we are going to select one from the priority queue
			res = selectBestNode(bSol, solver);
		} else {
			if(!selectNextPlungingNode(bSol, solver)){
				plunging = false;
				res = selectBestNode(bSol, solver);
			} else {
				res = true;
			}
		}
	} else {
		res = selectBestNode(bSol, solver);
	}

	_END_EVENT(prof, "NODE_SELECTION")

	return res;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
VAR_TYPE BFSStrategy<NodeComp, PLUNGE_MODE> ::getParentObjValue(){
	return currentN->parent->objValue;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::setCurrentNodeObjValue(VAR_TYPE objValue){
	currentN->objValue = objValue;
	currentN->nChild = 0;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::initNSS(IntegerVariables *inIV, Reliability *inR,
												  SimplexProblem *inCProb, SimplexSolution *inISol){

	NodeSelectionStrategy::initNSS(inIV, inR, inCProb, inISol);

	tree = new Tree<NodeComp>();
	sortedNodes = new SortedNodesV2<NodeComp>(cProb->isMin());
	currentN = focusN = tree->root;

	initXBounds = new float2[cProb->nVar];
	memcpy(initXBounds, cProb->xBounds, cProb->nVar*sizeof(float2));

}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void BFSStrategy<NodeComp, PLUNGE_MODE> ::updatePCost(float gain){
	if(currentN->isLowChild()){ // downward child
		rel->addGainDown(currentN->modifiedBound,
				         gain/currentN->fVal);
	} else { // upward child
		rel->addGainUp(currentN->modifiedBound,
				         gain/currentN->fVal);
	}
}

/*
template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE>::setModeParallel(int inNWorker){

	nWorker = inNWorker;
	execMode = PARALLEL;
	// todo implement

	sem_init(pool, 1, 0);
	return true;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool BFSStrategy<NodeComp, PLUNGE_MODE>::isTerminated(){
	bool terminated = sortedNodes->isEmpty();
	if(execMode == SEQUENTIAL) {
		return terminated;
	} else {
		// Ensure that the queue is empty and that all worker are waiting
		terminated = terminated && (nWaitingWorker == nWorker);

		// We have indeed terminated, we need to release tasks
		if(terminated) {
			for(int i=0; i<nWorker; i++)
				sem_post(pool);
		}

		return terminated;
	}
}
*/

#endif /* BFSSTRATEGY_DEF */
