//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BFSStragegy.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef PARBFSSTRAGEGY_H_
#define PARBFSSTRAGEGY_H_

#include "ParNodeSelectionStrategy.h"
#include "../Node/NodePtr.h"
#include "../DataStructure/ParSortedNodes.h"
#include "../DataStructure/Tree.h"

#include "../wtypes.h"

#include <semaphore.h>

// Ratio of min / max pseudocost to take into account
#define MU							0.3
// Minimal ratio of node to process during plunging
// (0.1*level == min nb node to process)
#define RATIO_MIN_PLUNGING			0.1
// Minimal ratio of node to process during plunging
// (0.1*level == max nb node to process)
#define RATIO_MAX_PLUNGING			0.5
// Gap tolerance between best sol and current sol while pluging
#define GAP_THRESHOLD				0.25

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
class ParBFSStrategy: public ParNodeSelectionStrategy {
public:
	ParBFSStrategy(int inNWorker, Logger *inL);
	virtual ~ParBFSStrategy();

	//! Overload of initNSS method (see NodeSelectionStrategy abstract class).
	void initNSS(IntegerVariables *inIV, ParReliability *inR,
				 SimplexProblem *inIProb, SimplexSolution *inISol);

	//! Clear the nodes that have an obj value that is worse than the best solution (bSol) obj value.
	void clearUselessNodes(SimplexSolution *bSol);
	short int createNodes(int workerID, int intVarPos, SimplexSolution *cSol, SimplexProblem *cProb);
	bool selectNextNode(int workerID, SimplexSolution *bSol, SimplexProblem *cProb, Solver *solver);
	VAR_TYPE getParentObjValue(int workerID);
	void setCurrentNodeObjValue(int workerID, VAR_TYPE objValue);
	void updatePCost(int workerID, float gain);
	bool isTerminated();

private:
	// Types
	typedef Node<NodeComp> SpecNode;
	typedef SpecNode* SpecNodePtr;

	// Shared data
	//* Plunging strategy
	unsigned int maxLevel;
	unsigned int plungMin, plungMax;

	//* Critical section and synchronisation
	sem_t pool, noGet, noClear, cntGetterMutex, cntWaitingWMutex;
	int nWaitingWorker, nGetters;
	bool terminated;

	//* Constant init bounds (read only after init)
	float2 *initXBounds;

	//* Data structures
	Tree<NodeComp> *tree;
	ParSortedNodes<NodeComp> *sortedNodes;

	// Local data
	bool *plunging; //* plunging mode
	unsigned int *plungeDone;

	//* current and focus nodes
	SpecNodePtr *focusN;
	SpecNodePtr *currentN;

	//* Local plunging buffer nodes
	vector<NodePtr<NodeComp> > *childrenNodes;

	/*****************************************************************************/
	/*********************** Create node related functions ***********************/
	/*****************************************************************************/

	//! Process the estimate downards and upwards score of the child nodes.
	void processEstimates(unsigned int varIndex, SimplexSolution *cSol,
						  double &estimateDown, double &estimateUp);
	//! Insert a node in the correct data structure.
	void insertNode(int workerID, SpecNode *node);

	/*****************************************************************************/
	/*********************** Select node related functions ***********************/
	/*****************************************************************************/
	//! Select the node having the best obj value (evaluation depends on NodeComp).
	bool selectBestNode(int workerID, SimplexSolution *bSol, SimplexProblem *cProb, Solver *solver);
	//! Control if the gap between the current node obj value and best node obj value gap
	//! is lower than a certain threshold (GAP_THRESHOLD).
	bool isGapAcceptable(int workerID, SimplexSolution *bSol);
	//! Plunge into the tree (local DFS).
	bool selectNextPlungingNode(int workerID, SimplexSolution *bSol, Solver *solver);

	//! Entering the critical section (writer)
	void enterClearSortedNodes();
	//! quitting the critical section (writer)
	void leaveClearSortedNodes();

	//! Entering the critical section (reader)
	void enterGetterSortedNodes();
	//! quitting the critical section (reader)
	void leaveGetterSortedNodes();

	void waitOnWorkOrTerminate(int workerID);

};

#endif /* PARBFSSTRAGEGY_H_ */
