//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BFSStragegy_def.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef PARBFSSTRATEGY_DEF
#define PARBFSSTRATEGY_DEF

#include "ParBFSStrategy.h"

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
ParBFSStrategy<NodeComp, PLUNGE_MODE>::ParBFSStrategy(int inNWorker, Logger *inL) :
												ParNodeSelectionStrategy(inNWorker, inL){
	// Plunging strat. globals
	maxLevel = 0;
	plungMin = plungMax = 0;

	// Global "constant"
	initXBounds = NULL;

	// Thread local data
	// *local nodes
	focusN = new SpecNodePtr[nWorker];
	currentN = new SpecNodePtr[nWorker];

	// *local temp. nodes for plunging and plunging bool
	childrenNodes = new vector<NodePtr<NodeComp> >[nWorker];

	plungeDone = new unsigned int[nWorker];
	plunging = new bool[nWorker];
	for(int i=0; i<nWorker; i++) {
		plungeDone[i] = 0;
		plunging[i] = false;
	}

	// Data structures
	sortedNodes = NULL;
	tree = NULL;

	// Is it terminated
	terminated = false;

	// counters
	nGetters = nWaitingWorker = 0;

	// Semaphores
	sem_init(&pool, 1, 0);
	sem_init(&noGet, 1, 1);
	sem_init(&noClear, 1, 1);
	sem_init(&cntGetterMutex, 1, 1);
	sem_init(&cntWaitingWMutex, 1, 1);
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
ParBFSStrategy<NodeComp, PLUNGE_MODE>::~ParBFSStrategy() {

	delete [] currentN;
	delete [] focusN;
	delete [] childrenNodes;

	if(sortedNodes != NULL)
		delete sortedNodes;
	if(tree != NULL)
		delete tree;
	if(initXBounds != NULL)
		delete [] initXBounds;
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::processEstimates(unsigned int intVarPos, SimplexSolution *cSol,
															  double &estimateDown, double &estimateUp) {
	int index = 0;
	double upCost, downCost, f;
	double estimate = 0;

	// for each fractional "integer" defined variable (but the branching var)
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		if(!cSol->isIntegral(index) && intVarPos != i){
			// Process the lowest estimate decrease
			f = cSol->xVal[index] - floor(cSol->xVal[index]);
			upCost = (1.0-f)*rel->getPcostUp(i);
			downCost = f*rel->getPcostDown(i);
			estimate += upCost < downCost ? upCost : downCost;
		}
	}

	// Special case for branching variable
	index = intVar->getVarIndex(intVarPos);
	// in case we are going up
	estimateUp = (ceil(cSol->xVal[index]) - cSol->xVal[index]) * rel->getPcostUp(intVarPos);
	estimateUp += estimate;
	// in case we are going down
	estimateDown = (cSol->xVal[index] - floor(cSol->xVal[index])) * rel->getPcostDown(intVarPos);
	estimateDown += estimate;

	// If max changing the sign
	if(iProb->isMax()){
		estimateUp *= -1.0;
		estimateDown *= -1.0;
	}

	// Adding the current value
	estimateUp += cSol->objVal;
	estimateDown += cSol->objVal;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::insertNode(int workerID, SpecNode *node){
	if(PLUNGE_MODE){
		if(!plunging[workerID]) {
			sortedNodes->push(node);
			sem_post(&pool); // Add one work to do on the pool
		} else {
			childrenNodes[workerID].push_back(NodePtr<NodeComp>(node));
		}
	} else {
		sortedNodes->push(node);
		sem_post(&pool); // Add one work to do on the pool
	}
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
short int ParBFSStrategy<NodeComp, PLUNGE_MODE> ::createNodes(int workerID, int intVarPos, SimplexSolution *cSol, SimplexProblem *cProb) {
	int index = intVar->getVarIndex(intVarPos);
	int lo, up;
	short newNode = 0;
	double estimateDown, estimateUp;

	// Get the estimate "integral solution" obj value
	processEstimates(intVarPos, cSol, estimateDown, estimateUp);

	// Check lower bound, new node ?
	lo = floor(cSol->xVal[index]);
	if(lo >= cProb->xBounds[index].x){
		tree->createChildLo(currentN[workerID]);
		currentN[workerID]->child_lo->estimateValue = estimateDown;
		currentN[workerID]->child_lo->modifiedBound = intVarPos;
		currentN[workerID]->child_lo->fVal = cSol->xVal[index] - lo;
		currentN[workerID]->child_lo->oldBound = cProb->xBounds[index];
		currentN[workerID]->child_lo->newBound = make_float2(cProb->xBounds[index].x,lo);

		newNode++;
		insertNode(workerID, currentN[workerID]->child_lo);

		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Lower node created ( x" << index << " - bound(" << currentN[workerID]->child_lo->newBound.x;
			oss << ", " << currentN[workerID]->child_lo->newBound.y << ") - level " << currentN[workerID]->child_lo->level;
			oss << " obj : " << currentN[workerID]->child_lo->objValue << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	}

	// Check upper bound, new node ?
	up = ceil(cSol->xVal[index]);
	if(up <= cProb->xBounds[index].y){
		tree->createChildUp(currentN[workerID]);
		currentN[workerID]->child_up->estimateValue = estimateUp;
		currentN[workerID]->child_up->modifiedBound = intVarPos;
		currentN[workerID]->child_up->fVal = up - cSol->xVal[index];
		currentN[workerID]->child_up->oldBound = cProb->xBounds[index];
		currentN[workerID]->child_up->newBound = make_float2(up,cProb->xBounds[index].y);

		newNode++;
		insertNode(workerID, currentN[workerID]->child_up);

		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Upper node created ( x" << index << " - bound(" << currentN[workerID]->child_up->newBound.x;
			oss << ", " << currentN[workerID]->child_up->newBound.y << ") - level " << currentN[workerID]->child_up->level;
			oss << " obj : " << currentN[workerID]->child_up->objValue << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	}

	if(PLUNGE_MODE){
		if(newNode == 2 && plunging[workerID]){
			if(cmpNode(index, cSol) < 0){ // swaping
				NodePtr<NodeComp> tmp1 = childrenNodes[workerID].back();
				childrenNodes[workerID].pop_back();
				NodePtr<NodeComp> tmp2 = childrenNodes[workerID].back();
				childrenNodes[workerID].pop_back();
				childrenNodes[workerID].push_back(tmp1);
				childrenNodes[workerID].push_back(tmp2);
			}
		}
	}

	return newNode;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::enterClearSortedNodes() {
	// check if there are already a clearer
	sem_wait(&noClear);
	// check if there are getters
	sem_wait(&noGet);
	// release the "getter" semaphore
	sem_post(&noGet);
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::leaveClearSortedNodes() {
	sem_post(&noClear);
}


template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::clearUselessNodes(SimplexSolution *bSol) {
	if(sortedNodes->isEmpty()) return;

	// Entering the critical section
	enterClearSortedNodes();

	// Do the clear part
	if(bSol != NULL){
		if(NodeComp == BEST){
			//sortedNodes->sort();
			if(iProb->isMin()){
				while(!sortedNodes->isEmpty() && sortedNodes->worst()->objValue >= bSol->objVal){
					tree->deleteLeafAndCleanTree(sortedNodes->worst());
					sortedNodes->popWorst();
					sem_wait(&pool);
				}
			} else {
				while(!sortedNodes->isEmpty() && sortedNodes->worst()->objValue <= bSol->objVal){
					tree->deleteLeafAndCleanTree(sortedNodes->worst());
					sortedNodes->popWorst();
					sem_wait(&pool);
				}
			}
		} else {
			sortedNodes->initIterator(); // Init at worst node
			if(iProb->isMin()){
				while(sortedNodes->getCurrentNode() != sortedNodes->end()){
					if(sortedNodes->getCurrentNode()->objValue >= bSol->objVal){
						tree->deleteLeafAndCleanTree(sortedNodes->getCurrentNode());
						sortedNodes->deleteCurrentNode();
						sem_wait(&pool);
					} else {
						sortedNodes->iterNextNode();
					}
				}
			} else {
				while(sortedNodes->getCurrentNode() != sortedNodes->end()){
					if(sortedNodes->getCurrentNode()->objValue <= bSol->objVal){
						tree->deleteLeafAndCleanTree(sortedNodes->getCurrentNode());
						sortedNodes->deleteCurrentNode();
						sem_wait(&pool);
						//this->nCreatedNode--;
					} else {
						sortedNodes->iterNextNode();
					}
				}
			}
		}
	}

	// Leaving the critical section
	leaveClearSortedNodes();

}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::enterGetterSortedNodes() {
	int temp;
	// check if no clear occuring
	sem_wait(&noClear);
	// get the counter mutex
	sem_wait(&cntGetterMutex);
	// update getters counter
	temp = nGetters;
	nGetters++;
	// release counter mutex
	sem_post(&cntGetterMutex);
	// If we are the first getter, take the get semaphore
	if(temp == 0) sem_wait(&noGet);
	// release the clear semaphore
	sem_post(&noClear);
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::leaveGetterSortedNodes() {
	int temp;
	// take the counter mutex
	sem_wait(&cntGetterMutex);
	// refresh the counter
	nGetters--;
	temp = nGetters;
	// release the counter mutex;
	sem_post(&cntGetterMutex);
	// if we are the last getter, release the getter lock
	if(temp == 0) sem_post(&noGet);
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::waitOnWorkOrTerminate(int workerID) {
	int tmp;//, value;

	// Increment the number of waiting workers
	sem_wait(&cntWaitingWMutex);
	nWaitingWorker++;
	tmp = nWaitingWorker;
	sem_post(&cntWaitingWMutex);

	// If all the worker are waiting and there are no pending node : terminate
	//sem_getvalue(&pool, &value);
	//cout << "Sem value : " << value << " - Remaining nodes : " << sortedNodes->getNbNodes() << endl;
	if(tmp == nWorker && sortedNodes->isEmpty()) {

		cout << "Worker [" << workerID << "] send the terminate message." << endl;

		// Announce that the BFS is terminated
		terminated = true;

		// Wake the other workers
		for(int i=0; i < nWorker; i++)
			sem_post(&pool);

	} else { // Else wait for some work
		sem_wait(&pool);
	}

	sem_wait(&cntWaitingWMutex);
	nWaitingWorker--;
	sem_post(&cntWaitingWMutex);
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool ParBFSStrategy<NodeComp, PLUNGE_MODE> ::selectBestNode(int workerID, SimplexSolution *bSol,
														    SimplexProblem *cProb, Solver *solver) {
	int index = 0;
	bool modified[intVar->nIntVar];
	float2 newBounds[intVar->nIntVar];

	// Wait until work is available
	waitOnWorkOrTerminate(workerID);

	if(terminated){
		sem_wait(&cntWaitingWMutex);
		cout << "Worker [" << workerID << "] terminated." << endl;
		sem_post(&cntWaitingWMutex);
		return false;
	}

	// Begin of critical section
	enterGetterSortedNodes();

	if(sortedNodes->isEmpty()) {
		cout << "Worker [" << workerID << "] empty priority queue." << endl;
		leaveGetterSortedNodes();
		return false;
	}

	// Get best
	focusN[workerID] = currentN[workerID] = sortedNodes->popBest();

	// init
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		newBounds[i] = initXBounds[intVar->getVarIndex(i)];
		modified[i] = false;
	}

	// Create new bounds
	while(currentN[workerID]->parent != NULL){
		if(!modified[currentN[workerID]->modifiedBound]){
			newBounds[currentN[workerID]->modifiedBound] = currentN[workerID]->newBound;
			modified[currentN[workerID]->modifiedBound] = true;
		}
		currentN[workerID] = currentN[workerID]->parent;
	}

	// Change bound, only for the different one
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		if((newBounds[i].x != cProb->xBounds[index].x) ||
		   (newBounds[i].y != cProb->xBounds[index].y)){
			solver->changeBound(index, newBounds[i]);
		}
	}

	// set current node as focuses node
	currentN[workerID] = focusN[workerID];

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		index = intVar->getVarIndex(currentN[workerID]->modifiedBound);
		oss << "W[" << workerID << "] Best node selected : ( x" << index << " - bound(" << currentN[workerID]->newBound.x;
		oss << ", " << currentN[workerID]->newBound.y << ") - level " << currentN[workerID]->level;
		oss << " obj : " << currentN[workerID]->objValue << ").";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	// Update plunging configuration
	if(maxLevel < currentN[workerID]->level){
		maxLevel = currentN[workerID]->level;
		plungMin = round(RATIO_MIN_PLUNGING*maxLevel);
		plungMax = round(RATIO_MAX_PLUNGING*maxLevel);
	}

	if(PLUNGE_MODE && plungMax > 0){
		plunging[workerID] = true;
		plungeDone[workerID] = 0;
	}

	// end of critical section
	leaveGetterSortedNodes();

	return true;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool ParBFSStrategy<NodeComp, PLUNGE_MODE> ::isGapAcceptable(int workerID, SimplexSolution *bSol) {
	double best;

	if(iProb->isMin()){
		if(bSol != NULL){
			best = bSol->objVal;
		} else {
			best = DBL_MAX;
		}

		return ((currentN[workerID]->objValue - iSol->objVal)/(best - iSol->objVal)) < GAP_THRESHOLD;
	} else {
		if(bSol != NULL) {
			best = bSol->objVal;
		} else {
			best = -DBL_MAX;
		}

		return ((iSol->objVal - currentN[workerID]->objValue)/(iSol->objVal - best)) < GAP_THRESHOLD;
	}
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool ParBFSStrategy<NodeComp, PLUNGE_MODE> ::selectNextPlungingNode(int workerID, SimplexSolution *bSol, Solver *solver) {
	int index;

	// No more children
	if(childrenNodes[workerID].empty()) return false;

	// End of PLUNGE_MODE
	if(plungeDone[workerID] >= plungMax || !isGapAcceptable(workerID, bSol)){
		// Copying non-visited node into bestNode list
		while(!childrenNodes[workerID].empty()){
			sortedNodes->push(childrenNodes[workerID].back().ptr);
			sem_post(&pool); // Add one work to do on the pool
			childrenNodes[workerID].pop_back();
		}
		return false;
	}

	// Backtracking if needed
	while(currentN[workerID]->level >= childrenNodes[workerID].back().ptr->level){
		index = intVar->getVarIndex(currentN[workerID]->modifiedBound);
		solver->changeBound(index, currentN[workerID]->oldBound);
		if(currentN[workerID]->isLowChild()){
			currentN[workerID] = currentN[workerID]->parent;
			tree->deleteLeaf(currentN[workerID]->child_lo);
		} else {
			currentN[workerID] = currentN[workerID]->parent;
			tree->deleteLeaf(currentN[workerID]->child_up);
		}
	}

	currentN[workerID] = childrenNodes[workerID].back().ptr;
	childrenNodes[workerID].pop_back();
	index = intVar->getVarIndex(currentN[workerID]->modifiedBound);
	solver->changeBound(index, currentN[workerID]->newBound);

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		index = intVar->getVarIndex(currentN[workerID]->modifiedBound);
		oss << "Plunging node selected : ( x" << index << " - bound(" << currentN[workerID]->newBound.x;
		oss << ", " << currentN[workerID]->newBound.y << ") - level " << currentN[workerID]->level;
		oss << " obj : " << currentN[workerID]->objValue << ").";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	plungeDone[workerID]++;

	return true;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool ParBFSStrategy<NodeComp, PLUNGE_MODE> ::selectNextNode(int workerID, SimplexSolution *bSol, SimplexProblem *cProb, Solver *solver) {
	bool result;

	// do the get stuff
	if(PLUNGE_MODE){
		if(!plunging[workerID]){ // we are going to select one from the priority queue
			result = selectBestNode(workerID, bSol, cProb, solver);
		} else {
			if(!selectNextPlungingNode(workerID, bSol, solver)){
				plunging[workerID] = false;
				result = selectBestNode(workerID, bSol, cProb, solver);
			} else {
				result = true;
			}
		}
	} else {
		result = selectBestNode(workerID, bSol, cProb, solver);
	}

	return result;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
VAR_TYPE ParBFSStrategy<NodeComp, PLUNGE_MODE> ::getParentObjValue(int workerID){
	return currentN[workerID]->parent->objValue;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::setCurrentNodeObjValue(int workerID, VAR_TYPE objValue){
	currentN[workerID]->objValue = objValue;
	currentN[workerID]->nChild = 0;
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::initNSS(IntegerVariables *inIV, ParReliability *inR,
												  SimplexProblem *inIProb, SimplexSolution *inISol){
	ParNodeSelectionStrategy::initNSS(inIV, inR, inIProb, inISol);

	tree = new Tree<NodeComp>();
	sortedNodes = new ParSortedNodes<NodeComp>(iProb->isMin());

	for(int i=0; i<nWorker; i++)
		currentN[i] = focusN[i] = tree->root;

	initXBounds = new float2[iProb->nVar];
	memcpy(initXBounds, iProb->xBounds, iProb->nVar*sizeof(float2));

}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
void ParBFSStrategy<NodeComp, PLUNGE_MODE> ::updatePCost(int workerID, float gain){
	if(currentN[workerID]->isLowChild()){ // downward child
		rel->addGainDown(currentN[workerID]->modifiedBound,
				         gain/currentN[workerID]->fVal);
	} else { // upward child
		rel->addGainUp(currentN[workerID]->modifiedBound,
				         gain/currentN[workerID]->fVal);
	}
}

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
bool ParBFSStrategy<NodeComp, PLUNGE_MODE>::isTerminated(){
	return terminated;
}

#endif /* PARBFSSTRATEGY_DEF */
