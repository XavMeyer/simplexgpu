//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * NodeSelectionStrategy.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef NODESELECTIONSTRATEGY_H_
#define NODESELECTIONSTRATEGY_H_

#include "../Misc/IntegerVariables.h"
#include "../Misc/Reliability.h"
#include "../../Solver/utils/Logger.h"
#include "../../Solver/SimplexProblem.h"
#include "../../Solver/SimplexSolution.h"
#include "../../Solver/simplex/Solver.h"
#include "../../Solver/utils/UniqueProfiler.h"

#include "../wtypes.h"

class NodeSelectionStrategy {
public:
	NodeSelectionStrategy(Logger *inLogger) {
		logger = inLogger;
#if PROFILING
		prof = UniqueProfiler::getInstance();
#endif
		//execMode = SEQUENTIAL;
		//nWorker = 1;
		//solver = inSolver;
	}

	virtual ~NodeSelectionStrategy(){
		logger = NULL;
		//solver = NULL;
		intVar = NULL;
		rel = NULL;
	}

	//! Init the Node Selection Strategy, has to be done during the B&B init.
	virtual void initNSS(IntegerVariables *inIV, Reliability *inR,
				 SimplexProblem *inCProb, SimplexSolution *inISol) {
		intVar = inIV;
		rel = inR;
		iSol = inISol;
		cProb = inCProb;
	}

	//virtual bool setModeParallel(int inNWorker) = 0;

	//virtual bool isTerminated() = 0;

	//! In this function new node should be created (branching decisions)
	virtual short createNodes(int intVarPos, SimplexSolution *cSol) = 0;
	//! In this function the next node should be selected and the problem (cProb)
	//! must be adapted accordingly.
	virtual bool selectNextNode(SimplexSolution *bSol, Solver *solver) = 0;

	//! This function return the parent score in order to update pseudo-costs
	virtual VAR_TYPE getParentObjValue() = 0;

	//! set the current no objectif value
	virtual void setCurrentNodeObjValue(VAR_TYPE objValue) = 0;

	//! Update the current branching variable for a given gain.
	virtual void updatePCost(float gain) = 0;

	virtual void clearUselessNodes(SimplexSolution *bSol) = 0;

protected:

	//int nWorker;
	//execution_mode_t execMode;

	Reliability *rel;
	IntegerVariables *intVar;
	Logger *logger;
	SimplexSolution *iSol;
	SimplexProblem *cProb;

#if PROFILING
	UniqueProfiler *prof;
#endif

	/**
	 * \brief Compare two newly created nodes and order them.
	 * 		  The comparison is done by using the Martin's idea. This consists
	 * 		  to prioritize the node going further away from the initial sol. (iSol).
	 *
	 * \param[in] value Current value of the selected variable
	 *
	 * \return -1 if the lower bound has priority,
	 *          0 both are equal (chose wathever you want),
	 *          1 if the upper bound has priority
	 *
	 */
	short int cmpNode(int index, SimplexSolution *cSol){

		// If value further away (floor(val)
		if(cSol->xVal[index] < iSol->xVal[index]){
			return -1;
		} else if(cSol->xVal[index] > iSol->xVal[index]){
			return 1;
		}

		return 0;
	}
};

#endif /* NODESELECTIONSTRATEGY_H_ */
