//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DFSStrategy.h
 *
 *  Created on: Jul 3, 2011
 *      Author: meyerx
 */

#ifndef DFSSTRATEGY_H_
#define DFSSTRATEGY_H_

#include "NodeSelectionStrategy.h"
#include <list>

class DFSStrategy: public NodeSelectionStrategy {
public:
	DFSStrategy(Logger *inL);
	virtual ~DFSStrategy();

	short int createNodes(int intVarPos, SimplexSolution *cSol);
	bool selectNextNode(SimplexSolution *bSol, Solver *solver);
	VAR_TYPE getParentObjValue();
	void setCurrentNodeObjValue(VAR_TYPE objValue);
	void updatePCost(float gain);
	void clearUselessNodes(SimplexSolution *bSol);
	//bool isTerminated();

private:

	typedef enum {DOWNWARD, UPWARD} child_type_t;

	int level;
	int nWaitingWorker;

	typedef struct{
		unsigned int level;
		int modifiedBound;
		float fVal;
		child_type_t child_type;
		float2 oldBound;
		float2 newBound;
		double objValue;
	} branching_decision_t;

	list<branching_decision_t> currentDecisions;
	list<branching_decision_t> pendingDecisions;
};

#endif /* DFSSTRATEGY_H_ */
