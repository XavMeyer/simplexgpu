//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BFSStragegy.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef BFSSTRAGEGY_H_
#define BFSSTRAGEGY_H_

#include "NodeSelectionStrategy.h"
#include "../Node/NodePtr.h"
#include "../DataStructure/SortedNodes.h"
#include "../DataStructure/SortedNodesV2.h"
#include "../DataStructure/Tree.h"

#include "../wtypes.h"

#include <semaphore.h>

// Ratio of min / max pseudocost to take into account
#define MU							0.3
// Minimal ratio of node to process during plunging
// (0.1*level == min nb node to process)
#define RATIO_MIN_PLUNGING			0.1
// Minimal ratio of node to process during plunging
// (0.1*level == max nb node to process)
#define RATIO_MAX_PLUNGING			0.5
// Gap tolerance between best sol and current sol while pluging
#define GAP_THRESHOLD				0.25

template <node_comparator_t NodeComp, bool PLUNGE_MODE>
class BFSStrategy: public NodeSelectionStrategy {
public:
	BFSStrategy(Logger *inL);
	virtual ~BFSStrategy();

	short int createNodes(int intVarPos, SimplexSolution *cSol);
	bool selectNextNode(SimplexSolution *bSol, Solver *solver);
	VAR_TYPE getParentObjValue();
	void setCurrentNodeObjValue(VAR_TYPE objValue);
	void updatePCost(float gain);

	//! Overload of initNSS method (see NodeSelectionStrategy abstract class).
	void initNSS(IntegerVariables *inIV, Reliability *inR,
				 SimplexProblem *inCProb, SimplexSolution *inISol);

	//! Clear the nodes that have an obj value that is worse than the best solution (bSol) obj value.
	void clearUselessNodes(SimplexSolution *bSol);

private:
	typedef Node<NodeComp> SpecNode;

	bool plunging;

	//sem_t *pool;
	//int nWaitingWorker;

	unsigned int maxLevel;
	unsigned int plungDone, plungMin, plungMax;

	float2 *initXBounds;

	SpecNode *focusN;
	SpecNode *currentN;

	Tree<NodeComp> *tree;
	SortedNodesV2<NodeComp> *sortedNodes;
	vector<NodePtr<NodeComp> > childrenNodes;

	/*****************************************************************************/
	/*********************** Create node related functions ***********************/
	/*****************************************************************************/

	//! Process the estimate downards and upwards score of the child nodes.
	void processEstimates(unsigned int varIndex, SimplexSolution *cSol,
			              double &estimateDown, double &estimateUp);
	//! Insert a node in the correct data structure.
	void insertNode(SpecNode *node);

	/*****************************************************************************/
	/*********************** Select node related functions ***********************/
	/*****************************************************************************/
	//! Select the node having the best obj value (evaluation depends on NodeComp).
	bool selectBestNode(SimplexSolution *bSol, Solver *solver);
	//! Control if the gap between the current node obj value and best node obj value gap
	//! is lower than a certain threshold (GAP_THRESHOLD).
	bool isGapAcceptable(SimplexSolution *bSol);
	//! Plunge into the tree (local DFS).
	bool selectNextPlungingNode(SimplexSolution *bSol, Solver *solver);

};

#endif /* BFSSTRAGEGY_H_ */
