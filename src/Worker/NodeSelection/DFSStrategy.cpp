//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DFSStrategy.cpp
 *
 *  Created on: Jul 3, 2011
 *      Author: meyerx
 */

#include "DFSStrategy.h"

DFSStrategy::DFSStrategy(Logger *inL) :
						 NodeSelectionStrategy(inL){
	level = 0;
	nWaitingWorker = 0;

}

DFSStrategy::~DFSStrategy() {

}

short int DFSStrategy::createNodes(int intVarPos, SimplexSolution *cSol) {

	int i = intVarPos;
	int index = intVar->getVarIndex(i);//intVarIndex[i];
	int lo, up;
	short newNode = 0;

	// Check lower bound, new node ?
	lo = floor(cSol->xVal[index]);
	up = ceil(cSol->xVal[index]);
	branching_decision_t decisionLo = {(level+1), i,
									   (cSol->xVal[index] - lo),
									   DOWNWARD,
									   cProb->xBounds[index],
									   make_float2(cProb->xBounds[index].x,lo),
									   cSol->objVal};

	branching_decision_t decisionUp = {(level+1), i,
									   (up - cSol->xVal[index]),
									   UPWARD,
									   cProb->xBounds[index],
									   make_float2(up,cProb->xBounds[index].y),
									   cSol->objVal};

	if((lo >= cProb->xBounds[index].x) && up <= (cProb->xBounds[index].y)){
		if(this->cmpNode(index, cSol) < 0){
			pendingDecisions.push_back(decisionUp);
			pendingDecisions.push_back(decisionLo);
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "Lower node created ( x" << index << " - bound(" << up;
				oss << ", " << cProb->xBounds[index].y << ") - level " << level+1;
				oss << " obj : " << cSol->objVal << ")." << endl;
				oss << "Upper node created ( x" << index << " - bound(" << cProb->xBounds[index].x;
				oss << ", " << lo << ") - level " << level+1;
				oss << " obj : " << cSol->objVal << ").";
				logger->log(Logger::LOG_DEBUG, oss.str());
			}
		} else {
			pendingDecisions.push_back(decisionLo);
			pendingDecisions.push_back(decisionUp);
			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "Upper node created ( x" << index << " - bound(" << cProb->xBounds[index].x;
				oss << ", " << lo << ") - level " << level+1;
				oss << " obj : " << cSol->objVal << ")." << endl;
				oss << "Lower node created ( x" << index << " - bound(" << up;
				oss << ", " << cProb->xBounds[index].y << ") - level " << level+1;
				oss << " obj : " << cSol->objVal << ").";
				logger->log(Logger::LOG_DEBUG, oss.str());
			}
		}
		newNode += 2;
	} else if(lo >= cProb->xBounds[index].x){
		pendingDecisions.push_back(decisionLo);
		newNode++;
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Lower node created ( x" << index << " - bound(" << up;
			oss << ", " << cProb->xBounds[index].y << ") - level " << level+1;
			oss << " obj : " << cSol->objVal << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	} else if(up <= cProb->xBounds[index].y){
		pendingDecisions.push_back(decisionUp);
		newNode++;
		if(logger->isActive()){
			ostringstream oss(ostringstream::out);
			oss << "Upper node created ( x" << index << " - bound(" << cProb->xBounds[index].x;
			oss << ", " << lo << ") - level " << level+1;
			oss << " obj : " << cSol->objVal << ").";
			logger->log(Logger::LOG_DEBUG, oss.str());
		}
	}

	return newNode;
}

bool DFSStrategy::selectNextNode(SimplexSolution *bSol, Solver *solver) {

	int index;
	branching_decision_t decision;

	if(pendingDecisions.empty()) return false;

	_START_EVENT(prof, "SELECTE_NEXT_NODE");

	// If there is already some decision made
	if(!currentDecisions.empty()){

		// Going up into the tree if necessary

		while(pendingDecisions.back().level <= currentDecisions.back().level){
			decision = currentDecisions.back();
			currentDecisions.pop_back();

			index = intVar->getVarIndex(decision.modifiedBound);
			solver->changeBound(index, decision.oldBound);
			level--;

			if(logger->isActive()){
				ostringstream oss(ostringstream::out);
				oss << "Backtracking ( level : " << level << ").";
				logger->log(Logger::LOG_DEBUG, oss.str());
			}

			if(level == 0) break; //We are the root thus we cannot go higher in the tree
		}
	}

	// We are now sure that we are at the good level in the tree
	decision = pendingDecisions.back();
	pendingDecisions.pop_back();
	currentDecisions.push_back(decision);

	index = intVar->getVarIndex(decision.modifiedBound);
	solver->changeBound(index, decision.newBound);
	level++;

	if(logger->isActive()){
		ostringstream oss(ostringstream::out);
		oss << "Solving x" << index << " (int nb " << decision.modifiedBound << ")";
		oss << " - bound(" << decision.newBound.x << ", "<< decision.newBound.y;
		oss << ") - level " << decision.level << " obj : " << decision.objValue;
		oss << ").";
		logger->log(Logger::LOG_DEBUG, oss.str());
	}

	_END_EVENT(prof, "SELECTE_NEXT_NODE");

	return true;
}

VAR_TYPE DFSStrategy::getParentObjValue(){
	branching_decision_t prevDec;

	// If we have no decision pending, we are at root
	if(currentDecisions.empty())
		return iSol->objVal;

	prevDec = *(--currentDecisions.end());

	return prevDec.objValue;
}

void DFSStrategy::setCurrentNodeObjValue(VAR_TYPE objValue){
	// If we are at root, we quit
	if(currentDecisions.empty()) return;

	currentDecisions.back().objValue = objValue;
}

void DFSStrategy::updatePCost(float gain){
	if(currentDecisions.back().child_type == DOWNWARD){ // downward child
		rel->addGainDown(currentDecisions.back().modifiedBound,
				         gain/currentDecisions.back().fVal);
	} else { // upward child
		rel->addGainUp(currentDecisions.back().modifiedBound,
				         gain/currentDecisions.back().fVal);
	}
}

void DFSStrategy::clearUselessNodes(SimplexSolution *bSol){

}
/*
bool DFSStrategy::setModeParallel(int inNWorker){

	nWorker = inNWorker;
	// todo implement
	return false;
}

bool DFSStrategy::isTerminated(){
	return pendingDecisions.empty();
}
*/
