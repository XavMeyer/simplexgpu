//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchingStrategy.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef PARBRANCHINGSTRATEGY_H_
#define PARBRANCHINGSTRATEGY_H_

#include "../wtypes.h"
#include "../Misc/IntegerVariables.h"
#include "../Misc/ParReliability.h"
#include "../../Solver/simplex/Solver.h"
#include "../../Solver/SimplexSolution.h"
#include "../../Solver/utils/Logger.h"
#include "../../Solver/utils/UniqueProfiler.h"
#include "../../Solver/SimplexProblem.h"


class ParBranchingStrategy {
public:

	//! Constructor.
	ParBranchingStrategy(Logger *inL){
		logger = inL;
#if PROFILING
	prof = UniqueProfiler::getInstance();
#endif
	}

	//! Destructor.
	virtual ~ParBranchingStrategy(){
		logger = NULL;
		intVar = NULL;
		rel = NULL;
		iProb = NULL;
	}

	//! Initialize the object related to the problem.
	virtual void initBS(IntegerVariables *inIV, ParReliability *inR, SimplexProblem *inIProb, SimplexSolution *inSol){
		intVar = inIV;
		rel = inR;
		iProb = inIProb;
	}

	//! Select the next variable to branch on.
	virtual node_type_t selectNextVariable(int &intVarPos, SimplexSolution *cSol, SimplexProblem *cProb, Solver *solver) = 0;

protected:

	ParReliability *rel;
	IntegerVariables *intVar;
	Logger *logger;
	SimplexProblem *iProb;

#if PROFILING
	UniqueProfiler *prof;
#endif

	float processScore(float qP, float qM){
		return (qP+EPS1)*(qM+EPS1);
	}

	float processScore(float mu, float qP, float qM){
		float min, max;

		min = qP < qM ? qP : qM;
		max = qM > qP ? qM : qP;

		return (1-mu)*min + mu*max;
	}

};

#endif /* PARBRANCHINGSTRATEGY_H_ */
