//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ReliabilityBranching.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef PARRELIABILITYBRANCHING_H_
#define PARRELIABILITYBRANCHING_H_

#include "ParBranchingStrategy.h"

class ParReliabilityBranching: public ParBranchingStrategy {
public:
	ParReliabilityBranching(Logger *inL) : ParBranchingStrategy(inL){
		tempSol = NULL;
	}
	virtual ~ParReliabilityBranching(){
		if (tempSol != NULL)
			delete tempSol;
	}

	//! Select the next variable to branch on.
	node_type_t selectNextVariable(int &intVarPos, SimplexSolution *cSol, SimplexProblem *cProb, Solver *solver);

	void initBS(IntegerVariables *inIV, ParReliability *inR, SimplexProblem *inP, SimplexSolution *inSol);

private:

	SimplexSolution *tempSol;

	void getBestVar(int &bestVar, float &bestScore, SimplexSolution *cSol);
	solver_res_t solve(float &gain, SimplexSolution *cSol, Solver *solver);
	node_type_t updateScore(int &bestVar, float &bestScore, SimplexSolution *cSol, SimplexProblem *cProb, Solver *solver);
};

#endif /* PARRELIABILITYBRANCHING_H_ */
