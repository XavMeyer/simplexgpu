//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SmallestIndexFirst.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef SMALLESTINDEXFIRST_H_
#define SMALLESTINDEXFIRST_H_

#include "BranchingStrategy.h"

class SmallestIndexFirst: public BranchingStrategy {
public:

	//! Constructor, call the super class constructor.
	SmallestIndexFirst(Logger *inL) : BranchingStrategy(inL){}

	//! Destructor.
	virtual ~SmallestIndexFirst(){}

	//! Select the next variable to branch on.
	node_type_t selectNextVariable(int &intVarPos, SimplexSolution *cSol, Solver *solver){
		int index;
		for(unsigned int i=0; i<intVar->nIntVar; i++){
			index = intVar->getVarIndex(i);
			if(!cSol->isIntegral(index)){
				intVarPos = i;
				return BRANCH;
			}
		}
		return LEAF;
	}
};

#endif /* SMALLESTINDEXFIRST_H_ */
