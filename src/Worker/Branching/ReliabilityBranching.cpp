//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ReliabilityBranching.cpp
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#include "ReliabilityBranching.h"

void ReliabilityBranching::getBestVar(int &bestVar, float &bestScore, SimplexSolution *cSol) {
	int index;
	float score, f;

	_START_EVENT(prof, "GET_BEST_VAR");

	// Process best score
	// For each integral defined variable
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		// If not integer, check if best score
		if(!cSol->isIntegral(index)){
			f = cSol->xVal[index] - floor(cSol->xVal[index]);
			score = processScore(f*rel->getPcostDown(i), (1.0-f)*rel->getPcostUp(i));
			if(bestVar == -1 || score > bestScore){
				bestVar = i;
				bestScore = score;
			}
		}
	}

	_END_EVENT(prof, "GET_BEST_VAR");

}

solver_res_t ReliabilityBranching::solve(float &gain, SimplexSolution *cSol, Solver *solver) {
	solver_res_t res;
	unsigned int nbIter = MAX_ITER_STRONG_BRANCHING;

	_START_EVENT(prof, "PROBLEM_RELAXATION");

	if(meanIter != 0 && meanIter < MAX_ITER_STRONG_BRANCHING)
		nbIter = meanIter > MIN_ITER_STRONG_BRANCHING ? meanIter : MIN_ITER_STRONG_BRANCHING;

	// solving
	res = solver->solve(2*nbIter);

	nbRelaxation++;
	meanIter += (solver->getCounter() - meanIter)/nbRelaxation;

	/*if(res == Solver::MAX_ITER_REACHED){
		cout << "Hello" << endl;
		getchar();
	}*/

	// Adding the pseudocost update directly after solving
	if(res == SUCCESS || res == MAX_ITER_REACHED){
		// retrieve the solution
		solver->getSolution(tempSol);

		// Process gain
		gain = tempSol->objVal - cSol->objVal;
		if(cProb->isMax())
			gain *= -1.0;
	}

	_END_EVENT(prof, "PROBLEM_RELAXATION")

	return res;
}

node_type_t ReliabilityBranching::updateScore(int &bestVar, float &bestScore, SimplexSolution *cSol, Solver *solver) {
	int i, index;
	bool feasible;
	float gainDown, gainUp, lo, up, score;
	float2 oldBound, newBound;
	solver_res_t res;

	_START_EVENT(prof, "UPDATE_SCORE")

	updateEtaRel();

	// Update some score (by strong branching)
	while((i = rel->getNextLessReliable()) != -1){

		// get true index from index array
		index = intVar->getVarIndex(i);

		if(cSol->isIntegral(index)) // check if already integer
			continue; //if it is.. skipping the "strong branching"

		gainDown = rel->getPcostDown(i);
		gainUp = rel->getPcostUp(i);

		// Get initial bound
		oldBound = newBound = cProb->xBounds[index];
		feasible = false;

		lo = floor(cSol->xVal[index]); // Low child update
		if(lo >= oldBound.x){
			newBound = make_float2(oldBound.x,lo);

			_START_EVENT(prof, "CHANGE_BOUND")
			solver->changeBound(index, newBound);
			_END_EVENT(prof, "CHANGE_BOUND")

			res =  solve(gainDown, cSol, solver);
			if(res == SUCCESS || res == MAX_ITER_REACHED){
				gainDown = gainDown/(cSol->xVal[index]-lo);
				rel->addGainDown(i, gainDown);
				feasible = true;
			}
		}
		up = ceil(cSol->xVal[index]); // Up child update
		if(up <= oldBound.y){
			newBound = make_float2(up,oldBound.y);

			_START_EVENT(prof, "CHANGE_BOUND")
			solver->changeBound(index, newBound);
			_END_EVENT(prof, "CHANGE_BOUND")

			res = solve(gainUp, cSol, solver);
			if(res == SUCCESS || res == MAX_ITER_REACHED){
				gainUp = gainUp/(up - cSol->xVal[index]);
				rel->addGainUp(i, gainUp);
				feasible = true;
			}
		}

		// Bound changed at least once
		if((newBound.x != oldBound.x) || (newBound.y != oldBound.y)){
			_START_EVENT(prof, "CHANGE_BOUND")
			solver->changeBound(index, oldBound); // Putting old back
			_END_EVENT(prof, "CHANGE_BOUND")
		}

		if(!feasible) {
			_END_EVENT(prof, "UPDATE_SCORE")
			return PRUNE; // not feasible, pruning
		}

		score = processScore(gainDown, gainUp);
		if(score > bestScore){
			bestVar = i;
			bestScore = score;
		}
	}

	_END_EVENT(prof, "UPDATE_SCORE")
	return BRANCH;
}

void ReliabilityBranching::updateEtaRel(){

	int etaRel;
	double strBranchIters = (double)meanIter * (double)nbRelaxation;
	double prbBranchIters = (double)cProb->meanIter * (double)cProb->nbRelaxation;
	double maxStrBranchIters = (prbBranchIters/2.0) + 100000.0;

	if(strBranchIters > maxStrBranchIters) {
		// If we are over the maximum number of strong branching iteration etaRel => 0
		etaRel = 0;
	} else if(strBranchIters < (maxStrBranchIters/2.0)) {
		// If we are below the half of the maximum number of strong branching iterations etaRel => DEFAULT_ETA
		etaRel = DEFAULT_ETA;
	} else {
		// Else we just set an intermediate value between DEFAULT_ETA and 0 (defined linearly)
		etaRel = 2*DEFAULT_ETA*(1-(strBranchIters/maxStrBranchIters));
	}

	/*cout << "***********************************************************" << endl;
	cout << "Strong branching iterations : " << strBranchIters << " - mean : " << meanIter << endl;
	cout << "Problem branching iterations : " << prbBranchIters << " - mean : " << cProb->meanIter << endl;
	cout << "Maximum strong branching iterations : " << maxStrBranchIters << endl;
	cout << "Eta rel : " << etaRel << endl;

	getchar();*/

	rel->setEtaRel(etaRel);

}


node_type_t ReliabilityBranching::selectNextVariable(int &intVarPos, SimplexSolution *cSol, Solver *solver){

	// Pseudo cost
	// Declaration
	int bestVar = -1;
	float bestScore = -FLT_MAX;
	node_type_t res;

	_START_EVENT(prof, "VARIABLE_SELECTION");

	// get the best score (and var index)
	getBestVar(bestVar, bestScore, cSol);

	// if no bestVar, then all integer
	if(bestVar == -1){
		_END_EVENT(prof, "VARIABLE_SELECTION")
		return LEAF;
	}

	// Sort variable by "unreliability"
	rel->sortLessReliableFirst();

	// Update some unreliable variable score
	res = updateScore(bestVar, bestScore, cSol, solver);
	if(res == PRUNE) {
		_END_EVENT(prof, "VARIABLE_SELECTION")
		return PRUNE;
	}

	// Return next var
	intVarPos = bestVar;

	_END_EVENT(prof, "VARIABLE_SELECTION")
	return BRANCH;
}

void ReliabilityBranching::initBS(IntegerVariables *inIV,
								  Reliability *inR,
		                          SimplexProblem *inP){
	BranchingStrategy::initBS(inIV, inR, inP);
	tempSol = new SimplexSolution(cProb->nVar, cProb->nRow);
	meanIter = 0;
	nbRelaxation = 0;
}

