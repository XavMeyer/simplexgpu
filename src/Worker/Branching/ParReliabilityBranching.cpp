//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ReliabilityBranching.cpp
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#include "ParReliabilityBranching.h"

void ParReliabilityBranching::getBestVar(int &bestVar, float &bestScore, SimplexSolution *cSol) {
	int index;
	float score, f;

	// Process best score
	// For each integral defined variable
	for(unsigned int i=0; i<intVar->nIntVar; i++){
		index = intVar->getVarIndex(i);
		// If not integer, check if best score
		if(!cSol->isIntegral(index)){
			f = cSol->xVal[index] - floor(cSol->xVal[index]);
			score = processScore(f*rel->getPcostDown(i), (1.0-f)*rel->getPcostUp(i));
			if(bestVar == -1 || score > bestScore){
				bestVar = i;
				bestScore = score;
			}
		}
	}
}

solver_res_t ParReliabilityBranching::solve(float &gain, SimplexSolution *cSol, Solver *solver) {
	solver_res_t res;

	// solving
	res = solver->solve();

	// Adding the pseudocost update directly after solving
	if(res == SUCCESS){
		// retrieve the solution
		solver->getSolution(tempSol);

		// Process gain
		gain = tempSol->objVal - cSol->objVal;
		if(iProb->isMax())
			gain *= -1.0;
	}

	return res;
}

node_type_t ParReliabilityBranching::updateScore(int &bestVar, float &bestScore, SimplexSolution *cSol, SimplexProblem *cProb, Solver *solver) {
	int i, index;
	bool feasible;
	float gainDown, gainUp, lo, up, score;
	float2 oldBound, newBound;
	ReliabilityList *relList = rel->getSortedReliability();

	// Update some score (by strong branching)
	while((i = relList->getNextLessReliable()) != -1){

		// get true index from index array
		index = intVar->getVarIndex(i);

		if(cSol->isIntegral(index)) // check if already integer
			continue; //if it is.. skipping the "strong branching"

		gainDown = rel->getPcostDown(i);
		gainUp = rel->getPcostUp(i);

		// Get initial bound
		oldBound = newBound = cProb->xBounds[index];
		feasible = false;

		lo = floor(cSol->xVal[index]); // Low child update
		if(lo >= oldBound.x){
			newBound = make_float2(oldBound.x,lo);
			solver->changeBound(index, newBound);
			if(solve(gainDown, cSol, solver) == SUCCESS){
				gainDown = gainDown/(cSol->xVal[index]-lo);
				rel->addGainDown(i, gainDown);
				feasible = true;
			}
		}
		up = ceil(cSol->xVal[index]); // Up child update
		if(up <= oldBound.y){
			newBound = make_float2(up,oldBound.y);
			solver->changeBound(index, newBound);
			if(solve(gainUp, cSol, solver) == SUCCESS){
				gainUp = gainUp/(up - cSol->xVal[index]);
				rel->addGainUp(i, gainUp);
				feasible = true;
			}
		}

		// Bound changed at least once
		if((newBound.x != oldBound.x) || (newBound.y != oldBound.y))
			solver->changeBound(index, oldBound); // Putting old back

		if(!feasible) {
			delete relList;
			return PRUNE; // not feasible, pruning
		}

		score = processScore(gainDown, gainUp);
		if(score > bestScore){
			bestVar = i;
			bestScore = score;
		}
	}

	delete relList;

	return BRANCH;
}


node_type_t ParReliabilityBranching::selectNextVariable(int &intVarPos, SimplexSolution *cSol, SimplexProblem *cProb, Solver *solver){

	// Pseudo cost
	// Declaration
	int bestVar = -1;
	float bestScore = -FLT_MAX;
	node_type_t res;

	// get the best score (and var index)
	getBestVar(bestVar, bestScore, cSol);

	// if no bestVar, then all integer
	if(bestVar == -1) return LEAF;

	// Update some unreliable variable score
	res = updateScore(bestVar, bestScore, cSol, cProb, solver);
	if(res == PRUNE) return PRUNE;

	// Return next var
	intVarPos = bestVar;
	return BRANCH;
}

void ParReliabilityBranching::initBS(IntegerVariables *inIV,
									 ParReliability *inR,
		                             SimplexProblem *inP,
		                             SimplexSolution *inSol){
	ParBranchingStrategy::initBS(inIV, inR, inP, inSol);
	tempSol = inSol->clone();
}

