//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * StrongBranching.h
 *
 *  Created on: Jul 4, 2011
 *      Author: meyerx
 */

#ifndef STRONGBRANCHING_H_
#define STRONGBRANCHING_H_

#include "BranchingStrategy.h"

// todo evaluate an infeasability in a better way
#define INFEASIBLE_DELTA		1000.0

template<int N_ITER = -1>
class StrongBranching: public BranchingStrategy {
public:

	//! Constructor, call the super class constructor.
	StrongBranching(Logger *inL) : BranchingStrategy(inL){
		tempSol = NULL;
	}

	//! Destructor.
	virtual ~StrongBranching(){
		if(tempSol != NULL)
			delete tempSol;
	}

	//! Select the next variable to branch on.
	node_type_t selectNextVariable(int &intVarPos, SimplexSolution *cSol, Solver *solver){
		int n, index;
		float bestScore, score;

		_START_EVENT(prof, "VARIABLE_SELECTION")

		n = 0;
		intVarPos = -1;
		bestScore = -FLT_MAX;

		// For each integer variable
		for(int i=0; i<intVar->nIntVar; i++){
			index = intVar->getVarIndex(i);
			// Check if is integer or floating
			if(!cSol->isIntegral(index)){
				score = processVarScore(index, cSol, solver); // Try to branch downward and upward
				if(score == NAN) {
					_END_EVENT(prof, "VARIABLE_SELECTION")
					return PRUNE; // Both branch are infeasible
				}
				if(intVarPos == -1 || bestScore < score) {// At least 1 branch is feasible
					intVarPos = i;
					bestScore = score;
					n = 0; // Reset counter
				}
				n++;
			}
			// We break out of the loop when there has been
			// no better score for N_ITER variables
			if(N_ITER > 0 && n >= N_ITER) break;
		}

		_END_EVENT(prof, "VARIABLE_SELECTION")

		// If no variable have been tester
		if(intVarPos == -1)
			return LEAF; // We are at a final leaf (integer sol)
		else
			return BRANCH; // otherwhise, we have found a variable to branch on
	}

private:

	SimplexSolution *tempSol;

	//! Branch on variable "index" and return the branching score of this variable.
	float processVarScore(int index, SimplexSolution *cSol, Solver *solver){
		float lo, up, loGain, upGain;
		float2 oldBound, newBound;

		if(tempSol == NULL)
			tempSol = cSol->clone();

		loGain = upGain = NAN;
		oldBound = cProb->xBounds[index];

		// Try to branch downwards
		lo = floor(cSol->xVal[index]);
		if(lo >= cProb->xBounds[index].x){
			newBound = make_float2(cProb->xBounds[index].x,lo);
			solver->changeBound(index, newBound);
			if(solver->solve() == SUCCESS){
				solver->getSolution(tempSol);
				loGain = tempSol->objVal - cSol->objVal;
			}
		}
		// Try to branch upwards
		up = ceil(cSol->xVal[index]);
		if(up <= cProb->xBounds[index].y){
			newBound = make_float2(up,cProb->xBounds[index].y);
			solver->changeBound(index, newBound);
			if(solver->solve() == SUCCESS){
				solver->getSolution(tempSol);
				upGain = tempSol->objVal - cSol->objVal;
			}
		}

		// if bounds have changed, restore old bonds
		if(newBound.x != oldBound.x || newBound.y != oldBound.y)
			solver->changeBound(index, oldBound);

		// return the score (NAN if both branch are infeasible)
		if(loGain == NAN && loGain == NAN) {
			return NAN;
		} else {
			// todo add reliability update with infeasible delta
			loGain = (loGain == NAN) ? INFEASIBLE_DELTA : loGain;
			upGain = (upGain == NAN) ? INFEASIBLE_DELTA : upGain;
			return processScore(upGain, loGain);
		}
	}

	void initBS(IntegerVariables *inIV, Reliability *inR, SimplexProblem *inP){
		BranchingStrategy::initBS(inIV, inR, inP);
		tempSol = new SimplexSolution(cProb->nVar, cProb->nRow);
	}


};

#endif /* STRONGBRANCHING_H_ */
