//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BranchAndBoundV2.h
 *
 *  Created on: Jun 29, 2011
 *      Author: meyerx
 */

#ifndef CUTANDBRANCHV2_H_
#define CUTANDBRANCHV2_H_

#include "NodeSelection/NodeSelectionStrategy.h"
#include "Branching/BranchingStrategy.h"
#include "CutGenerator/cg_includes.h"

#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/SimplexProblemStats.h"
#include "../Solver/simplex/Solver.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/utils/Logger.h"
#include "../Solver/utils/UniqueProfiler.h"

#define INFO_FREQUENCY		5000

template<class BranchStrat, class NodeSelStrat, class CutGen, class SolverImpl>
class CutAndBranchV2 {
public:
	CutAndBranchV2();
	CutAndBranchV2(Logger *inL);
	CutAndBranchV2(int device);
	CutAndBranchV2(Logger *inL, int device);

	virtual ~CutAndBranchV2();

	SimplexSolution * CutAndBranchV2(SimplexProblem *inP);

	unsigned int getNSolvedNode();
	unsigned int getNCreatedNode();
	unsigned int getNInfNode();

private:

	unsigned int nCreatedNode, nSolvedNode, nInfNode;

	// Node selection strategy
	NodeSelectionStrategy *nss;
	// Branching strategy
	BranchingStrategy *bs;
	// Cut generator
	CutGenerator *cg;

	// Simplex solver
	SolverImpl *solver;

	// Logger for debug, info, etc. messages
	Logger *logger;
	Logger *solverLogger;

	// Initial problem
	SimplexProblem *problem;
	// Current problem
	SimplexProblem *cProb;

	// Integer variable indices
	IntegerVariables *intVar;
	// Pseudo-costs / Reliability
	Reliability *rel;

	// current solution
	SimplexSolution *cSol;
	// best solution
	SimplexSolution *bSol;
	// initial solution (first relaxation)
	SimplexSolution *iSol;

#if PROFILING
	UniqueProfiler *prof;
#endif

	//! Init variables and classes.
	void init(int device);

	//! set the problem to solve
	void setProblem(SimplexProblem *inP);

	//! Init the branch and bound algorithm.
	void initCutAndBranchV2(SimplexProblem *inP);

	//! First relaxation, solve the initial (non integral) problem.
	void initialRelaxation();

	//! do the cut part
	void addCuts();

	//! Solve a problem and update pseudocost if solving is successful.
	solver_res_t solve();

};

#endif /* CUTANDBRANCHV2_H_ */
