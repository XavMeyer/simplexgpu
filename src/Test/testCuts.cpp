//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * testWorker.cpp
 *
 *  Created on: May 20, 2011
 *      Author: meyerx
 */

#include <iostream>

#include "../Worker/CutAndBranch_def.hpp"
#include "../Worker/BranchAndBound_def.hpp"
#include "../Worker/Branching/SmallestIndexFirst.h"
#include "../Worker/Branching/StrongBranching.h"
#include "../Worker/Branching/ReliabilityBranching.h"
#include "../Worker/NodeSelection/DFSStrategy.h"
#include "../Worker/NodeSelection/BFSStrategy_def.hpp"
#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/MPSReaderOpti.h"
#include "../Solver/utils/Logger.h"
#include "../Solver/simplexGPU/SolverMono.h"

#include "../Worker/CutGenerator/cg_includes.h"

#include "cblas.h"

#define ONLY_CUT_GEN 1

using namespace std;


SimplexSolution * test_BNB(Logger *logger, int device, SimplexProblem *p){

	//BranchAndBound<SmallestIndexFirst, DFSStrategy, SolverMono> bnb(logger, device);
	BranchAndBound<ReliabilityBranching, BFSStrategy<BEST, false>, SolverMono> bnb(logger, device);
	SimplexSolution *ss = bnb.branchAndBound(p);

	return ss->clone();
}

SimplexSolution * getMarkshare4Sol(SimplexProblem *cProb){
	/**** DBG ****/
		SimplexSolution *_rSol = new SimplexSolution(cProb->nVar, cProb->nSlack);
		for(int i=0; i<_rSol->nVar; i++){
			_rSol->xVal[i] = 0;
		}
		_rSol->xVal[2] = 1.0;
		_rSol->xVal[4] = 1.0;
		_rSol->xVal[5] = 1.0;
		_rSol->xVal[6] = 1.0;
		_rSol->xVal[9] = 1.0;
		_rSol->xVal[10] = 1.0;
		_rSol->xVal[11] = 1.0;
		_rSol->xVal[16] = 1.0;
		_rSol->xVal[17] = 1.0;
		_rSol->xVal[18] = 1.0;
		_rSol->xVal[19] = 1.0;
		_rSol->xVal[24] = 1.0;
		_rSol->xVal[26] = 1.0;
		_rSol->xVal[29] = 1.0;
		_rSol->xVal[30] = 1.0;
		/*****END DBG*****/

		return _rSol;

}

SimplexSolution * get_DualSolution(SimplexProblem *p, SimplexProblem *initP) {

	SimplexSolution *dualSol = new SimplexSolution(p->nSlack, p->nVar);
	for(int i=0; i<p->nSlack; i++){
		if(p->basis[i] < p->nVar)
			dualSol->sVal[p->basis[i]] = 0.0;
		else
			dualSol->xVal[p->basis[i]-p->nVar] = 0.0;

	}

	for(int i=0; i<p->nVar; i++){
		if(p->xInd[i] < p->nVar)
			dualSol->sVal[p->xInd[i]] = -p->objFunc[i];
		else
			dualSol->xVal[p->xInd[i]-p->nVar] = -p->objFunc[i];
	}

	dualSol->objVal = cblas_ddot(dualSol->nVar, dualSol->xVal, 1, &initP->eqs[IDX2C(0, p->nCol-1, p->nRow)], 1);

	return dualSol;
}

SimplexProblem * cut_generation(Logger *logger, int device, SimplexProblem *p, SimplexProblem *iP, SimplexSolution *ss){
	vector<Inequality*>::iterator it;
	Logger cutLogger(Logger::ALL);
	//Initial relaxation
	SolverMono s(&cutLogger, device);
	//iP->print();
	s.setProblem(p);
	s.solve();
	s.getProblem(p);
	//p->print();
	SimplexSolution *sol = new SimplexSolution(p->nVar, p->nSlack);
	s.getSolution(sol);
	//printf("%s\n", sol->toString().c_str());
	SimplexSolution *dualSol = get_DualSolution(p, iP);
	//printf("%s\n", dualSol->toString().c_str());

	//Cut generation
	CutPool cutPool(2000);
	CMIRCutGen cmir(&cutPool, iP, &cutLogger);
	GomoryCutGen gomory(&cutPool, iP, &cutLogger);
	IntegerVariables iv(iP);
	//NonIntegralVariables integralV(&iv, sol);


	cout << cmir.generateCuts(sol, &iv, p) << " CMIR cuts created." << endl;
	cout << gomory.generateCuts(sol, &iv, p) << " Gomory MIR cuts created." << endl;
	cout << cutPool.getNbCuts() << " cuts created." << endl;
#if !ONLY_CUT_GEN
	cout << cutPool.toString().c_str();
	if(cutPool.DBG_checkCuts(ss)){
		cout << "Cuts accepts the real solution." << endl;
	} else {
		cout << "At least one cut doesn't accept the real solution (and has been removed). Remains :" << endl;
	}
#endif

	CutSelector cutSel(&cutPool);
	vector<Inequality*> * cuts = cutSel.selectCuts(iP, sol);
	cout << cuts->size() << " cuts selected." << endl;
#if !ONLY_CUT_GEN
	cout << "Selected cuts : " << endl;
	for(it = cuts->begin(); it != cuts->end(); it++){
		cout << (*it)->toString() << endl;
	}
#endif

	SimplexProblem *cutPb = iP->clone();
	cutPb->addRows(cuts);

	delete dualSol;
	delete cuts;
	delete sol;

	return cutPb;
}

void cut_and_branch(Logger *logger, int device, SimplexProblem *cutPb){
	//cutPb->print();
	BranchAndBound<ReliabilityBranching, BFSStrategy<BEST, false>, SolverMono> bnb(logger, device);
	bnb.branchAndBound(cutPb);
}


void test_cuts(Logger *logger, int device, SimplexProblem *p, SimplexSolution *ss){

	SimplexProblem *cutPb;
	SimplexProblem *iP = p->clone();

	cutPb = cut_generation(logger, device, p, iP, ss);
#if !ONLY_CUT_GEN
	cut_and_branch(logger, device, cutPb);
#endif

	delete cutPb;
	delete iP;
}


int main(int argc, char ** argv){

	int device;
	Logger *logger = new Logger(Logger::LOW);
	MPSReaderOpti *m = new MPSReaderOpti(argv[1]);
	SimplexProblem *p = m->readFile();
	SimplexSolution *ss = NULL;

	if(!strcmp(argv[2], "-max"))
		p->changeToMaximize();
	else if (!strcmp(argv[2], "-min"))
		p->changeToMinimize();
	else
		return -1;

	if(argc < 2)
		device = 0;
	else
		device = atoi(argv[3]);

	printf("Device : %d \n", device);

#if !ONLY_CUT_GEN
	ss = test_BNB(logger, device, p);

	//ss = getMarkshare4Sol(p);
#endif

	test_cuts(logger, device, p, ss);

	delete logger;
	delete m;
	delete p;
	if(ss != NULL)
		delete ss;

	return 0;

}
