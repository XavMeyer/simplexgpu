//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * testWorker.cpp
 *
 *  Created on: May 20, 2011
 *      Author: meyerx
 */

#include <iostream>


#include "../Worker/BranchAndBound_def.hpp"
#include "../Worker/Branching/SmallestIndexFirst.h"
#include "../Worker/Branching/StrongBranching.h"
#include "../Worker/Branching/ReliabilityBranching.h"
#include "../Worker/NodeSelection/DFSStrategy.h"
#include "../Worker/NodeSelection/BFSStrategy_def.hpp"
#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/MPSReaderOpti.h"
#include "../Solver/utils/Logger.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/simplex/TwoPhaseSimplex.hpp"
#include "../Solver/simplexGPU/RevisedSparseAMethodGPU.h"

using namespace std;

int main(int argc, char ** argv){

	int device;
	Logger *logger = new Logger(Logger::LOW);
	MPSReaderOpti *m = new MPSReaderOpti(argv[1]);
	SimplexProblem *iP = m->readFile();
	SimplexProblem *p = iP->Clone();

	if(!strcmp(argv[2], "-max"))
		p->changeToMaximize();
	else if (!strcmp(argv[2], "-min"))
		p->changeToMinimize();
	else
		return -1;

	printf("ARGC : %d \n", argc);

	if(argc < 2)
		device = 0;
	else
		device = atoi(argv[3]);

	printf("Device : %d \n", device);

	//BranchAndBound<SmallestIndexFirst, DFSStrategy, SolverMono> bnb(logger, device);
	//BranchAndBound<ReliabilityBranching, BFSStrategy<BEST, false>, SolverMono> bnb(logger, device);
	BranchAndBoundV2<ReliabilityBranching, BFSStrategy<BEST, false>, TwoPhaseSimplex<RevisedSparseAMethodGPU>> bnb(logger, device);
	bnb.branchAndBound(p);

	return 0;

}
