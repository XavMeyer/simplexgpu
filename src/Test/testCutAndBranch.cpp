//    PSICO is an implementation of the Branch-and-bound and simplex algorithms for CUDA devices (GPU).
//    Copyright (C) 2015  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * testCutAndBranch.cpp
 *
 *  Created on: Aug 10, 2011
 *      Author: meyerx
 */

#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#include "../Worker/CutAndBranch_def.hpp"
#include "../Worker/BranchAndBound_def.hpp"
#include "../Worker/Branching/SmallestIndexFirst.h"
#include "../Worker/Branching/StrongBranching.h"
#include "../Worker/Branching/ReliabilityBranching.h"
#include "../Worker/NodeSelection/DFSStrategy.h"
#include "../Worker/NodeSelection/BFSStrategy_def.hpp"
#include "../Solver/SimplexProblem.h"
#include "../Solver/SimplexSolution.h"
#include "../Solver/MPSReaderOpti.h"
#include "../Solver/utils/Logger.h"
#include "../Solver/utils/CustomProfiling.h"
#include "../Solver/simplexGPU/SolverMono.h"
#include "../Solver/simplex/TwoPhaseSimplex.hpp"
#include "../Solver/simplexGPU/RevisedSparseAMethodGPU.h"

#include "../Worker/CutGenerator/cg_includes.h"

#include "../Worker/ManagerCNB_def.hpp"

#include "cblas.h"

#define RESULT_PATH			"result/ILP/EST_PLUNGE/"
#define BNB_PATH			"BNB/"
#define CNB_PATH			"CNB/"
#define CNB_2GPU_PATH		"CNB_2GPU/"
#define CNB_3GPU_PATH		"CNB_3GPU/"

using namespace std;

void writeToFile(string filePath, string toWrite){
	ofstream myfile;
	myfile.open(filePath.c_str());
	myfile << toWrite.c_str();
	myfile.close();
}


void test_BNB(string problemName, Logger *logger, int device, SimplexProblem *p){
	CustomProfiling cp;
	ostringstream oss(ostringstream::out);
	ostringstream ossCont(ostringstream::out);
	BranchAndBound<SmallestIndexFirst, BFSStrategy<BEST, false>, SolverMono> bnb(logger, device);
	cp.startTime();
	SimplexSolution *ss = bnb.branchAndBound(p);
	cp.endTime();
	oss << RESULT_PATH << BNB_PATH << problemName.c_str() << ".sol";

	ossCont << "Time : " << cp.duration() << endl;
	ossCont << "Created nodes : " << bnb.getNCreatedNode() << endl;
	ossCont << "Visited nodes : " << bnb.getNSolvedNode() << endl;
	ossCont << "Infeasible nodes : " << bnb.getNInfNode() << endl;
	ossCont << ss->toString();

	writeToFile(oss.str(), ossCont.str());
}

void test_CNB(string problemName, Logger *logger, int device, SimplexProblem *p){
	CustomProfiling cp;
	ostringstream oss(ostringstream::out);
	ostringstream ossCont(ostringstream::out);
	CutAndBranch<ReliabilityBranching, BFSStrategy<ESTIMATE, true>, DefaultCutGen, TwoPhaseSimplex<RevisedSparseAMethodGPU> > cnb(logger, device);
	cp.startTime();
	SimplexSolution *ss = cnb.cutAndBranch(p);
	cp.endTime();

	/*oss << RESULT_PATH << CNB_PATH << problemName.c_str() << "_SNV2.sol";

	ossCont << "Time : " << cp.duration() << endl;
	ossCont << "Created nodes : " << cnb.getNCreatedNode() << endl;
	ossCont << "Visited nodes : " << cnb.getNSolvedNode() << endl;
	ossCont << "Infeasible nodes : " << cnb.getNInfNode() << endl;
	ossCont << ss->toString();

	writeToFile(oss.str(), ossCont.str());*/
}

void test_M_CNB(string problemName, Logger *logger, SimplexProblem *p){
	CustomProfiling cp;
	ostringstream oss(ostringstream::out);
	ostringstream ossCont(ostringstream::out);
	ManagerCNB<DefaultCutGen, SolverMono> mCNB(logger, 2);
	cp.startTime();
	SimplexSolution *ss = mCNB.cutAndBranch(p);
	cp.endTime();

	/*cout << "RESULT : " << endl;
	ossCont << "Time : " << cp.duration() << endl;
	cout << ss->toString();*/

	oss << RESULT_PATH << CNB_2GPU_PATH << problemName.c_str() << "_v1.sol";

	ossCont << "Time : " << cp.duration() << endl;
	ossCont << "Created nodes : " << mCNB.getNCreatedNode() << endl;
	ossCont << "Visited nodes : " << mCNB.getNSolvedNode() << endl;
	ossCont << "Infeasible nodes : " << mCNB.getNInfNode() << endl;
	ossCont << ss->toString();

	writeToFile(oss.str(), ossCont.str());
}

int main(int argc, char ** argv){

	int device;
	string problemName(argv[1]);
	Logger *logger = new Logger(Logger::ALL);
	MPSReaderOpti *m = new MPSReaderOpti(argv[1]);
	SimplexProblem *p = m->readFile();

	if(!strcmp(argv[2], "-max"))
		p->changeToMaximize();
	else if (!strcmp(argv[2], "-min"))
		p->changeToMinimize();
	else
		return -1;

	if(argc < 2)
		device = 0;
	else
		device = atoi(argv[3]);

	printf("Device : %d \n", device);

	problemName = problemName.substr(problemName.rfind('/')+1, problemName.size()-(5+problemName.rfind('/')));

	printf("Problem name : %s \n", problemName.c_str());

	printf("Problem size %d x %d\n", p->nRow, p->nCol);

	//test_BNB(problemName, logger, device, p);

	test_CNB(problemName, logger, device, p);

	//test_M_CNB(problemName, logger, p);

	delete logger;
	delete m;
	delete p;

	return 0;

}
